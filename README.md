# KidPocketFramework : an interactive voxel deformation framework.

This repository contains the files for the project KidPocketFramework.

This project has many dependencies, all are listed in section 2.1.
As it was made a while ago, it has some dependencies that might not be provided by your package manager.
As such, we have included the necessary older versions of libraries for the project.
Please follow the instructions in section 2.2 closely.

## 1. About the project

-- TODO

## 2. Dependencies

Important : read carefully sections 2.1 and 2.2, or else the project will not compile.

#### 2.1 Version-agnostic dependencies

For this project, you will need the following libraries :

| Library Name | Description |
|:------------:|-------------|
| blas | Basic Linear Algebra Subprograms |
| cblas | C interface to blas |
| GLEW | OpenGL Extension Wrangler library |
| GSL | GNU Scientific library |
| GLUT | OpenGL Utility library |
| GMP | GNU library for arbitrary-precision arithmetic |
| libQGLViewer | C++ library based on Qt that eases the creation of OpenGL 3D viewers. |
| MD4C | Markdown parser for C |
| MPFR | Multiple-precision floating-point computation with correct rounding |
| OpenMP | Multi-threading library |
| SuiteSparse | Collection of sparse matrix calculus library done on the GPU |

#### 2.2 Compiling Boost and CGAL from source

***UPDATE*** *(April 7th, 2020)* : You can now simply run the `install_script.sh` script to compile and install Boost & CGAL from source. Warning : you must be in the `external` folder for the script to work.

For this project, we will need boost version 1.65, and CGAL version 4.10. As those versions are getting old,
and might not be proposed by your package manager, we have a patched archive of each of them in the `external/` folder.

Here is how you can extract the libraries needed, compile them, and then compile the project.

1. Go to the `external/` folder, here, you'll find two files : `backup_boost_1_65_0_patched.tar.gz` and `backup_CGAL-4.10_patched.tar.xz`.

2. Make a directory for boost, one for CGAL, and another one for each of their compiled versions, as such :
```sh
$ mkdir -p boost-1.65 CGAL-4.10 compiled/boost compiled/cgal
```

3. Extract the files for boost and CGAL in their respective directories :
```sh
$ tar -xvzf backup_boost_1_65_0_patched.tar.gz --strip 1 -C boost-1.65
$ tar -xvf backup_CGAL-4.10_patched.tar.xz --strip 1 -C CGAL-4.10
```

4. Compile boost :
	1. Get into the boost folder and run the provided script, `./bootstrap.sh`.
	It will create boost's own installer, which will in turn compile and install boost at your request.

	2. Run the following command to install boost :
	
	```sh
	$ ./b2 --prefix=$(pwd)/../compiled/boost/ --build-type=complete --layout=tagged variant=debug link=shared threading=multi runtime-link=shared -a install
	```

	3. Boost should now be compiled. At the end of the quite lengthy compilation, it should say that about 14k targets have been updated, or give you a message along the lines of "Boost has been compiled".

5. Compile CGAL :
	1. Get into CGAL's folder and run the following command :

	```sh
	cmake -B $(pwd)/../compiled/cgal/ -DBOOST_ROOT:PATH=$(pwd)/../compiled/boost -DBoost_DEBUG:BOOL=ON -DBoost_NO_BOOST_CMAKE:BOOL=ON -DBoost_INCLUDE_DIR=$(pwd)/../boost-1.65 -DCMAKE_INSTALL_PREFIX:PATH=$(pwd)/../compiled/cgal -DCGAL_INSTALL_DOC_DIR:PATH=doc/CGAL-4.10 -DCGAL_INSTALL_MAN_DIR:PATH=man/man1
	```

	2. Go to `external/compiled/cgal` and run the following command to build the library :

	```sh
	$ make -s install/local
	```

	3. CGAL should now be compiled and installed locally.

6. You can now compile the main project. The CMake file is automatically configured for this specific file structure within the project.
If you've done all the steps right, you should be able to compile the project KidPocketFramework using :

```sh
$ cd ${Git_Project_Root}/KidPocketFramework
$ cmake -B build -DCMAKE_BUILD_TYPE=Debug
$ cmake --build build -- -s
$ ./kidPocketFramework # Run the project
```
