#!/bin/sh

# Path to the external/ folder, on the
# user's computer (useful for later)
EXT_ROOT=$(pwd)
# Tar program, with stripping :
TARSTRIP="tar --strip 1 -x"

# Creating folders :
mkdir -p boost-1.65 CGAL-4.10 compiled/boost compiled/cgal

# Building Boost :
# extracting archive :
$TARSTRIP -zf ./backup_boost_1_65_0_patched.tar.gz -C boost-1.65
# cding into boost dir, and compiling it :
cd boost-1.65
./bootstrap.sh
./b2 --prefix=../compiled/boost --build-type=complete --layout=tagged variant=debug link=shared threading=multi runtime-link=shared -j 12 -a install
# testing if everything went according to plan :
if [ ! -f $EXT_ROOT/compiled/boost/lib/libboost_system-mt-d.so ]; then
	echo "Boost not built, something went wrong."
fi

# Building CGAL :
cd $EXT_ROOT
# extracting archive :
$TARSTRIP -f ./backup_CGAL-4.10_patched.tar.xz -C CGAL-4.10
cd CGAL-4.10
# Creating CMake cache
cmake -B $(pwd)/../compiled/cgal/ -DBOOST_ROOT:PATH=$(pwd)/../compiled/boost -DBoost_DEBUG:BOOL=ON -DBoost_NO_BOOST_CMAKE:BOOL=ON -DBoost_INCLUDE_DIR=$(pwd)/../boost-1.65 -DCMAKE_INSTALL_PREFIX:PATH=$(pwd)/../compiled/cgal -DCGAL_INSTALL_DOC_DIR:PATH=doc/CGAL-4.10 -DCGAL_INSTALL_MAN_DIR:PATH=man/man1
cd $EXT_ROOT/compiled/cgal
# Compiling CGAL :
make -s install/local -j
# Check for any problems :
if [ ! -d include/CGAL/Mesh_3 ]; then
	echo "CGAL not build, something went wrong"
fi

echo "Boost and CGAL were successfully build from source."
