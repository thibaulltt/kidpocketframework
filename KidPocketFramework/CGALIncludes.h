#ifndef CGALINCLUDES_H
#define CGALINCLUDES_H

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>

#include <CGAL/Labeled_image_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/Image_3.h>

#include "Mesh_complex_3.h"
#include "Mesh_3_image_3_domain.h"
// Domain

#include "make_mesh_3_from_labeled_triangulation_3.h"
#include "BasicPoint.h"
#include "Tetrahedron.h"

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

typedef Mesh_3_image_3_domain<BasicPoint, Tetrahedron, K> Triangulated_mesh_domain;
typedef CGAL::Mesh_triangulation_3<Triangulated_mesh_domain>::type My_Tr;

typedef CGAL::Labeled_image_mesh_domain_3<CGAL::Image_3,K> Mesh_domain;
typedef Mesh_domain::Subdomain_index Subdomain_index;
// Triangulation
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
typedef Mesh_complex_3<Tr> Mesh_3;
typedef Mesh_3::Facet Facet;

typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;

// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;

typedef Mesh_domain::Subdomain_index Subdomain_index;
typedef Mesh_domain::Surface_index Surface_index;


typedef K::Point_3 Point_3;
typedef K::Plane_3 Plane_3;

typedef CGAL::Image_3 Image;

#endif // CGALINCLUDES_H
