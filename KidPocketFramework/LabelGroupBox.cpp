#include "LabelGroupBox.h"


#include <QScrollArea>
#include <QPushButton>
#include <iostream>
#include <algorithm>
#include <vector>

LabelGroupBox::LabelGroupBox(QWidget *parent):QGroupBox(parent)
{
    init();
}

LabelGroupBox::LabelGroupBox(const QString &title, QWidget *parent): QGroupBox(title, parent)
{
    init();
}

void LabelGroupBox::init()
{
    //Main layout of the group box
    QVBoxLayout * vBoxLayout = new QVBoxLayout(this);

    QFrame * frame = new QFrame();
    m_gridLayout = new QGridLayout(frame);

    //Defining a scroll area to fit all the labels
    QScrollArea * scrollArea = new QScrollArea(this);
    scrollArea->setWidget(frame);

    scrollArea->setWidgetResizable(true);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    vBoxLayout->addWidget(scrollArea);

    //Signal mapper to parse the clicked checkbox in order to know which label visible has changed
    //Note that i is the index of the label in the given list not the label value
    m_signalMapper = new QSignalMapper(this);
    connect(m_signalMapper, SIGNAL(mapped(const int &)), this, SLOT(setVisibility(const int &)));

    QHBoxLayout * buttonVBoxLayout = new QHBoxLayout();

    //Select all and discard all push buttons
    QPushButton *selectAllPushButton = new QPushButton("Select All", this);
    buttonVBoxLayout->addWidget(selectAllPushButton);

    QPushButton *discardAllPushButton = new QPushButton("Discard All", this);
    buttonVBoxLayout->addWidget(discardAllPushButton);

    connect(discardAllPushButton, SIGNAL(clicked()), this, SLOT(discardAll()));
    connect(selectAllPushButton, SIGNAL(clicked()), this, SLOT(selectAll()));

    vBoxLayout->addLayout(buttonVBoxLayout);

    this->adjustSize();
}

//Initialization of the label list
void LabelGroupBox::setLabels(const std::vector<Index> &labels)
{
    if( labels.size() == 0 )
    {
        std::cout << "LabelGroupBox::setLabels()::The label list shouldn't be empty"  << std::endl;
        return;
    }

    std::vector<QColor> colors;
    computeDefaultColors(colors);
    setLabels(labels, colors);
}

//Initialization of the label list with colors
void LabelGroupBox::setLabels(const std::vector<Index> &labels, const std::vector<QColor> &colors)
{
    if( labels.size() == 0 )
    {
        std::cout << "LabelGroupBox::setLabels()::The label list shouldn't be empty"  << std::endl;
        return;
    }
    assert( labels.size() == colors.size() && "The number of colors should match the number of labels");

    m_labels_id.clear();

    m_labels.clear();
    m_labels = labels;

    m_colors.clear();
    m_colors = colors;

    //If not color list provided set default colors
    if (m_colors.size() != m_labels.size() ) {
        computeDefaultColors(m_colors);
    }

    //Label list is checked for double or negative values
    for ( unsigned int i = 0 ; i < labels.size() ; i++ ){
        assert( labels[i] >= 0 && "Labels should be positive");
        assert( m_labels_id.find(labels[i]) == m_labels_id.end() && "Labels should be unique in the list");

        //Lable map to keep track of the index in the list (0..m_labels.size()) from label value
        m_labels_id[labels[i]] = i ;
    }

    //We build here the list of triple: text label, color label, checkbox per label in the list contained in m_QDatalist
    //We reuse the ones present int the list
    //We hide the ones that are no longer needed and add some to the list if necessary

    //First hide all the existing ones in the list
    for ( unsigned int i = 0 ; i < m_QDataList.size() ; i++ ){
        m_QDataList[i].colorQLabel->setVisible(false);
        m_QDataList[i].textQLabel->setVisible(false);
        m_QDataList[i].visibilityCheckBox->setVisible(false);
    }

    for ( unsigned int i = 0 ; i < m_labels.size() ; i++ ){

        //Reuse of the existing ones
        if( m_QDataList.size() > 0 && i < m_QDataList.size() ){ //if the index is in the m_QData range
            m_QDataList[i].colorQLabel->setPalette(m_colors[i]);  //change the displayed color
            m_QDataList[i].textQLabel->setText(QString::number(m_labels[i])); // udpate the text
            if(m_labels[i]>0) //Set box to checked to display by deafult except for background label 0
                m_QDataList[i].visibilityCheckBox->setChecked(true);

            //Set all the QData to visible
            m_QDataList[i].colorQLabel->setVisible(true);
            m_QDataList[i].textQLabel->setVisible(true);
            m_QDataList[i].visibilityCheckBox->setVisible(true);

        } else {

            //We need more elements that the existing ones
            //Create a QData struct element
            LabelQData labelQData;

            //Create instances for each
            labelQData.textQLabel = new QLabel(QString::number(m_labels[i])); //Transform the Label index number to text
            m_gridLayout->addWidget(labelQData.textQLabel, static_cast<int>(i), 0); //Add object to the layout

            //Create and put the color into the color label
            labelQData.colorQLabel = new QLabel(this);
            labelQData.colorQLabel->setPalette(m_colors[i]);
            labelQData.colorQLabel->setAutoFillBackground(true);

            m_gridLayout->addWidget(labelQData.colorQLabel, static_cast<int>(i), 1); //Add object to the layout

            //Create the visibility checkbox
            labelQData.visibilityCheckBox = new QCheckBox(this);
            if(m_labels[i]>0) //set it to check except for background label 0
                labelQData.visibilityCheckBox->setChecked(true);
            m_gridLayout->addWidget(labelQData.visibilityCheckBox, static_cast<int>(i), 2);

            //create an element in the signal mapper to have an event with the label index (0..ma_labels.size()-1) given
            m_signalMapper->setMapping(labelQData.visibilityCheckBox, static_cast<int>(i));
            //conecting the signal of the mapper to the clicked event
            connect(labelQData.visibilityCheckBox, SIGNAL(clicked()), m_signalMapper, SLOT (map()));

            //Adding the computed data to the QData list, one element per label
            m_QDataList.push_back(labelQData);
        }
    }

}

//Compute default color function
//called if none given by the user
void LabelGroupBox::computeDefaultColors( std::vector<QColor> &colors )
{
    assert( m_labels.size() > 0 &&"There should be given label values");

    //Clearing the color information
    colors.clear();
    colors.resize(m_labels.size());

    for ( unsigned int i = 0 ; i < m_labels.size() ; i++ ){
        if(m_labels[i] > 0 ) //Compute number of label dependant colors (note doesn't depend on the label value)
            colors[i].setHsvF(0.98*static_cast<double>(i)/static_cast<double>(m_labels.size()), 0.8,0.8);
        else
            colors[i] = Qt::black; //setting the background label color to black

    }
}


//Update label color to user provided ones
void LabelGroupBox::setColors(const std::vector<QColor> &colors)  {

    assert( colors.size() == m_labels.size() && "The number of colors should match the number of labels");

    m_colors.clear();
    m_colors = colors; //Setting the vector to the input one

    for ( unsigned int i = 0 ; i < m_colors.size() ; i++ ){
        m_QDataList[i].colorQLabel->setPalette(m_colors[i]); //Updating color Qlabel
    }
}

void LabelGroupBox::setColors(const std::vector<Index> &labels, const std::vector<QColor> &colors)  {
    //TODO: Ensurewe don't need to have a bigger list of color than labesl
    assert( labels.size() == colors.size() && "The number of colors should match the number of labels");

    m_colors.clear();
    m_colors = colors;

    for ( unsigned int i = 0 ; i < labels.size() ; i++ ){
        //We check if the label list is coherent: necessary?
        auto it = m_labels_id.find(labels[i]); //Label exists in the list
        if( it != m_labels_id.end() ){
            m_QDataList[it->second].colorQLabel->setPalette(colors[i]); //Updating color Qlabel
        }
    }
}

//Accessor to the ith label in the list
Index LabelGroupBox::label( unsigned int i ){
    assert( i < m_labels.size()  && "Label id out of vector bounds");
    return  m_labels.at(i);
}

//Function called when the ith checkbox status has changed
void LabelGroupBox::setVisibility(int i){
    assert( i < static_cast<int>(m_QDataList.size())  && i >= 0 &&"Index i should be in data bounds");
    //Emit the signal that a class might be connected to
    emit visibilityValueChanged(static_cast<unsigned int>(i), m_QDataList[static_cast<unsigned int>(i)].visibilityCheckBox->isChecked());
}

//Setting all the checkboxes to checked
void LabelGroupBox::selectAll(){
    for ( unsigned int i = 0 ; i < m_QDataList.size() ; i++ ){
        if( !m_QDataList[i].visibilityCheckBox->isChecked() ){
            m_QDataList[i].visibilityCheckBox->setChecked(true); //checking boxes
        }
    }
    //Emit the signal that a class might be connected to so the the user interaction is taken into account
    emit selectAllPushButton();
}

//Unchecks all the boxes
void LabelGroupBox::discardAll(){
    for ( unsigned int i = 0 ; i < m_QDataList.size() ; i++ ){
        if( m_QDataList[i].visibilityCheckBox->isChecked() ){
            m_QDataList[i].visibilityCheckBox->setChecked(false); //unchecking boxes
        }
    }
    //Emit the signal that a class might be connected to so the the user interaction is taken into account
    emit discardAllPushButton();
}

//Accessor to ith label visibility
bool LabelGroupBox::getVisibility( unsigned int i ){
    assert( i < m_QDataList.size() &&"Index i should be in data bounds");
    return m_QDataList[i].visibilityCheckBox->isChecked();
}

//Accessor to the visibibility status of by label
bool LabelGroupBox::getVisibilityFromLabel( Index i ){
    auto it = m_labels_id.find(i);
    assert( it != m_labels_id.end() &&"Label should be in the list");
    return m_QDataList[it->second].visibilityCheckBox->isChecked();
}
