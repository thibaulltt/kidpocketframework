#ifndef TETMESHCREATOR_H
#define TETMESHCREATOR_H

#include "BasicPoint.h"
#include "GLUtilityMethods.h"
#include "CGAL/Triangulation_incremental_builder_3.h"
#include <algorithm>

#include "CellInfo.h"
#include "SeparationInfo.h"
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_3/Robust_intersection_traits_3.h>

#include "Mesh_triangulation_with_info_3.h"
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>

#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>
#include "CageManipInterface.h"


#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>

#include "VoxelGrid.h"
#include "TetraLRISolver.h"
#include <list>
// IO
#include <CGAL/IO/Polyhedron_iostream.h>



#include <CGAL/Simple_cartesian.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_triangle_primitive.h>
#include "Histogram.h"
typedef CGAL::Simple_cartesian<double> SK;

typedef SK::Triangle_3 Triangle_3;

typedef std::list<Triangle_3>::iterator Iterator;
typedef CGAL::AABB_triangle_primitive<SK,Iterator> Primitive;
typedef CGAL::AABB_traits<SK, Primitive> AABB_triangle_traits;
typedef CGAL::AABB_tree<AABB_triangle_traits> Tree;


#include <queue>

// Domain
// (we use exact intersection computation with Robust_intersection_traits_3)
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Mesh_3::Robust_intersection_traits_3<K> Geom_traits;
typedef CGAL::Polyhedron_3<Geom_traits> Input_Polyhedron;
typedef CGAL::Polyhedral_mesh_domain_3<Input_Polyhedron, Geom_traits> Polyhedral_mesh_domain;

// Triangulation
// Triangulation

typedef CGAL::Mesh_triangulation_with_info_3<Mesh_domain, int, CellInfo, K>::type Triangulation;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Triangulation> C3t3_with_info;

typedef CGAL::Triangulation_incremental_builder_3<Triangulation> Builder;

// Criteria
typedef CGAL::Mesh_criteria_3<Triangulation> Triangulation_mesh_criteria;

typedef K::Vector_3 Vector_3;
typedef K::Point_3 Point_3;

// To avoid verbose function and named parameters call
using namespace CGAL::parameters;

enum DistortionType { CELL_BASED=0, VERTEX_BASED=1 };
enum SamplingType { EDGE_MID_POINT=0, CELL_BARYCENTER=1 };

typedef K::FT FT;

typedef FT (Function)(const Point_3&);

// Sizing field
struct Uniform_sizing_field
{
    typedef ::FT FT;

    typedef Mesh_domain::Index Index;

    float cell_value;
    float facet_value;
    float ratio;
    float angle;
    float approximation;

    FT operator()(const Point_3& p, const int, const Index&) const
    {
        return cell_value;
    }
};

class TetMeshCreator
{
public:
    TetMeshCreator():dataLoaded(false){
        transfer_mesh = new Triangulation();
        cageInterface = new CMInterface< BasicPoint >();
        voxel_grid = new VoxelGrid();
    }
    TetMeshCreator(CMInterface< BasicPoint > * cageInterface, VoxelGrid * voxelGrid);

    const BasicPoint & getBBMin(){ return BBMin; }
    const BasicPoint & getBBMax(){ return BBMax; }

    void transferMeshFromPolyhedron(const std::vector<BasicPoint> & V, const std::vector<Triangle > & T, double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize);
    void createFromPolygon(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize);
    void transferMeshFromImgAdaptativeSize(Image & image, double sizeMul, double percentage, bool optimize);
    void transferMeshFromImg(Image & image, double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize, bool optimize);
    void createPoissonPointDistribution(unsigned int pointNb=1000, float r=0.1);
    void createFromPointDistribution();
    void createFromPointDistributionFittedToPoints();
    void clearPointDistribution();
    void updateVertices( vector<BasicPoint> & points );
    void updateVertices(  );

    void initializeCage();
    const C3t3_with_info & getC3t3(){return c3t3_with_info;}
    void getMesh(std::vector<BasicPoint> & vertices, std::vector<Triangle> & triangles, std::vector<Subdomain_index> & tr_si,
                 std::vector<Tetrahedron> & tetrahedra, std::vector<Subdomain_index> & tet_si);
    void getMesh(std::vector<BasicPoint> & V, std::vector<Tetrahedron> & Tet);
    void getEnvelope(std::vector<BasicPoint> & V, std::vector<Triangle> & T);
    void computeDeformation(VoxelGrid & result);
    void computePseudoRasterDeformation(VoxelGrid & result);
    void computePseudoRasterDeformation(VoxelGrid & HRGrid, std::ifstream & inputImaFile, VoxelGrid & HRresult, std::ofstream & outputImaFile );
    void drawPointDistribution();
    void drawBoundingBox(){}
    void drawCageTetMesh();
    void drawWithVertexGradient(const Triangulation::Cell_handle & ch, int i, int n_dir=1);
    void drawTransferMesh(bool displayDistortions, bool displayOutliers, QColor & defaultColor, float radius);
    void drawPointsToAdd();
    void drawVertices(float radius);
    void drawEdges();


    void computeBasisLaplacians();
    void computeTetVertexGradient();
    void computeDistortions();
    void adaptSampling();

    void clear();
    void updateMesh(const std::vector<BasicPoint> & vertices , const std::vector<Tetrahedron> & tetrahedra, const std::vector<Subdomain_index> & tet_ids );
    bool deformPointsUsingTransferMesh( const std::vector<BasicPoint> & positions, std::vector<BasicPoint> & results );
    void checkIfPointsAreInsideTrMesh();

    void setSamplingType(SamplingType st){samplingType = st;}
    void setDistortionType(DistortionType st){distortionType = st;}

    void setClipeEquation(const BasicPoint & clipN, const BasicPoint & pointN){ clippingNormal = clipN; clippingNormal.normalize(); pointOnClipping = pointN; }
    void setOrthongonalCut(const BasicPoint & _cut, const BasicPoint & _cutDirection){ cut = _cut; cutDirection = _cutDirection; }

    void checkAndMarkOutliers();

    void saveTransferMesh(std::string fname){  /*std::ofstream medit_file(string.c_str());   c3t3_with_info.output_to_medit(medit_file);*/}
    void initializeForDistortionComputation();
    std::vector<float> size_field;

    GRID_TYPE value(int i );
    void updatePositions(std::vector< std::pair<Voxel, GRID_TYPE > > & outside);
    void computeTransferMeshDistortion(std::vector<int> & values, float & min_value, float & max_value);
    std::vector<float> & getSizingField(){return size_field; }
    std::vector<float> const & getSizingField() const {return size_field; }
    std::vector<float> & getError(){return error_field; }
    std::vector<float> const & getError() const {return error_field; }

    void exude();
    void set_transfer_mesh_to_initial_state();
    void set_visibility_check(bool _v_check){visibility_check = _v_check; }
    void addNewSeparation( const Separation_info & separation_info );
    void separateLabels( std::pair<Subdomain_index, Subdomain_index> labels);
    void separateLabels( const Separation_info & separation_info, const std::vector<bool> & selected_vertices );
    void select( const Separation_info & separation_info, std::vector<bool> & selected_vertices  );

    void automaticSeparation(  );

    inline void glVertex( BasicPoint const & p )
    {
        glVertex3f( p[0] , p[1] , p[2] );
    }
    inline void glNormal( BasicPoint const & p )
    {
        glNormal3f( p[0] , p[1] , p[2] );
    }

protected :

    void computeVerticesNormals();
    void computeBB();
    bool locate( const Point_3 & p, Triangulation::Locate_type & lt, int & li, int & lj, Triangulation::Cell_handle & ch, Triangulation::Cell_handle start, std::vector<bool> & visited );
    Triangulation::Cell_handle locate(const Point_3 & p, Triangulation::Locate_type & lt, int & li, int & lj, Triangulation::Cell_handle start );
    void computeSizeField();
    void computeContiniousSizeField( const std::vector<float> & vertices_average_sizing );
    void surfaceToPolyhedron(const std::vector<BasicPoint> & vertices, const std::vector< std::vector<int> > & triangles, Input_Polyhedron & polyhedron );
    void updateMeshFromC3t3( C3t3_with_info & c3t3 );
    float getDistortionColor(float value);
    bool isVisiblePoint( const Point_3 & point );
    bool isVisibleTet(const Triangulation::Cell_handle & ch);

    bool hasInComplexCell( const Triangulation::Vertex_handle & vh0, const Triangulation::Vertex_handle & vh1  );
    bool hasInComplexCell( const Triangulation::Edge & edge );
    bool is_tet_in_complex( const Triangulation::Cell_handle & ch );
    void computeInitialTetBB( const Triangulation::Cell_handle & ch, BasicPoint & BB_min, BasicPoint & BB_max );
    void computeDeformedTetBB( const Triangulation::Cell_handle & ch, BasicPoint & BB_min, BasicPoint & BB_max  );
    void clear_triangulations();

    void computeBasisTransform( const Triangulation::Cell_handle &ch , std::vector<BasicPoint> & D);
    bool invertMatrix( const std::vector<BasicPoint> & m, std::vector<BasicPoint> & invM);
    void computeBasisTransforms( );
    float computeVolumeBasisLaplacian( const Triangulation::Cell_handle & ch );
    float computeTopologicalBasisLaplacian ( const Triangulation::Cell_handle & ch );
    float computeCellVolume(const Triangulation::Cell_handle & ch);
    float computeNorm(const std::vector<BasicPoint> &m );
    bool locatePointInComplex(const BasicPoint & point, Triangulation::Cell_handle & ch, const Triangulation::Cell_handle & start );
    bool findInComplexCell( Triangulation::Cell_handle &ch, Triangulation::Locate_type locate_type, int li, int lj);


    void initializeErrorQueue();

    float computeVertexGradient( const Triangulation::Vertex_handle & vh, std::vector< Geom_traits::Vector_3 > & vertices_motion );
    void updateNewVertexAndNeighborsGradients( const Triangulation::Vertex_handle & vh, std::vector< K::Vector_3 > &vertices_motion );

    void addVertexAndUpdate( const K::Vector_3 & point, std::vector< K::Vector_3 > &vertices_motion, Triangulation::Cell_handle ch = Triangulation::Cell_handle());
    void splitEdgesAndUpdate( const Triangulation::Vertex_handle & vh, std::vector< K::Vector_3 > &vertices_motion);
//    void addVertexAtTetrahedronBarycenterAndUpdate( const Point_3 & point, Triangulation::Cell_handle & ch );

    void getOppositeEdgeIndices(int i0, int i1, int & i2, int & i3 );
    Point_3 computeCellBarycenter(const Triangulation::Cell_handle & ch);

    void addVertexAtBarycenter(Triangulation::Cell_handle & ch, std::vector< K::Vector_3 > &vertices_motion);
    void addVerticesAtCellsBarycenterAndUpdate(Triangulation::Vertex_handle & vh, std::vector< K::Vector_3 > &vertices_motion);
    void computeVerticesMotion( std::vector< Geom_traits::Vector_3 > & vertices_motion );

    bool computeSteinerPointPosition( const Triangulation::Vertex_handle & vh , K::Vector_3 & average_position );

//    void computeInitialBarycentricCoords( const std::vector<BasicPoint> & positions );
//    void recomputeInitialInfos( const std::vector<BasicPoint> & positions );
    inline BasicPoint to_basic_point(const Point_3 & point);
    inline BasicPoint to_basic_point(const Geom_traits::Vector_3 & point){return BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));    }
    inline Point_3 to_point_3(const BasicPoint & point){return Point_3(point[0], point[1] , point[2]);}

    bool isPointInTransferTetMesh(const BasicPoint & point, Triangulation::Cell_handle start = Triangulation::Cell_handle());
    bool isPointInTransferTetMesh(const Point_3 & point, Triangulation::Cell_handle start = Triangulation::Cell_handle());
    bool isPointInTransferTetMesh(const Point_3 & point, Triangulation::Cell_handle & ch, Triangulation::Cell_handle start = Triangulation::Cell_handle());

    bool isPointInCageTetMesh(const BasicPoint & point, Triangulation::Cell_handle start = Triangulation::Cell_handle());
    bool isPointInCageTetMesh(const Point_3 & point, Triangulation::Cell_handle start = Triangulation::Cell_handle());
    bool isPointInCageTetMesh(const Point_3 & point, Triangulation::Cell_handle & ch, Triangulation::Cell_handle start = Triangulation::Cell_handle());

    bool isPointInCell( const Point_3 & p, const Triangulation::Cell_handle & c, Triangulation::Locate_type & lt, int & li, int & lj);

    void computeBarycentricCoordinates(const Triangulation::Cell_handle & ch , const BasicPoint & point, float & ld0, float & ld1, float & ld2, float & ld3 );
//    bool computeBaryCoords(Triangulation::Cell_handle & ch, const BasicPoint & point, float & ld0, float & ld1, float & ld2, float & ld3 );
    void glFacet(const Triangulation::Facet & facet);
    void glSmoothFacet(const Triangulation::Facet & facet);

    inline bool is_in_complex(const Triangulation::Cell_handle & ch);
    inline void add_to_complex(const Triangulation::Cell_handle & ch);

    inline void updateOutOfComplexCells();
//    inline void remove_from_complex(const Triangulation::Cell_handle & ch);
//

    void initializeLRISolver();
    std::vector<BasicPoint> points;

    C3t3_with_info from_poly_c3t3;
    C3t3_with_info c3t3_with_info;

    BasicPoint BBMin;
    BasicPoint BBMax;

    float _r;

    bool dataLoaded;

    int indices[4][3];

    Triangulation * transfer_mesh;
    std::vector<Point_3> initial_vertices_positions;

    Triangulation::Cell_handle defStart;
    SK::Point_3 p_start;
    std::vector< std::pair<Triangulation::Cell_handle, std::vector <float> > > initPointBaryCoords;

    std::vector< float > vertices_gradient;
    std::vector<float> error_field;

    float max_gradient;
    float min_gradient;
    float refinement_limit;
    float min_size_value;
    float max_size_value;

    bool visibility_check;

    typedef std::pair<Triangulation::Vertex_handle , int> VHandleSug;
    typedef std::pair<float, VHandleSug > ErrorVHandleSug;
    std::priority_queue < ErrorVHandleSug, std::deque< ErrorVHandleSug > , std::less<ErrorVHandleSug> > vertex_error_queue;

    std::priority_queue < std::pair<float, Triangulation::Cell_handle> , std::deque<std::pair<float, Triangulation::Cell_handle> > , std::less<std::pair<float, Triangulation::Cell_handle> > > cell_error_queue;

    std::vector<bool> outlier_vertices;

    std::vector<int> suggestion;

    CMInterface< BasicPoint > * cageInterface;
    VoxelGrid * voxel_grid;

    std::vector<BasicPoint> added_points;

    float objectifGradient;
    DistortionType distortionType;
    SamplingType samplingType;

    BasicPoint clippingNormal;
    BasicPoint pointOnClipping;

    BasicPoint cut;
    BasicPoint cutDirection;

    mutable CGAL::Random rng;

    std::vector<Geom_traits::Vector_3> vertices_normals;
    bool outlierChecked;
    float computationTime;

    // Mesh criteria
    Uniform_sizing_field initial_sizing_field;

    TetraLRISolver tetraLRISolver;
    std::vector< Separation_info > separation_information;

    void computeErrorField();
    void computeContiniousError(const std::vector<float> & vertices_average_error, int iterations=1 );
    float average_error;
    bool use_error_field;
    bool isAutomaticSeparation;

};

#endif // TETMESHCREATOR_H
