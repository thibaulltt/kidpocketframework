#include "SurfaceCreator.h"


#include <CGAL/Surface_mesh_default_criteria_3.h>
#include <CGAL/Complex_2_in_triangulation_3.h>
#include <CGAL/IO/Complex_2_in_triangulation_3_file_writer.h>
#include <fstream>
#include <CGAL/make_surface_mesh.h>
#include <CGAL/IO/output_surface_facets_to_polyhedron.h>
#include <CGAL/Implicit_surface_3.h>

#include <CGAL/basic.h>
#include <CGAL/iterator.h>

// Simplification function
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>

// Stop-condition policy
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_cost.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Midpoint_placement.h>


// surface mesh

typedef CGAL::Inverse_index< Polyhedron::Vertex_const_iterator > Index;

// c2t3
typedef CGAL::Complex_2_in_triangulation_3<Surface_Tr> C2t3;


typedef CGAL::Implicit_surface_3<GT, Gray_level_image> Surface_3;

// Adaptor for Polyhedron_3
#include <CGAL/Surface_mesh_simplification/HalfedgeGraph_Polyhedron_3.h>

// Simplification function
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>

// Stop-condition policy
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_stop_predicate.h>


namespace SMS = CGAL::Surface_mesh_simplification ;

SurfaceCreator::SurfaceCreator( const CGAL::Image_3 & img , int greyLevel, float angle, float size, float approximation, BasicPoint offset  )
{

    Surface_Tr tr;            // 3D-Delaunay triangulation
    C2t3 c2t3 (tr);   // 2D-complex in 3D-Delaunay triangulation



    // the 'function' is a 3D gray level image
    Gray_level_image image( img , float(greyLevel));

    // Carefully choosen bounding sphere: the center must be inside the
    // surface defined by 'image' and the radius must be high enough so that
    // the sphere actually bounds the whole image.
    GT::Point_3 bounding_sphere_center(float(img.xdim()), float(img.ydim()), float(img.zdim()));
    GT::FT bounding_sphere_squared_radius = float(img.xdim())*float(img.xdim()) + float(img.ydim())*float(img.ydim()) +float(img.zdim())*float(img.zdim()) +1 ;
    GT::Sphere_3 bounding_sphere(bounding_sphere_center,
                                 bounding_sphere_squared_radius);

    // definition of the surface, with 10^-5 as relative precision
    Surface_3 surface(image, bounding_sphere, 1e-5);

    // defining meshing criteria
    CGAL::Surface_mesh_default_criteria_3<Surface_Tr> criteria(angle,
                                                               size,
                                                               approximation);

    // meshing surface, with the "manifold without boundary" algorithm
    CGAL::make_surface_mesh(c2t3, surface, criteria, CGAL::Manifold_tag());

    CGAL::output_surface_facets_to_polyhedron(c2t3, polyhedron);
/*
    for( Polyhedron::Vertex_const_iterator vit = polyhedron.vertices_begin(); vit != polyhedron.vertices_end(); ++vit) {
        GT::Point_3 & p = vit->point();
        p= GT::Point_3(vit->point().x()- offset[0], vit->point().y()- offset[1], vit->point().z()- offset[2]) ;
    }
*/
    recomputeVertexNormals();

}

void SurfaceCreator::getMesh(std::vector<BasicPoint> & vertices, std::vector<Triangle> & triangles){
    // Print header.
    vertices.clear();
    triangles.clear();

    for( Polyhedron::Vertex_const_iterator vit = polyhedron.vertices_begin(); vit != polyhedron.vertices_end(); ++vit) {
        vertices.push_back(BasicPoint(CGAL::to_double( vit->point().x()),
                                      CGAL::to_double( vit->point().y()),
                                      CGAL::to_double( vit->point().z())));
    }

    Index index( polyhedron.vertices_begin(), polyhedron.vertices_end());

    for( Polyhedron::Facet_const_iterator fi = polyhedron.facets_begin(); fi != polyhedron.facets_end(); ++fi) {
        Polyhedron::Halfedge_around_facet_const_circulator hc = fi->facet_begin();
        Polyhedron::Halfedge_around_facet_const_circulator hc_end = hc;
        std::size_t n = circulator_size( hc);
        CGAL_assertion( n >= 3);
        std::vector<int> ids;
        do {
            ids.push_back(index[ Polyhedron::Vertex_const_iterator(hc->vertex())]);
            ++hc;
        } while( hc != hc_end);
        triangles.push_back(Triangle(ids[0], ids[1], ids[2]));
    }
}

void SurfaceCreator::recomputeVertexNormals(){

    normals.clear();
    normals.resize(polyhedron.size_of_vertices(), BasicPoint(0.,0.,0.));

    Index index( polyhedron.vertices_begin(), polyhedron.vertices_end());

    for( Polyhedron::Facet_const_iterator fi = polyhedron.facets_begin(); fi != polyhedron.facets_end(); ++fi) {
        Polyhedron::Halfedge_around_facet_const_circulator hc = fi->facet_begin();
        Polyhedron::Halfedge_around_facet_const_circulator hc_end = hc;
        std::size_t s = circulator_size( hc);
        CGAL_assertion( s >= 3);

        BasicPoint points[3];
        int i = 0;
        do {
            points[i] = BasicPoint(CGAL::to_double( hc->vertex()->point().x() ), CGAL::to_double( hc->vertex()->point().y() ), CGAL::to_double( hc->vertex()->point().z() ));
            ++hc;
            ++i;
        } while( hc != hc_end);

        BasicPoint n = cross( points[2] - points[0], points[1] - points[0] );
        do {
            normals [index[ Polyhedron::Vertex_const_iterator(hc->vertex())]] += n;
            ++hc;
        } while( hc != hc_end);
    }

    for( unsigned int i = 0 ; i < normals.size() ; i ++ ){
       normals[i].normalize();
    }
}

void SurfaceCreator::simplify(unsigned int edgeNb){

    // This is a stop predicate (defines when the algorithm terminates).
    // In this example, the simplification stops when the number of undirected edges
    // left in the surface drops below the specified number (1000)
    SMS::Count_stop_predicate<Polyhedron> stop(edgeNb);

   // This the actual call to the simplification algorithm.
    // The surface mesh and stop conditions are mandatory arguments.
    // The index maps are needed because the vertices and edges
    // of this surface mesh lack an "id()" field.
    int r = SMS::edge_collapse
              (polyhedron
              ,stop
               ,CGAL::parameters::vertex_index_map(get(CGAL::vertex_external_index,polyhedron))
                                 .halfedge_index_map  (get(CGAL::halfedge_external_index  ,polyhedron))
                                 .get_cost (SMS::Edge_length_cost <Polyhedron>())
                                 .get_placement(SMS::Midpoint_placement<Polyhedron>())
              );

    recomputeVertexNormals();
}

void SurfaceCreator::draw(){

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    Index index( polyhedron.vertices_begin(), polyhedron.vertices_end());
    glBegin(GL_TRIANGLES);
    for( Polyhedron::Facet_const_iterator fi = polyhedron.facets_begin(); fi != polyhedron.facets_end(); ++fi) {
        Polyhedron::Halfedge_around_facet_const_circulator hc = fi->facet_begin();
        Polyhedron::Halfedge_around_facet_const_circulator hc_end = hc;
        std::size_t s = circulator_size( hc);
        CGAL_assertion( s >= 3);

        do {
            glNormal(normals[index[ Polyhedron::Vertex_const_iterator(hc->vertex())]]);

            glVertex3f(CGAL::to_double( hc->vertex()->point().x() ), CGAL::to_double( hc->vertex()->point().y() ), CGAL::to_double( hc->vertex()->point().z() ));
            ++hc;
        } while( hc != hc_end);
    }
    glEnd();
}
