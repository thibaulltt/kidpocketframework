/****************************************************************************
*****************************************************************************/


//#include "Mesh_3_image_3_domain.h"
#include "MesherViewer.h"

#include <algorithm>
#include <QGLViewer/manipulatedFrame.h>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QFormLayout>
#include <QSpinBox>
#include <QDialogButtonBox>
#include <QComboBox>
#include <QString>

#include <CGAL/Mesh_3/dihedral_angle_3.h>

#if QT_VERSION >= 0x040000
# include <QKeyEvent>
#endif


using namespace qglviewer;
using namespace std;

static unsigned int max_operation_saved = 10;

MesherViewer::MesherViewer(QWidget *parent) : QGLViewer(parent) {}

MesherViewer::~MesherViewer(){}

void MesherViewer::init()
{
	animationTimer.setAnimationLengthInMilliSeconds(5000);
	animationTimer.playAnimation = false;

	// Absolutely needed for MouseGrabber
	setMouseTracking(true);

	restoreStateFromFile();

	// The ManipulatedFrame will be used as the clipping plane
	setManipulatedFrame(new ManipulatedFrame());

	// Enable plane clipping
	//   glEnable(GL_CLIP_PLANE0);


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	mode = MESHING;

	cageInterface = CMInterface< BasicPoint >(this);

	manipulator = new RotationManipulator;
	connect(manipulator , SIGNAL(moved()) , this , SLOT(updateFromCMInterface()));
	connect(manipulator , SIGNAL(mouseReleased()) , this , SLOT(manipulatorReleased()));

	rselection = new RectangleSelection;
	connect( rselection , SIGNAL(add(QRectF , bool )) , this , SLOT(addToSelection(QRectF , bool)) );
	connect( rselection , SIGNAL(remove(QRectF)) , this , SLOT(removeFromCageSelection(QRectF)) );
	connect( rselection , SIGNAL(apply()) , this , SLOT(computeManipulatorForCageDeformation()) );

	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);

	initLightsAndMaterials();

	setKeyDescription(Qt::Key_R, "Refresh display");
	setKeyDescription(Qt::Key_D, "Change display mode");
	setKeyDescription(Qt::Key_I, "Initialization ofr cage deformation");

	displayMode = LIGHTED;

	displayType = IMAGE;

	cageLoaded = false;
	cageInitialized = false;
	displayWireCage = false;
	displayCageNormals = false;
	displayDeformedImage = false;
	displayCageProblems = false;
	envelopCage = false;
	useRandomColor = false;
	displayBB = false;
	displayTetrahedron = false;
	displaySegmentation = false;
	displaySurface = true;
	drawModel = false;
	movingLight = false;
	displayCageBB = false ;
	displaySampling= false ;
	displayTransferMesh= false ;
	displayCageTets= false ;
	displayPointsToAdd = false;
	displayDistortions = false;
	displayEdges = false;
	displayBC = false;
	enableClipping = false;
	separationActive = false;
	gridOffsetActive= false;
	displayOutliers = false;
	displaySegmentation= false;
	lr_deformation = false;
	cutDirection = BasicPoint(1.,1.,1.);

	useTransferMesh = false;

	displayBB = false;
	sphereScale = 3.;
	manipulatorScale = 1.;
	rasterType = VECTOR_FIELD;
	glewInit();

	//Check if the GLSL code can be executed
	if (GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader)
		cout << "Ready for GLSL\n";
	else {
		cout << "Not totally ready \n";
		exit(1);
	}

	BBMax = BasicPoint(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	BBMin = BasicPoint(FLT_MAX, FLT_MAX, FLT_MAX);

	setBackgroundColor(QColor(255,255,255));
	error_count = 0;
}

void MesherViewer::initLightsAndMaterials() {

	lightInfo[0] = 0;
	lightInfo[1] = 0;
	lightInfo[2] = 0;

	GLTools::initLights();
	//GLTools::setDefaultMaterial();
	//GLTools::setSunriseLight();
	GLTools::setDefaultMaterial();
	glDisable(GL_CULL_FACE);
	float lmodel_twoside[] =
	{GL_TRUE};
	glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, lmodel_twoside);
}

void MesherViewer::useMovingLight(bool _useMovingLight){
	movingLight =  _useMovingLight ;
	//    if(movingLight){
	//        GLTools::setMovingSpotLight();
	//        GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
	//    }
	//    else {
	//        GLTools::initLights();
	//        GLTools::setSunriseLight();
	//    }
	update();
}


void MesherViewer::mousePressEvent(QMouseEvent* e )
{

	if( ( e->modifiers() & Qt::ShiftModifier ) )
	{
		if( mode != MESHING )
		{
			if( rselection->isInactive() )
			{
				rselection->activate();
			}
			rselection->mousePressEvent( e , camera() );
			update();
			return;
		}
	}

	else if ((e->button() == Qt::MidButton) && (e->modifiers() & Qt::ControlModifier))
	{
		manipulator->clear();
		manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
		cageInterface.make_selected_fixed_handles();
	}

	else if ((e->button() == Qt::LeftButton) && (e->modifiers() & Qt::ControlModifier))
	{
		bool found;
		qglviewer::Vec point = camera()->pointUnderPixel(e->pos(), found);
		if( found ){
			manipulator->clear();
			manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
			cageInterface.make_fixed_handles(BasicPoint(point[0], point[1], point[2]), sphereScale*3.);
		}
	}
	else if ((e->button() == Qt::RightButton) && (e->modifiers() & Qt::ControlModifier))
	{
		bool found;
		qglviewer::Vec point = camera()->pointUnderPixel(e->pos(), found);
		if( found ){
			manipulator->clear();
			manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
			cageInterface.select(BasicPoint(point[0], point[1], point[2]), sphereScale*3. );
		}
	}

	QGLViewer::mousePressEvent(e);
}

void MesherViewer::mouseMoveEvent(QMouseEvent* e  )
{
	//   if( ( e->modifiers() & Qt::ShiftModifier ) )
	if( ! rselection->isInactive() )
	{
		if( mode != MESHING )
		{
			if( rselection->isInactive() )
			{
				rselection->activate();
			}
			rselection->mouseMoveEvent( e , camera() );
			update();
			return;
		}
	}

	QGLViewer::mouseMoveEvent(e);
}

void MesherViewer::mouseReleaseEvent(QMouseEvent* e  )
{

	if( ! rselection->isInactive() )
	{
		if( mode != MESHING )
		{
			if( rselection->isInactive() )
			{
				rselection->activate();
			}
			rselection->mouseReleaseEvent( e , camera() );
			rselection->deactivate();
			update();
			return;
		}
	}

	QGLViewer::mouseReleaseEvent(e);
}

void MesherViewer::manipulatorReleased(){
	saveCurrentCageState();
	if(mode == IMAGE_DEFORMATION){
		updateGrid();
	}
}

void MesherViewer::updateGridWithTransferMesh(bool minimalDisplay){
	if(lr_deformation)
		updateGridWithTransferMeshHR();

	if(useTransferMesh){
		defGrid.clearAll();
		QString fileName;

		if(minimalDisplay){
			fileName = QFileDialog::getSaveFileName(this, "Save grid as ", "./", "Data (*.dim)");

			// In case of Cancel
			if ( fileName.isEmpty() ) {
				return;
			}
		}

		voxelGrid.getDeformedVoxelGrid(defGrid, tetMeshCreator.getBBMin(), tetMeshCreator.getBBMax());

		tetMeshCreator.computeDeformation(defGrid);

		if(minimalDisplay){

			voxelGrid.clear();
			segmentation.clear();
			displayDeformedImage = false;
			saveGrid(fileName);
		} else {

			defGrid.sort();
			displayDeformedImage = true;
			//updateImageDisplay();
		}
	}
}

void MesherViewer::updateGridWithTransferMeshHR(){
	if(useTransferMesh){

		std::cout << "Update HR" << std::endl;
		HRDefGrid.clearAll();

		HRGrid.getDeformedVoxelGrid(HRDefGrid, tetMeshCreator.getBBMin(), tetMeshCreator.getBBMax());

		// Creating files
		QString fileName;

		fileName = QFileDialog::getSaveFileName(this, "Save grid as ", "./", "Data (*.dim)");

		// In case of Cancel
		if ( fileName.isEmpty() ) {
			return;
		}

		if(!fileName.endsWith(".dim"))
			fileName.append(".dim");

		QString outputImaName = QString(fileName);
		outputImaName.replace(".dim", ".ima" );

		std::ofstream outputImaFile (outputImaName.toUtf8(), ios::out | ios::binary);
		if (!outputImaFile.is_open())
			return ;

		std::ofstream dimFile (fileName.toUtf8());
		if (!dimFile.is_open())
			return ;

		dimFile << HRDefGrid.dim(0); dimFile << " " << HRDefGrid.dim(1); dimFile<< " "  << HRDefGrid.dim(2) << endl;

		dimFile << "-type U8" << endl;

		dimFile << "-dx "; dimFile << HRDefGrid.d(0) << endl;
		dimFile << "-dy "; dimFile << HRDefGrid.d(1) << endl;
		dimFile << "-dz "; dimFile << HRDefGrid.d(2) << endl;

		dimFile.close();

		char buffer = 0;

		QTime t;
		t.start();

		for(unsigned int i = 0 ; i < HRDefGrid.size() ; i ++){
			outputImaFile.write (&buffer, 1);
		}

		///
		std::ifstream imaFile (imaName.toUtf8(), ios::in|ios::binary|ios::ate);
		if (!imaFile.is_open())
			return;

		tetMeshCreator.computePseudoRasterDeformation(HRGrid, imaFile, HRDefGrid, outputImaFile);

		std::cout << "Time elapsed to deform out-of-core HR grid: " << t.elapsed() << " ms " << HRDefGrid.size() << std::endl;

		imaFile.close();
		outputImaFile.close();
	}
}

void MesherViewer::setUseARAP(bool _useARAP){
	cageInterface.setUseARAP(_useARAP);
	if(_useARAP)
		cageInterface.cageChanged(manipulator);

	updateFromCMInterface();
	update();
}

void MesherViewer::saveCurrentCageState(){

	if( Q.size() == max_operation_saved )
		Q.pop_front();
	Q.push_back(cageInterface.get_cage_vertices());

}

void MesherViewer::updateGrid(){
	if(mode == IMAGE_DEFORMATION){
		if(useTransferMesh){
		}
		else{
			defGrid.clearAll();
			std::map<unsigned int, unsigned int> VMap;
			voxelGrid.projectInRegularGrid(defGrid, VMap, 1., true);
			VMap.clear();

			//updateCuttingPlanes(false);
		}
		update();
	}
}


void MesherViewer::rasterizeGrid( unsigned int neighborhoodsize, double resolution){
	if(mode == IMAGE_DEFORMATION){

		defGrid.clearAll();

		if( rasterType == VECTOR_FIELD )
			voxelGrid.rasterizeUsingVectorFields(defGrid, neighborhoodsize, resolution);
		else if( rasterType == NEAREST_NEIGHBOR )
			voxelGrid.rasterizeClosest(defGrid, neighborhoodsize, resolution);
		else if( rasterType == DILATATION )
			voxelGrid.rasterize(defGrid, resolution);

		updateImageDisplay();
	}
}

void MesherViewer::setGridOffset(int offset, bool useGridSegmentation){

	/*voxelGrid.clear();
//    std::vector<GRID_TYPE> & data = voxelGrid.data();
//    voxelGrid = VoxelGrid(data, voxelGrid.dim(0), voxelGrid.dim(1) , voxelGrid.dim(2), voxelGrid.d(0) , voxelGrid.d(1), voxelGrid.d(2), voxelGrid.getOffset(),offset);
//    */
#if 1
	unsigned int init_dim [3] = { voxelGrid.dim(0), voxelGrid.dim(1), voxelGrid.dim(2) };

	//If size of the image changed
	if(voxelGrid.updateOffset(offset, segmentation)){
		if( lr_deformation ){


			const Voxel & voffset = voxelGrid.getVoxelOffset();

			Voxel HRDiffSize( (voxelGrid.dim(0) - init_dim[0] )*2, (voxelGrid.dim(1) - init_dim[1] )*2, (voxelGrid.dim(2) - init_dim[2] )*2 );

			std::cout << HRDiffSize << std::endl;

			unsigned int new_dim [3] = {  HRGrid.dim(0) + HRDiffSize.i(), HRGrid.dim(1)+ HRDiffSize.j(), HRGrid.dim(2) + HRDiffSize.j() };

			std::cout << HRDiffSize << std::endl;

			std::cout << "voffset "  << voffset << std::endl;

			// Creating files
			QString fileName = QFileDialog::getSaveFileName(this, "Save grid as ", "./", "Data (*.dim)");

			// In case of Cancel
			if ( fileName.isEmpty() ) {
				return;
			}

			if(!fileName.endsWith(".dim"))
				fileName.append(".dim");

			QString outputImaName = QString(fileName);
			outputImaName.replace(".dim", ".ima" );

			std::ofstream outputImaFile (outputImaName.toUtf8(), ios::out | ios::binary);
			if (!outputImaFile.is_open())
				return ;

			std::ofstream dimFile (fileName.toUtf8());
			if (!dimFile.is_open())
				return ;

			dimFile << new_dim[0]; dimFile << " " << new_dim[1]; dimFile<< " "  << new_dim[2] << endl;

			dimFile << "-type U8" << endl;

			dimFile << "-dx "; dimFile << HRGrid.d(0) << endl;
			dimFile << "-dy "; dimFile << HRGrid.d(1) << endl;
			dimFile << "-dz "; dimFile << HRGrid.d(2) << endl;

			dimFile.close();

			char buffer = 0;

			int newSize =  new_dim[0]* new_dim[1]* new_dim[2];
			for(unsigned int i = 0 ; i < newSize ; i ++){
				outputImaFile.write (&buffer, 1);
			}

			std::ifstream imaFile (imaName.toUtf8(), ios::in|ios::binary|ios::ate);
			if (!imaFile.is_open())
				return;

			std::cout << "Updating now" << std::endl;
			char current_icharacter = 0;
			for(int i = 0 ; i < HRGrid.dim(0) ; i++){
				for(int j = 0 ; j <  HRGrid.dim(1) ; j++){
					for(int k = 0 ; k < HRGrid.dim(2) ; k++){

						int input_index = HRGrid.getGridIndex( i,j,k );

						imaFile.seekg(input_index, ios::beg);
						imaFile.read( &current_icharacter, 1 );

						if( (int)current_icharacter > 0 ){
							int output_index =  (i + voffset.i()) + (j + voffset.j())*new_dim[0] + (k + voffset.k())*new_dim[0]*new_dim[1];
							outputImaFile.seekp(output_index, ios::beg);
							outputImaFile.write( &current_icharacter, 1 );
						}
					}
				}
			}

			outputImaFile.close();
			imaFile.close();

			imaName = fileName;
			HRGrid = VoxelGrid(this, new_dim[0], new_dim[1], new_dim[2], HRGrid.d(0), HRGrid.d(1), HRGrid.d(2), false );
		}
	}
#else
	Voxel voffset = Voxel(offset*2, offset*2, offset*2);

	Voxel new_dim (HRGrid.dim(0) + offset*4, HRGrid.dim(1) + offset*4, HRGrid.dim(2) + offset*4);

	std::cout << "voffset "  << voffset << std::endl;

	// Creating files
	QString fileName = QFileDialog::getSaveFileName(this, "Save grid as ", "./", "Data (*.dim)");

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	if(!fileName.endsWith(".dim"))
		fileName.append(".dim");

	QString outputImaName = QString(fileName);
	outputImaName.replace(".dim", ".ima" );

	std::ofstream outputImaFile (outputImaName.toUtf8(), ios::out | ios::binary);
	if (!outputImaFile.is_open())
		return ;

	std::ofstream dimFile (fileName.toUtf8());
	if (!dimFile.is_open())
		return ;

	dimFile << new_dim.i(); dimFile << " " << new_dim.j(); dimFile<< " "  << new_dim.k() << endl;

	dimFile << "-type U8" << endl;

	dimFile << "-dx "; dimFile << HRGrid.d(0) << endl;
	dimFile << "-dy "; dimFile << HRGrid.d(1) << endl;
	dimFile << "-dz "; dimFile << HRGrid.d(2) << endl;

	dimFile.close();

	char buffer = 0;

	int newSize =  new_dim.i()*new_dim.j()*new_dim.k();
	for(unsigned int i = 0 ; i < newSize ; i ++){
		outputImaFile.write (&buffer, 1);
	}

	std::ifstream imaFile (imaName.toUtf8(), ios::in|ios::binary|ios::ate);
	if (!imaFile.is_open())
		return;

	std::cout << "Updating now" << std::endl;
	char current_icharacter = 0;
	for(int i = 0 ; i < HRGrid.dim(0) ; i++){
		for(int j = 0 ; j <  HRGrid.dim(1) ; j++){
			for(int k = 0 ; k < HRGrid.dim(2) ; k++){

				int input_index = HRGrid.getGridIndex( i,j,k );

				imaFile.seekg(input_index, ios::beg);
				imaFile.read( &current_icharacter, 1 );

				if( (int)current_icharacter > 0 ){
					int output_index =  (i + voffset.i()) + (j+voffset.j())*new_dim.i() + (k+voffset.k())*new_dim.i()*new_dim.j();
					outputImaFile.seekp(output_index, ios::beg);
					outputImaFile.write( &current_icharacter, 1 );
				}
			}
		}
	}

	outputImaFile.close();
	imaFile.close();

	imaName = fileName;
	HRGrid = VoxelGrid(this->context(), new_dim[0], new_dim[1], new_dim[2], HRGrid.d(0), HRGrid.d(1), HRGrid.d(2), false );
#endif


	//updateCuttingPlanes(false);
	update();
}

void MesherViewer::restaureLastState(){

	if( Q.size() > 0 ){

		if(Q.size() > 1)
			Q.pop_back();

		setCageTopositions(Q.back());

		if(Q.size() > 1)
			Q.pop_back();
	}
	setCageTopositions(Q.back());
        
	if(Q.size() > 1)
		Q.pop_back();
}
    


void MesherViewer::setCageTopositions(const vector<BasicPoint> & positions){

	manipulator->clear();
	manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);

	cageInterface.setCageToPosition(positions);


	displayDeformedImage = false;
	if(mode == MESH_DEFORMATION){
		updatePointsFromCMInterface(mesh.getVertices(), false);
		mesh.recomputeNormals();

		updatePointsFromCMInterface(visu_mesh.getVertices(), false);
		visu_mesh.computeTexturesData();
		//updateCuttingPlanes(true);
	} else if(mode == IMAGE_DEFORMATION){
		updatePointsFromCMInterface(voxelGrid.getPositions(), false);
		updateGrid();
	}

}

void MesherViewer::addToSelection( QRectF const & zone , bool moving )
{
	if(manipulator->getEtat()) manipulator->deactivate();

	if(separationActive) {
		float modelview[16];
		glGetFloatv(GL_MODELVIEW_MATRIX , modelview);
		float projection[16];
		glGetFloatv(GL_PROJECTION_MATRIX , projection);

		std::vector<Subdomain_index> indexToLabel;
		segmentation.getSubdomainIndices(indexToLabel);
		QDialog * dialog = new QDialog(this);
		dialog->setWindowTitle(tr("Label separation"));

		QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);

		QGroupBox * labelsGroupBox = new QGroupBox("Label separation");
		dilogLayout->addWidget(labelsGroupBox);

		QVBoxLayout * labelsVLayout = new QVBoxLayout(labelsGroupBox);

		QComboBox * l1ComboxBox = new QComboBox();
		QComboBox * l2ComboxBox = new QComboBox();

		std::vector<Subdomain_index> labelToIndex ;
		for( unsigned int i = 0 ; i < indexToLabel.size() ; i++ ){
			if(indexToLabel[i] > 0){
				l1ComboxBox->addItem(QString::number(indexToLabel[i]));
				l2ComboxBox->addItem(QString::number(indexToLabel[i]));
				labelToIndex.push_back(indexToLabel[i]);
			}
		}

		labelsVLayout->addWidget(l1ComboxBox);
		labelsVLayout->addWidget(l2ComboxBox);

		QGroupBox * gridSep = new QGroupBox("Separate Grid Labels");
		gridSep->setCheckable(true);
		gridSep->setChecked(false);
		QFormLayout * gridSepLayout = new QFormLayout(gridSep);
		QSpinBox * offNb = new QSpinBox();

		gridSepLayout->addRow("Offset Nb: ", offNb);

		labelsVLayout->addWidget(gridSep);
		QDialogButtonBox * buttonBox = new QDialogButtonBox();
		buttonBox->setOrientation(Qt::Horizontal);
		buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

		QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
		QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));

		dilogLayout->addWidget(buttonBox);
		int i = dialog->exec();

		if(i == QDialog::Rejected || l1ComboxBox->count() == 0 || l2ComboxBox->count() == 0)
			return;

		int l1 = labelToIndex[l1ComboxBox->currentIndex()];
		int l2 = labelToIndex[l2ComboxBox->currentIndex()];


		if( l1!=l2 ){

			Separation_info separation_info = Separation_info( zone, modelview, projection, l1, l2);

			tetMeshCreator.addNewSeparation(separation_info);
			if(gridSep->isChecked())
				segmentation.addNewSeparation(separation_info, offNb->value());
			cageInitialized = false;
			mesh.clear();

			tetMeshCreator.getMesh( mesh.getVertices(), mesh.getTriangles(), mesh.getTriangles_subdomain_ids(), mesh.getTetrahedra(),  mesh.getTetrahedra_subdomain_ids() );

			useTransferMesh = true;

			mesh.updateModel();
		}
	} else if(gridOffsetActive){
		float modelview[16];
		glGetFloatv(GL_MODELVIEW_MATRIX , modelview);
		float projection[16];
		glGetFloatv(GL_PROJECTION_MATRIX , projection);

		std::vector<Subdomain_index> indexToLabel;
		segmentation.getSubdomainIndices(indexToLabel);
		QDialog * dialog = new QDialog(this);
		dialog->setWindowTitle(tr("Label separation"));

		QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);

		QFormLayout * gridSepLayout = new QFormLayout();
		QSpinBox * offNb = new QSpinBox();
		offNb->setRange(-20,20);
		gridSepLayout->addRow("Offset Nb: ", offNb);

		dilogLayout->addLayout(gridSepLayout);


		QDialogButtonBox * buttonBox = new QDialogButtonBox();
		buttonBox->setOrientation(Qt::Horizontal);
		buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

		QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
		QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));

		dilogLayout->addWidget(buttonBox);
		int i = dialog->exec();

		if(i == QDialog::Rejected )
			return;

		int l1 = offNb->value();

		Separation_info separation_info = Separation_info( zone, modelview, projection, 0, 0);
		segmentation.dilatation(separation_info, l1);

	} else
		cageInterface.select(zone , moving);
}

void MesherViewer::removeFromCageSelection( QRectF const & zone )
{
	if(manipulator->getEtat()) manipulator->deactivate();
	cageInterface.unselect(zone);
}

void MesherViewer::computeManipulatorForCageDeformation()
{

	if(!cageInitialized) initializeCage();
	cageInterface.computeManipulatorForSelection( manipulator );
}

void MesherViewer::clear(){

	mesh_3 = Mesh_3();
	mesh.clear();

	cageInterface.clear();

	manipulator->deactivate();

	cageLoaded = false;
	cageInitialized = false;

	//surfaceCreator = //surfaceCreator();

	Q.clear();

}

void MesherViewer::clearImage(){

	voxelGrid.clearAll();
	defGrid.clearAll();
	segmentation.clearAll();
	tetMeshCreator.clear();
	displayDeformedImage = false;
}

bool MesherViewer::openIMALR(const QString & fileName, VoxelGrid & hr_grid, VoxelGrid & grid ){

	std::cout << "Open LR" << std::endl;
	imaName = QString(fileName);

	imaName.replace(".dim", ".ima" );
	std::ifstream imaFile (imaName.toUtf8(), ios::in|ios::binary|ios::ate);
	if (!imaFile.is_open())
		return false;

	std::ifstream dimFile (fileName.toUtf8());
	if (!dimFile.is_open())
		return false;

	//HR grid input size
	int n[3];
	double d[3];

	dimFile >> n[0]; dimFile >> n[1]; dimFile >> n[2];

	string dummy, type;

	dimFile >> dummy;
	while (dummy.find("-type")==string::npos)
		dimFile >> dummy;

	dimFile >> type;

	while (dummy.find("-dx")==string::npos)
		dimFile >> dummy;

	dimFile >> d[0];

	dimFile >> dummy;
	while (dummy.find("-dy")==string::npos)
		dimFile >> dummy;

	dimFile >> d[1];

	dimFile >> dummy;
	while (dummy.find("-dz")==string::npos)
		dimFile >> dummy;

	dimFile >> d[2];


	cout << "(nx,dx) = ( " << n[0] << " ; " << d[0] << " ) "<< endl;
	cout << "(ny,dy) = ( " << n[1] << " ; " << d[1] << " ) "<< endl;
	cout << "(nz,dz) = ( " << n[2] << " ; " << d[2] << " ) "<< endl;

	//Find LR grid size and initialize
	unsigned int lr_n[3];
	unsigned int lr_p[3];
	for(int i = 0 ; i < 3 ; i ++){
		lr_n[i] = int(n[i]/2.);
		lr_p[i]=0;
		if( lr_n[i]%2 > 0) lr_p[i]++;
	}

	grid = VoxelGrid(this, lr_n[0]+lr_p[0], lr_n[1]+lr_p[1], lr_n[2]+lr_p[2], d[0]*2, d[1]*2, d[2]*2, false );
	hr_grid = VoxelGrid(this, n[0], n[1], n[2], d[0], d[1], d[2], false );

	QTime t;
	t.start();

	grid.clearToDefault();
	//

	char current_character = 0;

	// Seek to the beginning of the file
	imaFile.seekg(0, ios::beg);

	for( unsigned int i = 0, oi = 0 ; i < lr_n[0] ; i++, oi+=2 ){
		for( unsigned int j = 0, oj = 0 ; j < lr_n[1] ; j++, oj+=2){
			for( unsigned int k = 0, ok = 0 ; k < lr_n[2] ; k++, ok += 2 ){

				std::map<int, int> ids;

				for( unsigned int x = 0; x < 2 ; x++ ){
					for( unsigned int y = 0; y < 2 ; y++ ){
						for( unsigned int z = 0; z < 2 ; z++ ){
							if( oi+x < n[0] && oj+y < n[1] && ok+z < n[2] ) {
								int offset_index = hr_grid.getGridIndex(oi+x,oj+y,ok+z);
								imaFile.seekg(offset_index, ios::beg);

								imaFile.read( &current_character, 1 );

								ids[(int)current_character]++;

							}
						}
					}
				}

				int maxNb = 0 ;
				int id = 0;

				for(std::map<int, int>::iterator it = ids.begin() ; it != ids.end() ; it++){
					if( it->second > maxNb ){
						id = it->first;
						maxNb = it->second;
					}
				}

				grid.setValue( i, j, k, (GRID_TYPE)id + VoxelGrid::BACKGROUND_GRID_VALUE );
			}
		}
	}


	imaFile.close();


	std::cout << "Time elapsed to downsampled on the fly: " << t.elapsed() << " ms " << hr_grid.size() << std::endl;


	return true;

}

bool MesherViewer::openIMA(const QString & fileName, VoxelGrid & grid ){

	QString imaName = QString(fileName);

	imaName.replace(".dim", ".ima" );
	std::ifstream imaFile (imaName.toUtf8());
	if (!imaFile.is_open())
		return false;

	std::ifstream dimFile (fileName.toUtf8());
	if (!dimFile.is_open())
		return false;

	int n[3];
	double d[3];

	dimFile >> n[0]; dimFile >> n[1]; dimFile >> n[2];

	string dummy, type;

	dimFile >> dummy;
	while (dummy.find("-type")==string::npos)
		dimFile >> dummy;

	dimFile >> type;

	while (dummy.find("-dx")==string::npos)
		dimFile >> dummy;

	dimFile >> d[0];

	dimFile >> dummy;
	while (dummy.find("-dy")==string::npos)
		dimFile >> dummy;

	dimFile >> d[1];

	dimFile >> dummy;
	while (dummy.find("-dz")==string::npos)
		dimFile >> dummy;

	dimFile >> d[2];


	cout << "(nx,dx) = ( " << n[0] << " ; " << d[0] << " ) "<< endl;
	cout << "(ny,dy) = ( " << n[1] << " ; " << d[1] << " ) "<< endl;
	cout << "(nz,dz) = ( " << n[2] << " ; " << d[2] << " ) "<< endl;

	unsigned int size = n[0]*n[1]*n[2];
	unsigned int sizeIn = size;

	if( type.find("S16")!=string::npos )
		sizeIn = size*2;
	if( type.find("FLOAT")!=string::npos )
		sizeIn = size*4;

	unsigned char * data = new unsigned char[sizeIn];

	imaFile.read((char*)data, sizeIn);

	std::vector<GRID_TYPE> _data(size);

	if( type.find("S16")!=string::npos ){
		for(unsigned int i = 0, j=0 ; i < size ; i ++, j+=2)
			_data[i] = (GRID_TYPE)data[j] + VoxelGrid::BACKGROUND_GRID_VALUE;
	} else if( type.find("FLOAT")!=string::npos ){
		float * floatArray = (float*) data;

		for(unsigned int i = 0 ; i < size ; i ++)
			_data[i] = (GRID_TYPE)floatArray[i] + VoxelGrid::BACKGROUND_GRID_VALUE;

		delete [] floatArray;
	} else {
#if 1
		for(unsigned int i = 0 ; i < size ; i ++)
			_data[i] = (GRID_TYPE)data[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
#else
		for(unsigned int i = 0 ; i < n[0] ; i++){
			for(unsigned int j = 0 ; j < n[1] ; j++){
				for(unsigned int k = 0 ; k < n[2] ; k++){

					int id = i + j*n[0] + k*n[0]*n[1];
					int id1 = i + j*n[0] + (n[2]-1-k)*n[0]*n[1];
					_data[id1] = (GRID_TYPE)data[id] + VoxelGrid::BACKGROUND_GRID_VALUE;
				}
			}
					}
#endif


	}

	/*
		for(unsigned int i = 0 ; i < size ; i ++){
		if( _data[i] == 53 + VoxelGrid::BACKGROUND_GRID_VALUE )
			_data[i] = 53 + VoxelGrid::BACKGROUND_GRID_VALUE;
			else
		_data[i] = VoxelGrid::BACKGROUND_GRID_VALUE;
			}
*/
	delete [] data;

	grid = VoxelGrid( this, _data, n[0], n[1] , n[2], d[0] , d[1], d[2], false);

	return true;

}

void MesherViewer::openOBJ(const QString & filename){

	clear();
	clearImage();

	std::string name;
	Subdomain_index si ;
	extractInfoFromFileName(QFileInfo(filename), name, si);

	std::vector<BasicPoint> & V = mesh.getVertices();
	std::vector<Triangle> & T = mesh.getTriangles();

	FileIO::objLoader(name, V, T);

	mesh.getTriangles_subdomain_ids().resize(T.size(), si);

	updateInternalData();

}

void MesherViewer::openMESH(const QString & filename){

	ifstream myfile (filename.toLatin1());

	if (!myfile.is_open())
		exit (EXIT_FAILURE);

	clear();
	clearImage();

	string meshString;
	unsigned int sizeV, sizeT, sizeTet, dimension;

	std::vector<BasicPoint> vertices;
	std::vector<Triangle> triangles;
	std::vector<Tetrahedron> tetrahedra;

	std::vector<Subdomain_index> triangles_subdomain_ids;
	std::vector<Subdomain_index> tetrahedra_subdomain_ids;

	myfile >> meshString;
	while (meshString.find("Dimension")==string::npos)
		myfile >> meshString;

	myfile>> dimension;
	cout << meshString << " " << dimension << endl;
	while (meshString.find("Vertices")==string::npos)
		myfile >> meshString;

	myfile >> sizeV;
	cout << meshString << " " << sizeV << endl;

	int s;
	for (unsigned int i = 0; i < sizeV; i++) {
		double p[3];
		for (unsigned int j = 0; j < 3; j++)
			myfile >> p[j];

		vertices.push_back (BasicPoint(p[0], p[1], p[2]));
		myfile >> s;
	}

	while (meshString.find("Triangles")==string::npos)
		myfile >> meshString;

	myfile >> sizeT;
	cout << meshString << " " << sizeT << endl;
	for (unsigned int i = 0; i < sizeT; i++) {
		unsigned int v[3];
		for (unsigned int j = 0; j < 3; j++)
			myfile >> v[j];

		myfile >> s;

		triangles.push_back (Triangle (v[0]-1, v[1]-1, v[2]-1, s));
		triangles_subdomain_ids.push_back(s);
	}

	if( dimension == 3 ){
		while (meshString.find("Tetrahedra")==string::npos)
			myfile >> meshString;

		myfile >> sizeTet;
		cout << meshString << " " << sizeTet << endl;
		for (unsigned int i = 0; i < sizeTet; i++) {
			unsigned int v[4];
			for (unsigned int j = 0; j < 4; j++)
				myfile >> v[j];
			myfile >> s;
			tetrahedra.push_back (Tetrahedron (v[0]-1, v[1]-1, v[2]-1, v[3]-1,s));
			tetrahedra_subdomain_ids.push_back(s);

		}
	}
	myfile.close ();

	Triangulated_mesh_domain my_domain (vertices, tetrahedra, tetrahedra_subdomain_ids);
	mesh = Mesh(CGAL::make_mesh_3_from_labeled_triangulation_3<Mesh_3>(my_domain));

	updateInternalData();


}

void MesherViewer::openTransferMESH(const QString & filename){

	ifstream myfile (filename.toLatin1());

	if (!myfile.is_open())
		exit (EXIT_FAILURE);

	string meshString;
	unsigned int sizeV, sizeT, sizeTet, dimension;

	std::vector<BasicPoint> vertices;
	std::vector<Triangle> triangles;
	std::vector<Tetrahedron> tetrahedra;

	std::vector<Subdomain_index> triangles_subdomain_ids;
	std::vector<Subdomain_index> tetrahedra_subdomain_ids;

	myfile >> meshString;
	while (meshString.find("Dimension")==string::npos)
		myfile >> meshString;

	myfile>> dimension;
	cout << meshString << " " << dimension << endl;
	while (meshString.find("Vertices")==string::npos)
		myfile >> meshString;

	myfile >> sizeV;
	cout << meshString << " " << sizeV << endl;

	int s;
	for (unsigned int i = 0; i < sizeV; i++) {
		double p[3];
		for (unsigned int j = 0; j < 3; j++)
			myfile >> p[j];

		vertices.push_back (BasicPoint(p[0], p[1], p[2]));
		myfile >> s;
	}

	while (meshString.find("Triangles")==string::npos)
		myfile >> meshString;

	myfile >> sizeT;
	cout << meshString << " " << sizeT << endl;
	for (unsigned int i = 0; i < sizeT; i++) {
		unsigned int v[3];
		for (unsigned int j = 0; j < 3; j++)
			myfile >> v[j];

		myfile >> s;

		triangles.push_back (Triangle (v[0]-1, v[1]-1, v[2]-1, s));
		triangles_subdomain_ids.push_back(s);
	}

	if( dimension == 3 ){
		while (meshString.find("Tetrahedra")==string::npos)
			myfile >> meshString;

		myfile >> sizeTet;
		cout << meshString << " " << sizeTet << endl;
		for (unsigned int i = 0; i < sizeTet; i++) {
			unsigned int v[4];
			for (unsigned int j = 0; j < 4; j++)
				myfile >> v[j];
			myfile >> s;
			tetrahedra.push_back (Tetrahedron (v[0]-1, v[1]-1, v[2]-1, v[3]-1,s));
			tetrahedra_subdomain_ids.push_back(s);

		}
	}
	myfile.close ();

	tetMeshCreator.updateMesh(vertices, tetrahedra, tetrahedra_subdomain_ids);

	mesh.clear();

	tetMeshCreator.getMesh( mesh.getVertices(), mesh.getTriangles(), mesh.getTriangles_subdomain_ids(), mesh.getTetrahedra(),  mesh.getTetrahedra_subdomain_ids() );

	mesh.updateModel();

	visu_mesh = VisuMesh(&texture);
	std::vector<BasicPoint> & visu_vertices = visu_mesh.getVertices();
	std::vector<Tetrahedron> & visu_tetrahedra = visu_mesh.getTetrahedra();
	std::vector<BasicPoint> & textureCoords = visu_mesh.getTextureCoords();

	tetMeshCreator.getMesh( visu_vertices, visu_tetrahedra );

	for (unsigned int i = 0; i < visu_vertices.size(); i++) {
		const BasicPoint &position = visu_vertices[i];
		textureCoords.push_back( BasicPoint(std::min(1.f,position[0]/texture.getXMax()), std::min(1.f,position[1]/texture.getYMax()), std::min(1.f,position[2]/texture.getZMax())) );
	}
	visu_mesh.updateVisuMesh();
	visu_mesh.buildVisibilityTexture(textDisplayMap);
	//TODO fix !!!
}



void MesherViewer::openOFFFiles(const QFileInfoList &fileInfoList){

	clear();
	clearImage();

	for(int i = 0 ; i < fileInfoList.size() ; i++){

		std::string name;
		Subdomain_index si ;

		extractInfoFromFileName(fileInfoList.at(i), name, si);

		vector<BasicPoint> V;
		vector<Triangle> T;

		offLoader(name, V, T);
		mesh.addToMesh(V, T, si);

	}

	updateInternalData();

}

void MesherViewer::extractInfoFromFileName( const QFileInfo & fileInfo , std::string & filename, Subdomain_index & si ){


	QString base = fileInfo.baseName();

	int firstIndex = base.indexOf(QRegExp("[0-9]"));
	int lastIndex = base.lastIndexOf(QRegExp("[0-9]"));
	//        QString baseNamePart = base.mid(0, firstIndex);

	QString siString = base.mid(firstIndex, lastIndex);

	bool ok;
	si = siString.toInt(&ok);

	if (!ok)
	{
		si = DEFAULT_SI;
		std::cout << "Use basename<0-padded number>.<ending>, set to default subdomain index " << si << std::endl;
	}
	filename = fileInfo.filePath().toStdString();
}


void MesherViewer::openOFF (const QString & filename) {

	clear();
	clearImage();

	std::string name;
	Subdomain_index si ;
	extractInfoFromFileName(QFileInfo(filename), name, si);

	std::vector<BasicPoint> & vertices = mesh.getVertices();
	std::vector<Triangle> & triangles = mesh.getTriangles();

	std::vector<Subdomain_index> & triangles_subdomain_ids = mesh.getTriangles_subdomain_ids();

	offLoader(name, vertices, triangles);

	for(unsigned int i = 0 ; i < triangles.size() ; i++){
		for( int v = 0 ; v < 3 ; v++ ){
			int id = triangles[i].getVertex(v);
			if(id < 0 || id >= vertices.size())
				cout << "Bad index" << endl;
		}
	}

	triangles_subdomain_ids.resize(triangles.size(), si);

	updateInternalData();

}


void MesherViewer::openModel (const QString & filename) {

	std::string name;
	Subdomain_index si ;
	extractInfoFromFileName(QFileInfo(filename), name, si);

	std::vector<BasicPoint> & vertices = model.getVertices();
	std::vector<Triangle> & triangles = model.getTriangles();

	std::vector<Subdomain_index> & triangles_subdomain_ids = model.getTriangles_subdomain_ids();

	offLoader(name, vertices, triangles);

	triangles_subdomain_ids.resize(triangles.size(), si);

	model.updateModel();

}

void MesherViewer::offLoader ( const std::string & filename, std::vector<BasicPoint> & V, std::vector<Triangle> & T){

	V.clear();
	T.clear();

	std::ifstream in (filename.c_str ());
	if (!in)
		exit (EXIT_FAILURE);

	std::string offString;
	unsigned int sizeV, sizeT, tmp;
	in >> offString >> sizeV >> sizeT >> tmp;
	for (unsigned int i = 0; i < sizeV; i++) {
		double v[3];
		for (unsigned int j = 0; j < 3; j++)
			in >> v[j];
		V.push_back (BasicPoint (v[0], v[1], v[2]));
	}
	int s;
	for (unsigned int i = 0; i < sizeT; i++) {
		in >> s;

		unsigned int v[s];
		for (int j = 0; j < s; j++)
			in >> v[j];

		T.push_back (Triangle (v[0], v[1], v[2]));

		if(s == 4)
			T.push_back (Triangle (v[1], v[2], v[4]));

	}
	in.close ();
}

void MesherViewer::openOBJFiles(const QFileInfoList & fileInfoList){

	clear();
	clearImage();

	for(int i = 0 ; i < fileInfoList.size() ; i++){

		std::string name;
		Subdomain_index si ;

		extractInfoFromFileName(fileInfoList.at(i), name, si);

		vector<BasicPoint> V;
		vector<Triangle> T;

		FileIO::objLoader(name, V, T);
		mesh.addToMesh(V, T, si);

	}

	updateInternalData();

}


void MesherViewer::locateCageProblems(){
	if(cageLoaded ){
		if(!cageInitialized) initializeCage();
		cageInterface.locateProblems(mesh.getTriangles());
	}
}

void MesherViewer::updateDirectMeshDeformation(){
	cageInterface.clear();
	cageInterface.setMode(REALTIME);
	cageInterface.setMethod(GREEN);

	std::vector<Triangle> & triangles = mesh.getTriangles();
	std::vector<int> indices (triangles.size());
	for( unsigned int i = 0 ; i < triangles.size() ; i++ )
		indices[i]=i;

	cageInterface.loadCageAndInitialize( mesh.getVertices() , triangles , indices );
	cageInitialized = true;
	cageLoaded = true;
	displayCage = true;
	manipulator->clear();
	manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
	saveCurrentCageState();

	double average = cageInterface.getAverage_edge_halfsize();
	sphereScale = scene_radius*0.01 /average;
	if(sphereScale > 1.) sphereScale = 1.;
}

void MesherViewer::useCGALEnvelopAsCage(){

	cageInterface.clear();
	cageInterface.setMode(REALTIME);
	cageInterface.setMethod(GREEN);

	cageInterface.loadCageAndInitialize( mesh.getVertices() , mesh.getTriangles() , mesh.getCGAL_envelop() );
	cageInitialized = true;
	envelopCage = true;
	cageLoaded = true;
	displayCage = true;
	manipulator->clear();
	manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
	saveCurrentCageState();

	double average = cageInterface.getAverage_edge_halfsize();
	sphereScale = scene_radius*0.01 /average;
	if(sphereScale > 1.) sphereScale = 1.;

}

void MesherViewer::saveCamera(const QString &filename){
	ofstream out (filename.toUtf8());
	if (!out)
		exit (EXIT_FAILURE);

	out << camera()->position() << " " <<
		   camera()->viewDirection() << " " <<
		   camera()->upVector() << " " <<
		   camera()->fieldOfView();
	out << endl;
	out.close ();
}

std::istream & operator>>(std::istream & stream, qglviewer::Vec & v)
{
	stream >>
			v.x >>
			v.y >>
			v.z;

	return stream;
}
void MesherViewer::openCamera(const QString &filename){

	std::ifstream file;
	file.open(filename.toStdString());

	qglviewer::Vec pos;
	qglviewer::Vec view;
	qglviewer::Vec up;
	float fov;

	file >> pos >>
			view >>
			up >>
			fov;

	camera()->setPosition(pos);
	camera()->setViewDirection(view);
	camera()->setUpVector(up);
	camera()->setFieldOfView(fov);

	camera()->computeModelViewMatrix();
	camera()->computeProjectionMatrix();

	update();
}


void MesherViewer::openCage (const QString & filename) {
	tetMeshCreator.clear();

	cageInterface.clear();
	cageInterface.setMode(REALTIME);
	cageInterface.setMethod(GREEN);
	cageInterface.open_OFF_cage(filename.toStdString() );

	cageInitialized = false;
	envelopCage = false;
	cageLoaded = true;
	displayCage = true;
	manipulator->clear();
	manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
	saveCurrentCageState();

	double average = cageInterface.getAverage_edge_halfsize();
	sphereScale = scene_radius*0.01 /average;
	if(sphereScale > 1.) sphereScale = 1.;

	tetMeshCreator = TetMeshCreator(&cageInterface, &voxelGrid);

	BasicPoint bb, BB;
	cageInterface.getCageBBox(bb, BB);
	updateCuttingPlanes(bb,BB);

	interpolationStruct.keys.clear();
	interpolationStruct.keys.push_back( cageInterface.get_cage_vertices() );
}

void MesherViewer::openFinalCage (const QString & filename) {

	if(!cageInitialized) initializeCage();

	vector<BasicPoint> V;
	vector<Triangle> T;

	offLoader(filename.toStdString(), V, T);

	unsigned int VNb = cageInterface.get_cage_vertices_nb();

	if(VNb != V.size()){
		cout <<"Not the same cage!!" << endl;
		return;
	}

	setCageTopositions(V);

	saveCurrentCageState();

	BasicPoint bb, BB;
	cageInterface.getCageBBox(bb, BB);
	updateCuttingPlanes(bb,BB);

	interpolationStruct.keys.push_back( V );

	update();
}



void MesherViewer::saveCage (const QString & filename) {
	cageInterface.saveCage(filename.toStdString());
}

void MesherViewer::open3DImage(const QString & fileName){

	//Texture objet
	texture.clear();
	subdomain_indices.clear();
	text_subdomain_indices.clear();
	std::vector<unsigned char> data;
	unsigned int nx, ny, nz;
	float dx, dy, dz;


	//Load the data from the 3D image
	if(fileName.endsWith(".nii"))
		openNIFTI(fileName,data,subdomain_indices, nx,ny,nz,dx,dy,dz);
	else if (fileName.endsWith(".dim"))
		openIMA(fileName,data,subdomain_indices, nx,ny,nz,dx,dy,dz);
	else
		return;

	for( unsigned int i = 0 ; i < subdomain_indices.size() ; i++ ){
		int currentLabel = subdomain_indices[i];
		text_subdomain_indices.push_back(currentLabel);
		std::map<unsigned char, QColor>::iterator it = textColorMap.find( currentLabel );
		if( it == textColorMap.end() ){
			if( currentLabel ==0 )
				textColorMap[currentLabel] = QColor(0,0,0);
			else {
				std::map<Subdomain_index, QColor>::iterator sit = fileColors.find(currentLabel);
				if( sit == fileColors.end() )
					textColorMap[currentLabel].setHsvF(0.98*double(i)/subdomain_indices.size(), 0.8,0.8);
				else
					textColorMap[currentLabel] = sit->second;
			}
		}

		textDisplayMap[currentLabel] = true;
	}

	texture.build(data,subdomain_indices,nx,ny,nz,dx,dy,dz,textColorMap);

	qglviewer::Vec maxTexture (texture.getXMax(), texture.getYMax() , texture.getZMax());

	//  updateCamera(maxTexture/2. , maxTexture.norm() );

	BasicPoint center (texture.getXMax()/2., texture.getYMax()/2. , texture.getZMax()/2.);

	GLfloat light_position0[4] = {-center[0], -center[1], -center[2], 0};
	GLfloat light_position1[4] = {(float)maxTexture.x+center[0], (float)maxTexture.y+center[1], (float)maxTexture.z, 0.f};
	GLfloat light_position2[4] = {(float)maxTexture.x+center[0], 0, static_cast<float>(0.75*maxTexture.z)};
	GLfloat light_position3[4] = {(float)-center[0], 0.f, static_cast<float>(0.25*maxTexture.z)};
	// TODO : are the lines above or below correct ? From merge conflict.
	// GLfloat light_position1[4] = {maxTexture.x+center[0], maxTexture.y+center[1], maxTexture.z, 0};
	// GLfloat light_position2[4] = {maxTexture.x+center[0], 0, 0.75*maxTexture.z};
	// GLfloat light_position3[4] = {-center[0], 0, 0.25*maxTexture.z};
	GLfloat light_position4[4] = {42, 374, 161, 0};
	GLfloat light_position5[4] = {473, -351, -259, 0};
	GLfloat light_position6[4] = {-438, 167, -48, 0};


	glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
	glLightfv (GL_LIGHT2, GL_POSITION, light_position2);
	glLightfv (GL_LIGHT3, GL_POSITION, light_position3);
	glLightfv (GL_LIGHT4, GL_POSITION, light_position4);
	glLightfv (GL_LIGHT5, GL_POSITION, light_position5);
	glLightfv (GL_LIGHT6, GL_POSITION, light_position0);

	//Once the 3D image is loaded, grid size parameters are sent to the interface
	//qemit setMaxCutPlanes(texture.getWidth(), texture.getHeight(), texture.getDepth());

	makeDefaultMesh();
}

void MesherViewer::openNIFTI(const QString & filename, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
							 unsigned int & nx , unsigned int & ny , unsigned int & nz, float & dx , float & dy , float & dz ){

	//Load nifti segmented image
	nifti_image * nim = nifti_image_read(filename.toUtf8(), 1);

	int gridSize = nim->nx*nim->ny*nim->nz;
	QString dataType(nifti_datatype_string(nim->datatype));

	cout << "nim->datatype : " <<qPrintable(dataType) << endl;

	data.clear();
	data.resize( gridSize );

	if(dataType == QString("UINT8")){
		unsigned char * tempData = (unsigned char*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ ){
			unsigned char value = static_cast<unsigned char>(tempData[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
		delete [] tempData;
	}else if( dataType == QString("UINT16")){
		unsigned short * tempData = (unsigned short*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ ){
			unsigned char value = static_cast<unsigned char>(tempData[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
		delete [] tempData;
	} else if(dataType == QString("UINT32") ) {
		unsigned int * tempData = (unsigned int*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ ){
			unsigned char value = static_cast<unsigned char>(tempData[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
		delete [] tempData;;
	} else if( dataType == QString("UINT64") ){
		unsigned long * tempData = (unsigned long*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ ){
			unsigned char value = static_cast<unsigned char>(tempData[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
		delete [] tempData;
	} else if( dataType == QString("INT16")){
		short * tempData = (short*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ ){
			unsigned char value = static_cast<unsigned char>(tempData[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
		delete [] tempData;

	}

	nx = nim->nx; ny = nim->ny; nz = nim->nz;
	dx = nim->dx; dy = nim->dy; dz = nim->dz;

	delete [] nim;
}

void MesherViewer::openIMA(const QString & fileName, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
						   unsigned int & nx , unsigned int & ny , unsigned int & nz, float & dx , float & dy , float & dz ){
	QString imaName = QString(fileName);

	imaName.replace(".dim", ".ima" );
	std::ifstream imaFile (imaName.toUtf8());
	if (!imaFile.is_open())
		return;

	std::ifstream dimFile (fileName.toUtf8());
	if (!dimFile.is_open())
		return;

	dimFile >> nx; dimFile >> ny; dimFile >> nz;

	string dummy, type;

	dimFile >> dummy;
	while (dummy.find("-type")==string::npos)
		dimFile >> dummy;

	dimFile >> type;

	while (dummy.find("-dx")==string::npos)
		dimFile >> dummy;

	dimFile >> dx;

	dimFile >> dummy;
	while (dummy.find("-dy")==string::npos)
		dimFile >> dummy;

	dimFile >> dy;

	dimFile >> dummy;
	while (dummy.find("-dz")==string::npos)
		dimFile >> dummy;

	dimFile >> dz;


	cout << "(nx,dx) = ( " << nx << " ; " << dx << " ) "<< endl;
	cout << "(ny,dy) = ( " << ny << " ; " << dy << " ) "<< endl;
	cout << "(nz,dz) = ( " << nz << " ; " << dz << " ) "<< endl;

	unsigned int size = nx*ny*nz;
	unsigned int sizeIn = size;

	if( type.find("S16")!=string::npos )
		sizeIn = size*2;
	if( type.find("FLOAT")!=string::npos )
		sizeIn = size*4;

	unsigned char * tempData = new unsigned char[sizeIn];

	imaFile.read((char*)tempData, sizeIn);

	data.clear();
	data.resize(size);

	if( type.find("S16")!=string::npos ){
		for(unsigned int i = 0, j=0 ; i < size ; i ++, j+=2){
			unsigned char value = static_cast<unsigned char>(tempData[j]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
	} else if( type.find("FLOAT")!=string::npos ){
		float * floatArray = (float*) tempData;

		for(unsigned int i = 0 ; i < size ; i ++){
			unsigned char value = static_cast<unsigned char>(floatArray[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
		delete [] floatArray;
	} else {
		for(unsigned int i = 0 ; i < size ; i ++){
			unsigned char value = static_cast<unsigned char>(tempData[i]);
			data[i] = value;
			if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
				labels.push_back(value);
		}
	}

	delete [] tempData;
}

void MesherViewer::makeDefaultMesh(){

	visu_mesh = VisuMesh(&texture);

	std::vector<BasicPoint> & vertices = visu_mesh.getVertices();
	std::vector<Tetrahedron> & tetrahedra = visu_mesh.getTetrahedra();
	std::vector<BasicPoint> & textureCoords = visu_mesh.getTextureCoords();

	const Vec3Di & Vmin = texture.getVmin();
	const Vec3Di & Vmax = texture.getVmax();

	Vec3Di diff = Vmax - Vmin;

	for (int i = 0; i < 2; i++){
		for (int j = 0; j < 2; j++){
			for (int k = 0; k < 2; k++){
				BasicPoint position = BasicPoint( texture.dx()*(i*diff[0]+Vmin[0]+1) + (i-1.5)*texture.dx(),
						texture.dy()*(j*diff[1]+Vmin[1]+1)+ (j-1.5)*texture.dy(),
						texture.dz()*(k*diff[2]+Vmin[2]+1)+ (k-1.5)*texture.dz() );
				vertices.push_back ( position );
				textureCoords.push_back( BasicPoint(std::max(std::min(1.f,position[0]/texture.getXMax()),0.f),
										 std::max(std::min(1.f,position[1]/texture.getYMax()),0.f),
						std::max(std::min(1.f,position[2]/texture.getZMax()),0.f)) );
			}
		}
	}
	int i = 0 , j = 0, k = 0 ;
	tetrahedra.push_back(Tetrahedron(getIndice(i+1, j  , k  ), getIndice(i+1, j+1, k  ),
									 getIndice(i  , j+1, k  ), getIndice(i+1, j+1, k+1)));

	tetrahedra.push_back(Tetrahedron(getIndice(i  , j  , k+1), getIndice(i  , j  , k  ),
									 getIndice(i  , j+1, k+1), getIndice(i+1  , j  , k+1)));

	tetrahedra.push_back(Tetrahedron(getIndice(i  , j+1, k+1), getIndice(i+1, j  , k  ),
									 getIndice(i+1, j+1, k+1), getIndice(i+1  , j  , k+1)));

	tetrahedra.push_back(Tetrahedron(getIndice(i  , j  , k  ), getIndice(i+1, j  , k  ),
									 getIndice(i  , j+1, k+1), getIndice(i+1  , j  , k+1)));

	tetrahedra.push_back(Tetrahedron(getIndice(i  , j  , k  ), getIndice(i+1, j  , k  ),
									 getIndice(i  , j+1, k  ), getIndice(i  , j+1, k+1)));

	tetrahedra.push_back(Tetrahedron(getIndice(i  , j+1, k  ), getIndice(i+1, j  , k  ),
									 getIndice(i+1, j+1, k+1), getIndice(i  , j+1, k+1)));

	/*
	QString fileName( "./Test_save.mesh" );
	std::ofstream out (fileName.toUtf8());
	if (!out)
		exit (EXIT_FAILURE);

	out << "MeshVersionFormatted " << 1 << std::endl;
	out << "Dimension " << 3 << std::endl;
	out << "Vertices\n";
	out << vertices.size() << std::endl;

	// std::vector<Vertex_handle> vertices ( triangulation.number_of_vertices() );


	for(unsigned int i = 0 ; i < vertices.size(); i++){
		const BasicPoint & p = vertices[i].getPos();
		out << p[0] << " " << p[1] << " " << p[2] << " " << 2 << std::endl;
	}

	out << "Triangles\n";
	out << 0 << std::endl;

	out << "Tetrahedra\n";
	out << tetrahedra.size() << std::endl;
	for( unsigned int i = 0 ; i < tetrahedra.size() ; i ++ ){
		for( int v = 0; v < 4 ; v++ )
			out <<  tetrahedra[i].getVertex(v)+1 << " ";
		out << 1 << std::endl;
	}
	out << std::endl;
	out.close ();
*/
	visu_mesh.updateVisuMesh();
	visu_mesh.buildVisibilityTexture(textDisplayMap);
}


int MesherViewer::getIndice(int i, int j , int k){
	return i*2*2 + j*2 +k;
}

void MesherViewer::updateCamera(const qglviewer::Vec & center, float radius){
	camera()->setSceneCenter(center);
	camera()->setSceneRadius(radius);

	camera()->showEntireScene();
}

void MesherViewer::openImage(const QString & filename, bool full_res){

	std::cout << "open 3D image!!!!!!" << std::endl;


	clear();
	clearImage();

	if(full_res){
		if( filename.endsWith(".nii") )
			openNIFTI(filename, voxelGrid);
		else{
			if( !openIMA(filename, voxelGrid) )
				return ;
		}
	} else {
		if( filename.endsWith(".nii") )
			openNIFTILR(filename, HRGrid, voxelGrid);
		else{
			if( !openIMALR(filename, HRGrid, voxelGrid) )
				return ;
		}
	}

	lr_deformation = !full_res;
	initializeImage();
	open3DImage(filename);
	emit setImageLabels();
}

void MesherViewer::openSegmentation(const QString & filename){


	if( filename.endsWith(".nii") )
		openNIFTI(filename, segmentation);
	else{
		if(!openIMA(filename, segmentation) )
			return ;
	}

	segmentation.sortSeg();

	//    std::vector<Subdomain_index> indexToLabel;
	//    segmentation.getSubdomainIndices(indexToLabel);
	//    computeRandomColors(indexToLabel, sColorMap);
	//    for(unsigned int i = 0 ; indexToLabel.size() ; i ++){
	//        sDisplayMap[indexToLabel[i]] = true;
	//    }
	//    std::cout << "Segmentation " << segmentation.size() << std::endl ;
	//    emit setSegmentationLabels();
	//initializeImage();

}

void MesherViewer::openNIFTILR(const QString & filename, VoxelGrid & hr_grid, VoxelGrid & grid){

	//Load nifti segmented image
	nifti_image * nim = nifti_image_read(filename.toUtf8(), 1);

	int gridSize = nim->nx*nim->ny*nim->nz;
	QString dataType(nifti_datatype_string(nim->datatype));

	cout << "nim->datatype : " <<qPrintable(dataType) << endl;
	std::vector<GRID_TYPE> data (gridSize);

	if(dataType == QString("UINT8")){
		unsigned char * tempData = (unsigned char*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;
	}else if( dataType == QString("UINT16")){
		unsigned short * tempData = (unsigned short*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;
	} else if(dataType == QString("UINT32") ) {
		unsigned int * tempData = (unsigned int*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;;
	} else if( dataType == QString("UINT64") ){
		unsigned long * tempData = (unsigned long*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;
	} else if( dataType == QString("INT16")){
		short * tempData = (short*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;

	}

	int n[3] = {nim->nx, nim->ny, nim->nz};
	float d[3] = {nim->dx, nim->dy, nim->dz};

	grid = VoxelGrid(this, data, n[0], n[1] , n[2], d[0] , d[1], d[2], false);


	delete [] nim;
}


void MesherViewer::openNIFTI(const QString & filename, VoxelGrid & grid){

	//Load nifti segmented image
	nifti_image * nim = nifti_image_read(filename.toUtf8(), 1);

	int gridSize = nim->nx*nim->ny*nim->nz;
	QString dataType(nifti_datatype_string(nim->datatype));

	cout << "nim->datatype : " <<qPrintable(dataType) << endl;
	std::vector<GRID_TYPE> data (gridSize);

	if(dataType == QString("UINT8")){
		unsigned char * tempData = (unsigned char*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;
	}else if( dataType == QString("UINT16")){
		unsigned short * tempData = (unsigned short*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;
	} else if(dataType == QString("UINT32") ) {
		unsigned int * tempData = (unsigned int*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;;
	} else if( dataType == QString("UINT64") ){
		unsigned long * tempData = (unsigned long*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;
	} else if( dataType == QString("INT16")){
		short * tempData = (short*)nim->data;
		for(int i = 0 ; i<gridSize ; i++ )
			data[i] = (GRID_TYPE)tempData[i] + VoxelGrid::BACKGROUND_GRID_VALUE;
		delete [] tempData;

	}

	int n[3] = {nim->nx, nim->ny, nim->nz};
	float d[3] = {nim->dx, nim->dy, nim->dz};

	grid = VoxelGrid(this, data, n[0], n[1] , n[2], d[0] , d[1], d[2], false);


	delete [] nim;
}

void MesherViewer::export_vtk( std::string const & filename , std::string const & field_name , std::string const & field_type , int field_components, std::vector<float> & values, VoxelGrid & grid )
{

	std::ofstream myfile;
	myfile.open(filename.c_str());
	if (!myfile.is_open())
	{
		std::cout << filename << " cannot be opened" << std::endl;
		return;
	}

	int nx = grid.xdim();
	int ny = grid.ydim();
	int nz = grid.zdim();

	BasicPoint _origin(0.,0.,0.);
	BasicPoint _spacing(grid.d(0), grid.d(1), grid.d(2));
	//boucle z, y ,x push_back
	myfile << "# vtk DataFile Version 2.0" << std::endl;
	myfile << filename << std::endl << "ASCII" << std::endl << "DATASET STRUCTURED_POINTS" << std::endl << "DIMENSIONS " << nx << " " << ny << " " << nz << std::endl;
	myfile << "ORIGIN " << _origin << std::endl;
	myfile << "SPACING " << _spacing << std::endl;
	myfile << "POINT_DATA " << nx*ny*nz << std::endl;
	myfile << "SCALARS " << field_name << " " << field_type << " " << field_components << std::endl;
	myfile << "LOOKUP_TABLE default" << std::endl;

	for( unsigned int i = 0 ; i < nx*ny*nz ; ++i){
		myfile <<values[i] << std::endl;
	}
	myfile.close();

}

void MesherViewer::saveSizingFieldAsGrid(QString  fileName){

	if( voxelGrid.size() == 0 ) {
		cout << "GridViewer::saveToNifti(QString fileName)::Empty grid" << endl;
		return;
	}

	char *hdr_file, *data_file;

	if(!fileName.endsWith(".nii"))
		fileName.append(".nii");

	hdr_file = qstrdup(qPrintable(fileName));

	data_file = qstrdup(qPrintable(fileName));

	nifti_1_header hdr;
	nifti1_extender pad={0,0,0,0};
	FILE *fp;
	int ret,i;
	MY_DATATYPE *data=NULL;
	short do_nii;


	/********** make sure user specified .hdr/.img or .nii/.nii */
	if ( (strlen(hdr_file) < 4) || (strlen(data_file) < 4) ) {
		fprintf(stderr, "\nError: write files must end with .hdr/.img or .nii/.nii extension\n");
		exit(1);
	}

	if ( (!strncmp(hdr_file+(strlen(hdr_file)-4), ".hdr",4)) &&
		 (!strncmp(data_file+(strlen(data_file)-4), ".img",4)) ) {
		do_nii = 0;
	}
	else if ( (!strncmp(hdr_file+(strlen(hdr_file)-4), ".nii",4)) &&
			  (!strncmp(data_file+(strlen(data_file)-4), ".nii",4)) ) {
		do_nii = 1;
	}
	else {
		fprintf(stderr, "\nError: file(s) to be written must end with .hdr/.img or .nii/.nii extension\n");
		exit(1);
	}


	/********** fill in the minimal default header fields */
	bzero((void *)&hdr, sizeof(hdr));
	hdr.sizeof_hdr = MIN_HEADER_SIZE;
	hdr.dim[0] = 4;
	hdr.dim[1] = voxelGrid.xdim();
	hdr.dim[2] = voxelGrid.ydim();
	hdr.dim[3] = voxelGrid.zdim();
	hdr.dim[4] = 1;
	hdr.datatype = NIFTI_TYPE_UINT8;
	hdr.bitpix = 8;
	hdr.pixdim[1] = voxelGrid.dx();
	hdr.pixdim[2] = voxelGrid.dy();
	hdr.pixdim[3] = voxelGrid.dz();
	hdr.pixdim[4] = 1.;
	hdr.vox_offset = (float)0;
	if (do_nii)
		strncpy(hdr.magic, "n+1\0", 4);
	else
		strncpy(hdr.magic, "ni1\0", 4);


	/********** allocate buffer and fill with dummy data  */
	data = new MY_DATATYPE [hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]];
	if (data == NULL) {
		fprintf(stderr, "\nError allocating data buffer for %s\n",data_file);
		exit(1);
	}

	for (i=0; i<hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]; i++){
		data[i] = MY_DATATYPE(std::max(tetMeshCreator.value(i), (GRID_TYPE)0));
	}

	/********** write first 348 bytes of header   */
	fp = fopen(hdr_file,"w");
	if (fp == NULL) {
		fprintf(stderr, "\nError opening header file %s for write\n",hdr_file);
		exit(1);
	}
	ret = fwrite(&hdr, MIN_HEADER_SIZE, 1, fp);
	if (ret != 1) {
		fprintf(stderr, "\nError writing header file %s\n",hdr_file);
		exit(1);
	}


	/********** if nii, write extender pad and image data   */
	if (do_nii == 1) {

		ret = fwrite(&pad, 4, 1, fp);
		if (ret != 1) {
			fprintf(stderr, "\nError writing header file extension pad %s\n",hdr_file);
			exit(1);
		}

		ret = fwrite(data, sizeof(MY_DATATYPE), hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4], fp);
		if (ret != hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]) {
			fprintf(stderr, "\nError writing data to %s\n",hdr_file);
			exit(1);
		}

		fclose(fp);
	}


	/********** if hdr/img, close .hdr and write image data to .img */
	else {

		fclose(fp);     /* close .hdr file */

		fp = fopen(data_file,"w");
		if (fp == NULL) {
			fprintf(stderr, "\nError opening data file %s for write\n",data_file);
			exit(1);
		}
		ret = fwrite(data, sizeof(MY_DATATYPE), hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4], fp);
		if (ret != hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]) {
			fprintf(stderr, "\nError writing data to %s\n",data_file);
			exit(1);
		}

		fclose(fp);
	}

}

void MesherViewer::saveToIMA(QString fileName, VoxelGrid & grid){

	if( grid.size() == 0 ) {
		cout << "GridViewer::saveToNifti(QString fileName)::Empty grid" << endl;
		return;
	}

	if(!fileName.endsWith(".dim"))
		fileName.append(".dim");
	QString imaName = QString(fileName);
	imaName.replace(".dim", ".ima" );

	std::ofstream imaFile (imaName.toUtf8(), ios::out | ios::binary);
	if (!imaFile.is_open())
		return ;

	std::ofstream dimFile (fileName.toUtf8());
	if (!dimFile.is_open())
		return ;

	dimFile << grid.dim(0); dimFile << " " << grid.dim(1); dimFile<< " "  << grid.dim(2) << endl;

	dimFile << "-type U8" << endl;

	dimFile << "-dx "; dimFile << grid.d(0) << endl;
	dimFile << "-dy "; dimFile << grid.d(1) << endl;
	dimFile << "-dz "; dimFile << grid.d(2) << endl;

	dimFile.close();

	char buffer;

	for(unsigned int i = 0 ; i < grid.size() ; i ++){
		buffer = static_cast<char>(std::max(grid.getSubdomainIndex(grid.value(i)), 0 ) );
		imaFile.write (&buffer, 1);
	}
	imaFile.close();

}

void MesherViewer::saveGrid(QString fileName ){

	if(fileName.endsWith(".nii")){
		if( defGrid.size() > 0 )
			saveToNifti(fileName, defGrid);
		else{
			saveToNifti(fileName, voxelGrid);
		}
	} else if(fileName.endsWith(".dim")) {
		if( defGrid.size() > 0 )
			saveToIMA(fileName, defGrid);
		else{
			saveToIMA(fileName, voxelGrid);
		}
	} else {
		if( defGrid.size() > 0 )
			saveToVTK(fileName, defGrid);
		else
			saveToVTK(fileName, voxelGrid);
	}

}


void MesherViewer::saveSegmentation( QString fileName ){

	if(fileName.endsWith(".nii")){
		if( segmentation.size() > 0 )
			saveToNifti(fileName, segmentation);
	} else if(fileName.endsWith(".dim")) {
		if( segmentation.size() > 0 )
			saveToIMA(fileName, segmentation);
	} else {
		if( segmentation.size() > 0 )
			saveToVTK(fileName, segmentation);
	}

}

void MesherViewer::saveToNifti(QString fileName, VoxelGrid & grid ){

	if( grid.size() == 0 ) {
		cout << "GridViewer::saveToNifti(QString fileName)::Empty grid" << endl;
		return;
	}

	char *hdr_file, *data_file;

	if(!fileName.endsWith(".nii"))
		fileName.append(".nii");

	hdr_file = qstrdup(qPrintable(fileName));

	data_file = qstrdup(qPrintable(fileName));

	nifti_1_header hdr;
	nifti1_extender pad={0,0,0,0};
	FILE *fp;
	int ret,i;
	MY_DATATYPE *data=NULL;
	short do_nii;


	/********** make sure user specified .hdr/.img or .nii/.nii */
	if ( (strlen(hdr_file) < 4) || (strlen(data_file) < 4) ) {
		fprintf(stderr, "\nError: write files must end with .hdr/.img or .nii/.nii extension\n");
		exit(1);
	}

	if ( (!strncmp(hdr_file+(strlen(hdr_file)-4), ".hdr",4)) &&
		 (!strncmp(data_file+(strlen(data_file)-4), ".img",4)) ) {
		do_nii = 0;
	}
	else if ( (!strncmp(hdr_file+(strlen(hdr_file)-4), ".nii",4)) &&
			  (!strncmp(data_file+(strlen(data_file)-4), ".nii",4)) ) {
		do_nii = 1;
	}
	else {
		fprintf(stderr, "\nError: file(s) to be written must end with .hdr/.img or .nii/.nii extension\n");
		exit(1);
	}


	/********** fill in the minimal default header fields */
	bzero((void *)&hdr, sizeof(hdr));
	hdr.sizeof_hdr = MIN_HEADER_SIZE;
	hdr.dim[0] = 4;
	hdr.dim[1] = grid.xdim();
	hdr.dim[2] = grid.ydim();
	hdr.dim[3] = grid.zdim();
	hdr.dim[4] = 1;
	hdr.datatype = NIFTI_TYPE_UINT8;
	hdr.bitpix = 8;
	hdr.pixdim[1] = grid.dx();
	hdr.pixdim[2] = grid.dy();
	hdr.pixdim[3] = grid.dz();
	hdr.pixdim[4] = 1.;
	hdr.vox_offset = (float)0;
	if (do_nii)
		strncpy(hdr.magic, "n+1\0", 4);
	else
		strncpy(hdr.magic, "ni1\0", 4);


	/********** allocate buffer and fill with dummy data  */
	data = new MY_DATATYPE [hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]];
	if (data == NULL) {
		fprintf(stderr, "\nError allocating data buffer for %s\n",data_file);
		exit(1);
	}

	for (i=0; i<hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]; i++){
		data[i] = MY_DATATYPE(std::max(grid.originalValue(i), (GRID_TYPE)0));
	}

	/********** write first 348 bytes of header   */
	fp = fopen(hdr_file,"w");
	if (fp == NULL) {
		fprintf(stderr, "\nError opening header file %s for write\n",hdr_file);
		exit(1);
	}
	ret = fwrite(&hdr, MIN_HEADER_SIZE, 1, fp);
	if (ret != 1) {
		fprintf(stderr, "\nError writing header file %s\n",hdr_file);
		exit(1);
	}


	/********** if nii, write extender pad and image data   */
	if (do_nii == 1) {

		ret = fwrite(&pad, 4, 1, fp);
		if (ret != 1) {
			fprintf(stderr, "\nError writing header file extension pad %s\n",hdr_file);
			exit(1);
		}

		ret = fwrite(data, sizeof(MY_DATATYPE), hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4], fp);
		if (ret != hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]) {
			fprintf(stderr, "\nError writing data to %s\n",hdr_file);
			exit(1);
		}

		fclose(fp);
	}


	/********** if hdr/img, close .hdr and write image data to .img */
	else {

		fclose(fp);     /* close .hdr file */

		fp = fopen(data_file,"w");
		if (fp == NULL) {
			fprintf(stderr, "\nError opening data file %s for write\n",data_file);
			exit(1);
		}
		ret = fwrite(data, sizeof(MY_DATATYPE), hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4], fp);
		if (ret != hdr.dim[1]*hdr.dim[2]*hdr.dim[3]*hdr.dim[4]) {
			fprintf(stderr, "\nError writing data to %s\n",data_file);
			exit(1);
		}

		fclose(fp);
	}

}

void MesherViewer::saveToVTK(const QString &fileName, VoxelGrid & grid){
	std::vector<float> d_field;

	grid.getDistanceFiels(d_field);

	export_vtk(fileName.toStdString(), std::string("DISTANCE_FIELD"), std::string("FLOAT"), 1, d_field, grid);

}

void MesherViewer::initializeImage(){

	float x = voxelGrid.d(0), y = voxelGrid.d(1), z = voxelGrid.d(2);
	if( x < 1. ) x *= 10.;
	if( y < 1. ) y *= 10.;
	if( z < 1. ) z *= 10.;

	std::cout << "Processing image " << voxelGrid.dim(0) << ", " << voxelGrid.dim(1) << ", " << voxelGrid.dim(2) << " - "<< x << ", "<< y << ", " << z << std::endl;
	/*

	int size = nx*ny*nz;

	unsigned char * data = new unsigned char[size];

	for(int i = 0; i < size;i++)
		data[i] = (unsigned char)_data[i];

	ofstream out ("data.raw");
	if (!out)
		exit (EXIT_FAILURE);
	out << endl;
	out.close ();

	image.read_raw("data.raw", nx, ny , nz, dx , dy, dz);

	if( remove( "data.raw" ) != 0 )
		perror( "Error deleting file" );

	image.set_data(data);
*/

	bool onlyDisplay = false;
	if(onlyDisplay){
		voxelGrid.buildForDisplay();
		//  voxelGrid.getSegmentation(segmentation);
	}else {
		voxelGrid.sortAndInit(voxelGrid.data());;
		voxelGrid.getSegmentation(segmentation);
	}
	//    std::vector<Subdomain_index> subdomain_indices;
	//    segmentation.getSubdomainIndices(subdomain_indices);
	//    computeRandomColors(subdomain_indices, sColorMap);
	//    sDisplayMap.clear();
	//    for(unsigned int i = 0 ; subdomain_indices.size() ; i ++){
	//        sDisplayMap[subdomain_indices[i]] = true;
	//    }
	BasicPoint bbm = voxelGrid.getWorldCoordinate(voxelGrid.xdim(),voxelGrid.ydim(), voxelGrid.zdim());
	updateCamera( bbm*0.5, bbm.norm()*0.5);

	BasicPoint bb(-100.,-100.,-100.);
	updateCuttingPlanes(bb,bbm);
	cout << "Grid " << voxelGrid.getSubdomainNumber() << " labels, processed" << endl;

	updateImageDisplay();

}


void MesherViewer::offsetSelection(){
	/*
	QDialog * dialog = new QDialog(this);
	dialog->setWindowTitle(tr("Label separation"));

	QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);

	QGroupBox * labelsGroupBox = new QGroupBox("Label separation");
	dilogLayout->addWidget(labelsGroupBox);

	QVBoxLayout * labelsVLayout = new QVBoxLayout(labelsGroupBox);

	QComboBox * l1ComboxBox = new QComboBox();
	QComboBox * l2ComboxBox = new QComboBox();
	for( unsigned int i = 0 ; i < indexToLabel.size() ; i++ ){
		l1ComboxBox->addItem(QString::number(indexToLabel[i]));
		l2ComboxBox->addItem(QString::number(indexToLabel[i]));
	}

	labelsVLayout->addWidget(l1ComboxBox);
	labelsVLayout->addWidget(l2ComboxBox);

	QSpinBox * regionOffsetNb = new QSpinBox();
	regionOffsetNb->setRange(-30, 30);
	dilogLayout->addWidget(new QLabel("OffsetNb"));
	dilogLayout->addWidget(regionOffsetNb);

	QDialogButtonBox * buttonBox = new QDialogButtonBox();
	buttonBox->setOrientation(Qt::Horizontal);
	buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

	QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
	QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));

	dilogLayout->addWidget(buttonBox);
	int i = dialog->exec();

	if(i == QDialog::Rejected || l1ComboxBox->count() == 0 || l2ComboxBox->count() == 0)
		return;

	int l1 = indexToLabel[l1ComboxBox->currentIndex()];
	int l2 = indexToLabel[l2ComboxBox->currentIndex()];
	if(l1!=l2)
		viewer->separateLabels( l1,l2);
*/
}


/*
void MesherViewer::sortVoxels(){

	//Voxel::setDim(image.xdim(), image.ydim(), image.zdim());

	//Voxel::dx = image.vx();
	//Voxel::dy = image.vy();
	//Voxel::dz = image.vz();


	subdomain_indices.clear();
	displayMap.clear();
	for(map<Subdomain_index, vector<int> >::iterator it = sortedVoxels.begin() ; it != sortedVoxels.end() ; it++){
		subdomain_indices.push_back(it->first);
		displayMap[it->first] = true;
	}

	computeRandomColors();
}
*/
void MesherViewer::saveCGALEnvelop(const QString & fileName){

	// if( mode == MESH_DEFORMATION)
	saveOFF(fileName, mesh.getVertices(), mesh.getTriangles(), mesh.getCGAL_envelop());

	if( useTransferMesh ){
		std::vector<BasicPoint> vertices;
		std::vector<Triangle> triangles;
		tetMeshCreator.getEnvelope(vertices, triangles);
		FileIO::saveOFF(fileName.toStdString(), vertices, triangles );
	}
}

void MesherViewer::generateSurface(float angle, float size, float approximation){
	Image image;
	if(segmentation.size() > 0)
		segmentation.getBinaryImage(image);
	else
		voxelGrid.getBinaryImage(image);

	//surfaceCreator = //surfaceCreator( image, 1, angle, size, approximation );
}

void MesherViewer::saveSurface(const QString & fileName){

	std::vector<BasicPoint> vertices;
	std::vector<Triangle> triangles;

	//surfaceCreator.getMesh( vertices, triangles );

	if( vertices.size() > 0 )
		FileIO::saveOFF(fileName.toStdString(), vertices, triangles);

}

void MesherViewer::useSurfaceAsCage(){
	std::vector<BasicPoint> vertices;
	std::vector<Triangle> triangles;

	//surfaceCreator.getMesh( vertices, triangles );

	if( vertices.size() > 0 ){

		tetMeshCreator.clear();

		cageInterface.clear();
		cageInterface.setMode(REALTIME);
		cageInterface.setMethod(GREEN);
		cageInterface.loadCage (vertices, triangles);
		cageInitialized = false;
		envelopCage = false;
		cageLoaded = true;
		displayCage = true;
		manipulator->clear();
		manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
		saveCurrentCageState();

		double average = cageInterface.getAverage_edge_halfsize();
		sphereScale = scene_radius*0.01 /average;
		if(sphereScale > 1.) sphereScale = 1.;

		tetMeshCreator = TetMeshCreator(&cageInterface, &voxelGrid);
	}
}

void MesherViewer::useSurfaceAsMesh(){

	mesh.clear();

	//surfaceCreator.getMesh( mesh.getVertices(), mesh.getTriangles() );

	mesh.getTriangles_subdomain_ids().resize(mesh.getTriangles().size(), 1);

	mesh.updateModel();

	const vector<Subdomain_index> & subdomain_indices = mesh.getSubdomainIndices();

	displayMap.clear();
	for(unsigned int i = 0 ; i < subdomain_indices.size() ; i++)
		displayMap[subdomain_indices[i]] = true;
	computeRandomColors(subdomain_indices, colorMap);

	emit setMeshedLabels();
}

void MesherViewer::useTransferAsMesh(){

	tetMeshCreator.initializeCage();

	tetMeshCreator.updateVertices( mesh.getVertices() );
	tetMeshCreator.updateVertices( visu_mesh.getVertices() );
	mesh.updateModel();
	visu_mesh.computeTexturesData();



	cageInitialized = true;

	update();

}


void MesherViewer::changeLabelValue( unsigned int from, unsigned int to ){

	voxelGrid.changeLabelValue( from, to);
	update();
}

void MesherViewer::separateLabels(unsigned int l1, unsigned int l2 ){
	if(useTransferMesh){
		tetMeshCreator.separateLabels(std::make_pair(l1,l2));
		cageInitialized = false;
		mesh.clear();

		tetMeshCreator.getMesh( mesh.getVertices(), mesh.getTriangles(), mesh.getTriangles_subdomain_ids(), mesh.getTetrahedra(),  mesh.getTetrahedra_subdomain_ids() );

		useTransferMesh = true;

		mesh.updateModel();

	} else
		mesh.separateLabels(l1,l2);
}


void MesherViewer::saveMeshes(const QString & folderName){

	std::cout << "save triangles "<< std::endl;

	QDir currentDir(folderName);

	const std::vector<BasicPoint> & vertices = mesh.getVertices();
	const std::vector<Triangle> & triangles = mesh.getTriangles();

	std::map<Subdomain_index, vector<int> > & sortedTriangles = mesh.getSortedTriangles();

	std::cout << "save triangles "<< sortedTriangles.size() << std::endl;
	for(std::map<Subdomain_index, vector<int> >::iterator it = sortedTriangles.begin() ; it != sortedTriangles.end() ; it++) {
		const Subdomain_index & subdomain_index = it->first;
		QString fileName = QString(folderName);
		if(!fileName.endsWith("/")) fileName.append("/");
		fileName.append(currentDir.dirName()); fileName.append("_");
		fileName.append(QString::number(subdomain_index));
		fileName.append(".off");

		saveOFF(fileName, vertices, triangles, it->second);

	}

}

void MesherViewer::saveOFF(const QString &fileName, const std::vector<BasicPoint> & vertices, const std::vector<Triangle> & triangles, const std::vector<int> & indices){

	if( vertices.size() == 0 || indices.size() == 0) return;

	ofstream out (fileName.toUtf8());
	if (!out)
		exit (EXIT_FAILURE);

	out << "OFF " << endl;



	vector< bool > used_vertices(vertices.size(), false);

	int count = 0;
	for(unsigned int i = 0 ; i < indices.size() ; i++){
		const Triangle & t = triangles[indices[i]];

		for( int v = 0 ; v < 3 ; v ++ ){
			int vi = t.getVertex(v);

			if(!used_vertices[vi]){
				used_vertices[vi] = true;
				count++;
			}
		}

	}

	out << count << " " << indices.size() << " " << 0 << endl;

	count = 0;
	map<int, int> VMap;
	for(unsigned int v = 0; v < vertices.size(); v++){
		if(used_vertices[v]){
			const BasicPoint & point = vertices[v];
			out << point[0] << " " << point[1] << " " << point[2] << endl;
			VMap[v] = count++;
		}
	}


	for(unsigned int tId = 0; tId < indices.size(); tId++){

		out << 3 ;
		for(int i = 0 ; i < 3 ; i++ )
			out << " " << VMap[triangles[indices[tId]].getVertex(i)];
		out << endl;
	}

	out << endl;
	out.close ();
}


/*
void MesherViewer::sortTetrahedraBySubdomainIndex(map<Subdomain_index, vector<Tetrahedron> > & sortedTetrahedra ){

	if( tetrahedra.size() == 0 ) return;

	sortedTetrahedra.clear();
	for(unsigned int t = 0 ; t < tetrahedra.size() ; t++){
		sortedTetrahedra[ tetrahedra_subdomain_ids[t] ] = tetrahedra[t];
	}

}
*/
void MesherViewer::saveVRML(const QString &fileName){
	ofstream out (fileName.toUtf8());
	if (!out)
		exit (EXIT_FAILURE);

	out << "#VRML V2.0 utf8 " << std::endl;
	out << "Shape {" << std::endl;

	out << "geometry IndexedFaceSet {" << std::endl;
	out << "solid TRUE" << std::endl;

	out << "coord DEF coord_bla Coordinate { " << std::endl;
	out << "point [" << std::endl;
	const std::vector<BasicPoint> & vertices = mesh.getVertices();
	for(unsigned int i = 0 ; i < vertices.size(); i++){
		const BasicPoint & p = vertices[i];
		out << p[0] << " " << p[1] << " " << p[2] << " -1" << endl;
	}

	out << "] " << std::endl;
	out << "} " << std::endl;
	out << "coordIndex [ " << std::endl;

	const std::vector<Triangle> & triangles = mesh.getTriangles();

	for(unsigned int i = 0 ; i < triangles.size() ; i ++){
		for( int v = 0; v < 3 ; v++ )
			out <<  triangles[i].getVertex(v) << " ";
		out <<"-1, " << endl;
	}

	out << "] " << std::endl;
	out << "} " << std::endl;
	out << "} " << std::endl;
	out << endl;
	out.close ();
}
void MesherViewer::saveMESH(const QString &fileName){

	const std::vector<BasicPoint> & vertices = mesh.getVertices();
	//
	//    BasicPoint center;
	//    double scale;
	//    std::vector<BasicPoint> & vertices = mesh.getVertices();
	//    MeshTools::scaleAndCenterToUnitBox(vertices, center, scale);

	const std::vector<Triangle> & triangles = mesh.getTriangles();
	const std::vector<Tetrahedron> & tetrahedra = mesh.getTetrahedra();

	const std::vector<Subdomain_index> & triangles_subdomain_ids = mesh.getTriangles_subdomain_ids();
	const std::vector<Subdomain_index> & tetrahedra_subdomain_ids = mesh.getTetrahedra_subdomain_ids();


	vector<int> dimension (vertices.size(), -1);

	for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++){
		for( int v = 0; v < 4 ; v++ )
			dimension[tetrahedra[i].getVertex(v)] = 3;
	}

	for(unsigned int i = 0 ; i < triangles.size() ; i ++){
		for( int v = 0; v < 3 ; v++ )
			dimension[triangles[i].getVertex(v)] = 2;
	}

	ofstream out (fileName.toUtf8());
	if (!out)
		exit (EXIT_FAILURE);

	int dim = 3;
	if( tetrahedra.size()== 0 )
		dim = 2;

	out << "MeshVersionFormatted " << 1 << endl;
	out << "Dimension " << dim << endl;
	out << "Vertices\n";
	out << vertices.size() << endl;

	for(unsigned int i = 0 ; i < vertices.size(); i++){
		const BasicPoint & p = vertices[i];
		out << p[0] << " " << p[1] << " " << p[2] << " " << dimension[i] << endl;
	}

	out << "Triangles\n";
	out << triangles.size() << endl;

	for(unsigned int i = 0 ; i < triangles.size() ; i ++){
		for( int v = 0; v < 3 ; v++ )
			out <<  triangles[i].getVertex(v)+1 << " ";
		out << triangles_subdomain_ids[i] << endl;
	}

	out << "Tetrahedra\n";
	out << tetrahedra.size() << endl;
	for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++){
		for( int v = 0; v < 4 ; v++ )
			out <<  tetrahedra[i].getVertex(v)+1 << " ";
		out << tetrahedra_subdomain_ids[i] << endl;
	}
	out << endl;
	out.close ();

}

void MesherViewer::selectAll(){
	for(std::map<Subdomain_index, bool>::iterator it = displayMap.begin() ; it != displayMap.end(); ++it )
		displayMap[it->first] = true;
	update();
}

void MesherViewer::discardAll(){
	for(std::map<Subdomain_index, bool>::iterator it = displayMap.begin() ; it != displayMap.end(); ++it )
		displayMap[it->first] = false;
	update();
}

void MesherViewer::setVisibility(unsigned int i, bool visibility){
	if(displayMap.find(i) != displayMap.end())
		displayMap[i] = visibility;
	update();
}


void MesherViewer::selectIAll(){
	for(std::map<Subdomain_index, bool>::iterator it = iDisplayMap.begin() ; it != iDisplayMap.end(); ++it ){
		textDisplayMap[static_cast<unsigned char>(text_subdomain_indices[it->first+1])] = true;
		visu_mesh.buildVisibilityTexture(textDisplayMap);
	}
	update();
}

void MesherViewer::discardIAll(){
	for(std::map<Subdomain_index, bool>::iterator it = iDisplayMap.begin() ; it != iDisplayMap.end(); ++it ){
		iDisplayMap[it->first] = false;
		textDisplayMap[static_cast<unsigned char>(text_subdomain_indices[it->first+1])] = false;
		visu_mesh.buildVisibilityTexture(textDisplayMap);
	}
	update();
}

void MesherViewer::setIVisibility(unsigned int i, bool visibility){
	std::cout << "visibility " << i << std::endl;
	if(iDisplayMap.find(i) != iDisplayMap.end() && text_subdomain_indices.size() > 0 ){
		textDisplayMap[static_cast<unsigned char>(text_subdomain_indices[i+1])] = visibility;
		visu_mesh.buildVisibilityTexture(textDisplayMap);
		iDisplayMap[i] = visibility;
	}
	update();
}


void MesherViewer::selectSAll(){
	for(std::map<Subdomain_index, bool>::iterator it = sDisplayMap.begin() ; it != sDisplayMap.end(); ++it )
		sDisplayMap[it->first] = true;
	update();
}

void MesherViewer::discardSAll(){
	for(std::map<Subdomain_index, bool>::iterator it = sDisplayMap.begin() ; it != sDisplayMap.end(); ++it )
		sDisplayMap[it->first] = false;
	update();
}

void MesherViewer::setSVisibility(unsigned int i, bool visibility){
	if(sDisplayMap.find(i) != sDisplayMap.end())
		sDisplayMap[i] = visibility;
	update();
}



void MesherViewer::mesh3DImage(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize){

	cout << "MesherViewer::mesh3DImage( " << facetAngle << ", " <<  facetSize << ", " <<  facetApproximation << ", " <<  cellRatio << ", " <<  cellSize << " ) " << endl;

	clear();

	// Domain
	Image image;
	voxelGrid.getGridImage(image);
	Mesh_domain domain(image);

	// Mesh criteria
	Mesh_criteria criteria (facet_angle=facetAngle, facet_size=facetSize, facet_distance=facetApproximation,
							cell_radius_edge_ratio=cellRatio, cell_size=cellSize );

	// Meshing
	mesh_3 = CGAL::make_mesh_3<C3t3>(domain, criteria, no_exude(), no_perturb());
	mesh = Mesh(mesh_3);

	mesh.updateModel();

	const vector<Subdomain_index> & subdomain_indices = mesh.getSubdomainIndices();

	displayMap.clear();
	for(unsigned int i = 0 ; i < subdomain_indices.size() ; i++)
		displayMap[subdomain_indices[i]] = true;
	computeRandomColors(subdomain_indices, colorMap);

	emit setMeshedLabels();

}

void MesherViewer::meshTransfer(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize, bool fromMesh, bool optimize){

	if(fromMesh){
		tetMeshCreator.transferMeshFromPolyhedron(mesh.getVertices(), mesh.getTriangles(), facetAngle, facetSize, facetApproximation, cellRatio, cellSize );
	} else {

		Image image;
		if( segmentation.size() > 0 )
			segmentation.getGridImage(image);
		else
			voxelGrid.getBinaryImage(image);

		tetMeshCreator.transferMeshFromImg(image,facetAngle, facetSize, facetApproximation, cellRatio, cellSize, optimize );

		mesh.clear();

		tetMeshCreator.getMesh( mesh.getVertices(), mesh.getTriangles(), mesh.getTriangles_subdomain_ids(), mesh.getTetrahedra(),  mesh.getTetrahedra_subdomain_ids() );

		mesh.updateModel();

		visu_mesh = VisuMesh(&texture);
		std::vector<BasicPoint> & vertices = visu_mesh.getVertices();
		std::vector<Tetrahedron> & tetrahedra = visu_mesh.getTetrahedra();
		std::vector<BasicPoint> & textureCoords = visu_mesh.getTextureCoords();

		tetMeshCreator.getMesh( vertices, tetrahedra );

		for (unsigned int i = 0; i < vertices.size(); i++) {
			const BasicPoint &position = vertices[i];
			textureCoords.push_back( BasicPoint(std::min(1.f,position[0]/texture.getXMax()), std::min(1.f,position[1]/texture.getYMax()), std::min(1.f,position[2]/texture.getZMax())) );
		}
		visu_mesh.updateVisuMesh();
		visu_mesh.buildVisibilityTexture(textDisplayMap);

		const vector<Subdomain_index> & subdomain_indices = mesh.getSubdomainIndices();

		displayMap.clear();
		for(unsigned int i = 0 ; i < subdomain_indices.size() ; i++)
			displayMap[subdomain_indices[i]] = true;
		computeRandomColors(subdomain_indices, colorMap);

		emit setMeshedLabels();

		updateCuttingPlanes(mesh.getBBMin(), mesh.getBBMax());
	}

	error_count = 0;
	update();
}

void MesherViewer::meshTransferAdativeSize(double sizeMul, double percentage, bool optimize){


	Image image;
	if( segmentation.size() > 0 )
		segmentation.getGridImage(image);
	else
		voxelGrid.getBinaryImage(image);

	tetMeshCreator.transferMeshFromImgAdaptativeSize(image,sizeMul, percentage, optimize );

	mesh.clear();
	tetMeshCreator.getMesh( mesh.getVertices(), mesh.getTriangles(), mesh.getTriangles_subdomain_ids(), mesh.getTetrahedra(), mesh.getTetrahedra_subdomain_ids());

	mesh.updateModel();
	tetMeshCreator.getMesh( mesh.getVertices(), mesh.getTriangles(), mesh.getTriangles_subdomain_ids(), mesh.getTetrahedra(), mesh.getTetrahedra_subdomain_ids());

	visu_mesh = VisuMesh(&texture);
	std::vector<BasicPoint> & vertices = visu_mesh.getVertices();
	std::vector<Tetrahedron> & tetrahedra = visu_mesh.getTetrahedra();
	std::vector<BasicPoint> & textureCoords = visu_mesh.getTextureCoords();

	tetMeshCreator.getMesh( vertices, tetrahedra );
	std::vector<BasicPoint> & vertices = visu_mesh.getVertices();
	std::vector<Tetrahedron> & tetrahedra = visu_mesh.getTetrahedra();
	std::vector<BasicPoint> & textureCoords = visu_mesh.getTextureCoords();

	for (unsigned int i = 0; i < vertices.size(); i++) {
		const BasicPoint &position = vertices[i];
		textureCoords.push_back( BasicPoint(std::min(1.f,position[0]/texture.getXMax()), std::min(1.f,position[1]/texture.getYMax()), std::min(1.f,position[2]/texture.getZMax())) );
	}
	visu_mesh.updateVisuMesh();
	visu_mesh.buildVisibilityTexture(textDisplayMap);

	/*
	error_count++;
	std::cout << "Saving error and sizing field to vtk" << std::endl << std::endl;
	export_error_vtk(QString("%1_error.vtk").arg(error_count).toStdString());
	export_sizing_field_vtk(QString("%1_size.vtk").arg(error_count).toStdString());

	std::cout << "Saving error and sizing field to vtk" << std::endl << std::endl;
	*/
	update();
}

void MesherViewer::rescaleSurface(){

	std::vector<BasicPoint> & vertices = mesh.getVertices();

	const BasicPoint & bbmin = mesh.getBBMin();

	if( bbmin[0] < 0. || bbmin[1] < 0. || bbmin[2] < 0.){

	}
	for( unsigned int i = 0 ; i < vertices.size() ; i++ ){
		BasicPoint & v = vertices[i] ;
		for( int j = 0 ;  j < 3 ; j++ ){
			v[j] = ( (v[j] +1.) /2. )*1000.;
		}
	}
}

void MesherViewer::generateCageTetMesh(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize){

	cout << "MesherViewer::generateCagetetMesh( " << facetAngle << ", " <<  facetSize << ", " <<  facetApproximation << ", " <<  cellRatio << ", " <<  cellSize << " ) " << endl;

	tetMeshCreator.createFromPolygon(facetAngle, facetSize, facetApproximation, cellRatio, cellSize);

	//clear();


	//    mesh = Mesh(mesh_3);
	//
	//    mesh.update();

}


void MesherViewer::perturb(double time_limit, double sliver_bound){

	mesh.clear();

	Image image;
	voxelGrid.getGridImage(image);
	Mesh_domain domain(image);

	CGAL::perturb_mesh_3(mesh_3, domain, time_limit, sliver_bound);

	mesh = Mesh(mesh_3);
	mesh.updateModel();

	update();
}

void MesherViewer::lloyd_optimize_mesh_3(double time_limit, double max_iteration_number, double convergence, double freeze_bound){

	mesh.clear();

	Image image;
	voxelGrid.getGridImage(image);
	Mesh_domain domain(image);

	CGAL::lloyd_optimize_mesh_3(mesh_3, domain, time_limit, max_iteration_number, convergence, freeze_bound);

	mesh = Mesh(mesh_3);
	mesh.updateModel();

	update();
}

void MesherViewer::odt_optimize_mesh_3(double time_limit, double max_iteration_number, double convergence, double freeze_bound){

	mesh.clear();

	Image image;
	voxelGrid.getGridImage(image);
	Mesh_domain domain(image);

	CGAL::odt_optimize_mesh_3(mesh_3, domain);//, time_limit, max_iteration_number, convergence, freeze_bound);

	mesh = Mesh(mesh_3);
	mesh.updateModel();

	update();
}

void MesherViewer::exude(double sliver_bound, double time_limit){

	mesh.clear();

	CGAL::exude_mesh_3(mesh_3, sliver_bound, time_limit);

	mesh = Mesh(mesh_3);
	mesh.updateModel();

	update();
}

void MesherViewer::create_histogram(std::vector<int> & histo, double& min_value, double& max_value)
{

	histo.clear();
	histo.resize(181,0);

	min_value = 180.;
	max_value = 0.;

	for (C3t3::Cell_iterator cit = mesh_3.cells_begin() ; cit != mesh_3.cells_end() ; ++cit)
	{
		if( !mesh_3.is_in_complex(cit))
			continue;

		const Point_3& p0 = cit->vertex(0)->point();
		const Point_3& p1 = cit->vertex(1)->point();
		const Point_3& p2 = cit->vertex(2)->point();
		const Point_3& p3 = cit->vertex(3)->point();

		double a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0,p1,p2,p3)));
		histo[static_cast<int>(std::floor(a))] += 1;
		min_value = (std::min)(min_value, a);
		max_value = (std::max)(max_value, a);

		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p2, p1, p3)));
		histo[static_cast<int>(std::floor(a))] += 1;
		min_value = (std::min)(min_value, a);
		max_value = (std::max)(max_value, a);

		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p0, p3, p1, p2)));
		histo[static_cast<int>(std::floor(a))] += 1;
		min_value = (std::min)(min_value, a);
		max_value = (std::max)(max_value, a);

		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p2, p0, p3)));
		histo[static_cast<int>(std::floor(a))] += 1;
		min_value = (std::min)(min_value, a);
		max_value = (std::max)(max_value, a);

		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p1, p3, p0, p2)));
		histo[static_cast<int>(std::floor(a))] += 1;
		min_value = (std::min)(min_value, a);
		max_value = (std::max)(max_value, a);

		a = CGAL::to_double(CGAL::abs(CGAL::Mesh_3::dihedral_angle(p2, p3, p0, p1)));
		histo[static_cast<int>(std::floor(a))] += 1;
		min_value = (std::min)(min_value, a);
		max_value = (std::max)(max_value, a);

	}

}

QPixmap MesherViewer::getQualityHistogram(){
	Histogram histogram;

	double min_value, max_value;
	std::vector<int> histo;

	create_histogram(histo, min_value, max_value);

	histogram.build_histogram(histo, min_value, max_value);

	return histogram.getPixMap();
}

void MesherViewer::computeHistograms(QPixmap & distortionPixMap, QPixmap & elongationPixMap){
	Histogram distHistogram, elongationHistogram;

	std::vector< std::pair<Voxel, GRID_TYPE > > outside;


	tetMeshCreator.updatePositions(outside);


	float min_value, max_value;
	std::vector<int> histo;

	voxelGrid.computeElongation(voxelGrid.getPositions(),  histo, min_value, max_value);
	elongationHistogram.build_histogram(histo, min_value, max_value);;

	elongationPixMap = elongationHistogram.getPixMap();

	voxelGrid.computeDistortion(voxelGrid.getPositions(),  histo, min_value, max_value);
	distHistogram.build_histogram(histo, min_value, max_value);;
	distortionPixMap = distHistogram.getPixMap();

	//    if(useTransferMesh){
	//        for( unsigned int i = 0 ; i < outside.size(); i ++ ){
	//            std::pair<Voxel, GRID_TYPE > & idValue = outside[i];
	//            voxelGrid.setValue(voxelGrid.getGridIndex(idValue.first), idValue.second);
	//
	//        }
	//    }
}

void MesherViewer::export_distortion_vtk(const std::string & filename){

	std::vector< std::pair<Voxel, GRID_TYPE > > outside;
	if(useTransferMesh){

		tetMeshCreator.updatePositions(outside);

	}

	std::vector<float> distortion;

	voxelGrid.computeDistortion(voxelGrid.getPositions(), distortion);

	for(unsigned int i = 0 ; i < distortion.size(); i++){
		if(voxelGrid.value(i) > 0)
			distortion[i] = fabs(distortion[i] - 90.);
	}
	export_vtk(filename, std::string("Distortion"), std::string("FLOAT"), 1, distortion, voxelGrid);

	if(useTransferMesh){
		for( unsigned int i = 0 ; i < outside.size(); i ++ ){
			std::pair<Voxel, GRID_TYPE > & idValue = outside[i];
			voxelGrid.setValue(voxelGrid.getGridIndex(idValue.first), idValue.second);

		}
	}

}

void MesherViewer::export_sizing_field_vtk(const std::string &filename){
	const std::vector<float> & lobj = tetMeshCreator.getSizingField();

	std::vector<float> values(lobj);

	for(unsigned int i = 0 ; i < lobj.size() ; i++){
		values[i] = 1./lobj[i];
	}

	export_vtk(filename,std::string("Sizing_field"), std::string("FLOAT"), 1, values, voxelGrid);


}

void MesherViewer::export_error_vtk(const std::string &filename){
	std::vector<float> & value = tetMeshCreator.getError();

	export_vtk(filename,std::string("Error"), std::string("FLOAT"), 1, value, voxelGrid);

}

QPixmap MesherViewer::getVolumeHistogram(){

	std::vector<QColor> colors;

	for(std::map<Subdomain_index, QColor>::iterator it = iColorMap.begin() ; it != iColorMap.end() ; ++it)
		colors.push_back(it->second);
	Histogram histogram(colors);

	std::vector<float> histo;

	voxelGrid.compareVolumeLossWithGrid(defGrid, histo);

	histogram.build_histogram(histo);

	return histogram.getPixMap();
}


void MesherViewer::updateInternalData(){

	mesh.updateModel();

	const vector<Subdomain_index> & subdomain_indices = mesh.getSubdomainIndices();

	displayMap.clear();
	for(unsigned int i = 0 ; i < subdomain_indices.size() ; i++)
		displayMap[subdomain_indices[i]] = true;
	computeRandomColors(subdomain_indices, colorMap);

	updateCuttingPlanes(mesh.getBBMin(), mesh.getBBMax());

	updateCamera((mesh.getBBMax()- mesh.getBBMin())*0.5, (mesh.getBBMax()- mesh.getBBMin()).norm()*0.5);

	emit setMeshedLabels();

	update();

}

void MesherViewer::updateImageDisplay(){

	std::cout << "Update image display" << std::endl;

	std::vector<Subdomain_index> subdomain_indices;
	voxelGrid.getSubdomainIndices(subdomain_indices);
	std::vector<QColor> & cols = voxelGrid.getColors();
	iDisplayMap.clear();
	iColorMap.clear();

	for( unsigned int i = 0 ; i < cols.size() ; i ++ ){
		iDisplayMap[i] = true;
		iColorMap[i] = cols[i];
	}


	//  updateCuttingPlanes(voxelGrid.getBBMin(), voxelGrid.getBBMax());


	std::cout << "Update image display done" << std::endl;
	emit setImageLabels();
	// emit setSegmentationLabels();
}

void MesherViewer::updateCuttingPlanes(BasicPoint bb, BasicPoint BB){
	for(int i = 0 ; i < 3 ; i ++){
		BBMin[i] = bb[i]; //std::min(bb[i], BBMin[i]);
		BBMax[i] = BB[i]; //std::max(BB[i], BBMax[i]);
	}

	float r = (BBMax- BBMin).norm();
	BBMax += BasicPoint(r*0.01, r*0.01, r*0.01);
	BBMin -= BasicPoint(r*0.01, r*0.01, r*0.01);

	BasicPoint diff = BBMax - BBMin;
	float max = diff[0];
	max = std::max(max , diff[1]);
	max = std::max(max , diff[2]);

	if(voxelGrid.size() > 0){
		step = BasicPoint(1., 1., 1.);
		emit setMaxCutPlanes(int(diff[0]*step[0]), int(diff[1]*step[1]), int(step[2])* diff[2]);
	}else{
		step = BasicPoint( diff[0]/int(1000.*diff[0]/max), diff[1]/int(1000.*diff[1]/max), diff[2]/int(1000.*diff[2]/max) );
		emit setMaxCutPlanes(int(1000*diff[0]/max), int(1000*diff[1]/max), int(1000*diff[2]/max));
	}

	cut = BBMin;
	cutDirection = BasicPoint(1.,1.,1.);
}

void MesherViewer::updateCamera(const BasicPoint & center, float radius){

	scene_radius = radius;
	camera()->setSceneCenter(Vec(center[0], center[1], center[2]));
	camera()->setSceneRadius(radius*2.);
	manipulator->setDisplayScale(manipulatorScale*radius/3.);

	camera()->showEntireScene();
}

void MesherViewer::computeRandomColors(const std::vector<int> & subdomain_indices, std::map<Subdomain_index, QColor> & cMap){
	cMap.clear();

	if( subdomain_indices.size() == 1 ){
		cMap[subdomain_indices[0]] = QColor(255, 255, 255);
	}
	else {
		std::vector< pair<int, int> > sorted_ids;
		int off = 0;
		for( unsigned int i = 0 ; i < subdomain_indices.size() ; i ++ ){
			if(subdomain_indices[i] == 0) off = 1;
			sorted_ids.push_back(std::make_pair(subdomain_indices[i], i));
		}

		std::sort( sorted_ids.begin(), sorted_ids.end() );


		for( unsigned int i = 0 ; i < sorted_ids.size() ; i ++ ){
			int si = sorted_ids[i].first;
			QColor col(0,0,0);
			if( si > 0 ){
				if(useRandomColor){
					float r, g , b;
					RGB::get_random_RGB_from_HSV(r,g,b);

					col.setRgbF(r, g, b);
				} else {
					//double h = (280 - int(360*double(i-off)/(sorted_ids.size()-off)));
					//if( h <= 0. ) h = 360.;


					col.setHsvF(0.98*double(i-off)/(sorted_ids.size()-off), 0.6,1.);
					if( fileColors.size() > 0 )
						col = fileColors[si];
				}
			}
			cMap[si] = col;
		}
	}
}

void MesherViewer::openColors(const QString & fileName){

	std::ifstream colorStream (fileName.toUtf8());
	if (!colorStream.is_open())
		return;


	int si;
	float r,g,b;
	std::string dummy;
	std::cout <<  "Colors" << std::endl;

	fileColors.clear();
	while ( colorStream.good() )
	{
		colorStream >> si;
		if( colorStream.good() ){
			colorStream >> r; colorStream >> g; colorStream >> b;
			colorStream >> dummy;
			std::cout <<  si << " "<< r  << " "<< g  << " "<< b  << std::endl;
			if(r < 1.001 && g < 1.001 && b < 1.001)
				fileColors[si] = QColor(r*255., g*255.,b*255.);
			else fileColors[si] = QColor(r, g, b);
			subdomain_indices.push_back(si);
		}
	}
	colorStream.close();

}

void MesherViewer::setIndexDependantColors(bool indexdependant){

	iColorMap.clear();
	std::vector<Subdomain_index> subdomain_indices;

	voxelGrid.getSubdomainIndices(subdomain_indices);

	if(indexdependant){
		std::vector<QColor> & cols = voxelGrid.getColors();
		std::vector<QColor> & dcols = defGrid.getColors();

		int max_id = -INT_MAX;
		int min_id = INT_MAX;
		for( unsigned int i = 0 ; i < subdomain_indices.size() ; i ++ ){
			max_id = std::max(subdomain_indices[i], max_id);
			min_id = std::min(subdomain_indices[i], min_id);
		}


		for( unsigned int i = 0 ; i < subdomain_indices.size() ; i ++ ){


			float s = float(subdomain_indices[i]-min_id) / float(max_id - min_id);

			QColor col;
			col.setHsvF((250./360)*s, 0.8,1.);
			iColorMap[i] = col;
			cols[i] = col;
			dcols[i] = col;
		}


	} else {
		computeRandomColors(subdomain_indices, iColorMap);
	}

	update();
}
void MesherViewer::initializeCage(){

	std::cout << "initialize cage" << std::endl;
	if(mode == MESH_DEFORMATION){
		if(useTransferMesh)
			tetMeshCreator.initializeCage();
		else
			loadVerticesToCage(mesh.getVertices());
	}
	if(mode == IMAGE_DEFORMATION){
		if(!cageLoaded){
			cageInterface.clear();
			cageInterface.setMode(REALTIME);
			cageInterface.setMethod(GREEN);

			cageInterface.setBBCage(voxelGrid.getBBMin(), voxelGrid.getBBMax());
			cageInitialized = false;
			cageLoaded = true;
			manipulator->clear();
			manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
			saveCurrentCageState();
			update();
		}
		loadVerticesToCage(voxelGrid.getPositions());
	}

	updateFromCMInterface();
	update();
}


void MesherViewer::loadVerticesToCage( const vector<BasicPoint> & V ){
	if(!cageLoaded) return;

	cageInterface.clear_selection();
	std::vector< BasicPoint > & ipoints = cageInterface.get_mesh_vertices();
	ipoints.clear();


	for( unsigned int i = 0 ; i < V.size() ; i ++ ){
		const BasicPoint & point = V[i];
		ipoints.push_back(point);
	}
	QTime t;
	t.start();

	cageInterface.compute_cage_coordinates();

	std::cout << "Time elapsed for cage coordinates computation: " << t.elapsed() << " ms for " << V.size() << " points " << std::endl;
	cageInitialized = true;

}

void MesherViewer::updateFromInterpolationStruct(double t) {
	std::vector< BasicPoint > V( cageInterface.get_cage_vertices().size() );
	bool needsUpdate = interpolationStruct.interpolate(t , V);

	if(needsUpdate) {
		setCageTopositions(V);

		BasicPoint bb, BB;
		cageInterface.getCageBBox(bb, BB);
		updateCuttingPlanes(bb,BB);

		update();
	}
}

void MesherViewer::updateFromCMInterface(){


	updatePointsFromCMInterface(mesh.getVertices());
	mesh.recomputeNormals();

	updatePointsFromCMInterface(visu_mesh.getVertices());
	visu_mesh.computeTexturesData();
	//updateCuttingPlanes(true);
	/* else if(mode == DIRECT_MESH_DEFORMATION){
		cageInterface.cageChanged(manipulator);

		std::vector< BasicPoint > const & copoints = cageInterface.get_cage_vertices();
		std::vector< BasicPoint > & points = mesh.getVertices();
		for( unsigned int i = 0 ; i < points.size() ; i ++ ){
			points[i] = copoints[i];
		}

		mesh.recomputeNormals();
		updateCuttingPlanes(true);
	}*/

}

void MesherViewer::updatePointsFromCMInterface(vector<BasicPoint> & points, bool fromManipulator)
{
	QTime t;
	t.start();

	if(fromManipulator)
		cageInterface.cageChanged(manipulator);

	if(envelopCage){
		mesh.updateMesh( cageInterface.get_mesh_modified_vertices(), cageInterface.get_cage_vertices() );
	} else {
		if(useTransferMesh){
			tetMeshCreator.updateVertices(  );
			tetMeshCreator.updateVertices( mesh.getVertices() );
			tetMeshCreator.updateVertices( visu_mesh.getVertices() );
		} else {

			std::vector< BasicPoint > const & copoints = cageInterface.get_mesh_modified_vertices();

			for( unsigned int i = 0 ; i < points.size() ; i ++ ){
				points[i] = copoints[i];
			}
		}
	}

	std::cout << "Time elapsed to update positions: " << t.elapsed() << " ms" << std::endl;

	BasicPoint bb, BB;
	cageInterface.getCageBBox(bb, BB);
	updateCuttingPlanes(bb, BB);

}

void MesherViewer::drawCage(){

	glEnable(GL_LIGHTING);
	glEnable (GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	cageInterface.drawCageSelectedVertices();

	if(displayCageProblems){

		glColor3f(1.,1.,0.);
		cageInterface.drawProblems(sphereScale);
	}


	if(displayWireCage){
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glDisable(GL_LIGHTING);
		glPolygonMode(GL_FRONT_AND_BACK , GL_LINE );
		glColor3f(0.f,0.f,1.f);
		glLineWidth (2.0f);
		cageInterface.drawCage();
		glPolygonMode(GL_FRONT_AND_BACK , GL_FILL );

		glPopAttrib();
	} else {
		glDisable (GL_LIGHTING);


		glEnable (GL_POLYGON_OFFSET_LINE);
		glLineWidth (2.0f);
		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		glPolygonOffset (-1.0, 1.0);

		glColor3f(0.5f,0.5f,0.5f);
		cageInterface.drawCage();
		glDisable (GL_POLYGON_OFFSET_LINE);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

		glColor4f(0.f,0.f,1.f, 0.1f);
		cageInterface.drawOrderedCage();


	}

	glDisable (GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}

QPixmap MesherViewer::compareVolumeLossWithGrid( const QString & filename1 , const QString & filename2 ){

	std::vector<float> volume_diff;


	VoxelGrid grid, dGrid;

	if( filename1.endsWith(".nii") )
		openNIFTI(filename1, grid);
	else{
		if( !openIMA(filename1, grid) )
			return  QPixmap() ;
	}

	std::map<Subdomain_index, int>  vNbs_1;
	grid.computeVNumbers(vNbs_1);
	std::vector<QColor> colors (vNbs_1.size(), QColor(0,0,0) );

	int count = 0 ;
	for(std::map<Subdomain_index, int>::iterator it = vNbs_1.begin() ; it != vNbs_1.end() ; it++){
		QColor color(0,0,0);
		double h = (269 - int(360*double(count)/(vNbs_1.size())));
		if(h <= 0) h = 360;
		color.setHsvF(0.98*h/360., 1.,1.);

		colors[count++] = color;
	}

	float originalVoxelVolume = grid.d(0)*grid.d(1)*grid.d(2);
	grid.clearAll();

	if( filename2.endsWith(".nii") )
		openNIFTI(filename2, dGrid);
	else{
		if( !openIMA(filename2, dGrid) )
			return QPixmap() ;
	}

	std::map<Subdomain_index, int>  vNbs_2;
	dGrid.computeVNumbers(vNbs_2);
	float defVoxelVolume = dGrid.d(0)*dGrid.d(1)*dGrid.d(2);
	dGrid.clearAll();

	volume_diff.clear();
	std::vector<int> def_v_nbs;
	std::vector<int> original_v_nbs;

	float min_value = FLT_MAX;
	float max_value = -FLT_MAX;
	int min_nb = 0;
	int max_nb = 0;
	int vsum =0;




	for(std::map<Subdomain_index, int>::iterator it = vNbs_1.begin() ; it != vNbs_1.end() ; it++){
		Subdomain_index si = it->first;

		if( si > 0 ){

			int vNb = vNbs_1[si];
			int def_v_nb = vNbs_2[si];
			if(vNb == 0 || def_v_nb == 0)
				continue;
			float diff = (def_v_nb*defVoxelVolume - vNb*originalVoxelVolume);

			volume_diff.push_back(diff);

			original_v_nbs.push_back(vNb);
			def_v_nbs.push_back(def_v_nb);

			vsum += vNb;

			if( min_value > diff ){
				min_nb = vNb;
				min_value = std::min(diff, min_value);
			}
			if( max_value < diff ){
				max_nb = vNb;
				max_value = std::max(diff, max_value);
			}

		}
	}

	float diffPercent = 0.;
	float totalVolumeChange = 0.;
	float originalTotalVolume = vsum*originalVoxelVolume;
	for(unsigned int i = 0 ; i < volume_diff.size() ; i++ ){

		totalVolumeChange += volume_diff[i];
		diffPercent += fabs(volume_diff[i]);
		volume_diff[i] /= original_v_nbs[i]*originalVoxelVolume;
	}

	// volume_diff.push_back(originalTotalVolume);

	std::cout << "Grid of " << vsum << " voxels" << std::endl ;
	std::cout << "Relative max " << 100*max_value/float(max_nb*originalVoxelVolume) << " representing " << 100*float(max_nb)/vsum << " percent of the volume " << std::endl;
	std::cout << "Relative min " << 100*min_value/float(min_nb*originalVoxelVolume) << " representing " <<  100*float(min_nb)/vsum << " percent of the volume " <<  std::endl;
	std::cout << "Volume lost and gained " << 100*diffPercent/originalTotalVolume <<  " % "<< std::endl ;
	std::cout << "Volume loss " << 100*totalVolumeChange/originalTotalVolume<< std::endl;
	std::cout <<  "Min " << 100.*min_value/float(originalTotalVolume)<<  ", Max " << 100.*max_value/float(originalTotalVolume) << ",  average " << diffPercent /volume_diff.size() << std::endl;



	Histogram histogram(colors);

	histogram.build_histogram(volume_diff);

	return histogram.getPixMap();
	//    for(unsigned int i = 0 ; i < subdomain_indices.size() ; i++ ){
	//        Subdomain_index si = subdomain_indices[i];
	//
	//        if( si > 0 ){
	//            int n0 = grid.getVoxelNumber(si);
	//            int n1 = defGrid.getVoxelNumber(si);
	//
	//            std::cout << n0 << " " << n1 << std::endl;
	//
	//            float diff = (defVoxelVolume*n1 - originalVoxelVolume*n0)*n0;
	//
	//            volume_diff.push_back(diff);
	//            vsum += n0;
	//        }
	//
	//    }
	//
	//    float volumediff= 0.;
	//    float totalVolumeChange = 0;
	//    for(unsigned int i = 0 ; i < volume_diff.size() ; i++ ){
	//        volume_diff[i] = 100*volume_diff[i]/(vsum*vsum*originalVoxelVolume);
	//        totalVolumeChange += volume_diff[i];
	//        volumediff += fabs(volume_diff[i]);
	//        min_value = std::min(diff, min_value);
	//        max_value = std::max(diff, max_value);
	//
	//    }
	//
	//
	//    std::cout << "Volume changes " << volumediff <<  " % "<< std::endl ;
	//    std::cout << "Volume difference " << totalVolumeChange<< std::endl;
	//
	//    std::cout << "Volume changes " << volumediff <<  " % "<< std::endl ;
	//    std::cout << "Volume difference " << totalVolumeChange<< std::endl;
	//
	//    std::cout << "Grid of " << vsum << " voxels" << std::endl ;

	//    for(unsigned int i = 0 ; i < volume_diff.size() ; i++ ){
	//        volume_diff[i] = 100*volume_diff[i]/originalTotalVolume;
	//        totalVolumeChange += volume_diff[i];
	//        diffPercent += fabs(volume_diff[i]);
	//    }

	//    float diffPercent = 0.;
	//    float totalVolumeChange = 0.;
	//    float originalTotalVolume = vsum*originalVoxelVolume;
	//    for(unsigned int i = 0 ; i < volume_diff.size() ; i++ ){
	//        volume_diff[i] = 100*volume_diff[i]/originalTotalVolume;
	//        totalVolumeChange += volume_diff[i];
	//        diffPercent += fabs(volume_diff[i]);
	//    }
	//
	//  // volume_diff.push_back(originalTotalVolume);
	//

	//    std::cout << "Relative max " << 100*max_value/float(max_nb*originalVoxelVolume) << " representing " << 100*float(max_nb)/vsum << " percent of the volume " << std::endl;
	//    std::cout << "Relative min " << 100*min_value/float(min_nb*originalVoxelVolume) << " representing " <<  100*float(min_nb)/vsum << " percent of the volume " <<  std::endl;

	//    std::cout <<  "Min " << 100.*min_value/float(originalTotalVolume)<<  ", Max " << 100.*max_value/float(originalTotalVolume) << ",  average " << diffPercent /volume_diff.size() << std::endl;

}



void MesherViewer::draw(){



	//    GLTools::setDefaultMaterial();
	//
	//    QColor bgColor = backgroundColor();
	//    glClearColor(bgColor.redF(),bgColor.greenF(),bgColor.blueF(),bgColor.alphaF());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// GLTools::initLights();
	qglviewer::Vec cam = camera()->worldCoordinatesOf( qglviewer::Vec(0.,0.,0.) );

	GLfloat light_position2[4] = {(float)cam.x+10.f, (float)cam.y, (float)cam.z, 0.f};
	glLightfv (GL_LIGHT2, GL_POSITION, light_position2);

	QColor defaultTMColor (0.7*255, 255, 0.32*255);
	float sphere_radius = cageInterface.getSphereRadius()*sphereScale;
	//TODO set COLOR 0.7 1. 0.32
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH);

	drawClippingPlane();

	if(displaySampling)
		tetMeshCreator.drawPointDistribution();

	if(displayCageBB)
		tetMeshCreator.drawBoundingBox();

	if(displaySampling)
		tetMeshCreator.drawPointDistribution();

	if(displayPointsToAdd)
		tetMeshCreator.drawPointsToAdd();

	if(displayEdges)
		tetMeshCreator.drawEdges();

	glDisable(GL_CLIP_PLANE0);
	if(displayMode == LIGHTED || displayMode == LIGHTED_WIRE){

		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_LIGHTING);

		if(displayType == IMAGE){
			GLTools::setSingleSpotLight();
			visu_mesh.setOrthongonalCut( cut, cutDirection );
			visu_mesh.draw( cam );
			GLTools::setSunriseLight();
		}else if(displayType == MESH ){

			//mesh.drawBoundaries(true, displayMap, colorMap);
			if(displayTetrahedron)
				mesh.drawTetrahedra(false,true, displayMap, colorMap);
			else mesh.drawBoundariesVBO(false, true, displayMap, colorMap);

		}

		if(displaySegmentation){
			segmentation.drawVBO(false, true, displayBC, iDisplayMap , iColorMap);
		}
		glColor3f(0.949,0.4, 0.61);
		if(displaySurface)
			//surfaceCreator.draw();

			if(drawModel)
				model.draw();


		if(displayTransferMesh)
			tetMeshCreator.drawTransferMesh(displayDistortions, displayOutliers, defaultTMColor, sphere_radius);

		if(displayCageTets)
			tetMeshCreator.drawCageTetMesh();

		voxelGrid.drawOutliers();
		glDisable(GL_LIGHTING);

	}  else if(displayMode == WIRE){

		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		glDisable (GL_LIGHTING);
		if(displayType == IMAGE){
			GLTools::setSingleSpotLight();
			visu_mesh.setOrthongonalCut( cut, cutDirection );
			visu_mesh.draw( cam );
			GLTools::setSunriseLight();
		}else if(displayType == MESH ){

			if(displayTetrahedron)
				mesh.drawTetrahedra(true,true, displayMap, colorMap);
			else
				mesh.drawBoundariesVBO(true, true, displayMap, colorMap);
			//mesh.drawBoundaries(true, displayMap, colorMap);

		}

		if(displaySegmentation){
			segmentation.drawVBO(true, true, displayBC, iDisplayMap , iColorMap);
		}
		glColor3f(0.949,0.4, 0.61);
		if(displaySurface)
			//surfaceCreator.draw();

			if(drawModel)
				model.draw();

		if(displayTransferMesh)
			tetMeshCreator.drawTransferMesh(displayDistortions, displayOutliers, defaultTMColor,sphere_radius);

		if(displayCageTets)
			tetMeshCreator.drawCageTetMesh();

		glEnable(GL_LIGHTING);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

	}  else if(displayMode == SOLID ){

		glDisable (GL_LIGHTING);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		if(displayType == IMAGE){
			GLTools::setSingleSpotLight();
			visu_mesh.setOrthongonalCut( cut, cutDirection );
			visu_mesh.draw( cam );
			//GLTools::setDefaultMaterial();
			GLTools::setSunriseLight();
		}else if(displayType == MESH ){

			//mesh.drawBoundaries(true, displayMap, colorMap);
			if(displayTetrahedron)
				mesh.drawTetrahedra(true, true, displayMap, colorMap);
			else mesh.drawBoundariesVBO(true, true, displayMap, colorMap);

		}

		if(displaySegmentation){
			segmentation.drawVBO(true, true, displayBC, iDisplayMap , iColorMap);
		}
		glColor3f(0.949,0.4, 0.61);
		if(displaySurface)
			//surfaceCreator.draw();
			glColor3f(0.7,1.,0.32);
		if(drawModel)
			model.draw();
		if(displayTransferMesh)
			tetMeshCreator.drawTransferMesh(displayDistortions, displayOutliers, defaultTMColor,sphere_radius);

		if(displayCageTets)
			tetMeshCreator.drawCageTetMesh();

	}

	if(displayMode == SOLID || displayMode == LIGHTED_WIRE){
		glColor3f(0.,0.,0.);
		glEnable (GL_POLYGON_OFFSET_LINE);
		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		glLineWidth (1.0f);
		glPolygonOffset (-1.0, -1.0);

		if(displayType == IMAGE){
			GLTools::setSingleSpotLight();
			visu_mesh.setOrthongonalCut( cut, cutDirection );
			visu_mesh.draw( cam );
			GLTools::setSunriseLight();
		}else if(displayType == MESH ){

			//mesh.drawBoundaries(false, displayMap, colorMap);
			if(displayTetrahedron)
				mesh.drawTetrahedra(true, false, displayMap, colorMap);
			else
				mesh.drawBoundariesVBO(true, false, displayMap, colorMap);

		}

		if(displaySegmentation){
			segmentation.drawVBO(true, false, displayBC, iDisplayMap , iColorMap);

		}
		glColor3f(0.,0.,0.);
		if(displaySurface)
			//surfaceCreator.draw();

			if(drawModel)
				model.draw();

		QColor black(0,0,0);
		if(displayTransferMesh)
			tetMeshCreator.drawTransferMesh(displayDistortions, false , black,sphere_radius);

		if(displayCageTets)
			tetMeshCreator.drawCageTetMesh();

		glDisable (GL_POLYGON_OFFSET_LINE);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		glEnable (GL_LIGHTING);
	}

	if(enableClipping){
		glEnable(GL_CLIP_PLANE0);

		glEnable( GL_DEPTH_TEST );
		glEnable(GL_BLEND);
		glDisable(GL_LIGHTING);
		drawCutPlanes();
		glDisable(GL_BLEND);
	}
	if(displayBB){
		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		glColor3f(1.,0.,0.);
		BasicGL::drawCube(BBMin, BBMax);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
	}
	if(mode != MESHING && displayCage){

		drawCage();
		glDisable(GL_LIGHTING);
		glDisable( GL_DEPTH_TEST );
		manipulator->draw();
		glEnable( GL_DEPTH_TEST );

		glEnable(GL_BLEND);
		rselection->draw();
		glDisable(GL_BLEND);
		glEnable(GL_LIGHTING);

		if(displayCageNormals){
			glDisable(GL_LIGHTING);
			glColor3f(1., 0., 0.);
			cageInterface.drawTriangulatedCageFaceNormals(scene_radius*0.05);
			glEnable(GL_LIGHTING);
		}
		//        drawVertices();
	}
	glColor3f(1.f, 0.f, 0.f);
}



void MesherViewer::coordComputationTime(){

	QTime t ;
	t.start();
	std::vector<BasicPoint> & positions = voxelGrid.getPositions();
	double totalTime = cageInterface.computeDummy(positions) * (double)(positions.size());
	std::cout << "Total time for the complete voxel grid (evaluated) : " << totalTime << " s" << std::endl;
}


void MesherViewer::drawClippingPlane(){


	glEnable(GL_LIGHTING);

	glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

	glPushMatrix();
	glMultMatrixd(manipulatedFrame()->matrix());
	// Since the Clipping Plane equation is multiplied by the current modelView, we can define a
	// constant equation (plane normal along Z and passing by the origin) since we are here in the
	// manipulatedFrame coordinates system (we glMultMatrixd() with the manipulatedFrame matrix()).
	Vec to(0.,1.,0.);

	static const GLdouble constantEquation[] = { to.x, to.y, to.z, 0.0 };
	glClipPlane(GL_CLIP_PLANE0, constantEquation);

	// Draw a plane representation: Its normal...
	glColor3f(1.f, 0.f, 0.f);
	drawArrow(Vec(0.,0.,0.), to*scene_radius/9.);

	glPopMatrix();
	glDisable(GL_LIGHTING);

	GLdouble equation[4];
	glGetClipPlane(GL_CLIP_PLANE0, equation);

	qreal p[] = {0.,-equation[3]/equation[1], 0.};
	qreal projP[3];
	camera()->getWorldCoordinatesOf(p, projP);


	qreal norm[] = {equation[0] + p[0], equation[1]+ p[1], equation[2]+ p[2]};
	qreal normResult[3];
	camera()->getWorldCoordinatesOf(norm, normResult);

	BasicPoint normal(normResult[0]-projP[0], normResult[1]-projP[1], normResult[2]-projP[2]);
	BasicPoint point(projP[0], projP[1],projP[2]);

	defGrid.setClipeEquation( normal, point);
	defGrid.setOrthongonalCut( cut, cutDirection );

	voxelGrid.setClipeEquation( normal, point);
	voxelGrid.setOrthongonalCut( cut, cutDirection );

	mesh.setClipeEquation( normal, point);
	mesh.setOrthongonalCut( cut, cutDirection );
	tetMeshCreator.setClipeEquation( normal, point);
	tetMeshCreator.setOrthongonalCut( cut, cutDirection );


	voxelGrid.set_visibility_check(enableClipping);

	if(displayDeformedImage)
		defGrid.set_visibility_check(enableClipping);

	tetMeshCreator.set_visibility_check(enableClipping);
	mesh.set_visibility_check(enableClipping);

}


void MesherViewer::drawCutPlanes(){

	double x = cut[0] + cutDirection[0]*.001;
	double y = cut[1] + cutDirection[0]*.001;
	double z = cut[2] + cutDirection[0]*.001;

	glColor4f(1.0,0.,0.,0.25);
	glBegin(GL_QUADS);

	if(cutPlaneVisibility[0] == 1){
		// Right face
		glVertex3f( x, BBMin[1], BBMin[2]);	// Bottom Right Of The Texture and Quad
		glVertex3f( x, BBMax[1], BBMin[2]);	// Top Right Of The Texture and Quad
		glVertex3f( x, BBMax[1], BBMax[2]);	// Top Left Of The Texture and Quad
		glVertex3f( x, BBMin[1], BBMax[2]);	// Bottom Left Of The Texture and Quad
	}

	if(cutPlaneVisibility[2] == 1){
		// Front Face
		glVertex3f(BBMin[0], BBMin[1], z);	// Bottom Left Of The Texture and Quad
		glVertex3f(BBMax[0], BBMin[1], z);	// Bottom Right Of The Texture and Quad
		glVertex3f(BBMax[0], BBMax[1], z);	// Top Right Of The Texture and Quad
		glVertex3f(BBMin[0], BBMax[1], z);	// Top Left Of The Texture and Quad
	}

	if(cutPlaneVisibility[1] == 1){
		// Top Face
		glVertex3f(BBMin[0], y, BBMin[2]);	// Top Left Of The Texture and Quad
		glVertex3f(BBMin[0], y, BBMax[2]);	// Bottom Left Of The Texture and Quad
		glVertex3f(BBMax[0], y, BBMax[2]);	// Bottom Right Of The Texture and Quad
		glVertex3f(BBMax[0], y, BBMin[2]);	// Top Right Of The Texture and Quad
	}
	glEnd();


}

void MesherViewer::setMode(Mode _mode){
	mode  =_mode;
	if(mode == MESHING){
		manipulator->deactivate();
	}
	//    else if(DIRECT_MESH_DEFORMATION){
	//        updateDirectMeshDeformation();
	//    }
	update();
}

void MesherViewer::changeDisplayMode(){
	if(displayMode == LIGHTED)
		displayMode = LIGHTED_WIRE;
	else if(displayMode == LIGHTED_WIRE)
		displayMode = SOLID;
	else if(displayMode == SOLID)
		displayMode = WIRE;
	else
		displayMode = LIGHTED;
	update();
}

void MesherViewer::keyPressEvent(QKeyEvent *e)
{
	switch (e->key())
	{
	case Qt::Key_R : visu_mesh.reloadShaders(); update(); break;
	case Qt::Key_D :
		if(e->modifiers() & Qt::ControlModifier){
			voxelGrid.downSample(); defGrid.clearAll(); update(); break;
		} else {
			changeDisplayMode(); break;
		}
	case Qt::Key_I : initializeCage();; break;
	case Qt::Key_C : coordComputationTime();  break;
	case Qt::Key_V : displaySegmentation = !displaySegmentation ; update(); break;
	case Qt::Key_A :
		if(e->modifiers() & Qt::ControlModifier){
			manipulator->clear();
			manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
			if( e->modifiers() & Qt::ShiftModifier) cageInterface.fixe_all();
			else cageInterface.select_all();
		}
		update();
		QGLViewer::keyPressEvent(e);
		break;
	case Qt::Key_P :
		cageInterface.perturbeCage();
		loadVerticesToCage(mesh.getVertices());
		update(); break;
	case Qt::Key_U :
		if(e->modifiers() & Qt::ControlModifier){
			manipulator->clear();
			manipulator->setDisplayScale(manipulatorScale*scene_radius/9.);
			if( e->modifiers() & Qt::ShiftModifier) cageInterface.unfixe_all();
			else cageInterface.unselect_all();
		}
		update(); break;
	case Qt::Key_Z :
		if(e->modifiers() & Qt::ControlModifier) restaureLastState(); update(); break;
	case Qt::Key_Up :
		if(movingLight){
			lightInfo[0]+=10;
			GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
			update(); break;
		}
	case Qt::Key_Down :
		if(movingLight){
			lightInfo[0]-=10;
			GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
			update(); break;
		}
	case Qt::Key_Right :
		if(movingLight){
			lightInfo[1]+=10;
			GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
			update(); break;
		}
		voxelGrid.counter++;
		voxelGrid.computeDefaultColors();
		//mesh.incrementDisplayCount();
		update(); break;
	case Qt::Key_Left :
		if(movingLight){
			lightInfo[1]-=10;
			GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
			update(); break;
		}
		voxelGrid.counter--;
		voxelGrid.computeDefaultColors();
		update(); break;
		//mesh.decrementDisplayCount();
	case Qt::Key_Space :
	{
		animationTimer.togglePause();
		animationTimer.restart();
		startAnimation();
		update(); break;
	}
	case Qt::Key_S :
		animationTimer.recomputeU();
		if( animationTimer.isPlaying() ) {
			double uAnim = animationTimer.getU();
			if(uAnim > 1.0) {
				animationTimer.playAnimation = false;
				stopAnimation();
			}
			else{
				updateFromInterpolationStruct(uAnim);
			}

			// do something with uAnim
			std::cout << uAnim << std::endl;
		}
		update(); break;
		//        if(movingLight){
		//            lightInfo[2]+=10;
		//            GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
		//            update(); break;
		//        }
	case Qt::Key_Minus :
		if(movingLight){
			lightInfo[2]-=10;
			GLTools::MoveSpot(lightInfo[0], lightInfo[1], lightInfo[2]);
			update(); break;
		}
	default : QGLViewer::keyPressEvent(e);
	}
}

QString MesherViewer::helpString() const
{
	QString text("<h2>KidPocket Framework</h2>");

	text += "This application allows to mesh a 3D segmented image and to apply cage-based deformations to the obtained models unsing </br> <b>Green Coordinates*</b>.";

	text += "<p>";

	text += "<h3>Interaction</h3>";
	text += "<p>";
	text += "<h4>Basics</h4>";
	text += "<ul>";
	text += "<li><b>H</b>   :   make this help appear.</li>";
	text += "<li><b>R</b>   :   update display.</li>";
	text += "<li><b>D</b>   :   change display mode.</li>";
	text += "<li><b>Ctrl + Q</b>   :   close the application.</li>";
	text += "</ul>";
	text += "<h4>Open</h4>";
	text += "<ul>";
	text += "<li><b>Ctrl + I</b>    :    open 3D segmented image (*.nii, *.ima).</li>";
	text += "<li><b>Ctrl + O</b>    :    open a file containing a single surfacique mesh (*&lt;subdomain_index&gt;*.off, *&lt;subdomain_index&gt;*.obj) or multiple surfaciques meshes (*.mesh).</li>";
	text += "<li><b>Ctrl + M</b>    :    open multiples surfacique meshes (*&lt;subdomain_index&gt;*.off, *&lt;subdomain_index&gt;*.obj) by selecting container folder. <b>WARNING:</b> The basename should not contain numbers otherwise the subdomain id will not be extracted properly and the folder should not contain other (*.off, *.obj) meshes or they will be added to meshes.</li>";
	text += "<li><b>Ctrl + Shift + O</b>    :    open cage (*.off). <b>WARNING:</b> Make sure the normales are all pointing outside.</li>";
	text += "<li><b>Ctrl + Alt + Shift + O</b>    :    open final cage (*.off). <b>WARNING:</b> Make sure th.</li>";
	text += "</ul>";
	text += "<h4>Save</h4>";
	text += "<ul>";
	text += "<li><b>Ctrl + E</b>    :    save envelop (only works if a 3D image have been previously meshed).</li>";
	text += "<li><b>Ctrl + S</b>    :    save multiple surfaciques meshes in a single file (*.mesh).</li>";
	text += "<li><b>Ctrl + Shift + S</b>    :    save each boundary mesh in a single file by selecting the container folder (&lt;foldername&gt;_&lt;subdomain_index&gt;.off).</li>";
	text += "<li><b>Ctrl + Alt + S</b>   :   save current cage (*.off).</li>";
	text += "</ul>";
	text += "<h4>Deformation</h4>";
	text += "<ul>";
	text += "<li><b>Ctrl + mouse right</b>    :    select/unselect as moving handles cage vertices</li>";
	text += "<li><b>Ctrl + mouse left</b>    :    change vertices handle type: fixed to moving or moving to fixed</li>";
	text += "<li><b>Ctrl + mouse middle</b>    :    change all moving handle vertices to fixed</li>";
	text += "<li><b>Ctrl + A</b>    :    set all free vertices to moving handles</li>";
	text += "<li><b>Ctrl + Shift + A</b>    :    set all free vertices to fixed handles</li>";
	text += "<li><b>Ctrl + U</b>    :    unselect all moving handles</li>";
	text += "<li><b>Ctrl + Shift + U</b>    :    unselect all fixed handles</li>";
	text += "<li><b>Shift + mouse right click</b>    :    manipulate the selected vertices</li>";
	text += "<li><b>I</b>   :   initialize cage for deformation.</li>";
	text += "</ul>";
	text += "</p>";

	text += "<h3>User guide</h3>";
	text += "<p>";
	text += "<h4>Pipeline</h4>";
	text += "<ul>";
	text += "<li>Load a 3D image (*.nii or *.ima)</li>";
	text += "<li>Mesh</li>";
	text += "<li>Save envelop (*.off).</li>";
	text += "<li>Generate cage (using Blender).</li>";
	text += "<li>Load cage (*.off) and switch to 'deformation' mode for display.</li>";
	text += "<li>Initialize cage (otherwise automatically done before the first deformation).</li>";
	text += "<li>Select cage vertices. <ul><li>Fixed handles: red spheres.</li> <li>Moving handles: green spheres.</li> </ul></li>";
	text += "<li>Compute manipulator (Shift+right click).</li>";
	text += "<li>Operate deformations and iterate last 2 steps until getting the desired result. <b>WARNING:</b> Do not forget to unselect cage vertices before deforming a different area of the model.</li>";
	text += "<li>Save model and cage.</li>";
	text += "</ul>";
	text += "<h4>Remarks</h4>";
	text += "<ul>";
	text += "<li><b>FIRST</b> generate or open a model, <b>THEN</b> open a cage model. OFF files are supported.</li>";
	text += "<li>When you initialize the cage, there will be a latence time due to the calculation of the cage coordinates.</li>";
	text += "<li>To deform the model, use the 'Rectangle' selection tool to select the moving handles of cage vertices, by using the mouse while keeping 'Shift' pressed (discussed before).</li>";
	text += "<li>To unselect vertices, use the 'Rectangle' selection tool by using the mouse while keeping 'Ctrl + Shift' pressed.</li>";
	text += "<li>To disable the manipulation tool, right click on it.</li>";
	text += "</ul>";
	text += "</p>";

	text += "<h3>Biblio</h3>";
	text += "<p>";
	text += "(*) : LIPMAN, Y., LEVIN, D., AND COHEN-OR, D. 2008. Green coordinates. In ACM SIGGRAPH 2008 papers, ACM, 1-10.";
	text += "</p>";

	text += "</p>";
	return text;
}
