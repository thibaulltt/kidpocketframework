// Copyright (c) 2006-2009 INRIA Sophia-Antipolis (France).
// All rights reserved.
//
// This file is part of CGAL (www.cgal.org); you may redistribute it under
// the terms of the Q Public License version 1.0.
// See the file LICENSE.QPL distributed with CGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL: svn+ssh://scm.gforge.inria.fr/svn/cgal/branches/CGAL-3.6-branch/Mesh_3/include/CGAL/Mesh_triangulation_3.h $
// $Id: Mesh_triangulation_3.h 52705 2009-10-23 10:27:15Z stayeb $
//
//
// Author(s)     : Laurent Rineau, Stephane Tayeb


#ifndef MESH_ANY_TRIANGULATION_3_H
#define MESH_ANY_TRIANGULATION_3_H

#include <CGAL/Kernel_traits.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_3.h>

#include <CGAL/Triangulation_cell_base_with_info_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>

#include <CGAL/Mesh_vertex_base_3.h>
#include <CGAL/Mesh_cell_base_3.h>

namespace CGAL {


// Struct Mesh_triangulation_3
//
template<class MD, class VInfo=int, class CInfo=int, class K=typename Kernel_traits<MD>::Kernel>
struct Mesh_triangulation_3_with_info
  {
private:
  typedef Exact_predicates_inexact_constructions_kernel                                                     Geom_traits;
  typedef Mesh_vertex_base_3<Geom_traits, MD, Triangulation_vertex_base_with_info_3<VInfo, Geom_traits> >   Vertex_base;
  typedef Mesh_cell_base_3<Geom_traits, MD, Triangulation_cell_base_with_info_3<CInfo, Geom_traits> >       Cell_base;
  typedef Triangulation_data_structure_3<Vertex_base,Cell_base>                                             Tds;
  typedef Triangulation_3<Geom_traits, Tds>                                                                 Triangulation;

public:
  typedef Triangulation type;
  typedef type Type;
};  // end struct Mesh_triangulation_3



}  // end namespace CGAL

#endif // MESH_TRIANGULATION_3_H

