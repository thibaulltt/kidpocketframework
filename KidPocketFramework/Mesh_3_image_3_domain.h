#ifndef MESH_3_IMAGE_3_DOMAIN_H
#define MESH_3_IMAGE_3_DOMAIN_H


#include <boost/variant.hpp>
#include <boost/optional.hpp>

template<typename Point, typename Tetrahedron, class BGT, typename Si=int >
class Mesh_3_image_3_domain
{
public:

    typedef BGT R;
    typedef Point Point_;
    typedef Tetrahedron Tetrahedron_;

    typedef Si Subdomain_index;
    typedef typename BGT::Point_3    Point_3;
    typedef typename std::vector<Point>           Vertices;
    typedef typename Vertices::iterator           Vertices_iterator;
    typedef typename std::vector<Tetrahedron>     Tetrahedra;
    typedef typename Tetrahedra::iterator         Tetrahedra_iterator;
    typedef typename std::vector<Subdomain_index> Subdomain_indices;
    typedef typename Subdomain_indices::iterator  Subdomain_indices_iterator;

    //-------------------------------------------------------
    // Index Types
    //-------------------------------------------------------

    typedef boost::optional<Subdomain_index> Subdomain;
    /// Type of indexes for surface patch of the input complex
    typedef std::pair<Subdomain_index, Subdomain_index> Surface_index;
    typedef boost::optional<Surface_index> Surface_patch_index;
    /// Type of indexes to characterize the lowest dimensional face of the input
    /// complex on which a vertex lie
    typedef boost::variant<Subdomain_index, Surface_index> Index;

    /**
     * Returns the index to be stored in a vertex lying on the surface identified
     * by \c index.
     */
    Index index_from_surface_index(const Surface_index& index) const
    { return Index(index); }

    /**
     * Returns the index to be stored in a vertex lying in the subdomain
     * identified by \c index.
     */
    Index index_from_subdomain_index(const Subdomain_index& index) const
    { return Index(index); }

    /**
     * Returns the \c Surface_index of the surface patch
     * where lies a vertex with dimension 2 and index \c index.
     */
    Surface_index surface_index(const Index& index) const
    { return boost::get<Surface_index>(index); }

    /**
     * Returns the index of the subdomain containing a vertex
     *  with dimension 3 and index \c index.
     */
    Subdomain_index subdomain_index(const Index& index) const
    { return boost::get<Subdomain_index>(index); }


    /// Returns Surface_index from \c i and \c j
    Surface_index make_surface_index(const Subdomain_index i,
                                     const Subdomain_index j) const
    {
      if ( i < j ) return Surface_index(i,j);
      else return Surface_index(j,i);
    }

    Mesh_3_image_3_domain() {}
    Mesh_3_image_3_domain(const Vertices & points, const Tetrahedra & tetrahedra, const Subdomain_indices & subdomain_indices):
            points_(points),
            tetrahedra_ (tetrahedra),
            subdomain_indices_(subdomain_indices)
    {
    }

    Tetrahedra & tetrahedra(){return tetrahedra_;}
    const Tetrahedra & tetrahedra() const {return tetrahedra_;}

    Vertices & vertices(){return points_;}
    const Vertices & vertices() const {return points_;}

    Subdomain_indices & subdomain_indices(){return subdomain_indices_;}
    const Subdomain_indices & subdomain_indices() const {return subdomain_indices_;}

protected:
    Vertices points_;
    Tetrahedra tetrahedra_;
    Subdomain_indices subdomain_indices_;
};
#endif // MESH_3_IMAGE_3_DOMAIN_H
