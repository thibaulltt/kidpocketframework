#ifndef SEPARATIONINFO_H
#define SEPARATIONINFO_H

#include <QRectF>

// Separation information
struct Separation_info
{
    QRectF zone;
    float modelview[16];
    float projection[16];
    std::pair< int, int > labels;
public :
    Separation_info(){}
    Separation_info(const QRectF &_zone,
                    float _modelview[16],
                    float _projection[16],
                    int l1,
                    int l2){
        zone = _zone;
        for(int i = 0 ; i < 16; i++){
            modelview[i] = _modelview[i];
            projection[i] = _projection[i];
        }

        if(l1 < l2)
            labels = std::make_pair(l1,l2);
        else
            labels = std::make_pair(l2,l1);

    }

    bool is_in_selection(const BasicPoint & p)
    {

        float x = modelview[0] * p[0] + modelview[4] * p[1] + modelview[8] * p[2] + modelview[12];
        float y = modelview[1] * p[0] + modelview[5] * p[1] + modelview[9] * p[2] + modelview[13];
        float z = modelview[2] * p[0] + modelview[6] * p[1] + modelview[10] * p[2] + modelview[14];
        float w = modelview[3] * p[0] + modelview[7] * p[1] + modelview[11] * p[2] + modelview[15];
        x /= w;
        y /= w;
        z /= w;
        w = 1.f;

        float xx = projection[0] * x + projection[4] * y + projection[8] * z + projection[12] * w;
        float yy = projection[1] * x + projection[5] * y + projection[9] * z + projection[13] * w;
        float ww = projection[3] * x + projection[7] * y + projection[11] * z + projection[15] * w;
        xx /= ww;
        yy /= ww;

        xx = ( xx + 1.f ) / 2.f;
        yy = 1.f - ( yy + 1.f ) / 2.f;

        if( zone.contains( xx , yy ) ){
          return true;
        }
        return false;
    }
};

#endif // SEPARATIONINFO_H
