#ifndef __BUFFEROBJECT__H_
#define __BUFFEROBJECT__H_

/*
	This is a class for BufferObjects. VertexBuffer or IndexBuffer

	Matthias Holl�nder 2008/2009
*/


#include "OpenGLHeader.h"


class BufferObject
{
public:
	// ----------------------------------------------------
	// Cstr & Dstr
	// ----------------------------------------------------
	BufferObject( GLenum typeBuffer );
	BufferObject();
	~BufferObject();

	// ----------------------------------------------------
	// public methods
	// ----------------------------------------------------
	void						SetData(int iNumValues, GLsizeiptr sizeInBytes, GLvoid* pData, GLenum usage, GLenum valuetype);
	GLuint						GetBuffer() { return m_uiBuffer; }
	int							GetNumValues() { return m_iNumValues; }
	GLenum						GetValueType() { return m_ValueType; }
	void						Use();
	void						UnUse();

	void						Create( GLenum typeBuffer );		// For use with empty cstr

private:
	// ----------------------------------------------------
	// private attributes
	// ----------------------------------------------------
	GLuint						m_uiBuffer;
	GLenum						m_BufferType;
	GLenum						m_ValueType;						// such as GL_FLOAT, GL_UNSIGNED_INT
	int							m_iNumValues;
};





#endif // __BUFFEROBJECT__H_

