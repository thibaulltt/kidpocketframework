#include "ProgramObject.h"
#include <stdio.h>
#include <assert.h>
#include "BufferObject.h"


// ------------------------------------------------------------------------------------
// Track memory leaks
// ------------------------------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif


// ------------------------------------------------------------------------------------
// Constructor for a ProgramObject
// ------------------------------------------------------------------------------------
ProgramObject::ProgramObject()
{
	m_Program = glCreateProgramObjectARB();
}

// ------------------------------------------------------------------------------------
// Destructor for a ProgramObject
// ------------------------------------------------------------------------------------
ProgramObject::~ProgramObject()
{
	glDeleteObjectARB(m_Program);
}

// ------------------------------------------------------------------------------------
// Attaches a ShaderObject to the ProgramObject
// ------------------------------------------------------------------------------------
void ProgramObject::AddShader(ShaderObject* shader)
{
	glAttachObjectARB(m_Program, shader->GetShader());
}

// ------------------------------------------------------------------------------------
// Detaches a ShaderObject from the ProgramObject
// ------------------------------------------------------------------------------------
void ProgramObject::RemoveShader(ShaderObject* shader)
{
	glDetachObjectARB(m_Program, shader->GetShader());
}

// ------------------------------------------------------------------------------------
// Links the ShaderObjects to the ProgramObject
//
// Call this after all the ShaderObjects have been attached
// to the ProgramObject. The ShaderObject must be linked before
// it can be used.
// ------------------------------------------------------------------------------------
bool ProgramObject::Link( bool bBreakOnError  )
{
	GLint iLinked;
	glLinkProgramARB( m_Program );
	glGetObjectParameterivARB( m_Program, GL_OBJECT_LINK_STATUS_ARB, &iLinked );

	// This can have warnings and errors!
	CheckError();
	if ( !iLinked ) {
		if ( bBreakOnError ) {
			assert( iLinked && "There was an error while linking the shader object!" );
		}
		return false;
	}	
	return true;
}

// ------------------------------------------------------------------------------------
// prints out the error of the shader
// ------------------------------------------------------------------------------------
bool ProgramObject::CheckError()
{
    int		iInfoLogLength = 0;
    int		iCharsWritten  = 0;
    char*	szInfoLog;

    glGetObjectParameterivARB(m_Program, GL_OBJECT_INFO_LOG_LENGTH_ARB, &iInfoLogLength);
    if(iInfoLogLength > 1)
    {
        szInfoLog = new char[iInfoLogLength];
        glGetInfoLogARB( m_Program, iInfoLogLength, &iCharsWritten, szInfoLog );
        std::cout << szInfoLog << std::endl;

        delete [] szInfoLog;
		return false;
	}
	return true;
}

// ------------------------------------------------------------------------------------
// Begins the ProgramObject effect
// ------------------------------------------------------------------------------------
void ProgramObject::Begin()
{
	glUseProgramObjectARB( m_Program );
}

// ------------------------------------------------------------------------------------
// Ends the ProgramObject effect
// ------------------------------------------------------------------------------------
void ProgramObject::End()
{
	glUseProgramObjectARB( 0 );
}

// ------------------------------------------------------------------------------------
// Sets a single float uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform1f( const char* szUniformName, GLfloat fValue )
{
	glUniform1fARB( GetUniformLocation( szUniformName ), fValue );
}

// ------------------------------------------------------------------------------------
// Sets a 2 float uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform2f( const char* szUniformName, GLfloat fValue0, GLfloat fValue1 )
{
	glUniform2fARB(GetUniformLocation( szUniformName ), fValue0, fValue1);
}

// ------------------------------------------------------------------------------------
// Sets a 3 float uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform3f( const char* szUniformName, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2 )
{
	glUniform3fARB( GetUniformLocation( szUniformName ), fValue0, fValue1, fValue2 );
}

// ------------------------------------------------------------------------------------
// Sets a 4 float uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform4f( const char* szUniformName, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2, GLfloat fValue3 )
{
	glUniform4fARB( GetUniformLocation( szUniformName ), fValue0, fValue1, fValue2, fValue3 );
}

// ------------------------------------------------------------------------------------
// Sets a 1 uint uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform1ui( const char* szUniformName, GLuint uiVal )
{
	glUniform1uiEXT( GetUniformLocation( szUniformName ), uiVal );
}

// ------------------------------------------------------------------------------------
// Sets a 1 int uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform1i( const char* szUniformName, GLint iVal )
{
	glUniform1i(GetUniformLocation( szUniformName), iVal );
}

// ------------------------------------------------------------------------------------
// Sets a 2 int uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform2i(const char* szUniformName, GLint iVal0, GLint iVal1)
{
	glUniform2iARB(GetUniformLocation(szUniformName), iVal0, iVal1);
}

// ------------------------------------------------------------------------------------
// Sets a 3 int uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform3i( const char* szUniformName, GLint iVal0, GLint iVal1, GLint iVal2 )
{
	glUniform3iARB(GetUniformLocation( szUniformName), iVal0, iVal1, iVal2 );
}

// ------------------------------------------------------------------------------------
// Sets a 4 int uniform variable
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform4i( const char* szUniformName, GLint iVal0, GLint iVal1, GLint iVal2, GLint iVal3 )
{
	glUniform4iARB( GetUniformLocation( szUniformName ), iVal0, iVal1, iVal2, iVal3 );
}

// ------------------------------------------------------------------------------------
// Setting a float-array
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform1fv(const char* szUniformName, GLsizei iCount, GLfloat* pfValue )
{
	glUniform1fv( GetUniformLocation( szUniformName ), iCount, pfValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 2 floats
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform2fv(const char* szUniformName, GLsizei iCount, GLfloat* pfValue)
{
	glUniform2fv( GetUniformLocation( szUniformName ), iCount, pfValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 3 floats
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform3fv( const char* szUniformName, GLsizei iCount, GLfloat* pfValue )
{
	glUniform3fv( GetUniformLocation( szUniformName ), iCount, pfValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 4 floats
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform4fv( const char* szUniformName, GLsizei iCount, GLfloat* pfValue )
{
	glUniform4fv( GetUniformLocation( szUniformName), iCount, pfValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 1 int
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform1iv( const char* szUniformName, GLsizei iCount, GLint* piValue )
{
	glUniform1iv( GetUniformLocation( szUniformName ), iCount, piValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 2 int
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform2iv( const char* szUniformName, GLsizei iCount, GLint* piValue )
{
	glUniform2iv( GetUniformLocation( szUniformName ), iCount, piValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 3 int
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform3iv( const char* szUniformName, GLsizei iCount, GLint* piValue )
{
	glUniform3iv( GetUniformLocation( szUniformName ), iCount, piValue );
}

// ------------------------------------------------------------------------------------
// Setting a uniform variable 4 int
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniform4iv( const char* szUniformName, GLsizei iCount, GLint* piValue )
{
	glUniform4iv(GetUniformLocation( szUniformName ), iCount, piValue );
}

// ------------------------------------------------------------------------------------
// Setting a 2-Matrix
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniformMatrix2fv( const char* szUniformName, GLsizei iCount, GLboolean bTranspose, GLfloat* pfValues )
{
	assert( 0 && "Not tested yet!" );
	glUniformMatrix2fv( GetUniformLocation( szUniformName ), iCount, bTranspose, pfValues );	
}

// ------------------------------------------------------------------------------------
// Setting a 3-Matrix
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniformMatrix3fv( const char* szUniformName, GLsizei iCount, GLboolean bTranspose, GLfloat* pfValues )
{
	//assert(0 && "Not tested yet!");
	glUniformMatrix3fv( GetUniformLocation( szUniformName ), iCount, bTranspose, pfValues );	
}

// ------------------------------------------------------------------------------------
// Setting a 4-Matrix
// ------------------------------------------------------------------------------------
void ProgramObject::SetUniformMatrix4fv( const char* szUniformName, GLsizei iCount, GLboolean bTranspose, GLfloat* pfValues )
{
	//assert( 0 && "Not tested yet!");
	glUniformMatrix4fv( GetUniformLocation( szUniformName ), iCount, bTranspose, pfValues );	
}

// ------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------
void ProgramObject::SetVertexAttrib1f( GLuint uiIndex, GLfloat fValue )
{
	assert( 0 && "Not tested yet!" );
	glVertexAttrib1fv( uiIndex, &fValue );
}

// ------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------
void ProgramObject::SetVertexAttrib2f(GLuint uiIndex, GLfloat fValue0, GLfloat fValue1)
{
	assert( 0 && "Not tested yet!");
	glVertexAttrib2f( uiIndex, fValue0, fValue1 );
}

// ------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------
void ProgramObject::SetVertexAttrib3f( GLuint uiIndex, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2 )
{
	assert( 0 && "Not tested yet!" );
	glVertexAttrib3f( uiIndex, fValue0, fValue1, fValue2 );
}

// ------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------
void ProgramObject::SetVertexAttrib4f( GLuint uiIndex, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2, GLfloat fValue3 )
{
	assert( 0 && "Not tested yet!" );
	glVertexAttrib4f( uiIndex, fValue0, fValue1, fValue2, fValue3 );
}

// ------------------------------------------------------------------------------------
// Setting the Input-Type of the Geometry-Shader
// ------------------------------------------------------------------------------------
void ProgramObject::SetGeometryShaderInputType( GLenum type )
{
	assert(((type == GL_POINTS) || (type == GL_LINES) || (type == GL_LINES_ADJACENCY_EXT) || (type == GL_TRIANGLES ) || (type == GL_TRIANGLES_ADJACENCY_EXT) ) && ("Invalid Enum Type!"));
	glProgramParameteriEXT(m_Program, GL_GEOMETRY_INPUT_TYPE_EXT,type);
}

// ------------------------------------------------------------------------------------
// Setting the Output-Type of the Geometry-Shader
// ------------------------------------------------------------------------------------
void ProgramObject::SetGeometryShaderOutputType( GLenum type )
{
	assert(((type == GL_POINTS) || (type == GL_LINE_STRIP) || (type == GL_TRIANGLE_STRIP)) && ("Invalid Enum Type!"));
	glProgramParameteriEXT(m_Program, GL_GEOMETRY_OUTPUT_TYPE_EXT, type);
}

// ------------------------------------------------------------------------------------
// Setting the Number of Possible outputs of vertices
// ------------------------------------------------------------------------------------
void ProgramObject::SetGeometryShaderNumVerticesOut( int iNumVerticesOut )
{
	if ( iNumVerticesOut == 0 ) {
		// Take the maximum
		glGetIntegerv( GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT, &iNumVerticesOut );
	}

#if defined(_DEBUG) || defined (DEBUG)
	int iGetVal;
	glGetIntegerv( GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT, &iGetVal );
	assert( (iNumVerticesOut <= iGetVal) && ("Value too large for your graphics card!") );
#endif
	glProgramParameteriEXT( m_Program, GL_GEOMETRY_VERTICES_OUT_EXT, iNumVerticesOut );
}

// ------------------------------------------------------------------------------------
// Binding a Buffer to the Shader
// ------------------------------------------------------------------------------------
void ProgramObject::AddBuffer( BufferObject* pBuffer, const char* szBufferName )
{
	glUniformBufferEXT( m_Program, GetUniformLocation(szBufferName), pBuffer->GetBuffer() );
}

// ------------------------------------------------------------------------------------
// retrieving the size of a Buffer
// ------------------------------------------------------------------------------------
GLint ProgramObject::GetUniformBufferSize( const char* szBufferName )
{
    return glGetUniformBufferSizeEXT( m_Program, GetUniformLocation( szBufferName ) );
}

// ------------------------------------------------------------------------------------
// To get the Location of a Vertex Attribute:
// ------------------------------------------------------------------------------------
GLint ProgramObject::GetAttribLocation( const char* szAttribLocationName )
{	
	return glGetAttribLocation( m_Program, szAttribLocationName );
}

// ------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------
void ProgramObject::BindAttribLocation( GLuint uiIndex, const char* szAttribName )
{
	glBindAttribLocation( m_Program, uiIndex, szAttribName );
}

// ------------------------------------------------------------------------------------
// Retrieves the location of a uniform variable
// ------------------------------------------------------------------------------------
GLint ProgramObject::GetUniformLocation( const char* szUniformName )
{
	GLint iLocation = glGetUniformLocation( m_Program, szUniformName );

        if ( iLocation == -1 ) {
            std::cout << "WARNING: Uniform variable >>" << szUniformName << "<< not found! Maybe it's been optimized away!" << std::endl;
            // assert?
            //assert( 0 );
        }
        return iLocation;
}

