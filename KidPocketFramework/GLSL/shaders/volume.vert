/*
		This shader is used for rendering the input/coarse-mesh
		
		Matthias Holl�nder 2008/2009
*/

// --------------------------------------------------
// shader definition
// --------------------------------------------------
#version 130
#extension GL_EXT_gpu_shader4 : enable

// --------------------------------------------------
// Uniform variables:
// --------------------------------------------------
	uniform int offset;

	uniform int width;

	uniform vec3 clippingPoint;
	uniform vec3 clippingNormal;
	uniform vec3 cut;
	uniform vec3 cutDirection;
	uniform bool visibility_check; 
	uniform float scene_radius;

// --------------------------------------------------
// varying variables
// --------------------------------------------------
	varying vec3 vLightDir; 	

	varying vec3 position;
	varying vec3 normal;

	varying float visibility;

// --------------------------------------------------
// Vertex-Shader
// --------------------------------------------------

	uniform sampler2D u_Translations;

	

/**
* @brief function to convert 1D Index to 2D index (not normalized)
* @return The texture coordinate [0..iWrapSize]x[0..n] 
*/
ivec2 Convert1DIndexTo2DIndex_Unnormed( in unsigned int uiIndexToConvert, in int iWrapSize )
{
	int iY = int( uiIndexToConvert / unsigned int( iWrapSize ) );
	int iX = int( uiIndexToConvert - ( unsigned int( iY ) * unsigned int( iWrapSize ) ) );
	return ivec2( iX, iY );
}

float ComputeVisibility(vec3 point){

	float xVis = (point.x - cut.x)*cutDirection.x;
	float yVis = (point.y - cut.y)*cutDirection.y;
	float zVis = (point.z - cut.z)*cutDirection.z;

    	vec3 pos = point - clippingPoint;
    	float vis = dot( clippingNormal, pos );
    	if( vis < 0. || xVis < 0.0|| yVis < 0.0 || zVis < 0.0 )
		return 1000.;
    	else return 0.;
}

void main()
{
	ivec2 textCoord = Convert1DIndexTo2DIndex_Unnormed(unsigned int(gl_InstanceID + offset), width);

	vec3 textValue = texelFetch(u_Translations, textCoord, 0).xyz;

	vec4 v_position = vec4(textValue, 0.) + gl_Vertex;

	position = (gl_ModelViewMatrix * v_position).xyz; 
	normal = gl_NormalMatrix* gl_Normal;

	vLightDir = gl_LightSource[0].position.xyz - position;

	visibility = 0.; 
	if(visibility_check){
		visibility = ComputeVisibility(textValue);
	}
	
	gl_Position = gl_ModelViewProjectionMatrix*v_position;

	gl_FrontColor = gl_Color;

}
