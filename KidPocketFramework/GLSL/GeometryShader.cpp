#include "GeometryShader.h"
#include "ShaderInclude.h"

#include <assert.h>

// ------------------------------------------------------------------------------------
// Track memory leaks
// ------------------------------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif


// ------------------------------------------------------------------------------------
// static attributes:
// ------------------------------------------------------------------------------------
bool GeometryShader::m_bCheckedSupport = false;
bool GeometryShader::m_isAvailable = false;

// ------------------------------------------------------------------------------------
// static methods:
// ------------------------------------------------------------------------------------
GLboolean GeometryShader::IsAvailableGeometryShader()
{
        m_bCheckedSupport = true;
        return glewGetExtension( "GL_EXT_geometry_shader4" );
}

// ------------------------------------------------------------------------------------
// Cstr & Dstr
// ------------------------------------------------------------------------------------
GeometryShader::GeometryShader() :
	ShaderObject()
{
	m_TypeShader = TypeShader_GeometryShader;

        if ( !m_bCheckedSupport ) {
		m_isAvailable = IsAvailableGeometryShader();
	}

        if ( !m_isAvailable ) {
            std::cout << "GeometryShaders are not supported by your Graphics device!" << std::endl;
            assert( 0 );
            return;
	}
        m_ShaderHandle = glCreateShaderObjectARB( GEOMETRY_SHADER_EXT );
}

GeometryShader::~GeometryShader()
{
}

