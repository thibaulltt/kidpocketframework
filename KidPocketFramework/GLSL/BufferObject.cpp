#include "BufferObject.h"
#include <assert.h>

// ------------------------------------------------------------------------------------
// Track memory leaks
// ------------------------------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif


// ------------------------------------------------------------------------------------
// Cstr & Dstr
// ------------------------------------------------------------------------------------
BufferObject::BufferObject( GLenum typeBuffer ):
	m_BufferType( typeBuffer ),
	m_ValueType( 0 ),
	m_iNumValues( 0 )
{
	assert(((typeBuffer == GL_ARRAY_BUFFER) || (typeBuffer == GL_ELEMENT_ARRAY_BUFFER)|| (typeBuffer == GL_PIXEL_PACK_BUFFER) ||
		    (typeBuffer == GL_PIXEL_UNPACK_BUFFER) || (typeBuffer == GL_UNIFORM_BUFFER_EXT)) &&
		    ("Invalid BufferType"));
    glGenBuffers( 1, &m_uiBuffer );
}

BufferObject::BufferObject():
	m_BufferType( 0 ),
	m_ValueType( 0 ),
	m_iNumValues( 0 )
{
	glGenBuffers( 1, &m_uiBuffer );
}

BufferObject::~BufferObject()
{
	glDeleteBuffers( 1, &m_uiBuffer );
}

// ------------------------------------------------------------------------------------
// This can be used with the empty cstr. Use at your own risk
// ------------------------------------------------------------------------------------
void BufferObject::Create( GLenum typeBuffer )
{
	assert(((typeBuffer == GL_ARRAY_BUFFER) || (typeBuffer == GL_ELEMENT_ARRAY_BUFFER)|| (typeBuffer == GL_PIXEL_PACK_BUFFER) ||
		    (typeBuffer == GL_PIXEL_UNPACK_BUFFER) || (typeBuffer == GL_UNIFORM_BUFFER_EXT)) &&
		    ("Invalid BufferType"));
	m_BufferType = typeBuffer;
}

// ------------------------------------------------------------------------------------
// Setting the Buffer's data
// ------------------------------------------------------------------------------------
void BufferObject::SetData( int iNumValues, GLsizeiptr sizeInBytes, GLvoid* pData, GLenum usage, GLenum valuetype )
{
	assert(((usage == GL_STREAM_DRAW) || (usage == GL_STREAM_READ) || (usage == GL_STREAM_COPY) || (usage == GL_STATIC_DRAW) ||
			(usage == GL_STATIC_READ) || (usage == GL_STATIC_COPY) || (usage == GL_DYNAMIC_DRAW) || (usage == GL_DYNAMIC_READ) ||
			(usage == GL_DYNAMIC_COPY)) && ("Invalid usage Parameter!"));
    glBindBuffer( m_BufferType, m_uiBuffer );							// Bind the Buffer
    glBufferData( m_BufferType, sizeInBytes, pData, usage );			// Set the Data
	glBindBuffer( m_BufferType, 0 );									// Unbind the buffer
	m_iNumValues = iNumValues;
	m_ValueType = valuetype;
}

// ------------------------------------------------------------------------------------
// Using the buffer
// ------------------------------------------------------------------------------------
void BufferObject::Use()
{
	glBindBuffer( m_BufferType, m_uiBuffer );
}

// ------------------------------------------------------------------------------------
// Unbinding the buffer
// ------------------------------------------------------------------------------------
void BufferObject::UnUse()
{
	glBindBuffer( m_BufferType, 0 );
}
