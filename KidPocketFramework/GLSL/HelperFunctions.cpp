#include "HelperFunctions.h"


#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// ---------------------------------------------------------------
// Track memory leaks
// ---------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif

// ---------------------------------------------------------------------
// remove pragma with Visual Studio
// ---------------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning(disable : 4996)
#endif

// ---------------------------------------------------------------------
// This will retrieve one line of input
// it returns the number of chars read
// ---------------------------------------------------------------------
int MyGetLine(std::ifstream& file_in, char* szDstBuffer, int iDstBufferSize)
{
	if (file_in.eof() || file_in.fail()) {
		return 0;
	}

	// retrieve one line:
	int iCurrentPos = 0;
	int iRead = 0;
	char c = 0;															// CurrentChar
	while (true) {
		if (iCurrentPos == iDstBufferSize - 1) {
			// Buffer for read-in probably too small
			assert(0 && "Your buffer for read-in is too small!");
			break;
		}
		if (!file_in.good()) {
			// eof probably
			if (!file_in.eof()) {
				assert(0 && "Not EOF what happened?");
			}
			break;
		}
		c = file_in.get();

		if (file_in.fail()) {
			break;
		}
		++iRead;

		if (c == '\n') break;											// UNIX
		if (c == '\r') {												// Windows/MAC
			int iPos = file_in.tellg();
			c = file_in.get();											// Read 1 more
			if (c != '\n') {											// Then it was Max-file probably
				// reset the position:
				file_in.seekg(iPos, std::ios::beg);
			}
			else {
				++iRead;
			}
			break;
		}
		szDstBuffer[iCurrentPos] = c;
		++iCurrentPos;
	}
	szDstBuffer[iCurrentPos] = 0;										// Zero-terminated
	return iRead;
}

// ---------------------------------------------------------------------------------
// Checking if a file exists
// ---------------------------------------------------------------------------------
bool FileExists(const char* szFilename)
{
	if (szFilename == 0) { return false; }

	FILE* pFile = fopen(szFilename, "r");
	if (pFile == 0) {
		return false;
	}
	else {
		fclose(pFile);
		return true;
	}
}

// ---------------------------------------------------------------------------------
// Since strlwr is not Ansi-C but a microsoft creation we might need this one
// The function will convert a string to lower-case
// ---------------------------------------------------------------------------------
char* strlwr_mine(char* szString)
{
	char* pSave = szString;
	char* pCurrent = &szString[0];
	while(*pCurrent != 0) {
		if (pCurrent[0] >= 65 && pCurrent[0] <= 90) {
			pCurrent[0] += 32;
		}
		++pCurrent;
	}
	return pSave;												// return pointer to string (org)
}

// ---------------------------------------------------------------------------------
// for const char*
// ---------------------------------------------------------------------------------
char* strlwr_mine(const char* szString)
{
	return strlwr_mine( const_cast<char *>(szString) );
}

// ---------------------------------------------------------------------------------
// get a random number (int)
// ---------------------------------------------------------------------------------
int	GetRandomBetween(int iMin, int iMax)
{
	const float fDiv = 1.0f / (RAND_MAX + 1.0f);
	int iRand = rand();
	int iRange = iMax - iMin + 1; 
	return iMin + (int) ( iRange * iRand * fDiv ); 
}

// ---------------------------------------------------------------------------------
// get a random number (float)
// ---------------------------------------------------------------------------------
float GetRandomBetween(float fMin, float fMax)
{
	const float fDiv = 1.0f / (RAND_MAX + 1.0f);
	int iRand = rand();
	float fRange = fMax - fMin + 1.0f; 
	return fMin + ( fRange * iRand * fDiv ); 
}

// ---------------------------------------------------------------------------------
// Getting the file path from file-name
// ---------------------------------------------------------------------------------
void ExtractFilePathFromFile( const std::string& strFilename, char* szDestFilePath )
{
    const char* szFilename = strFilename.c_str();
        char* cPos1 = const_cast< char* > (strrchr( szFilename, '\\' ) );		// For win-like
        char* cPos2 = const_cast< char* > (strrchr( szFilename, '/' ) );		// For unix-like
	
	int iPos1 = cPos1? (int)(cPos1 - szFilename + 1) : 0 ;
	int iPos2 = cPos2? (int)(cPos2 - szFilename + 1) : 0 ;

        iPos1 = std::max( iPos1, iPos2 );
	strncpy( &szDestFilePath[0], &szFilename[0], iPos1+1 );
	szDestFilePath[ iPos1 ] = 0;
}

