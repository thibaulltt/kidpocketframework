#ifndef __HELPER_FUNCTIONS__H_
#define __HELPER_FUNCTIONS__H_


/*
	Here are some functions that are needed quite often. Like testing if a file exists...

	Matthias Holl�nder 2008/2009
*/


#include <fstream>


// This will load one line from stream and zero-terminate it, it returns the number of chars
// read in line (NOT the size of szDstBuffer [think of empty lines!!!])
int		MyGetLine(std::ifstream& file_in, char* szDstBuffer, int iDstBufferSize);

// This will check if a file exists
bool		FileExists(const char* szFilename);

char*		strlwr_mine(char* szString);
char*		strlwr_mine(const char* szString);

int		GetRandomBetween(int iMin, int iMax);
float		GetRandomBetween(float fMin, float fMax);

void		ExtractFilePathFromFile( const std::string& strFilename, char* szDestFilePath );



#define MxSafeDelete( x )   if ( x ) { delete x; x = 0; }


#endif


