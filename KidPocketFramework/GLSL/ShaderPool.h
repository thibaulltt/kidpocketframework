#ifndef __SHADERPOOL__H_
#define __SHADERPOOL__H_

/*
	This is one of the important classes: my ShaderPool.
	Every Shader can be retrieved by asking this class. That's why it's a singleton.

	Matthias Holl�nder 2008/2009
*/



#include "ShaderInclude.h"

#include <list>

class ShaderPool
{
private:
	// ------------------------------------------------------------
	// private cstr for singleton
	// ------------------------------------------------------------
	ShaderPool();
public:
	// ------------------------------------------------------------
	// dstr & singleton method
	// ------------------------------------------------------------
	~ShaderPool();
        static ShaderPool*			GetInstance();

	// ------------------------------------------------------------
	// public methods
	// ------------------------------------------------------------
        void					ReloadShaders();


        ShaderPackage * GetFirstPackage(){return myFirstShader;}
        ShaderPackage * GetVolumePackage(){return myVolumeShader;}
        ShaderPackage * GetSurfacePackage(){return mySurfaceShader;}
        ShaderPackage * GetTrMeshPackage(){return myTrMeshShader;}
        ShaderPackage * GetTetrahedraPackage(){return myTetrahedraShader;}

private:
        bool					Initialize( bool bBreakOnError = false );

	// ------------------------------------------------------------
	// private struct
	// ------------------------------------------------------------
	struct ShaderInitializer {
		const char*				m_pszVertexShaderFile;
		const char*				m_pszGeometryShaderFile;
		const char*				m_pszFragmentShaderFile;

		ShaderPackage**		m_pPointerToPrivateMember;			// careful, pointer to pointer!


		ShaderInitializer( const char* pszVertexShaderFile,
						   const char* pszGeometryShaderFile,
						   const char* pszFragmentShaderFile,
						   ShaderPackage** pPtrToPrvtMmbr ):
		m_pszVertexShaderFile( pszVertexShaderFile ),
		m_pszGeometryShaderFile( pszGeometryShaderFile ),
		m_pszFragmentShaderFile( pszFragmentShaderFile ),
		m_pPointerToPrivateMember( pPtrToPrvtMmbr )
		{
		}
	};


	// ------------------------------------------------------------
	// private attributes
	// ------------------------------------------------------------

        ShaderPackage * myFirstShader ;
        ShaderPackage * myVolumeShader ;
        ShaderPackage * mySurfaceShader ;
        ShaderPackage * myTrMeshShader ;
        ShaderPackage * myTetrahedraShader;
        bool					m_bIsInitialized;

	// ------------------------------------------------------------
	// private methods
	// ------------------------------------------------------------
        void					Release();


        // All Shaderpackages in form of ptr to ptr
        std::list< ShaderPackage** >            m_ListPackages;
};








#endif
