#ifndef _GEOMETRY_SHADER__H_
#define _GEOMETRY_SHADER__H_

/*
	A Simple GeometryShader-class. Inherits from the general shader.

	Matthias Holl�nder 2008/2009
*/

#include "ShaderObject.h"

class GeometryShader : public ShaderObject
{
public:
	// ----------------------------------------------------------------
	// Cstr & Dstr
	// ----------------------------------------------------------------
	GeometryShader();
	virtual ~GeometryShader();


private:
	// ----------------------------------------------------------------
	// private static methods
	// ----------------------------------------------------------------
	GLboolean			IsAvailableGeometryShader();

	// ----------------------------------------------------------------
	// private static attributes
	// ----------------------------------------------------------------
        static bool             	m_bCheckedSupport;
        static bool             	m_isAvailable;

};


#endif // _GEOMETRY_SHADER__H_


