#ifndef __SHADER_INCLUDE__H_
#define __SHADER_INCLUDE__H_

/*
	Each class that want's to use a shader should include this class.
	Matthias Holl�nder 2008/2009
*/


#include "ProgramObject.h"
#include "GeometryShader.h"
#include "VertexShader.h"
#include "FragmentShader.h"



// where the hell are those defined? what's the actual header? ( I got those from NVIDIA-website )
#define GEOMETRY_SHADER_EXT									0x8DD9
#define MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_EXT				0x8C29
#define MAX_GEOMETRY_VARYING_COMPONENTS_EXT					0x8DDD
#define MAX_VERTEX_VARYING_COMPONENTS_EXT					0x8DDE
#define MAX_VARYING_COMPONENTS_EXT							0x8B4B
#define MAX_GEOMETRY_UNIFORM_COMPONENTS_EXT					0x8DDF
#define MAX_GEOMETRY_OUTPUT_VERTICES_EXT					0x8DE0
#define MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_EXT			0x8DE1

#define LINES_ADJACENCY_EXT									0x000A
#define LINE_STRIP_ADJACENCY_EXT							0x000B
#define TRIANGLES_ADJACENCY_EXT								0x000C
#define TRIANGLE_STRIP_ADJACENCY_EXT						0x000D

#define FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT			0x8DA8
#define FRAMEBUFFER_INCOMPLETE_LAYER_COUNT_EXT				0x8DA9

#define FRAMEBUFFER_ATTACHMENT_LAYERED_EXT					0x8DA7
#define FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER_EXT			0x8CD4
#define PROGRAM_POINT_SIZE_EXT								0x8642


// ----------------------------------------------------------------
// Helper class to store a set of shaders and a Program together
// ----------------------------------------------------------------
class ShaderPackage
{
public:
	// ----------------------------------------------------------------
	// Cstr & Dstr
	// ----------------------------------------------------------------
	ShaderPackage(VertexShader*   pVertexShader,
				  GeometryShader* pGeometryShader,
				  FragmentShader* pFragmentShader,
				  ProgramObject*  pProgram) :
		m_pVertexShader(pVertexShader),
		m_pGeometryShader(pGeometryShader),
		m_pFragmentShader(pFragmentShader),
		m_pProgram(pProgram)
	{
	}
	ShaderPackage() :
		m_pVertexShader(0),
		m_pGeometryShader(0),
		m_pFragmentShader(0),
		m_pProgram(0)
	{
	}
	~ShaderPackage()
	{
		if (m_pVertexShader) {
			delete m_pVertexShader;
			m_pVertexShader = 0;
		}
		if (m_pGeometryShader) {
			delete m_pGeometryShader;
			m_pGeometryShader = 0;
		}
		if (m_pFragmentShader) {
			delete m_pFragmentShader;
			m_pFragmentShader = 0;
		}
		if (m_pProgram) {
			delete m_pProgram;
			m_pProgram = 0;
		}
	}

	// ----------------------------------------------------------------
	// public methods:
	// ----------------------------------------------------------------
	VertexShader*		GetVertexShader()	{ return m_pVertexShader; }
	GeometryShader*		GetGeometryShader() { return m_pGeometryShader; }
	FragmentShader*		GetFragmentShader() { return m_pFragmentShader; }
	ProgramObject*		GetProgram()		{ return m_pProgram; }

	void				SetVertexShader(VertexShader* pVertexShader)		{ m_pVertexShader = pVertexShader; }
	void				SetGeometryShader(GeometryShader* pGeometryShader)	{ m_pGeometryShader = pGeometryShader; }
	void				SetFragmentShader(FragmentShader* pFragmentShader)	{ m_pFragmentShader = pFragmentShader; }
	void				SetProgramObject(ProgramObject* pProgramObject)		{ m_pProgram = pProgramObject; }
	void				SetAll(VertexShader* pVertexShader,
							   GeometryShader* pGeometryShader,
							   FragmentShader* pFragmentShader,
							   ProgramObject* pProgramObject)
	{
		m_pVertexShader = pVertexShader;
		m_pGeometryShader = pGeometryShader;
		m_pFragmentShader = pFragmentShader;
		m_pProgram = pProgramObject;
	}

private:
	// ----------------------------------------------------------------
	// private attributes
	// ----------------------------------------------------------------
	VertexShader*		m_pVertexShader;
	GeometryShader*		m_pGeometryShader;
	FragmentShader*		m_pFragmentShader;
	
	ProgramObject*		m_pProgram;

};


#endif

