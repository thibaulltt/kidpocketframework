#ifndef OPENGLHEADER_H
#define OPENGLHEADER_H
#define GLEW_STATIC 1

#include <GL/glew.h>														// includes gl, glu
#include <GL/glut.h>
#include <GL/gl.h>
#include <stdio.h>
#include <iostream>

#define GL_BUFFER_OFFSET( i ) ( ( char* ) NULL + ( i ) )

#if defined( _DEBUG ) || defined( DEBUG )
        #define GetOpenGLError() __GetOpenGLError( ( char* )__FILE__, ( int )__LINE__ )
#else
        #define GetOpenGLError()
#endif

inline int __GetOpenGLError ( char* szFile, int iLine )
{
        int    retCode = 0;
        GLenum glErr = glGetError();
        while (glErr != GL_NO_ERROR) {
            std::cout << "GLError in file << " << szFile << " @ line " << iLine << ":" << gluErrorString( glErr ) << std::endl;
                retCode = 1;
                glErr = glGetError();
        }
        return retCode;
}



#endif // OPENGLHEADER_H
