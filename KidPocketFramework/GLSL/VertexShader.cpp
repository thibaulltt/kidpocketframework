#include "VertexShader.h"

// ----------------------------------------------------------------
// Track memory leaks
// ----------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif

// ----------------------------------------------------------------
// Constructor for a VertexShader.
// Initializes the handle to the shader to a vertex shader.
// ----------------------------------------------------------------
VertexShader::VertexShader() :
	ShaderObject()
{
	m_TypeShader = TypeShader_VertexShader;
	m_ShaderHandle = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
}

// ----------------------------------------------------------------
// Destructor for a VertexShader
// ----------------------------------------------------------------
VertexShader::~VertexShader()
{
}

