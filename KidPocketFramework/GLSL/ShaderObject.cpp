#include "ShaderObject.h"
#include "ShaderInclude.h"
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <assert.h>
using namespace std;

// ----------------------------------------------------------------
// pragmas
// ----------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning(disable: 4996)
#endif

// ----------------------------------------------------------------
// Track memory leaks
// ----------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif


// ----------------------------------------------------------------
// Constructor for the ShaderObject
// ----------------------------------------------------------------
ShaderObject::ShaderObject() :
	m_TypeShader(TypeShader_Unknown)
{
}

// ----------------------------------------------------------------
// Destructor for the ShaderObject
// ----------------------------------------------------------------
ShaderObject::~ShaderObject()
{
	glDeleteObjectARB(m_ShaderHandle);
}

// ----------------------------------------------------------------
// Loads and compiles the shader code.
// The shader-code can contain %d and stuff that's why here's
// variable input
// ----------------------------------------------------------------
bool ShaderObject::LoadShader(bool bBreakOnError, const char* szFilename, ...)
{
	// Check for compilation of the shader
	GLint	iCompileStat = -1;

	// Load shader source code
	GLcharARB* szSource = GetFileContents(szFilename);
	if (szSource == 0) {
            std::cout << "Could not retrieve FileContents from file: "<< szFilename << "!" << std::endl;
            assert( 0 );
            return false;
	}

	// Variable parameter stuff:
	size_t sizeSource = strlen(szSource);
	const int iBufferAdditional = 100;
	char* szBuffer = new char[sizeSource + iBufferAdditional];

	va_list list;
	va_start(list, szFilename);
	vsprintf(szBuffer,szSource, list);
	va_end(list);

	// Load source code into shaders
	glShaderSourceARB(m_ShaderHandle, 1, (const char**) &szBuffer, 0);

	// Delete the shader source code
	delete [] szSource;
	delete [] szBuffer;

	// Compile the shader and print out the log file
	glCompileShaderARB(m_ShaderHandle);
	glGetObjectParameterivARB(m_ShaderHandle, GL_OBJECT_COMPILE_STATUS_ARB, &iCompileStat);

	CheckError();

	if ( !iCompileStat ) {
            std::cout << "Failed to Load Shader: " << szFilename << std::endl;
		if ( bBreakOnError ) {
			assert( 0 );
		}
		return false;
	}
	return true;	
}

// ----------------------------------------------------------------
// For loading a shader by just supplying the code
// ----------------------------------------------------------------
bool ShaderObject::LoadShaderChar(bool bBreakOnError, const char* szShaderText)
{
	if ( !szShaderText ) {
		if ( bBreakOnError ) {
			assert(0 && "Empty Shaders not allowed!");
		}
		return false;
	}
	// Check for compilation of the shader
	GLint	iCompileStat = -1;

	// Load source code into shaders
	glShaderSourceARB(m_ShaderHandle, 1, (const char**) &szShaderText, 0);

	// Compile the shader and print out the log file
	glCompileShaderARB(m_ShaderHandle);
	glGetObjectParameterivARB(m_ShaderHandle, GL_OBJECT_COMPILE_STATUS_ARB, &iCompileStat);

	CheckError();

	if ( !iCompileStat ) {
		if (bBreakOnError) {
			assert(0 && "The shader could not be compiled");
		}
		return false;
	}
	return true;
}

// ----------------------------------------------------------------
// Error Output
// ----------------------------------------------------------------
bool ShaderObject::CheckError()
{
    int		iInfoLogLength = 0;
    int		iCharsWritten  = 0;
    char*	szInfoLog = 0;

    glGetObjectParameterivARB(m_ShaderHandle, GL_OBJECT_INFO_LOG_LENGTH_ARB, &iInfoLogLength);
    if (iInfoLogLength > 1) {
        std::cout << "**********************************************" << std::endl;
        switch (m_TypeShader) {
                case TypeShader_VertexShader:
                    std::cout << "Vertex Shader:" << std::endl;
                    break;
                case TypeShader_GeometryShader:
                    std::cout << "Geometry Shader:" << std::endl;
                    break;
                case TypeShader_FragmentShader:
                    std::cout << "Fragment Shader:" << std::endl;
                    break;
                default:
                case TypeShader_Unknown:
                    std::cout << "UNKNOWN SHADER-TYPE:" << std::endl;
                    assert( 0 && "What kind of Shader is this?" );
        }
        std::cout << "**********************************************" << std::endl;

        szInfoLog = new char[iInfoLogLength];
        glGetInfoLogARB( m_ShaderHandle, iInfoLogLength, &iCharsWritten, szInfoLog );

        // log printout
        std::cout << szInfoLog << std::endl;

        delete [] szInfoLog;
        return false;
    }
    return true;
}

// ----------------------------------------------------------------
// Return handle to shader
// ----------------------------------------------------------------
GLhandleARB& ShaderObject::GetShader()
{
	return m_ShaderHandle;
}

// ----------------------------------------------------------------
// Load a text-file
// ----------------------------------------------------------------
char* ShaderObject::GetFileContents(const char* szFilename)
{
	if (!szFilename) return 0;
	char*	szBuffer = 0;

	FILE* pFile = fopen(szFilename, "r");
	if (!pFile) return 0;

	// Get the file's size:
	fseek(pFile, 0, SEEK_END);
	int iCount = ftell(pFile);
	rewind(pFile);
	if (iCount > 0) {
		szBuffer = new char[iCount+1];
		iCount = (int)fread(szBuffer, (int)sizeof(char), iCount, pFile);
		szBuffer[iCount] = '\0';										// append zero at end
	}
	fclose(pFile);
	return szBuffer;
}

