#include "TetraLRISolver.h"

void TetraLRISolver::collect_edges( const Triangulation & triangulation , const std::vector<Triangulation::Cell_handle> & _tetrahedra )
{
    edges.clear();
    for( unsigned int i = 0 ; i < _tetrahedra.size() ; i ++ ){
        const Triangulation::Cell_handle & ch = _tetrahedra[i];

        for( int v = 0 ; v < 4 ; v ++ ){
            for( int vn = v + 1 ; vn < 4 ; vn ++ ){
                std::pair<Triangulation::Vertex_handle, Triangulation::Vertex_handle> edge = std::make_pair(ch->vertex(v), ch->vertex(vn));
                if( std::find( edges.begin(), edges.end(), edge ) == edges.end() ){
                    edges.push_back( edge );
                }
            }
        }
    }
}

void TetraLRISolver::collect_vertices( const std::vector<Triangulation::Cell_handle> & _tetrahedra , unsigned int nb_of_mesh_vertices ){

    verts_mapping_from_mesh_to_solver.clear();
    verts_mapping_from_mesh_to_solver.resize(nb_of_mesh_vertices , -1);

    verts_mapping_from_solver_to_mesh.clear();
    verts_initial_positions.clear();

    for( unsigned int i = 0 ; i < _tetrahedra.size() ; i ++ ){
        const Triangulation::Cell_handle & ch = _tetrahedra[i];

        for( int v = 0 ; v < 4 ; v ++ ){
            const Triangulation::Vertex_handle & vh = ch->vertex(v);
            if( verts_mapping_from_mesh_to_solver[vh->info()] == -1 )
            {
                verts_mapping_from_mesh_to_solver[vh->info()] = verts_mapping_from_solver_to_mesh.size();
                verts_mapping_from_solver_to_mesh.push_back(vh);
                verts_initial_positions.push_back(vh->point());
            }
        }
    }
}

void TetraLRISolver::collect_constraints_vertices( const std::vector<Triangulation::Cell_handle> & tetrahedra, const std::vector<int> & unknown_tets, const std::vector<bool> & unknown_vertices ){

    std::vector<bool> marked_vertices (unknown_vertices.size(), false);
    verts_constraints.clear();

    for( unsigned int i = 0 ; i < unknown_tets.size() ; i ++ ){
        const Triangulation::Cell_handle & ch = tetrahedra[ unknown_tets[ i ] ];

        for( int v = 0 ; v < 4 ; v ++ ){
            unsigned int v_index = ch->vertex(v)->info();
            if( !unknown_vertices[ v_index ] && !marked_vertices[ v_index ]){
                verts_constraints.push_back( verts_mapping_from_mesh_to_solver[ v_index ] );
                marked_vertices[ v_index ] = true;
            }
        }
    }

}

void TetraLRISolver::load_tetrahedra( const Triangulation & triangulation ,
                                      const std::vector<Triangulation::Cell_handle> & _tetrahedra,
                                      const std::vector<int> & unknown_tets, const std::vector<int> & handle_tets ,
                                      const std::vector<bool> & unknown_vertices )
{
    // collect infos:

    unknown_tetrahedra = std::vector<int>(unknown_tets);
    handle_tetrahedra = std::vector<int>(handle_tets);
    tetrahedra = std::vector<Triangulation::Cell_handle>(_tetrahedra);

    collect_edges(triangulation , tetrahedra);
    collect_vertices( tetrahedra, unknown_vertices.size() );
    collect_constraints_vertices( tetrahedra, unknown_tets, unknown_vertices );

    unsigned int neighbor_count = 0 ;
    for( unsigned int i = 0 ; i < unknown_tetrahedra.size() ; i ++ ){
        const Triangulation::Cell_handle & ch = tetrahedra[ unknown_tetrahedra[i] ];
        for( int j = 0 ; j < 4 ; j++ ){
            if( !triangulation.is_infinite(ch->neighbor(j)) && ch->neighbor(j)->subdomain_index() != Subdomain_index() ){
                neighbor_count++;
            }
        }
    }

    // set dimensions:
    BasisSolver.setDimensions( unknown_tets.size() + handle_tets.size() , unknown_tets.size() + handle_tets.size() , 9 );
    VerticesSolver.setDimensions( edges.size() + verts_constraints.size() , verts_mapping_from_solver_to_mesh.size() , 3 );

    // give non zeros in A matrices:
    BasisSolver.specifyNonZeroInA( unknown_tets.size() + neighbor_count + handle_tets.size() );
    VerticesSolver.specifyNonZeroInA( 2*edges.size() + verts_constraints.size() );

    // allocate:
    BasisSolver.allocate();
    VerticesSolver.allocate();

    bool volume_dependant = true;

    if(volume_dependant){
        std::cout << "Volume weights" << std::endl;
        std::vector<float> volumes (tetrahedra.size(), 0.);
        for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++){
            const Triangulation::Cell_handle & ch = tetrahedra[ i ];
            const Point_3 &p0 = ch->vertex(0)->point();
            const Point_3 &p1 = ch->vertex(1)->point();
            const Point_3 &p2 = ch->vertex(2)->point();
            const Point_3 &p3 = ch->vertex(3)->point();

            K::Vector_3 e10 = p1 - p0;
            K::Vector_3 e20 = p2 - p0;

            volumes[ch->info().index()] = fabs(CGAL::cross_product(e10, e20)*(p3 - p0))/6.;
        }

        // set values in BasisSolver's A matrix:
        for( unsigned int i = 0 ; i < unknown_tetrahedra.size() ; i ++ ){
            const Triangulation::Cell_handle & ch = tetrahedra[ unknown_tetrahedra[i] ];
            std::vector<int> neighbors;
            float v_sum = 0.;
            for( int j = 0 ; j < 4 ; j++ ){
                if( !triangulation.is_infinite(ch->neighbor(j)) && ch->neighbor(j)->subdomain_index() != Subdomain_index() ){
                    unsigned int n_index = ch->neighbor(j)->info().index();
                    neighbors.push_back(n_index);
                    v_sum += volumes [ n_index ];
                }
            }
            unsigned int index = ch->info().index();

            BasisSolver.addValueInA( i , index , -1.*volumes[index] );

            for( int j = 0 ; j < neighbors.size() ; j++ ){
                BasisSolver.addValueInA( i , neighbors[j] , (volumes[index]*volumes[ neighbors[j] ])/(double)v_sum );
            }

            for( unsigned int coord = 0 ; coord < 9 ; ++coord )
                BasisSolver.setValueInB( i , coord , 0.0 );
        }

        volumes.clear();

    } else {
        std::cout << "Constant weights" << std::endl;
        // set values in BasisSolver's A matrix:
        for( unsigned int i = 0 ; i < unknown_tetrahedra.size() ; i ++ ){
            const Triangulation::Cell_handle & ch = tetrahedra[ unknown_tetrahedra[i] ];
            std::vector<int> neighbors;
            for( int j = 0 ; j < 4 ; j++ ){
                if( !triangulation.is_infinite(ch->neighbor(j)) && ch->neighbor(j)->subdomain_index() != Subdomain_index() ){
                    neighbors.push_back(ch->neighbor(j)->info().index());
                }
            }
            unsigned int index = ch->info().index();

            BasisSolver.addValueInA( i , index , -1 );

            for( int j = 0 ; j < neighbors.size() ; j++ ){
                BasisSolver.addValueInA( i , neighbors[j] , 1.0/(double)neighbors.size() );
            }

            for( unsigned int coord = 0 ; coord < 9 ; ++coord )
                BasisSolver.setValueInB( i , coord , 0.0 );
        }
    }

    for( unsigned int i = 0 ; i < handle_tetrahedra.size() ; i ++ ){
        const Triangulation::Cell_handle & ch = tetrahedra[ handle_tetrahedra[i] ];

        unsigned int index = ch->info().index();
        BasisSolver.addValueInA( unknown_tetrahedra.size() + i , index , 1.0 );
    }

    // set values in VerticesSolver's A matrix:
    for( unsigned int i = 0 ; i < edges.size() ; i++ ){
        unsigned int id_v0 = verts_mapping_from_mesh_to_solver[ edges[i].first->info()  ];
        unsigned int id_v1 = verts_mapping_from_mesh_to_solver[ edges[i].second->info() ];
        VerticesSolver.addValueInA( i , id_v1 , 1.0 );
        VerticesSolver.addValueInA( i , id_v0 , -1.0 );
    }
    for( unsigned int i = 0 ; i < verts_constraints.size() ; i++ ){
        VerticesSolver.addValueInA( edges.size() + i , verts_constraints[i] , 1.0 );
    }

    // factorization:
    BasisSolver.factorize();
    VerticesSolver.factorize();
}

void TetraLRISolver::update_constraints( const Triangulation & triangulation, const std::vector<bool> & _outlier_vertices )
{
    // set values in B BasisSolver matrix:
    for( unsigned int t = 0 ; t < handle_tetrahedra.size() ; t ++ ){
        const std::vector<BasicPoint> & basis_def = tetrahedra[handle_tetrahedra[t]]->info().basis_def();
        for( int i = 0 ; i < 3 ; i++  )
        {
            for( int j = 0 ; j < 3 ; j++  )
            {
                BasisSolver.setValueInB( unknown_tetrahedra.size() + t, 3*i + j, basis_def[i][j]);
            }
        }
    }

    // solve BasisSolver:
    BasisSolver.solve();
    // get values:
    for( unsigned int t = 0 ; t < unknown_tetrahedra.size() ; t ++ ){
        std::vector<BasicPoint> basis_def(3);
        unsigned int tetra_index_in_solver = tetrahedra[unknown_tetrahedra[t]]->info().index();
        for( int i = 0 ; i < 3 ; i++  )
        {
            for( int j = 0 ; j < 3 ; j++  )
            {
                basis_def[i][j] = BasisSolver.getSolutionValue( tetra_index_in_solver, 3*i + j);
            }
        }
        tetrahedra[unknown_tetrahedra[t]]->info().set_basis_def(basis_def);
    }

    // set values in B VerticesSolver matrix:
    for( unsigned int e = 0 ; e < edges.size() ; ++e )
    {
        Triangulation::Cell_handle ch;
        int v0, v1;

        triangulation.is_edge(edges[e].first, edges[e].second, ch, v0, v1);

        Triangulation::Cell_circulator cell_circulator = triangulation.incident_cells( ch, v0, v1);
        Triangulation::Cell_circulator done(cell_circulator);

        int nTetraOnEdge = 0;
        BasicPoint eTransform(0,0,0);
        do{
            if(!triangulation.is_infinite(cell_circulator)){
                if( cell_circulator->subdomain_index() != Subdomain_index() ){
                    ++nTetraOnEdge;
                    // get tetra def basis:
                    const std::vector<BasicPoint> & def_basis = cell_circulator->info().basis_def();
                    // find new orientation for edge e: eTransform = def_basis * (v1 - v0)
                    K::Vector_3 v0v1 = verts_initial_positions[verts_mapping_from_mesh_to_solver[ edges[e].second->info() ]] -
                                       verts_initial_positions[verts_mapping_from_mesh_to_solver[ edges[e].first->info() ]];
                    for( int i = 0 ; i < 3 ; i ++)
                        for( int j = 0 ; j < 3 ; j ++)
                            eTransform[i] += def_basis[j][i] * v0v1[j];
                }
            }
        }while (++cell_circulator != done);
        eTransform /= nTetraOnEdge;

        for( unsigned int coord = 0 ; coord < 3 ; ++coord )
            VerticesSolver.setValueInB( e , coord , eTransform[coord] );
    }

    for( unsigned int i = 0 ; i < verts_constraints.size() ; i ++ ){
        unsigned int vertexInSolver = verts_constraints[i];
        Point_3 pos = verts_mapping_from_solver_to_mesh[vertexInSolver]->point();
        for( unsigned int coord = 0 ; coord < 3 ; ++coord )
            VerticesSolver.setValueInB( edges.size() + i , coord , pos[coord] );
    }

    // solve:
    VerticesSolver.solve();
    // get values:verts_mapping_from_solver_to_mesh
    double p [3];
    for( unsigned int v = 0 ; v < verts_mapping_from_solver_to_mesh.size() ; ++v ){
        for( unsigned int coord = 0 ; coord < 3 ; ++coord )
            p[coord] = VerticesSolver.getSolutionValue( v,coord );
        Triangulation::Vertex_handle & vh = verts_mapping_from_solver_to_mesh[v];
        if( _outlier_vertices[vh->info()] )
            vh->set_point(Point_3(p[0], p[1], p[2]));
    }
    // free:
    BasisSolver.freeSolution();
    VerticesSolver.freeSolution();
}

void TetraLRISolver::clear(){
    tetrahedra.clear();
    handle_tetrahedra.clear();
    unknown_tetrahedra.clear();

    BasisSolver.free();
    VerticesSolver.free();

    edges.clear();

    verts_mapping_from_solver_to_mesh.clear();
    verts_initial_positions.clear();
    verts_mapping_from_mesh_to_solver.clear();
    verts_constraints.clear();

    tetrahedra.clear();
    handle_tetrahedra.clear();
    unknown_tetrahedra.clear();

}
