#ifndef TEXTURE_H
#define TEXTURE_H

#include "Vec3D.h"
#include "OpenGLHeader.h"
#include "ShaderPool.h"
#include "ShaderInclude.h"

#include <QString>
#include <QColor>
#include <QVector>
#include <QGLViewer/vec.h>
//#include "nifti1_io.h"                  /*** NIFTI-1 header specification ***/


class Texture
{
private :

    GLuint textureId;

    unsigned int n[3];
    float d[3];
    unsigned int gridSize;

    float minD;

    double xCut;
    double yCut;
    double zCut;

    double xCutPosition;
    double yCutPosition;
    double zCutPosition;

    int xCutDirection;
    int yCutDirection;
    int zCutDirection;

    double xMax;
    double yMax;
    double zMax;

    bool xCutDisplay;
    bool yCutDisplay;
    bool zCutDisplay;

    ShaderPool * shaderPool;
    ShaderPackage * shaderPackage;
    ProgramObject * programObject;

    bool textureCreated;

//    std::vector<unsigned char> _labels;
//    std::map<unsigned int, int> _labelIDMap;

public:

    Vec3Di Vmin;
    Vec3Di Vmax;

    Texture();
    ~Texture();

    void init();

    void clear();

    void initTexture();

    void deleteTexture();

    void draw();
    void drawCube();
    void fillCut();
    void drawBoundingBox();
    void drawCutPlanes();

    int getWidth(){return n[0];}
    int getHeight(){return n[1];}
    int getDepth(){return n[2];};
    int getGridSize(){return n[0]*n[2]*n[1];}

    const Vec3Di & getVmin() const { return Vmin; }
    Vec3Di & getVmin() { return Vmin; }
    const Vec3Di & getVmax() const { return Vmax; }
    Vec3Di & getVmax() { return Vmax; }

//    const std::vector<unsigned char> & getLabels() const { return _labels; }
//    int getIdOfLabel( unsigned char label) { std::map<unsigned char, unsigned int>::iterator it =_labelIDMap.find(label);
//                                             if( it != _labelIDMap.end() ) return (int)it->second; else return -1;}

    float dx(){return d[0];}
    float dy(){return d[1];}
    float dz(){return d[2];}

    void setXCut(int _xCut);
    void setYCut(int _yCut);
    void setZCut(int _zCut);

    void invertXCut(){xCutDirection *= -1;}
    void invertYCut(){yCutDirection *= -1;}
    void invertZCut(){zCutDirection *= -1;}

    void setXCutDisplay(bool _xCutDisplay){xCutDisplay = _xCutDisplay;}
    void setYCutDisplay(bool _yCutDisplay){yCutDisplay = _yCutDisplay;}
    void setZCutDisplay(bool _zCutDisplay){zCutDisplay = _zCutDisplay;}

    float getXMax(){return xMax;}
    float getYMax(){return yMax;}
    float getZMax(){return zMax;}

    GLuint getTextureId(){return textureId;}

    float getGridStep(){return minD;}

    void build(const std::vector<unsigned char> & data, const std::vector<unsigned char> & labesl,
               unsigned int & nx , unsigned int & ny , unsigned int & nz,
               float & dx , float & dy , float & dz,
               std::map<unsigned char, QColor> & colorMap );

};

#endif // TEXTURE_H
