#include <iostream>
#include <vector>
#include <cmath>

#include <QPainter>


enum SplitCondition { MAX_POINT, DATA_VALUE };

template < class Point, size_t dim, class Data = int >
class RegularBspTree {
public:
    RegularBspTree(Point const& min, Point const& max, int maxDepth, int maxPoints, SplitCondition condition = MAX_POINT) :
            _cellMin(min),
            _cellMax(max),
            _isLeaf(true),
            _depth(0),
            _maxDepth(maxDepth),
            _maxPoints(maxPoints),
            _childrenCount(std::pow(2, dim)),
            _splitCondition(condition)
    {}

    RegularBspTree() :
            _isLeaf(true),
            _depth(0),
            _maxDepth(10),
            _maxPoints(10),
            _childrenCount(std::pow(2, dim))
    {}


    void getLeafs(std::vector< RegularBspTree<Point, dim, Data>* >& leafs)
    {
        if (!_isLeaf)
        {
            for (int i = 0; i < _childrenCount; ++i)
            {
                _children[i].getLeafs(leafs);
            }
        }
        else
        {
            leafs.push_back(this);
        }
    }

    void setDepth(int d)
    {
        _depth = d;
    }

    bool getIsLeaf() const
    {
        return _isLeaf;
    }

    void setMin(Point const& min)
    {
        _cellMin = min;
    }

    void setMax(Point const& max)
    {
        _cellMax = max;
    }

    void setSplitCondition(SplitCondition const& condition)
    {
        _splitCondition = condition;
    }


    Point const& getMin() const
    {
        return _cellMin;
    }

    Point const& getMax() const
    {
        return _cellMax;
    }

    int const& getMaxPoints() const
    {
        return _maxPoints;
    }

    int const& getDepth() const
    {
        return _depth;
    }

    int const& getMaxDepth() const
    {
        return _depth;
    }

    std::vector< RegularBspTree<Point, dim, Data> > const& getChildren() const
    {
        return _children;
    }

    std::vector<Point> const& getPoints() const
    {
        return _points;
    }

    std::vector<Data> const& getData() const
    {
        return _data;
    }

    SplitCondition const& getSplitCondition() const
    {
        return _splitCondition;
    }
//
//    RegularBspTree<Point, dim, Data > & addPoint(std::vector<Point> const& points, std::vector<Data> const& data = std::vector<Data>())
//    {
//
//    }

    RegularBspTree<Point, dim, Data > & addPoint(Point const& point, Data const& data = Data())
    {        

        if (_isLeaf)
        {
            // add point into cell directly or if full, split the cell
            // and add then
            bool toSplit;
            if(_splitCondition == MAX_POINT){
                toSplit = needsSplitMaxPointCondition();
            } else {
                toSplit = needsSplitDataCondition(data);
            }

            if (toSplit)
            {

                split();

                return addPoint(point, data);
            }
            else
            {
                // std::cout << "Leaf okay, pushing." << std::endl;
                _points.push_back(point);
                _data.push_back(data);

                assert(_points.size() == _data.size());
            }
        }
        else
        {
            // find the right child and add point there
            // std::cout << "Not a leaf, adding to child." << std::endl;
            for (int i = 0; i < _childrenCount; ++i)
            {
                if (_children[i].isPointIn(point))
                {
                    return _children[i].addPoint(point, data);
                }
            }
        }

        return *this;
    }


    bool needsSplitMaxPointCondition(){
        return (int)_points.size() >= _maxPoints && _depth < _maxDepth;
    }

    bool needsSplitDataCondition(const Data & data){

        for(unsigned int i = 0; i < _data.size(); i++){
            if(_data[i] != data){
                return true;
            }
        }
        return false;

    }

    bool isPointIn(Point const& point) const
    {
        // return (point >= _cellMin && point <= _cellMax);

        for (unsigned int i = 0; i < dim; ++i)
        {
            if (point[i] < _cellMin[i] || point[i] > _cellMax[i]) return false;
        }

        return true;
    }

    void split()
    {
        if (!_isLeaf) return;

        _children.resize(_childrenCount, RegularBspTree<Point, dim, Data >(_cellMin, _cellMax, _maxDepth, _maxPoints));

        _isLeaf = false;

        _children[0].setDepth(_depth + 1);

        for (unsigned int d = 1; d <= dim; ++d)
        {
            int spacing = _childrenCount / std::pow(2, d);

            for (unsigned int j = 0; j < std::pow(2, d - 1); ++j)
            {
                int parentIndex = j * spacing * 2;
                int childIndex = parentIndex + spacing;
                RegularBspTree const& parent = _children[parentIndex];

                RegularBspTree leftChild = parent;
                leftChild._children.clear();
                RegularBspTree rightChild = parent;
                rightChild._children.clear();

                leftChild._cellMax[d - 1]  -= 0.5f * (parent._cellMax[d - 1] - parent._cellMin[d - 1]);
                rightChild._cellMin[d - 1] += 0.5f * (parent._cellMax[d - 1] - parent._cellMin[d - 1]);

                _children[parentIndex] = leftChild;
                _children[childIndex] = rightChild;
            }
        }

        for (unsigned int i = 0; i < _points.size(); ++i)
        {
            Point const& point = _points[i];
            Data const& data = _data[i];

            for (int j = 0; j < _childrenCount; ++j)
            {
                if (_children[j].isPointIn(point))
                {
                    _children[j].addPoint(point, data);
                    break;
                }
            }
        }

        _points.clear();
        _data.clear();
    }

    friend std::ostream& operator<< (std::ostream& stream, RegularBspTree const& q)
    {
        stream << q._depth << " " << q._cellMin << " " << q._cellMax << std::endl;

        for (unsigned int i = 0; i < q._points.size(); ++i)
        {
            std::cout << "[" << q._points[i] << "] Data: " << q._data[i] << std::endl;
        }


        if (!q._isLeaf)
        {
            std::cout << "Children:" << std::endl;

            for (int i = 0; i < q._childrenCount; ++i)
            {
                stream << q._children[i] << std::endl;
            }
        }

        return stream;
    }

    RegularBspTree const& query(Point const& point) const
    {
        if (!_isLeaf)
        {
            for (int j = 0; j < _childrenCount; ++j)
            {
                if (_children[j].isPointIn(point))
                {
                    return _children[j].query(point);
                }
            }
        }

        return *this;
    }

protected:
    Point _cellMin;
    Point _cellMax;
    std::vector< RegularBspTree<Point, dim, Data> > _children;

    std::vector<Point> _points;
    std::vector<Data> _data;

    bool _isLeaf;
    int _depth;
    int _maxDepth;
    int _maxPoints;

    int _childrenCount;

    SplitCondition _splitCondition;

};



template <class Point>
void draw(QPainter & p, RegularBspTree<Point, 2> const& quadTree, Point const& min, Point const& max)
{
    // std::cout << "Drawing" << std::endl;

    p.setPen(Qt::black);

    float xRange = max[0] - min[0];
    float yRange = max[1] - min[1];

    QRect rect(
            std::floor((quadTree.getMin()[0] - min[0]) / xRange * p.window().width() + 0.5f),
            std::floor((quadTree.getMin()[1] - min[1]) / yRange * p.window().height() + 0.5f),
            std::floor((quadTree.getMax()[0] - quadTree.getMin()[0]) / xRange * p.window().width() + 0.5f),
            std::floor((quadTree.getMax()[1] - quadTree.getMin()[1]) / yRange * p.window().width() + 0.5f)
            );

    p.drawRect(rect);

    std::vector<Point> const& points = quadTree.getPoints();

    p.setPen(Qt::red);

    for (unsigned int i = 0; i < points.size(); ++i)
    {
        p.drawEllipse(
                (points[i][0] - min[0]) / xRange * p.window().width(),
                (points[i][1] - min[1]) / xRange * p.window().width(),
                2, 2);
    }

    if (!quadTree.getIsLeaf())
    {
        std::vector<RegularBspTree<Point, 2> > const& children = quadTree.getChildren();

        for (unsigned int i = 0; i < children.size(); ++i)
        {
            draw(p, children[i], min, max);
        }
    }
}

template <class Point>
void draw(RegularBspTree<Point, 3> const& octree)
{

    //std::cout <<"draw"<< std::endl;

    if (octree.getIsLeaf()){
        //std::cout <<"is leaf"<< std::endl;
        Point diff = octree.getMax() - octree.getMin();
        Point center = octree.getMin() + diff/2.;

        glPolygonMode(GL_FRONT_AND_BACK , GL_LINE );
        glEnable( GL_DEPTH_TEST );
        glEnable(GL_DEPTH);


        glPushMatrix();
        glTranslatef(center[0], center[1], center[2]);
        glScalef(diff[0], diff[1], diff[2]);
        glDisable(GL_LIGHTING);
        glColor3f(0.f,0.f,1.f);
        Point Min(-0.5f,-0.5f,-0.5f);
        Point Max(0.5f,0.5f,0.5f);

        Point points[8] = {Min,
                                Point (Max[0], Min[1], Min[2]),
                                Point (Max[0], Max[1], Min[2]),
                                Point (Min[0], Max[1], Min[2]),

                                Point (Min[0], Min[1], Max[2]),
                                Point (Max[0], Min[1], Max[2]),
                                Max,
                                Point (Min[0], Max[1], Max[2])};



        glBegin(GL_QUADS);

        glNormal3f(0.f,0.f,-1.f);
        glVertex(points[3]); glVertex(points[2]); glVertex(points[1]); glVertex(points[0]);
        glNormal3f(1.f,0.f,0.f);
        glVertex(points[1]); glVertex(points[2]); glVertex(points[6]); glVertex(points[5]);
        glNormal3f(0.f,1.f,0.f);
        glVertex(points[2]); glVertex(points[3]); glVertex(points[7]); glVertex(points[6]);
        glNormal3f(-1.f,0.f,0.f);
        glVertex(points[3]); glVertex(points[0]); glVertex(points[4]); glVertex(points[7]);
        glNormal3f(0.f,-1.f,0.f);
        glVertex(points[0]); glVertex(points[1]); glVertex(points[5]); glVertex(points[4]);
        glNormal3f(0.f,0.f,1.f);
        glVertex(points[5]); glVertex(points[6]); glVertex(points[7]); glVertex(points[4]);

        glEnd();
        glPolygonMode(GL_FRONT_AND_BACK , GL_FILL );

        const std::vector<Point> & positions = octree.getPoints();

        float norm = diff.norm();

        glBegin(GL_POINTS);
        for(unsigned int i = 0 ; i < positions.size() ; i ++){

            const Point & point = positions[i];
            glColor3f(point[0]/norm, point[1]/norm, point[2]/norm);
            glVertex(positions[i]);
        }

        glEnd();


        glPopMatrix();

        glDisable( GL_DEPTH_TEST );
        glDisable(GL_DEPTH);




    }else {

        std::vector<RegularBspTree<Point, 3> > const& children = octree.getChildren();
        for (unsigned int i = 0; i < children.size(); ++i)
        {
            draw(children[i] );
        }

    }
}
