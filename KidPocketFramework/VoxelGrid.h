#ifndef VOXELGRID_H
#define VOXELGRID_H

#include <QGLContext>

#include "CGALIncludes.h"
#include <map>
#include <vector>
#include <iostream>
#include "BasicPoint.h"
#include "RegularBspTree.h"
#include "Tetrahedron.h"
#include "Triangle.h"
#include "Voxel.h"
#include "SeparationInfo.h"

#include <QGLContext>
#include <QGLViewer/qglviewer.h>
#define DEFAULT_VOFFSET 0

typedef char GRID_TYPE;

// -------------------------------------------------
// Intermediate Voxel structure for hashed adjacency
// -------------------------------------------------

class VoxelGrid
{
public:


	VoxelGrid() : _offset(BasicPoint(0.,0.,0.))
	{
		init(0, 0, 0, 1.,1.,1.);
	}

   // ~VoxelGrid(){clear(); }
	//TODO destructor!!!

    VoxelGrid(const QOpenGLContext * icontext, const std::vector<GRID_TYPE> & data, unsigned int x,unsigned int y, unsigned int z, float _dx, float _dy, float _dz,
              BasicPoint offset = BasicPoint(0.,0.,0.), int _VOffset = DEFAULT_VOFFSET);

    VoxelGrid(const QOpenGLContext * icontext, const std::vector<GRID_TYPE> & data, unsigned int x,unsigned int y, unsigned int z, float _dx, float _dy, float _dz,bool sort,
              BasicPoint offset = BasicPoint(0.,0.,0.), int _VOffset = DEFAULT_VOFFSET);


    VoxelGrid(const QOpenGLContext * icontext, unsigned int nx,unsigned int ny, unsigned int nz, float dx, float dy, float dz, bool allocate_memory, BasicPoint offset = BasicPoint(0.,0.,0.), int _VOffset = DEFAULT_VOFFSET):
            _offset(offset),
            VOffset(_VOffset){
        setContext(icontext);
        init(nx, ny, nz, dx, dy, dz, allocate_memory);
    }

	VoxelGrid(const VoxelGrid & voxelGrid){
		init(voxelGrid._dim[0], voxelGrid._dim[1], voxelGrid._dim[2], voxelGrid._d[0], voxelGrid._d[1], voxelGrid._d[2]);

		voxels = voxelGrid.voxels;
		positions = std::vector<BasicPoint>(voxelGrid.positions);
		sortedVoxels = voxelGrid.sortedVoxels;
		subdomain_indices = voxelGrid.subdomain_indices;
		colors = voxelGrid.colors;
		BBMin = voxelGrid.BBMin;
		BBMax = voxelGrid.BBMax;
		VOffset = voxelGrid.VOffset;
		clippingNormal = voxelGrid.clippingNormal;
		pointOnClipping = voxelGrid.pointOnClipping;

		_max = voxelGrid._max;
		_size = voxelGrid._size;

		grid_data = voxelGrid.grid_data;

		BasicPoint max (dx()/2., dy()/2., dz()/2.);
		BasicPoint min (-dx()/2., -dy()/2., -dz()/2.);
		vnumbers = voxelGrid.vnumbers;
//        initTextures( min, max );
//        computeTranslationData();

    }
    void setContext( const QOpenGLContext * icontext ) { input_context = (QOpenGLContext *)icontext; }
    void clearVBOs();
    inline float square( float x ){ return x*x ;}

	inline float wendland_kernel( float x , float h )
	{
		if( x > h )
			return 0.f;

		return square( square( 1.f - x/h))*(4*x/h+1);
	}

	void createDisplayLists();
	void createDisplayList( unsigned int i );
	void clearDisplayLists();

	void buildForDisplay( );

	void computeEnvelopVoxels();
	void meshGrid(std::vector<BasicPoint> & points, std::vector<Triangle> & triangles, std::vector<Tetrahedron> & tets);
	int getIndice(int i, int j , int k);

	unsigned int getSubdomainNumber(){ return subdomain_indices.size(); }
	void getSubdomainIndices(std::vector<Subdomain_index> & _subdomain_indices);
	GRID_TYPE getGridTypeValue(Subdomain_index si);

	Subdomain_index getSubdomainIndex(GRID_TYPE g_value);

	//const std::vector<Subdomain_index> & getSubdomainIndices() const{ return subdomain_indices;}

	std::vector< Voxel > & getVoxels(){ return voxels ; }
	std::vector< Voxel > const & getVoxels() const { return voxels ; }

	std::vector< BasicPoint > & getPositions(){ return positions ; }
	std::vector< BasicPoint > const & getPositions() const { return positions ; }

	const BasicPoint & getOffset() const { return _offset; }

	bool is_in_domains(unsigned int i, unsigned int j, unsigned int k){ GRID_TYPE v = value(i,j,k); if( v <= BACKGROUND_GRID_VALUE ) return false; return true; }
	bool is_in_offseted_domains(unsigned int i, unsigned int j, unsigned int k){ GRID_TYPE v = value(i,j,k); if( v < BACKGROUND_GRID_VALUE ) return false; return true; }
	bool is_in_offseted_domains(unsigned int i){ GRID_TYPE v = value(i); if( v < BACKGROUND_GRID_VALUE ) return false; return true; }
	unsigned int xdim(){ return _dim[0]; }
	unsigned int ydim(){ return _dim[1]; }
	unsigned int zdim(){ return _dim[2]; }

	unsigned int size(){ return _size; }

	unsigned int dim(int i){ assert(i >= 0 && i < 3); return _dim[i];}

	double dx(){ return _d[0]; }
	double dy(){ return _d[1]; }
	double dz(){ return _d[2]; }

	const BasicPoint & offset(){ return _offset ; }

	std::vector<GRID_TYPE> & data(){ return grid_data; }
	const std::vector<GRID_TYPE> & data() const { return grid_data; }

	double d(int i){ assert(i >= 0 && i < 3); return _d[i];}

	void getBinaryImage(Image & img);
	void getGridImage(Image & img);

	void getDistanceFiels( std::vector<float> &d_field );
	float getRadius(){return (BBMax-BBMin).norm()/2.;}
	BasicPoint getCenter(){return (BBMax-BBMin)/2.;}

	const BasicPoint & getBBMax(){return BBMax;}
	const BasicPoint & getBBMin(){return BBMin;}
	const BasicPoint & getOffset(){return _offset;}
	int getVOffset(){return VOffset; }
	const Voxel & getVoxelOffset(){return voffset; }
	void computeBoundingBox();
	void setClipeEquation(const BasicPoint & clipN, const BasicPoint & pointN){ clippingNormal = clipN; clippingNormal.normalize(); pointOnClipping = pointN; }
	void setOrthongonalCut(const BasicPoint & _cut, const BasicPoint & _cutDirection){ cut = _cut; cutDirection = _cutDirection; }
	void rasterize(VoxelGrid & deformed_grid, double resolution);
	void rasterizeClosest(VoxelGrid & deformed_grid, unsigned int neighborhoodSize, double resolution);
	void rasterizeUsingVectorFields(VoxelGrid & deformed_grid, unsigned int neighborhoodSize, double resolution);

	void draw(bool voxelColor, const std::map<Subdomain_index, bool> & displayMap, const std::map<Subdomain_index, QColor> & colorMap );
	void drawVBO(bool solid, bool voxelColor, bool displayBC, const std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap );
	void drawPoints(std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap);
	void drawBoundingBox();

	void dilatation( int voffsetNb , unsigned int dx, unsigned int dy, unsigned int dz, const Voxel & voffset, bool s_changed);

	void downSample();
	GRID_TYPE value( unsigned int i ) const ;
	GRID_TYPE originalValue( unsigned int i ) { return value(i) - BACKGROUND_GRID_VALUE; }

	//std::vector<GRID_TYPE> & getSegmentation(){return segmentation;}
	//const std::vector<GRID_TYPE> & getSegmentation(){return segmentation;}

	void setSegmentation(const std::vector<GRID_TYPE> &segmentation);
	void saveInfoToFile(const std::string & filename);

	int getVoxelNumber(const Subdomain_index si );

	void compareVolumeLossWithGrid(  VoxelGrid & grid , std::vector< float > & weight_diff);
	void computeElongation(const std::vector<BasicPoint> & defPositions, std::vector<int> & values, float & min_value, float & max_value);

	void clear();
	void clearAll(){grid_data.clear();    clearVBOs();
		clearDisplayLists();clear();}

	void setColors( const std::vector<QColor> & _colors );
	void setWhite();

	void computeDefaultColors();

	void checkForOutliers();
	bool ignoreOutliers();
	void drawOutliers();
	void computeDistortion(const std::vector<BasicPoint> & defPositions, std::vector<int> & values, float & min_value, float & max_value);
	void computeDistortion(const std::vector<BasicPoint> &defPositions, std::vector<float> &distortions);
	void getDeformedVoxelGrid(VoxelGrid & voxelGrid, const BasicPoint & bbmin, const BasicPoint & bbmax);

	void projectInRegularGrid(VoxelGrid & deformed_grid, std::map<unsigned int, unsigned int> & VMap, double resolution=1., bool sort = false);

	float computeMinPointDist();

	void addNewSeparation(Separation_info & Separation_info, int value);
	void dilatation(Separation_info &separation_info, int value);

	std::vector <QColor> & getColors(){return colors; }
	BasicPoint getWorldCoordinate( unsigned int i, unsigned int j, unsigned int k );
	void setValue( unsigned int i, unsigned int j, unsigned int k, const GRID_TYPE & v );
	void sort();
	void sortSeg();

	GRID_TYPE value(const BasicPoint & point )const { Voxel v = getGridCoordinate(point); if(isInGrid(v))return value(v); return DEFAULT_GRID_VALUE;}
	GRID_TYPE value( unsigned int i, unsigned int j, unsigned int k ) const;

	void computePositions();
	unsigned int getGridIndex(unsigned int i, unsigned int j, unsigned int k ) const;
	void clearToDefault();
	Voxel getGridCoordinate(const BasicPoint & point) const ;
	void getNeighborhoodBorns(const Voxel & voxel, Voxel & v_born_min, Voxel & v_born_max, int kernel_size = 1 );
	unsigned int getGridIndex(const Voxel & voxel ) const ;
	void addAndUpdateVoxel(Voxel & v , GRID_TYPE i){sortedVoxels[i].push_back(voxels.size()); voxels.push_back(v); positions.push_back(getWorldCoordinate(v)); }
	void setValue( unsigned int i, const GRID_TYPE & v );
	bool isInGrid(const Voxel & voxel)const;

	Subdomain_index getInitialSubdomainIndex(unsigned int i){
		return value(i) - BACKGROUND_GRID_VALUE;
	}
	bool drawNice;
	bool getDrawNice(){ return drawNice; }
	void setDrawNice(bool _drawNice){ drawNice = _drawNice;
		if(drawNice && !textureCreated) {
			BasicPoint max (dx()/2., dy()/2., dz()/2.);
			BasicPoint min (-dx()/2., -dy()/2., -dz()/2.);

			initTextures( min, max );
			computeTranslationData();
		}
		drawNice = _drawNice;
	}

	void set_visibility_check(bool _v_check){ visibility_check = _v_check; }

	bool updateOffset( int _Voffset, VoxelGrid & segmentation );
	static GRID_TYPE DEFAULT_GRID_VALUE;
	static GRID_TYPE BACKGROUND_GRID_VALUE;
	void sortAndInit(const std::vector<GRID_TYPE> & data);

	void applyTrilinearInterpolation();


	bool changeLabelValue( Subdomain_index from, Subdomain_index to );
	void computeVNumbers( std::map<Subdomain_index, int> & vNbs);
	int counter;

	std::vector<Voxel> & getEnvelopVoxels(){return envelopVoxels; }
	void getSegmentation(VoxelGrid & segmentation);
	void automaticSeparation();
	bool isPairToSeparate( Subdomain_index _l1, Subdomain_index _l2 );

	inline void glVertex( BasicPoint const & p )
	{
		glVertex3f( p[0] , p[1] , p[2] );
	}
	inline void glNormal( BasicPoint const & p )
	{
		glNormal3f( p[0] , p[1] , p[2] );
	}
protected:

	void makeCurrent() { input_context->makeCurrent() ; }
	void doneCurrent() { input_context->doneCurrent() ; }

	void computeTranslationData();

	GLuint createVBO(const void* data, int dataSize, GLenum target, GLenum usage);
	void deleteVBO(const GLuint vboId);

	void initTextures(const BasicPoint & min, const BasicPoint & max );

	GLuint translationTexture;
	GLuint verticesVBOId;
	GLuint normalsVBOId;
	GLuint indicesVBOId;



	int tWidth;
	int tHeight;

	std::vector<unsigned int> offsets;
	std::vector<Voxel> envelopVoxels;
	std::vector<std::pair<int, int> > VBOInfos;
	std::vector<int> userOffsets;

	void init( unsigned int nx, unsigned int ny, unsigned int nz, float dx, float dy, float dz, bool allocate_memory = true );
	void clearToZero();



	bool ajustSizeToOffset( );

	void getDeformedVoxelGrid(VoxelGrid & voxelGrid, double resolution);


	bool dilatate();
	bool dilatate(const Voxel & voxel);
	bool dilatate(unsigned int i , unsigned int j, unsigned int k);
	bool dilatation( const std::vector<int> & envelopVoxels );

	GRID_TYPE value( const Voxel & voxel ) const ;

	void setValue( const Voxel & voxel, const GRID_TYPE & v );



	void getNeighborhoodBorns(int i, int j, int k, Voxel & v_born_min, Voxel & v_born_max, int kernel_size = 1 );

	void collectSixNeighborhoodVoxels(const Voxel & voxel, std::vector<Voxel> & neighbors);
	void collectSixNeighborhoodVoxels(unsigned int i, unsigned int j, unsigned int k, std::vector<Voxel> & neighbors);


	bool isInGrid(int i, int j, int k)const;
	bool isInGrid(int value)const;

	BasicPoint getWorldCoordinate(const Voxel & voxel);


	BasicPoint cut;
	BasicPoint cutDirection;

	void drawCube(unsigned int vId);
	void drawCube(const BasicPoint & center);
	bool isVisiblePoint( const BasicPoint & point );

	void findGoodTexSize( unsigned int uiNumValuesNeeded, int& iTexWidth, int& iTexHeight, bool bUseAlphaChannel );

	int getNumberOfBoundaryVoxels();

	int round(float r);

	std::vector<GRID_TYPE> grid_data;
	std::vector<Voxel> voxels;
	std::vector<BasicPoint> positions;

	std::vector<std::vector<int> > sortedVoxels;

	std::vector<GRID_TYPE> subdomain_indices;
	std::vector<Subdomain_index> initial_subdomain_indices;
	std::vector<int> vnumbers;
	std::vector<QColor> colors;

	BasicPoint BBMin;
	BasicPoint BBMax;

	Voxel Vmin;
	Voxel Vmax;

	BasicPoint clippingNormal;
	BasicPoint pointOnClipping;

	BasicPoint _offset;

	unsigned int _dim[3];
	float _d[3];
	unsigned int _size;
	float _max;

	int VOffset;

	Voxel voffset;

	std::vector<int> outliers;

 //   std::map<Subdomain_index, unsigned int> subdomain_indices_to_indices;

	std::vector <GLuint> displayListIds;
	QOpenGLContext * input_context;

	bool textureCreated;
	bool visibility_check;

	GLfloat * colors_array;

	BasicPoint DisplayListOffset;


	//double time ;

};

#endif // VOXELGRID_H
