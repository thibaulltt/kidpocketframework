#ifndef MESH_COMPLEX_3_H
#define MESH_COMPLEX_3_H

#include "CGAL/Mesh_complex_3_in_triangulation_3.h"

/**
 * @class Mesh_complex_3
 * @brief A data-structure to represent and maintain a 3D complex embedded
 * in a 3D triangulation.
 */


template<typename Tr>
class Mesh_complex_3 : public CGAL::Mesh_complex_3_in_triangulation_3<Tr>
{
public:


    typedef typename CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
    //-------------------------------------------------------
    // MeshComplex_2InTriangulation3 types
    //-------------------------------------------------------
    // Triangulation types
    typedef Tr                              Triangulation;
    typedef typename C3t3::Vertex_handle    Vertex_handle;
    typedef typename C3t3::Cell_handle      Cell_handle;
    typedef typename C3t3::Facet            Facet;
    typedef typename C3t3::Edge             Edge;
    typedef typename C3t3::size_type        size_type;

    typedef typename C3t3::Cell_iterator    Cell_iterator;
    typedef typename C3t3::Facet_iterator   Facet_iterator;
    //-------------------------------------------------------
    // MeshComplex_3InTriangulation3 types
    //-------------------------------------------------------
    // Indices types
    typedef typename C3t3::Subdomain_index  Subdomain_index;
    typedef typename C3t3::Surface_index    Surface_index;
    typedef typename C3t3::Index          Index;


    Mesh_complex_3() : C3t3() {}
    Mesh_complex_3(const C3t3 & c3t3) : C3t3 ( c3t3 ) {}

    void sort(){
        sortedCells.clear();

        for( Cell_iterator cit = C3t3::cells_begin() ; cit != C3t3::cells_end() ; cit++ ){
             sortedCells[C3t3::subdomain_index(cit)] = cit;
        }

        for( Facet_iterator fit = C3t3::facets_begin() ; fit != C3t3::facets_end() ; fit++ ){
             sortedFacets[C3t3::surface_index(fit)] = fit;
        }

    }

protected:

    std::map< Subdomain_index, std::vector<Cell_handle> > sortedCells;
    std::map< Surface_index, std::vector<Facet> > sortedFacets;

    //Tr tr;
};

#endif // MESH_COMPLEX_3_H
