#########
######### Dependencies needed by the project :
#########

# Folder where boost, cgal and their compiled versions are located :
GET_FILENAME_COMPONENT(PROJ_ROOT ${CMAKE_SOURCE_DIR} DIRECTORY)
SET(EXT_DIR ${PROJ_ROOT}/external)

# CGAL, version 4.10, installed locally {{{
IF(EXISTS ${EXT_DIR}/compiled/cgal/lib)
	SET(CGAL_DIR ${EXT_DIR}/compiled/cgal)
ENDIF()
# }}}
# Boost 1.65, installed locally {{{
IF(EXISTS ${EXT_DIR}/compiled/boost/lib)
	SET(BOOST_ROOT ${EXT_DIR}/compiled/boost)
	SET(Boost_NO_BOOST_CMAKE TRUE)
	SET(Boost_INCLUDE_DIR ${EXT_DIR}/boost-1.65)
ENDIF()
# }}}
# Find the libraries needed by the project {{{
INCLUDE(cmake/Find_Libraries.cmake)
# Note : They are all stored in ${ALL_NEEDED_LIBS}
# }}}

# Find Boost version 1.65 (locally) with components :
FIND_PACKAGE(Boost 1.65.0 EXACT REQUIRED COMPONENTS system thread)
# Find CGAL version 4.10 (locally) with components :
FIND_PACKAGE(CGAL 4.10.1000 EXACT REQUIRED COMPONENTS Core ImageIO)
# Find Qt5 (any version) with components :
FIND_PACKAGE(Qt5 REQUIRED COMPONENTS Core Gui Widgets Xml OpenGL)
# Find other needed packages :
FIND_PACKAGE(OpenGL REQUIRED)
FIND_PACKAGE(GLUT REQUIRED)
FIND_PACKAGE(GLEW REQUIRED)
FIND_PACKAGE(nifti REQUIRED)
FIND_PACKAGE(lapack REQUIRED)

# Were Boost and CGAL found ? Error if not. {{{
IF(NOT Boost_FOUND)
	MESSAGE(STATUS "Boost not found")
	MESSAGE(FATAL_ERROR "Boost was not found. The project cannot be built.\nAdd -DBoost_DEBUG:BOOL=TRUE to your next CMake run to see where the error occured.\nNo binaries or build trees were produced.")
ENDIF()

IF(NOT CGAL_FOUND)
	MESSAGE(STATUS "CGAL not found")
	MESSAGE(FATAL_ERROR "Boost was not found. The project cannot be built.\nAdd -DBoost_DEBUG:BOOL=TRUE to your next CMake run to see where the error occured.\nNo binaries or build trees were produced.")
ENDIF()

MESSAGE(STATUS "Using CGAL v${CGAL_VERSION}")
MESSAGE(STATUS "Using Boost v${Boost_VERSION}")
# }}}

# Include directories {{{
SET(INCLUDE_DIRECTORIES ${INCLUDE_DIRECTORIES}
	${EXT_DIR}/compiled/cgal/include
	${Boost_INCLUDE_DIRS}
	#${GLUT_INCLUDE_DIRS}
	${GLEW_INCLUDE_DIRS}
	#${OpenGL_INCLUDE_DIRS}
	${Qt5Core_INCLUDE_DIRS}
	${Qt5Gui_INCLUDE_DIRS}
	${Qt5Widgets_INCLUDE_DIRS}
	${Qt5OpenGL_INCLUDE_DIRS}
	${Qt5Xml_INCLUDE_DIRS}

	# Local directories to add to the compilation :
	${CMAKE_SOURCE_DIR}/CageDeform
	${CMAKE_SOURCE_DIR}/GLSL
)
INCLUDE_DIRECTORIES(${INCLUDE_DIRECTORIES})
# }}}

