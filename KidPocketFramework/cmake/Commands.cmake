#########
######### File : Commands.cmake
######### Declare some post-build or post-link commands, and put them in a
######### function to be able to add them to any target.
#########

SET(NOTIF_OPTIONS "")
# Check for cmake icon existence :
IF(EXISTS /usr/share/icons)
	FIND_PATH(CMAKE_ICON_PATH NAMES CMakeSetup.png PATHS /usr/share/icons $ENV{HOME}/.local/share/icons)
	IF (CMAKE_ICON_PATH)
		IF(EXISTS ${CMAKE_ICON_PATH}/CMakeSetup.png)
			MESSAGE(STATUS "CMake icon found at >>${CMAKE_ICON_PATH}<<")
			SET(NOTIF_OPTIONS "--icon=${CMAKE_ICON_PATH}/CMakeSetup.png")
		ELSE()
			MESSAGE(STATUS "Did not find CMakeSetup.png in ${CMAKE_ICON_PATH}")
		ENDIF()
	ELSE()
		MESSAGE(STATUS "No CMake icon path found.")
	ENDIF()
ENDIF()

# Find the notification program
FIND_PROGRAM(NOTIFYSEND NAME notify-send)
MESSAGE(STATUS "Find_Program returned : ${NOTIFYSEND}")

SET(NOTIFICATIONS_ENABLED TRUE)

IF(NOTIFYSEND AND EXISTS ${NOTIFYSEND})
	MESSAGE(STATUS "Build notification enabled : notify-send found at ${NOTIFYSEND}.")
ELSE()
	MESSAGE(STATUS "Build notification disabled : notify-send not found.")
	SET(NOTIFICATIONS_ENABLED FALSE)
ENDIF()

FUNCTION(enable_notifications _target)
	IF(NOTIFICATIONS_ENABLED)
		MESSAGE(STATUS "Enabled notifications for target ${_target} with program ${NOTIFYSEND}")
		ADD_CUSTOM_COMMAND(TARGET ${_target} POST_BUILD COMMAND ${NOTIFYSEND} ${NOTIF_OPTIONS} "${_target} : done" "Target ${_target} is built !" USES_TERMINAL)
	ENDIF()
ENDFUNCTION()

# Copies the output of the given target to the source dir, post-build.
FUNCTION(copy_to_source target)
	ADD_CUSTOM_COMMAND(TARGET ${target} POST_BUILD
		COMMAND cp $<TARGET_FILE:${target}> ${CMAKE_SOURCE_DIR}/$<TARGET_FILE_NAME:${target}>
		COMMAND_EXPAND_LISTS
	)
ENDFUNCTION()
