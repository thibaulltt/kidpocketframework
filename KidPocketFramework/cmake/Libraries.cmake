# Interface for build options (link this with target to set c++ standard / compile-time options etc...)
ADD_LIBRARY(project_build_options INTERFACE)
TARGET_COMPILE_FEATURES(project_build_options INTERFACE cxx_std_14)

# Add multiple GCC/clang warning flags to the project
#add_compiler_warnings(project_build_options)

TARGET_LINK_LIBRARIES(project_build_options
	INTERFACE ${ALL_NEEDED_LIBS}
	INTERFACE -I${EXT_DIR}/compiled/cgal/include
	INTERFACE -I${EXT_DIR}/CGAL-4.10/include
)

