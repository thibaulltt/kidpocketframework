#####################################################
#####################################################
#####################################################
######    This file finds all the necessary    ######
######        libraries for the project.       ######
#####################################################
#####################################################
#####################################################

# Handy variable for all default paths for dynamic libraries :
SET(DLL_PATHS /usr/lib /usr/lib32 /usr/lib64 /usr/local/lib)

# OpenGL Libraries {{{
############
############ libQGLViewer
############
FIND_LIBRARY(QGLViewer_LIBRARY NAMES libQGLViewer-qt5.so PATHS ${DLL_PATHS})
IF(NOT QGLViewer_LIBRARY OR NOT EXISTS ${QGLViewer_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find QGLViewer library, set the path to a *.so file")
ENDIF()

############
############ GLEW
############
FIND_LIBRARY(glew_LIBRARY NAMES libGLEW.so PATHS ${DLL_PATHS})
IF(NOT glew_LIBRARY OR NOT EXISTS ${glew_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"GLEW\" library in ${DLL_PATHS}")
ENDIF()

############
############ GLU
############
FIND_LIBRARY(glu_LIBRARY NAMES libGLU.so PATHS ${DLL_PATHS})
IF(NOT glu_LIBRARY OR NOT EXISTS ${glu_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"GLU\" library in ${DLL_PATHS}")
ENDIF()

SET(OPENGL_ALL_LIBS ${QGLViewer_LIBRARY} ${glew_LIBRARY} ${glu_LIBRARY})
# }}}

# Mathematical all-purpose libs {{{
############
############ blas
############
FIND_LIBRARY(blas_LIBRARY NAMES libblas.so PATHS ${DLL_PATHS})
IF (NOT blas_LIBRARY OR NOT EXISTS ${blas_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"blas\" library in ${DLL_PATHS}")
ENDIF()

############
############ cblas
############
FIND_LIBRARY(cblas_LIBRARY NAMES libcblas.so PATHS ${DLL_PATHS})
IF (NOT cblas_LIBRARY OR NOT EXISTS ${cblas_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"cblas\" library in ${DLL_PATHS}")
ENDIF()

############
############ libGSL
############
FIND_LIBRARY(gsl_LIBRARY NAMES libgsl.so PATHS ${DLL_PATHS})
IF(NOT gsl_LIBRARY OR NOT EXISTS ${gsl_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"gsl\" library in ${DLL_PATHS}")
ENDIF()

############
############ MPFR
############
FIND_LIBRARY(mpfr_LIBRARY NAMES libmpfr.so PATHS ${DLL_PATHS})
IF(NOT mpfr_LIBRARY OR NOT EXISTS ${mpfr_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"mpfr\" library in ${DLL_PATHS}")
ENDIF()

############
############ GMP
############
FIND_LIBRARY(gmp_LIBRARY NAMES libgmp.so PATHS ${DLL_PATHS})
IF(NOT gmp_LIBRARY OR NOT EXISTS ${gmp_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"gmp\" library in ${DLL_PATHS}")
ENDIF()

############
############ GMPxx
############
FIND_LIBRARY(gmpxx_LIBRARY NAMES libgmpxx.so PATHS ${DLL_PATHS})
IF(NOT gmpxx_LIBRARY OR NOT EXISTS ${gmpxx_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"gmpxx\" library in ${DLL_PATHS}")
ENDIF()

SET(MATH_ARITHMETIC_ALL_LIBS ${blas_LIBRARY} ${cblas_LIBRARY} ${gsl_LIBRARY} ${mpfr_LIBRARY} ${gmp_LIBRARY} ${gmpxx_LIBRARY})
# }}}

# SuiteSparse : [GPU computed] Matrix mathematical libraries {{{
############
############ cholmod
############
FIND_LIBRARY(choldmod_LIBRARY NAMES libcholmod.so PATHS ${DLL_PATHS})
IF(NOT choldmod_LIBRARY OR NOT EXISTS ${choldmod_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"cholmod\" library in ${DLL_PATHS}")
ENDIF()

############
############ AMD (from suitesparse, same as cholmod)
############
FIND_LIBRARY(amd_LIBRARY NAMES libamd.so PATHS ${DLL_PATHS})
IF(NOT amd_LIBRARY OR NOT EXISTS ${amd_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"amd\" library in ${DLL_PATHS}")
ENDIF()

############
############ CCOLAMD
############
FIND_LIBRARY(ccolamd_LIBRARY NAMES libccolamd.so PATHS ${DLL_PATHS})
IF(NOT ccolamd_LIBRARY OR NOT EXISTS ${ccolamd_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"ccolamd\" library in ${DLL_PATHS}")
ENDIF()

############
############ COLAMD
############
FIND_LIBRARY(colamd_LIBRARY NAMES libcolamd.so PATHS ${DLL_PATHS})
IF(NOT colamd_LIBRARY OR NOT EXISTS ${colamd_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"colamd\" library in ${DLL_PATHS}")
ENDIF()

############
############ CAMD
############
FIND_LIBRARY(camd_LIBRARY NAMES libcamd.so PATHS ${DLL_PATHS})
IF(NOT camd_LIBRARY OR NOT EXISTS ${camd_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"camd\" library in ${DLL_PATHS}")
ENDIF()

SET(SUITESPARSE_ALL_LIBS ${amd_LIBRARY} ${camd_LIBRARY} ${ccolamd_LIBRARY} ${colamd_LIBRARY} ${choldmod_LIBRARY})
# }}}

# Local boost libs {{{
SET(LOCAL_PATHS ${CMAKE_SOURCE_DIR}/../external/compiled/boost/lib ${CMAKE_SOURCE_DIR}/../external/compiled/cgal/lib)

############
############ boost_thread 1.65
############
FIND_LIBRARY(boost_thread_LIBRARY NAMES libboost_thread-mt-d.so PATHS ${LOCAL_PATHS})
IF(NOT boost_thread_LIBRARY OR NOT EXISTS ${boost_thread_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"Boost Thread\" library in ${LOCAL_PATHS}")
ENDIF()

############
############ boost_system 1.65
############
FIND_LIBRARY(boost_system_LIBRARY NAMES libboost_system-mt-d.so PATHS ${LOCAL_PATHS})
IF(NOT boost_system_LIBRARY OR NOT EXISTS ${boost_system_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"Boost System\" library in ${LOCAL_PATHS}")
ENDIF()

SET(BOOST_LIBS ${boost_thread_LIBRARY} ${boost_system_LIBRARY})
# }}}

# Parallelism {{{
############
############ OpenMP
############
FIND_LIBRARY(omp_LIBRARY NAMES libomp.so PATHS ${DLL_PATHS})
IF(NOT omp_LIBRARY OR NOT EXISTS ${omp_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"omp\" library in ${DLL_PATHS}")
ENDIF()

############
############ pthread
############
FIND_LIBRARY(pthread_LIBRARY NAMES libpthread.so PATHS ${DLL_PATHS})
IF(NOT pthread_LIBRARY OR NOT EXISTS ${pthread_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"pthread\" library in ${DLL_PATHS}")
ENDIF()

SET(THREAD_ALL_LIBS ${omp_LIBRARY} ${pthread_LIBRARY})
# }}}

# Miscellanous {{{
############
############ m (math lib from glibc)
############
FIND_LIBRARY(m_LIBRARY NAMES libm.so PATHS ${DLL_PATHS})
IF(NOT m_LIBRARY OR NOT EXISTS ${m_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"m\" library in ${DLL_PATHS}")
ENDIF()

############
############ MD4C
############
FIND_LIBRARY(md4c_LIBRARY NAMES libmd4c.so PATHS ${DLL_PATHS})
IF(NOT md4c_LIBRARY OR NOT EXISTS ${md4c_LIBRARY})
	MESSAGE(FATAL_ERROR "Could not find the \"md4c\" library in ${DLL_PATHS}")
ENDIF()

SET(MISC_ALL_LIBS ${md4c_LIBRARY} ${m_LIBRARY})
# }}}

SET(ALL_NEEDED_LIBS ${OPENGL_ALL_LIBS} ${MATH_ARITHMETIC_ALL_LIBS} ${SUITESPARSE_ALL_LIBS} ${BOOST_LIBS} ${THREAD_ALL_LIBS} ${MISC_ALL_LIBS})

