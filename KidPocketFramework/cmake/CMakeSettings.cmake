#########
######### File CMakeSettings.cmake
######### CMake-specific setup (export_compile_commands, Qt5 integration ...)
######### as well as some flag values being set.
#########

# Early exits (Windows & Folder check) {{{
# Early exit for the Windows platform :
IF(WIN32 OR MINGW OR MSVC)
        MESSAGE(FATAL_ERROR "This project cannot be compiled on a Windows platform.")
ENDIF()

# Early exit if we're building in the same folder as the source code :
IF(CMAKE_BINARY_DIR STREQUAL CMAKE_SOURCE_DIR)
        MESSAGE(FATAL_ERROR "Please select another Build Directory !")
ENDIF()
# }}}

# To use clang before cmake .. do :
# export CC=clang-9
# export CXX=clang++-9
# export LD=ld.lld-9

# New compilation mode : debug, but with compiler optimization {{{
SET(CMAKE_CXX_FLAGS_DEBUGFAST
	"-Og -ggdb"
	CACHE STRING "Flags used by the C++ compiler to compile optimise debugging and allow for a better GDB experience."
	FORCE
)
# }}}

# Default build type : Debug {{{
# Set a default build type if none was specified
IF(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	MESSAGE(STATUS "Setting build type to 'DebugFast' as none was specified.")
	SET(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build." FORCE)
	# Set the possible values of build type for cmake-gui, ccmake
	SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo" "DebugFast")
ENDIF()

IF(${CMAKE_BUILD_TYPE} MATCHES "Debug")
	SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ftemplate-backtrace-limit=0 -ggdb -O0")
	MESSAGE(STATUS "Current build mode : ${CMAKE_BUILD_TYPE}")
	MESSAGE(STATUS "Flags used are now : ${CMAKE_CXX_FLAGS_DEBUG}")
ENDIF()
# }}}

# Generate compile_commands.json to make it easier to work with clang based tools
SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)
SET(CMAKE_USE_RELATIVE_PATHS TRUE)
# Required options for Qt :
SET(CMAKE_AUTORCC ON)
SET(CMAKE_AUTOMOC ON)

# Use ccache to build faster when developing {{{
MACRO(try_enable_ccache toggle)
	OPTION(ENABLE_CCACHE "HELP STRING DESCRIBING ENABLE_CCACHE" ${TOGGLE})

	IF(ENABLE_CCACHE)
		FIND_PROGRAM(CCACHE ccache)
		IF(CCACHE)
			# message(STATUS "Detected ccache : compilation will be cached")
			MESSAGE(STATUS "Found ccache : ${CCACHE}")
			SET(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE})
			# set(CCACHE_SLOPPINESS "pch_defines,time_macros") # Compatibility with precompiled headers
		ELSE()
			MESSAGE(STATUS "Cannot detect ccache : compilation will not be cached")
		ENDIF()
	ENDIF()
ENDMACRO()
# }}}

# Linking time optimisation (compilation is longer) {{{
MACRO(try_enable_ipo toggle)
	OPTION(ENABLE_IPO "Enable Iterprocedural Optimization, aka Link Time Optimization (LTO)" ${toggle})

	IF(ENABLE_IPO)
		INCLUDE(CheckIPOSupported)
		CHECK_IPO_SUPPORTED(RESULT result OUTPUT output)
		IF(result)
			MESSAGE(STATUS "Activated IPO")
			SET(CMAKE_INTERPROCEDURAL_OPTIMIZATION TRUE)
		ELSE()
			MESSAGE(STATUS "IPO is not supported: ${output}")
		ENDIF()
	ENDIF()
ENDMACRO()
# }}}

# Call the macros defined above :
try_enable_ccache(ON)
#try_enable_ipo(OFF)
