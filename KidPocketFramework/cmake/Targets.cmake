ADD_LIBRARY(glsl_wrapper
	# Header files :
	${CMAKE_SOURCE_DIR}/GLSL/BufferObject.h
	${CMAKE_SOURCE_DIR}/GLSL/FragmentShader.h
	${CMAKE_SOURCE_DIR}/GLSL/GeometryShader.h
	${CMAKE_SOURCE_DIR}/GLSL/HelperFunctions.h
	${CMAKE_SOURCE_DIR}/GLSL/OpenGLHeader.h
	${CMAKE_SOURCE_DIR}/GLSL/ProgramObject.h
	${CMAKE_SOURCE_DIR}/GLSL/ShaderInclude.h
	${CMAKE_SOURCE_DIR}/GLSL/ShaderObject.h
	${CMAKE_SOURCE_DIR}/GLSL/ShaderPool.h
	${CMAKE_SOURCE_DIR}/GLSL/VertexShader.h
	# Implementation files :
	${CMAKE_SOURCE_DIR}/GLSL/BufferObject.cpp
	${CMAKE_SOURCE_DIR}/GLSL/FragmentShader.cpp
	${CMAKE_SOURCE_DIR}/GLSL/GeometryShader.cpp
	${CMAKE_SOURCE_DIR}/GLSL/HelperFunctions.cpp
	${CMAKE_SOURCE_DIR}/GLSL/ProgramObject.cpp
	${CMAKE_SOURCE_DIR}/GLSL/ShaderObject.cpp
	${CMAKE_SOURCE_DIR}/GLSL/ShaderPool.cpp
	${CMAKE_SOURCE_DIR}/GLSL/VertexShader.cpp
)
TARGET_LINK_LIBRARIES(glsl_wrapper
	PUBLIC OpenGL
	PUBLIC glut
	PUBLIC GLEW
	PUBLIC ${project_build_options}
)

ADD_LIBRARY(cage_deform
	# Source files :
	${CMAKE_SOURCE_DIR}/CageDeform/BasicPoint.h
	${CMAKE_SOURCE_DIR}/CageDeform/CageCoordinates.h
	${CMAKE_SOURCE_DIR}/CageDeform/CageManipInterface.h
	${CMAKE_SOURCE_DIR}/CageDeform/GLUtilityMethods.h
	${CMAKE_SOURCE_DIR}/CageDeform/PCATools.h
	${CMAKE_SOURCE_DIR}/CageDeform/RectangleSelection.h
	${CMAKE_SOURCE_DIR}/CageDeform/RotationManipulator.h
	# Implementation files :
	${CMAKE_SOURCE_DIR}/CageDeform/GLUtilityMethods.cpp
)
TARGET_LINK_LIBRARIES(cage_deform
	PUBLIC glsl_wrapper
	PUBLIC Qt5::Widgets
	PUBLIC Qt5::Core
	PUBLIC ${project_build_options}
)

# GLSL and CageDeform are both non-Qt parts of the project, compile  them faster
SET_TARGET_PROPERTIES(glsl_wrapper PROPERTIES AUTOMOC OFF AUTORCC OFF AUTOUIC OFF)
# cage_deform needs QMouseGrabber, only disallow RCC and UIC
SET_TARGET_PROPERTIES(cage_deform PROPERTIES AUTORCC OFF AUTOUIC OFF)

ADD_LIBRARY(viewers STATIC
	# Header files :
	./VisuMesh.h
	./MesherViewer.h
	# Implementation files :
	./VisuMesh.cpp
	./MesherViewer.cpp
)
TARGET_LINK_LIBRARIES(viewers
	PUBLIC glsl_wrapper
	PUBLIC cage_deform
	PUBLIC ${project_build_options}
)
add_compiler_warnings(viewers)

ADD_EXECUTABLE(${CMAKE_PROJECT_NAME}
	# Header files :
	./AsRigidAsPossible.h
	./CGALIncludes.h
	./CellInfo.h
	./CholmodLSStruct.h
	./Edge.h
	./Hexahedron.h
	./Histogram.h
	./Mesh.h
	./Mesh_3_image_3_domain.h
	./Mesh_cell_base_with_info_3.h
	./Mesh_complex_3.h
	./Mesh_triangulation_3_with_info.h
	./Mesh_triangulation_with_info_3.h
	./Mesh_vertex_base_with_info_3.h
	./RegularBspTree.h
	./SeparationInfo.h
	./SurfaceCreator.h
	./TetMeshCreator.h
	./TetraLRISolver.h
	./Tetrahedron.h
	./Texture.h
	./Triangle.h
	./VBOHandler.h
	./Vec3D.h
	./Voxel.h
	./VoxelGrid.h
	./Window.h
	./make_mesh_3_from_labeled_triangulation_3.h

	# Main file :
	./Main.cpp

	# Implementation files :
	./AsRigidAsPossible.cpp
	./Histogram.cpp
	./Mesh.cpp
	./SurfaceCreator.cpp
	./TetMeshCreator.cpp
	./TetraLRISolver.cpp
	./Texture.cpp
	./VBOHandler.cpp
	./VoxelGrid.cpp
	./Window.cpp
)

TARGET_LINK_LIBRARIES(${CMAKE_PROJECT_NAME}
	PUBLIC cage_deform
	PUBLIC glsl_wrapper
	PUBLIC viewers
	PUBLIC CGAL::CGAL
	PUBLIC CGAL::CGAL_Core
	PUBLIC CGAL::CGAL_ImageIO
	PUBLIC Qt5::Gui
	PUBLIC Qt5::Widgets
	PUBLIC Qt5::Xml
	PUBLIC Qt5::OpenGL
	PUBLIC Qt5::Core
	PUBLIC lapack
	PUBLIC ${NIFTI_LIBRARIES}
	PUBLIC OpenGL
	PUBLIC glut
	PUBLIC GLEW
	PUBLIC ${ALL_NEEDED_LIBS}
	PUBLIC ${project_build_options}
)

add_compiler_warnings(${CMAKE_PROJECT_NAME})
enable_notifications(glsl_wrapper)
enable_notifications(cage_deform)
enable_notifications(viewers)
enable_notifications(${CMAKE_PROJECT_NAME})
copy_to_source(${CMAKE_PROJECT_NAME})
