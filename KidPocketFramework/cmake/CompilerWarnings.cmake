FUNCTION(add_compiler_warnings project_name)

	OPTION(WARNINGS_AS_ERRORS "Treat compiler warnings as errors" FALSE)

	TARGET_COMPILE_FEATURES(${project_name} PUBLIC cxx_std_14)

	SET(CLANG_WARNINGS
		-Wall
		-Wextra # reasonable and standard
		-Wshadow # warn the user if a variable declaration shadows one from a parent context
		-Wnon-virtual-dtor # warn the user if a class with virtual functions has a non-virtual destructor. This helps
				# catch hard to track down memory errors
		-Wold-style-cast # warn for c-style casts
		-Wcast-align # warn for potential performance problem casts
		-Wunused # warn on anything being unused
		-Woverloaded-virtual # warn if you overload (not override) a virtual function
		-Wpedantic # warn if non-standard C++ is used
		-Wconversion # warn on type conversions that may lose data
		-Wsign-conversion # warn on sign conversions
		-Wnull-dereference # warn if a null dereference is detected
		-Wdouble-promotion # warn if float is implicit promoted to double
		-Wformat=2 # warn on security issues around functions that format output (ie printf)
	)

	IF(WARNINGS_AS_ERRORS)
		SET(CLANG_WARNINGS ${CLANG_WARNINGS} -Werror)
		SET(MSVC_WARNINGS ${MSVC_WARNINGS} /WX)
	ENDIF()

	SET(GCC_WARNINGS
		#${CLANG_WARNINGS}
		#-Wmisleading-indentation # warn if identation implies blocks where blocks do not exist
		#-Wduplicated-cond # warn if if / else chain has duplicated conditions
		#-Wlogical-op # warn about logical operations being used where bitwise were probably wanted
		#-Wuseless-cast # warn if you perform a cast to the same type
		#-w
		-ftemplate-backtrace-limit=0
		-Wno-deprecated-declarations
		#--pedantic
	)

	IF(MSVC)
		SET(PROJECT_WARNINGS ${MSVC_WARNINGS})
	ELSEIF(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
		SET(PROJECT_WARNINGS ${CLANG_WARNINGS})
	ELSE()
		SET(PROJECT_WARNINGS ${GCC_WARNINGS})
	ENDIF()

	MESSAGE(STATUS "Set the compiler options of ${project_name} to ${PROJECT_WARNINGS}")

	TARGET_COMPILE_OPTIONS(${project_name} PUBLIC ${PROJECT_WARNINGS})

ENDFUNCTION()
