#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QColor>
#include <QPixmap>

class Histogram
{

public:
    Histogram();
    Histogram(const std::vector<QColor> & _colors): colors(_colors){}

    void build_histogram( std::vector<int> & histo_data , double min_value, double max_value, int m_id, int max_size );
    void build_histogram( std::vector<int> & histo_data , double min_value, double max_value );
    void build_histogram( std::vector<float> & histo_data );
    QPixmap getPixMap() const{ return histogram_; }
protected:
    QPixmap histogram_;
    QColor get_histogram_color(const double v) const;
    QColor get_label_color(const int v) const;

    std::vector<QColor> colors;
};

#endif // HISTOGRAM_H
