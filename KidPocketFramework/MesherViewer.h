/****************************************************************************


*****************************************************************************/

#ifndef MESHERVIEWER_H
#define MESHERVIEWER_H
#include "OpenGLHeader.h"
#include "nifti/nifti1.h"
typedef unsigned char MY_DATATYPE;

#define MIN_HEADER_SIZE 348
#define NII_HEADER_SIZE 352
#define DEFAULT_LABEL 0

#include "CGALIncludes.h"
#include "Mesh.h"
#include "VoxelGrid.h"
#include "SurfaceCreator.h"
#include "TetMeshCreator.h"
#include "Histogram.h"
#include "Texture.h"
#include "VisuMesh.h"

enum Mode {MESHING, MESH_DEFORMATION, IMAGE_DEFORMATION, DIRECT_MESH_DEFORMATION};
enum DisplayType {IMAGE, MESH, SEGMENTATION, NONE};
enum DisplayMode{ WIRE=0, SOLID=1, LIGHTED_WIRE=2, LIGHTED=3 };
enum RasterType { VECTOR_FIELD, NEAREST_NEIGHBOR, DILATATION };

#include <QColor>

#include <QGLViewer/qglviewer.h>
#include <QString>

#include "CageDeform/RectangleSelection.h"
#include "CageDeform/BasicPoint.h"

#include <CGAL/make_mesh_3.h>
#include <nifti/nifti1_io.h>
#include "GLUtilityMethods.h"

#include "CageDeform/CageManipInterface.h"

#define DEFAULT_SI 1

using namespace qglviewer;

#define CELL_RATIO 3.
#define CELL_SIZE 5.

#define FACET_ANGLE 30.
#define FACET_SIZE 2.
#define FACET_APPROXIMATION 4.


struct InterpolationStruct {
	std::vector< std::vector< BasicPoint > > keys;
	bool interpolate(double t , std::vector< BasicPoint > & pts ) {
	if( pts.size() < 2 ) return false;

	int key0 = (t* (keys.size()-1));
	int key1 = (key0 + 1) % keys.size();
	double alpha = (t* (double)(keys.size()-1)) - key0;
	key0 = (key0) % keys.size();

	int npts = keys[key0].size();
	for( int p = 0 ; p < npts ; ++p ) {
		pts[p] = (1.0 - alpha) * keys[key0][p] + alpha * keys[key1][p];
	}
	return true;
	}
};

#include <ctime>
#include <ratio>
#include <chrono>

struct AnimationTimer{
	bool playAnimation;
	double animationPeriodInMicroSeconds;
	double uAnimation;
	std::chrono::system_clock::time_point timerPrevious;
	bool loopAnimation;

	AnimationTimer() {
	playAnimation = false;
	animationPeriodInMicroSeconds = 100000000.0;
	uAnimation = 0.0;
	}

	void setLoopAnimation(bool l) {
	loopAnimation = l;
	}

	void setAnimationLengthInMilliSeconds( double totalTimeInMilliSeconds ) {
	animationPeriodInMicroSeconds = 1000 * totalTimeInMilliSeconds;
	}

	bool togglePause() {
	playAnimation = !playAnimation;
	if(playAnimation){
		timerPrevious = std::chrono::high_resolution_clock::now();
	}
	return playAnimation;
	}

	void restart( double u = 0.0 ) {
	timerPrevious = std::chrono::high_resolution_clock::now();
	uAnimation = u;
	}

	void recomputeU(  ) {
	if( playAnimation ) {
		auto timerCurrent = std::chrono::high_resolution_clock::now();
		auto elapsed = timerCurrent - timerPrevious;
		long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
		double uAnim = uAnimation + ((double)(microseconds)/animationPeriodInMicroSeconds);
		if(loopAnimation)
		uAnim = uAnim - floor(uAnim);
		timerPrevious = timerCurrent;
		uAnimation = uAnim;
	}
	}

	bool isPlaying() {
	return playAnimation;
	}

	void multiplySpeedBy( double ffactor ) {
	animationPeriodInMicroSeconds /= ffactor;
	}

	double getU() {
	return uAnimation;
	}
};

class MesherViewer : public QGLViewer
{
	Q_OBJECT
public :
	MesherViewer(QWidget *parent);
	~MesherViewer();

	InterpolationStruct interpolationStruct;
	AnimationTimer animationTimer;

	void mesh3DImage(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize);
	void meshTransfer(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize, bool fromMesh, bool optimize);
	void meshTransferAdativeSize(double sizeMul, double percentage, bool optimize);
	void generateCageTetMesh(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize);
	void generatePoissonSampling(int pointNb, double r){tetMeshCreator.createPoissonPointDistribution(pointNb, r); update();}
	bool openIMA(const QString & fileName, VoxelGrid & grid);
	bool openIMALR(const QString & fileName, VoxelGrid & hr_grid, VoxelGrid & grid);
	void openSegmentation( const QString & fileName );
	void openImage(const QString & filename, bool full_res);
	void openNIFTI(const QString & filename, VoxelGrid & grid);
	void openNIFTILR(const QString & filename, VoxelGrid & hr_grid, VoxelGrid & grid);
	void openOBJFiles(const QFileInfoList & fileInfoList );
	void openOFFFiles(const QFileInfoList & fileInfoList);
	void openOBJ(const QString & fileName);
	void openMESH(const QString & filename);
	void openTransferMESH(const QString & filename);
	void openOFF (const QString & filename);
	void openModel (const QString & filename) ;
	void offLoader ( const std::string & filename, std::vector<BasicPoint> & V, std::vector<Triangle> & T);
	void saveMeshes(const QString & folderName);
	void saveMESH(const QString & fileName);
	void saveVRML(const QString & fileName);
	void saveTransferMesh(const QString & fileName){tetMeshCreator.saveTransferMesh(fileName.toStdString()); }
	void saveOFF(const QString & fileName){FileIO::saveOFF(fileName.toStdString(), mesh.getVertices(), mesh.getTriangles());}
	void saveCGALEnvelop( const QString & fileName );
	void saveCage (const QString & filename);
	void export_vtk( std::string const & filename , std::string const & field_name , std::string const & field_type , int field_components, std::vector<float> & values, VoxelGrid & grid );
	void export_error_vtk(const std::string &filename);
	void export_sizing_field_vtk(std::string const & filename);
	void export_distortion_vtk(std::string const & filename);
	void openCage (const QString & filename);
	void saveSurface(const QString & filename);
	void openCamera (const QString & filename);
	void saveCamera(const QString & filename);
	void openFinalCage (const QString & filename);
	void openColors(const QString & fileName);
	void saveGridInfo(const std::string & filename) {voxelGrid.saveInfoToFile(filename);}
	void selectAll();
	void discardAll();
	void setVisibility(unsigned int i, bool visibility);

	void selectIAll();
	void discardIAll();
	void setIVisibility(unsigned int i, bool visibility);

	void selectSAll();
	void discardSAll();
	void setSVisibility(unsigned int i, bool visibility);
	const std::map<Subdomain_index, QColor> & getColorMap()const {return colorMap;}
	const std::map<Subdomain_index, QColor> & getIColorMap()const {return iColorMap;}
	std::map<unsigned char, QColor> getTextColorMap()const {return textColorMap;}
	const std::map<Subdomain_index, QColor> & getSColorMap()const {return sColorMap;}
	void setMode(Mode _mode);
	void setDisplayType(DisplayType _displayType){displayType = _displayType; update();}
	void initializeCage();
	void setCageTopositions(const vector<BasicPoint> & positions);
	void setRasterType(RasterType _rasterType){ rasterType = _rasterType; }
	double getSphereScale(){return sphereScale;}

	float getSceneRadius(){ return scene_radius; }
	double diagonal_length(){return (BBMax - BBMin).norm() ; }

	unsigned int getSurfaceHalfEdgeCount(){return surfaceCreator.getHalfEdgeCount();}
	void simplifySurface(unsigned int halfEdgeNb){ surfaceCreator.simplify(halfEdgeNb); update();}
	void generateSurface(float angle, float size, float approximation );
	void setGridOffset(int offset, bool useGridSegmentation);
	void rasterizeGrid(unsigned int neighborhoodsize, double resolution);
	void useCGALEnvelopAsCage();
	void saveGrid(QString fileName );
	void saveSegmentation( QString fileName );
	void saveSizingFieldAsGrid(QString fileName);
	void separateLabels(unsigned int l1, unsigned int l2 );
	void changeLabelValue( unsigned int from, unsigned int to );

	void segmentMesh(const std::vector<int> &labels){mesh.segmentMesh(labels);}

	void perturb(double time_limit, double sliver_bound);
	void lloyd_optimize_mesh_3(double time_limit, double max_iteration_number, double convergence, double freeze_bound);
	void odt_optimize_mesh_3(double time_limit, double max_iteration_number, double convergence, double freeze_bound);
	void exude(double sliver_bound, double time_limit);

	void setWhite(){voxelGrid.setWhite(); defGrid.setWhite(); update();}


	void perturb_transfer_mesh(double time_limit, double sliver_bound){

	}
	void lloyd_optimize_transfer_mesh(double time_limit, double max_iteration_number, double convergence, double freeze_bound){}
	void odt_optimize_transfer_mesh(double time_limit, double max_iteration_number, double convergence, double freeze_bound){}


	QPixmap getQualityHistogram();
	QPixmap getVolumeHistogram();
	void computeHistograms(QPixmap & distortionPixMap, QPixmap & elongationPixMap);
	QPixmap compareVolumeLossWithGrid(const QString & filename1, const QString & filename2);
	void coordComputationTime();

	void getImageSubdomainIndices(std::vector<Subdomain_index> & subdomain_indices){return voxelGrid.getSubdomainIndices(subdomain_indices);}
	void getSegmentationSubdomainIndices(std::vector<Subdomain_index> & subdomain_indices){return segmentation.getSubdomainIndices(subdomain_indices);}
	void getMeshSubdomainIndices(std::vector<Subdomain_index> & subdomain_indices){subdomain_indices = mesh.getSubdomainIndices(); return; }

	void updateFromInterpolationStruct(double t);
protected :
	virtual void init();
	virtual void draw();
	virtual void keyPressEvent(QKeyEvent *e);
	virtual void mousePressEvent(QMouseEvent *e);
	virtual void mouseMoveEvent(QMouseEvent *e);
	virtual void mouseReleaseEvent(QMouseEvent *e);
	virtual QString helpString() const;

	QString imaName;
	void updateGridWithTransferMeshHR();
	void saveToVTK(const QString &fileName, VoxelGrid & grid);
	void saveToNifti(QString fileName, VoxelGrid & grid);
	void saveToIMA(QString fileName,  VoxelGrid & grid);
	void extractInfoFromFileName( const QFileInfo & fileInfo , std::string & filename, Subdomain_index & si );
	void initLightsAndMaterials();
	void drawNormals();
	void drawCage();
	void drawClippingPlane();
	void drawCutPlanes();
	void updateInternalData();
	void updateImageDisplay();
	void clear();
	void clearImage();
	void initializeImage();
	void computeRandomColors(const std::vector<int> & subdomain_indices, std::map<Subdomain_index, QColor> & cMap);
	void updateCamera(const BasicPoint & center, float radius);

	void saveOFF(const QString &fileName, const std::vector<BasicPoint> & vertices, const std::vector<Triangle> & triangles, const std::vector<int> & indices);

	void loadVerticesToCage( const vector<BasicPoint> & V );
	void updatePointsFromCMInterface(std::vector<BasicPoint> & points, bool fromManipulator = true);
	void updateCuttingPlanes(BasicPoint bb, BasicPoint BB);
	void saveGridEnvelop(const QString &fileName, float angle, float size, float approximation );

	void updateDirectMeshDeformation();
	void changeDisplayMode();

	void restaureLastState();

	void create_histogram(std::vector<int> & histo, double& min_value, double& max_value);

	void offsetSelection();
	Plane_3 plane;
	CGAL::Image_3 image;
	std::map<Subdomain_index, QColor> colorMap;
	std::map<Subdomain_index, QColor> iColorMap;

	std::map<unsigned char, bool> textDisplayMap;
	std::map<unsigned char, QColor> textColorMap;
	std::vector<unsigned char> text_subdomain_indices;
	std::vector<unsigned char> subdomain_indices;
	void open3DImage(const QString & fileName);
	int getIndice(int i, int j , int k);
	void makeDefaultMesh();
	void openIMA(const QString & fileName, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
		 unsigned int & nx , unsigned int & ny , unsigned int & nz, float & dx , float & dy , float & dz );
	void openNIFTI(const QString & filename, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
		   unsigned int & nx , unsigned int & ny , unsigned int & nz, float & dx , float & dy , float & dz );
	void updateCamera(const qglviewer::Vec & center, float radius);
	std::map<Subdomain_index, QColor> fileColors;

	std::map<Subdomain_index, bool> displayMap;
	std::map<Subdomain_index, bool> iDisplayMap;

	std::map<Subdomain_index, QColor> sColorMap;
	std::map<Subdomain_index, bool> sDisplayMap;

	DisplayMode displayMode;
	DisplayType displayType;

	CMInterface< BasicPoint > cageInterface;
	RotationManipulator * manipulator;
	RectangleSelection * rselection;
	Mode mode;

	VoxelGrid segmentation;
	VoxelGrid voxelGrid;
	VoxelGrid defGrid;

	bool cageInitialized;
	bool cageLoaded;
	bool displayCage;
	bool displayWireCage;
	bool displayCageNormals;
	bool displayDeformedImage;
	bool displayCageProblems;
	bool useRandomColor;
	bool displayBB;
	bool displayBC;
	bool displayTetrahedron;
	bool displaySurface;
	bool movingLight;
	bool drawModel;
	bool displayCageBB;
	bool displaySampling;
	bool displayTransferMesh;
	bool displayCageTets;
	bool displayPointsToAdd;
	bool displayDistortions;
	bool displayEdges;
	bool useTransferMesh;
	bool enableClipping;
	bool separationActive;
	bool gridOffsetActive;
	bool displayOutliers;
	bool displaySegmentation;
	bool lr_deformation;
	std::deque< vector<BasicPoint> > Q;

	Mesh mesh;

	Mesh model;

	RasterType rasterType;

	double sphereScale;
	double manipulatorScale;

	bool envelopCage;

	BasicPoint cut;
	BasicPoint cutDirection;
	BasicPoint cutPlaneVisibility;
	BasicPoint BBMin;
	BasicPoint BBMax;
	BasicPoint step;

	SurfaceCreator surfaceCreator;
	TetMeshCreator tetMeshCreator;

	C3t3 mesh_3;

	int lightInfo [3];

	int error_count;

	VoxelGrid HRGrid;
	VoxelGrid HRDefGrid;

	float scene_radius;

	Texture texture;
	VisuMesh visu_mesh;
public slots :
	void manipulatorReleased();
	void updateFromCMInterface();
	void addToSelection(QRectF const &, bool);
	void removeFromCageSelection(QRectF const &);
	void computeManipulatorForCageDeformation();
	void saveCurrentCageState();
	void updateGrid();
	void setUseARAP(bool _useARAP);
	void setDisplayWireCage(bool _displayWCage){displayWireCage = _displayWCage; update();}
	void setDisplaySurface(bool _displaySurface){displaySurface = _displaySurface; update();}
	void setDrawModel(bool _drawModel){drawModel = _drawModel; update();}
	void setDisplayCageNormals(bool _displayCageNormals){displayCageNormals = _displayCageNormals; update();}
	void setSphereScale(double _sphereScale){sphereScale = _sphereScale; cageInterface.set_sphere_scale(sphereScale); update();}
	void setARAPIteration(int itNb){cageInterface.setIterationNb(itNb);}
	void locateCageProblems();
	void setDisplayCageProblems( bool _displayCageProblems ){ displayCageProblems = _displayCageProblems; update(); }
	void invertNormals(){mesh.invertNormals(); update();}
	void setXCut(int _x){ cut[0] = BBMin[0]+ _x*step[0]; update();}
	void setYCut(int _y){ cut[1] = BBMin[1]+ _y*step[1]; update();}
	void setZCut(int _z){ cut[2] = BBMin[2]+ _z*step[2]; update();}
	void invertXCut(){ cutDirection[0] *= -1 ; update();}
	void invertYCut(){ cutDirection[1] *= -1 ; update();}
	void invertZCut(){ cutDirection[2] *= -1 ; update();}
	void setXCutDisplay(bool _xCutDisplay){ int vis = 0 ; if(_xCutDisplay) vis = 1;  cutPlaneVisibility[0] = vis; update(); }
	void setYCutDisplay(bool _yCutDisplay){ int vis = 0 ; if(_yCutDisplay) vis = 1;  cutPlaneVisibility[1] = vis; update(); }
	void setZCutDisplay(bool _zCutDisplay){ int vis = 0 ; if(_zCutDisplay) vis = 1;  cutPlaneVisibility[2] = vis; update(); }
	void setDisplayBB(bool _displayBB){ displayBB = _displayBB; update();}
	void setDisplayBC(bool _displayBC){ displayBC = _displayBC; update();}
	void setDisplayTetrahedra( bool _display ){ displayTetrahedron = _display; update(); }
	void useSurfaceAsCage();
	void useSurfaceAsMesh();
	void recomputeColors(){voxelGrid.computeDefaultColors(); defGrid.computeDefaultColors(); update();}
	void rescaleCage(){cageInterface.rescale(10.); update();}
	void rescaleSurface();
	void useMovingLight(bool _useMovingLight);
	void checkForOutliers(){ if(useTransferMesh) tetMeshCreator.checkAndMarkOutliers(); else voxelGrid.checkForOutliers(); update();}
	void ignoreOutliers(){if(voxelGrid.ignoreOutliers()) initializeCage();}
	void setDisplayCageBB(bool _dis){displayCageBB = _dis; update();}
	void setDisplaySampling(bool _dis){displaySampling = _dis; update();}
	void setDisplayCageTets(bool _dis){displayCageTets = _dis; update();}
	void setDisplayTransferTets(bool _dis){displayTransferMesh = _dis; update();}
	void setDisplayOutliers(bool _dis){displayOutliers = _dis; update();}
	void setDisplayDistortions(bool _dis){displayDistortions = _dis; update();}
	void setDisplayPoints(bool _dis){displayPointsToAdd = _dis; update();};
	void setDisplayEdges(bool _dis){displayEdges = _dis; update();};
	void setDisplayCage(bool _dis){displayCage = _dis; update();}
	void recomputeTransferMesh(bool _fittogrid){if(_fittogrid) tetMeshCreator.createFromPointDistributionFittedToPoints() ;
		else tetMeshCreator.createFromPointDistribution(); update();}
	void useTransferAsMesh();
	void setUseTransferMesh( bool _useTransferMesh){useTransferMesh = _useTransferMesh; }
	void computeDistortions(){tetMeshCreator.computeDistortions(); update();}
	void setSamplingType(int st){ tetMeshCreator.setSamplingType(SamplingType(st)); }
	void setDistortionType(int dt){ tetMeshCreator.setDistortionType(DistortionType(dt));}
	void adaptSampling(){tetMeshCreator.adaptSampling(); update();}
	void updateGridWithTransferMesh(bool minimalDisplay);
	void initializeTransferMesh(){ tetMeshCreator = TetMeshCreator(&cageInterface, &voxelGrid);  }
	void setIndexDependantColors(bool);
	void exude_transfer_mesh(){
		tetMeshCreator.exude();
	}
	void setToCurrent(){tetMeshCreator.updateVertices(); tetMeshCreator.updateVertices(mesh.getVertices());}
	void setToInit(){tetMeshCreator.set_transfer_mesh_to_initial_state();}
	void drawNiceVoxels(bool _nice_display){
		if(displayDeformedImage){
			defGrid.setDrawNice(_nice_display);
		} else {
			voxelGrid.setDrawNice(_nice_display);
		}
		update();
	}
	void setClippingTest(bool _enableClipping){ enableClipping = _enableClipping ; update();}
	void setManipulatorScale(double _mScale){manipulatorScale = _mScale; manipulator->setDisplayScale(manipulatorScale*camera()->sceneRadius()/9.);update();}
	void applyTrilinearInterpolation(){if(voxelGrid.size()>0) voxelGrid.applyTrilinearInterpolation();};
	void setActiveSelection(){ separationActive = !separationActive; if(separationActive) gridOffsetActive = false;}
	void setActiveGridSelection(){ gridOffsetActive = !gridOffsetActive; if(gridOffsetActive) separationActive = false;  }
	void setDisplaySegmentation(bool _disp){displaySegmentation = _disp; update();}
	void updateGridDisplay(){ voxelGrid.buildForDisplay(); update(); };
	void automaticSeparation(){ segmentation.automaticSeparation(); }

signals:
	void setMaxCutPlanes(int _xMax, int _yMax, int _zMax);
	void setMeshedLabels();
	void setImageLabels();
	void setSegmentationLabels();
};


#endif // MESHERVIEWER_H
