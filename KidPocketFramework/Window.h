#ifndef WINDOW_H
#define WINDOW_H

#include <qmetatype.h>
#include <QMainWindow>
#include "MesherViewer.h"
#include <QActionGroup>
#include <QGroupBox>
#include <QCheckBox>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSlider>
#include <QGridLayout>

class Window : public QMainWindow
  {
    Q_OBJECT

  public:
    Window();

  private:
        MesherViewer * viewer;
        QDockWidget * mesherDockWidget;
        void initActions ();
        void initFileActions ();
        void initMenus();
        void initToolBars();
        void addMeshedLabels(const std::map<Subdomain_index, QColor> & colors);
        void addImageLabels(const std::map<Subdomain_index, QColor> & colors);
        void addSegmentationLabels(const std::map<Subdomain_index, QColor> & colors);
        QActionGroup * fileActionGroup;
        QToolBar * fileToolBar;
        void initDisplayDockWidgets();
        void initInfoDockWidget();
        void initToolsDockWidgets();
        QGroupBox * displayGroupBox;
        QVector<QCheckBox *> labelCheckButtons;
        std::vector<unsigned int> indexToLabel;
        QVector<QLabel *> meshLabels;
        QGridLayout * segGridLayout;
        QSignalMapper * signalMapper;
        QCheckBox * minimalMemoryCheckBox;
        QGroupBox * displayImageGroupBox;
        QVector<QCheckBox *> labelICheckButtons;
        QVector<QCheckBox *> labelSCheckButtons;
        std::vector<unsigned int> indexIToLabel;
        std::vector<unsigned int> indexSToLabel;
        QVector<QLabel *> imageLabels;
        QGridLayout * segIGridLayout;
        QSignalMapper * signalIMapper;

        QDockWidget * displayDockWidget;
        QDoubleSpinBox * lengthSpinBox;
        QPushButton * meshingPushButton;
        QComboBox * modeComboBox;

        QSpinBox * offsetSpinBox;

        QDoubleSpinBox * sphereRadiusSpinBox;
        void set3DImageDisplay();
        void setMeshedDisplay();
        void setMeshDisplay();
        QTabWidget * contents;
        QGroupBox * drawMeshGroupBox;
        QGroupBox * drawImageGroupBox;
        QGroupBox * drawSegmentationGroupBox;
        QPushButton * computeCoordinates;
        QGroupBox * getCuttingPlaneGroupBox(QWidget * parent);
        QSlider * xHSlider;
        QSlider * yHSlider;
        QSlider * zHSlider;

        QSpinBox * labelNbSpinBox;

        double get_approximate(double d, int precision, int& decimals);
        QSpinBox * neighborhoodSizeSpinBox;
        QDoubleSpinBox * gridResolutionSizeSpinBox;
        QSpinBox * pointNbSpinBox;
        QDoubleSpinBox *radiusDoubleSpinBox;
        QCheckBox * fitToGridCheckBox;
        QCheckBox * smoothTransferMeshCheckBox;
        void initQualityHistogramDockWidget();
        QGroupBox * createOptimizationGroupBox();
        QGroupBox * createTrOptimizationGroupBox();
        QCheckBox * useSegmentationCheckBox;
      //  void updateInfo();
        bool exude( double & sliverBoundValue, double & maxTime );
        bool perturb( double & maxTimeValue, double & sliverBoundValue );
        bool lloydSmoothing( double &maxTimeValue, double &maxIterationValue, double &convergenceRatioValue, double &freezeRatioValue);
        bool odtSmoothing( double &maxTimeValue, double &maxIterationValue, double &convergenceRatioValue, double &freezeRatioValue);
        bool meshingDialog(double diag, double &angleSpinBoxValue, double &sizeSpinBoxValue, double &approximationSpinBoxValue,
                           double &ratioSpinBoxValue, double &cellSizeSpinBoxValue );

        QLabel * displayLabel;
        QLabel * volumeLabel;
        QLabel * distortionLabel;
        QLabel * elongationLabel;

        bool loadFR();
 private slots:
        void openColors();
        void open3DImage();
        void openImageSegmentation();
        void openCage();
        void openFinalCage();
        void saveMeshes();
        void saveMesh();
        void saveTransferMesh();
        void saveCage();
        void saveGrid();
        void saveSegmentation();
        void openMesh();
        void openTransferMesh();
        void openModel();
        void openMeshes();
        void saveEnvelop();
        void saveSurface();
        void CGALMeshing();
        void transferMeshGeneration();
        void meshAdaptiveSize();
        void trMeshGenerationFromMesh();
        void setMeshedLabels();
        void setImageLabels();
        void setSegmentationLabels();
        void setVisibility(int i);
        void selectAll();
        void discardAll();
        void setIVisibility(int i);
        void selectIAll();
        void discardIAll();
        void setSVisibility(int i);
        void selectSAll();
        void discardSAll();
        void setMode(int mode);
        void setRasterMode(int mode);
        void setMaxCutPlanes( int x, int y , int z );
        void initializeCage();
        void setDisplayMesh(bool visible);
        void setDisplayImage(bool visible);
        void useCGALEnvelopAsCage();
        void separateHands();
        void lloydSmoothing_image_mesh();
        void lloydSmoothing_transfer_mesh();
        void odtSmoothing_image_mesh();
        void odtSmoothing_transfer_mesh();
        void exude_image_mesh();
        void exude_transfer_mesh();
        void perturb_image_mesh();
        void perturb_transfer_mesh();
        void segmentation();
        void generateCage();
        void simplifyCage();
        void updateGridOffset();
        void saveGridInfo();
        void changeBackgroundColor();
        void rasterize();
        void computeVolumeHistogram();
        void computeHistograms();
        void exportDistortion();
        void compareVolumes();
        void cageTetMeshing();
        void pointDistribution();
        void useAsMesh();
        void recomputeTransferMesh();
        void saveSizingFieldAsGrid();
        void saveCamera();
        void openCamera();
        void updateGridWithTransferMesh();
        void setLabelToZero();

  };


#endif // WINDOW_H

