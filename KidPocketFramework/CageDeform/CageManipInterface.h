#ifndef CAGEMANIPINTERFACE_H
#define CAGEMANIPINTERFACE_H



#include <vector>
#include <queue>
#include <map>
#include <set>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <cfloat>
#include <cmath>
#include <QOpenGLContext>


#include "./PCATools.h"
//#include "Manipulator.h"
#include "RotationManipulator.h"

#include "./CageCoordinates.h"

#include "../AsRigidAsPossible.h"

enum MeshModificationMode {INTERACTIVE , REALTIME};
enum MeshModificationMethod {GREEN=0 , MVC=1};




/*
  HOW TO USE IT :
  You don't need to specify anything when constructing the object , but use :
	setMode( int m ) to specify if you want INTERACTIVE or REALTIME
	setMethod( int m ) to specify if you use GREEN or MVC

  Then just fill the mesh and the cage , call reInitialize(),
  and you're done.

  EITHER
	you use open_OFF_mesh("")  and  THEN  open_OFF_cage("") and it will calculate the cage coordinates ,
  OR
	you fill the mesh and the cage BY ACCESSING  :
	  get_mesh_vertices()  ,  get_mesh_triangles()  ,  get_cage_vertices()  ,  get_cage_visu_triangles()  ,  and  get_cage_visu_quads(),
	AND THEN you call reInitialize().
  */


template< class point_t >
class CMInterface
{
	// MESH :
	vector< point_t > mesh_vertices;
	vector< vector< int > > mesh_triangles;
	vector< point_t > mesh_vertex_normals;

	vector< point_t > mesh_modified_vertices;





	// CAGE :
	vector< point_t > cage_vertices;
	vector< vector< int > > cage_triangles;
	vector< point_t > cage_triangle_normals;


	// INITIAL CAGE :
	vector< point_t > initial_cage_vertices;
	vector< vector< int > > initial_cage_triangles;
	vector< point_t > initial_cage_triangle_normals;

	vector < bool > outliers;

	// CAGE MANIP :
	vector< bool > cage_selected_vertices;
	vector< bool > cage_fixed_vertices;


	// CAGE VISU :
	vector< vector< int > > cage_visu_triangles;
	vector< vector< int > > cage_visu_quads;

	// GREEN COORDS :
	vector< vector< double > > GREENphiCoordinates;
	vector< vector< double > > GREENpsiCoordinates;
	vector< GreenCoords::GreenScalingFactor< point_t > > GREENscalingFactors;

	// MVC COORDS :
	vector< vector< pair< unsigned int , float > > > MVCCoordinates;

	// Deformation parameters :
	MeshModificationMode deformationMode;
	MeshModificationMethod deformationMethod;


	QGLViewer * input_context;
	QSurface* input_surface;

	GLuint small_sphere_index;
	GLuint sphere_index;

	float sphere_scale ;

	AsRigidAsPossible ARAP;
	bool useARAP;

	bool problems;
	std::vector<bool> problematicVertices;
	std::vector<bool> problematicTriangles;

	float average_edge_halfsize;


public:

	inline double getAverage_edge_halfsize(){return average_edge_halfsize;}
	//  ACCESS the input :
	inline vector< point_t > & get_mesh_vertices() { return mesh_vertices ; }
	inline vector< point_t > const & get_mesh_vertices() const { return mesh_vertices ; }

	inline vector< point_t > & get_mesh_vertex_normals() { return mesh_vertex_normals ; }
	inline vector< point_t > const & get_mesh_vertex_normals() const { return mesh_vertex_normals ; }

	inline vector< vector< int > > & get_mesh_triangles() { return mesh_triangles ; }
	inline vector< vector< int > > const & get_mesh_triangles() const { return mesh_triangles ; }

	inline vector< point_t > & get_cage_vertices() { return cage_vertices ; }
	inline vector< point_t > const & get_cage_vertices() const { return cage_vertices ; }
	inline int get_cage_vertices_nb() { return cage_vertices.size() ; }

	inline vector< point_t > & get_cage_initial_vertices() { return initial_cage_vertices ; }
	inline vector< point_t > const & get_cage_initial_vertices() const { return initial_cage_vertices ; }

	inline vector< vector< int > > & get_cage_triangles() { return cage_triangles ; }
	inline vector< vector< int > > const & get_cage_triangles() const { return cage_triangles ; }


	inline vector< vector< int > > & get_cage_visu_triangles() { return cage_visu_triangles ; }
	inline vector< vector< int > > const & get_cage_visu_triangles() const { return cage_visu_triangles ; }

	inline vector< vector< int > > & get_cage_visu_quads() { return cage_visu_quads ; }
	inline vector< vector< int > > const & get_cage_visu_quads() const { return cage_visu_quads ; }

	// ACCESS the output :
	inline vector< point_t > const & get_mesh_modified_vertices() const { return mesh_modified_vertices ; }

	inline vector< bool > & get_cage_selected_vertices () { return cage_selected_vertices; }
	inline const vector< bool > & get_cage_selected_vertices () const { return cage_selected_vertices; }

	inline vector< bool > & get_cage_fixed_vertices () { return cage_fixed_vertices; }
	inline const vector< bool > & get_cage_fixed_vertices () const { return cage_fixed_vertices; }

	void setContext( const QGLViewer * icontext ) { input_context = (QGLViewer*)icontext; }

	float getSphereRadius() {return average_edge_halfsize;}

	vector< bool > get_cage_handles_vertices (  ) {
	vector< bool > cage_handles_vertices ( cage_vertices.size(), false );
	for( unsigned int i = 0 ; i < cage_vertices.size() ; i++ ){
		if( cage_fixed_vertices[i] || cage_selected_vertices[i] )
			cage_handles_vertices[i] = true;
	}
	return cage_handles_vertices;
	}

	void make_selected_fixed_handles(){

	for( unsigned int i = 0 ; i < cage_vertices.size() ; i++ ){
		if(cage_selected_vertices[i]){
		cage_fixed_vertices[i] = true;
		cage_selected_vertices[i] = false;
		}
	}

	}

	void rescale(float scale){
	for( unsigned int i = 0 ; i < cage_vertices.size() ; i++ ){
		cage_vertices[i] = cage_vertices[i]*scale;
	}
	}

	void setIterationNb( int itNb){
	ARAP.setIterationNb(itNb);
	}

	CMInterface()
	{
	deformationMethod = GREEN   ;
	deformationMode = REALTIME;
	average_edge_halfsize = 1.;
	problems = false;
	useARAP = true;
	ARAP = AsRigidAsPossible();
	}

	CMInterface( const QGLViewer * icontext )
	{
		deformationMode = REALTIME;
		average_edge_halfsize = 1.;
		sphere_scale = 1.;
		setContext( icontext );
		problems = false;
		useARAP = true;
		ARAP = AsRigidAsPossible();
	}

	~CMInterface()
	{
		// delete it if it is not used any more
		glDeleteLists(sphere_index, 1);
		glDeleteLists(small_sphere_index, 1);
	}

	void set_sphere_scale( float _sphere_scale ){
		sphere_scale = _sphere_scale;
		build_sphere_list();
	}

	void build_sphere_list(){

		input_context->makeCurrent();

		glDeleteLists(sphere_index, 1);
		glDeleteLists(small_sphere_index, 1);

		sphere_index = glGenLists(1);
		small_sphere_index = glGenLists(1);

		glEnable(GL_LIGHTING);

		// compile the display list
		glNewList(sphere_index, GL_COMPILE);
		BasicGL::drawSphere( 0., 0., 0., sphere_scale*average_edge_halfsize/2., 15, 15 );
		glEndList();

		// compile the display list
		glNewList(small_sphere_index, GL_COMPILE);
		BasicGL::drawSphere( 0., 0., 0., sphere_scale*average_edge_halfsize/4., 15, 15 );
		glEndList();

		input_context->doneCurrent();

	}

	void setMode( int m )
	{
		deformationMode = MeshModificationMode(m);
	}
	void setMethod( int m )
	{
		deformationMethod = MeshModificationMethod(m);
	}

	void setUseARAP( bool _useARAP )
	{
		useARAP = _useARAP;
	}

	void drawPoints( std::vector< int > & pts )
	{
		glBegin( GL_POINTS );
		for( unsigned int i = 0 ; i < pts.size() ; ++i )
			glVertex( mesh_modified_vertices[ pts[i] ] );
		glEnd();
	}


	void clear()
	{
		mesh_vertices.clear();
		mesh_triangles.clear();
		mesh_vertex_normals.clear();

		mesh_modified_vertices.clear();
		outliers.clear();

		cage_vertices.clear();
		cage_triangles.clear();
		cage_triangle_normals.clear();

		initial_cage_vertices.clear();
		initial_cage_triangles.clear();
		initial_cage_triangle_normals.clear();

		cage_selected_vertices.clear();
		cage_fixed_vertices.clear();


		cage_visu_triangles.clear();
		cage_visu_quads.clear();

		GREENphiCoordinates.clear();
		GREENpsiCoordinates.clear();
		GREENscalingFactors.clear();

		MVCCoordinates.clear();

		ARAP.clear();

		problematicVertices.clear();
		problematicTriangles.clear();
		problems = false;
	}

	void clear_mesh_info(){

		mesh_vertices.clear();
		mesh_triangles.clear();
		mesh_vertex_normals.clear();

		mesh_modified_vertices.clear();

		GREENphiCoordinates.clear();
		GREENpsiCoordinates.clear();
		GREENscalingFactors.clear();

		MVCCoordinates.clear();

		outliers.clear();

	}

	void perturbeCage(){

		float min_dist = FLT_MAX;
		for(unsigned int i = 0 ; i < cage_vertices.size() ; i++ ){

			const point_t point = cage_vertices[i];
			for(unsigned int j = i+1 ; j < cage_vertices.size() ; j++ ){
				float dist = (cage_vertices[j] - point).norm();

					if( dist < min_dist ) min_dist = dist;

				}
		}

		min_dist = min_dist*0.000001;

		std::cout << min_dist << std::endl;
		for(unsigned int i = 0 ; i < cage_vertices.size() ; i++ ){
			cage_vertices[i] += point_t(Math::getRandomFloatBetween(-min_dist, min_dist), Math::getRandomFloatBetween(-min_dist, min_dist), Math::getRandomFloatBetween(-min_dist, min_dist));
		}
	}

	void locateProblems( const std::vector<Triangle> & triangles ){

		if( triangles.size() == 0 ) return;
		problematicVertices.clear();
		problematicVertices.resize( cage_vertices.size(), false );

		problematicTriangles.clear();
		problematicTriangles.resize( cage_triangles.size(), false );

		std::vector< std::vector<float> > phiLaplacien ( mesh_vertices.size() );

		for( unsigned int i = 0 ; i < mesh_vertices.size() ; i++ ){
			phiLaplacien[i].resize(cage_vertices.size(), 0.f);
		}

		std::vector< std::vector<float> > psiLaplacien ( mesh_vertices.size() );

		for( unsigned int i = 0 ; i < mesh_vertices.size() ; i++ ){
			psiLaplacien[i].resize(cage_triangles.size(), 0.f);
		}

		for( unsigned int t = 0 ; t < triangles.size() ; t++ ){
			unsigned int v1 = triangles[t].getVertex(0);
			unsigned int v2 = triangles[t].getVertex(1);
			unsigned int v3 = triangles[t].getVertex(2);
			for( unsigned int i = 0 ; i < cage_vertices.size() ; ++i )
			{
				phiLaplacien[ v1 ][ i ] += GREENphiCoordinates[ v1 ][i] - GREENphiCoordinates[ v2 ][i];
				phiLaplacien[ v1 ][ i ] += GREENphiCoordinates[ v1 ][i] - GREENphiCoordinates[ v3 ][i];
				phiLaplacien[ v2 ][ i ] += GREENphiCoordinates[ v2 ][i] - GREENphiCoordinates[ v1 ][i];
				phiLaplacien[ v2 ][ i ] += GREENphiCoordinates[ v2 ][i] - GREENphiCoordinates[ v3 ][i];
				phiLaplacien[ v3 ][ i ] += GREENphiCoordinates[ v3 ][i] - GREENphiCoordinates[ v2 ][i];
				phiLaplacien[ v3 ][ i ] += GREENphiCoordinates[ v3 ][i] - GREENphiCoordinates[ v1 ][i];
			}
			for( unsigned int i = 0 ; i < cage_triangles.size() ; ++i )
			{
				psiLaplacien[ v1 ][ i ] += GREENpsiCoordinates[ v1 ][i] - GREENpsiCoordinates[ v2 ][i];
				psiLaplacien[ v1 ][ i ] += GREENpsiCoordinates[ v1 ][i] - GREENpsiCoordinates[ v3 ][i];
				psiLaplacien[ v2 ][ i ] += GREENpsiCoordinates[ v2 ][i] - GREENpsiCoordinates[ v1 ][i];
				psiLaplacien[ v2 ][ i ] += GREENpsiCoordinates[ v2 ][i] - GREENpsiCoordinates[ v3 ][i];
				psiLaplacien[ v3 ][ i ] += GREENpsiCoordinates[ v3 ][i] - GREENpsiCoordinates[ v2 ][i];
				psiLaplacien[ v3 ][ i ] += GREENpsiCoordinates[ v3 ][i] - GREENpsiCoordinates[ v1 ][i];
				//std::cout <<  psiLaplacien[ v3 ][ i ] << std::endl;
			}
		}


		float philaplmax = -1, psilapmax = -1;

		for( unsigned int i = 0 ; i < mesh_vertices.size() ; i++ ){
			for( unsigned int j = 0 ; j < cage_vertices.size() ; ++j )
			{
				philaplmax = max( philaplmax , fabs(phiLaplacien[i][j])) ;
			}
		}
		for( unsigned int i = 0 ; i < mesh_vertices.size() ; i++ ){
			for( unsigned int j = 0 ; j < cage_triangles.size() ; ++j )
			{
				psilapmax = max( psilapmax , fabs(psiLaplacien[i][j])) ;
			}
		}


		for( unsigned int i = 0 ; i < mesh_vertices.size() ; i++ ){
			for( unsigned int j = 0 ; j < cage_vertices.size() ; ++j )
			{
				if( fabs(phiLaplacien[i][j]  ) > philaplmax / 2. ){
					problematicVertices[j] = true;
					problems = true;
				}
			}
		}
		for( unsigned int i = 0 ; i < mesh_vertices.size() ; i++ ){
			for( unsigned int j = 0 ; j < cage_triangles.size() ; ++j )
			{
				if( fabs(psiLaplacien[i][j]  ) > psilapmax / 2. ){
					problematicTriangles[j] = true;
					problems = true;
				}
			}
		}
	}

	bool computeOutlier( unsigned int i, float error_max){

		point_t up_position;
		getVertexInitialPosition(i, up_position);

		point_t diff = ( mesh_vertices[i] - up_position );

		return ( ! diff.isNull() && (diff.norm() > error_max) );
	}

	void computeOutliers(){

		BasicPoint bb, BB;
		getCageBBox(bb, BB);

		float error_max = (BB - bb).norm()/1000.;

		outliers.clear();
		outliers.resize(mesh_vertices.size());

		for( unsigned int v = 0 ; v < mesh_vertices.size() ; ++v )
			outliers[v] = computeOutlier(v, error_max);

	}

	void reInitialize()
	{

		cage_triangle_normals.clear();

		cage_selected_vertices.clear();
		cage_selected_vertices.resize( cage_vertices.size() , false);
		cage_fixed_vertices.clear();
		cage_fixed_vertices.resize( cage_vertices.size() , false);

		problems = false;
		problematicVertices.clear();
		problematicTriangles.clear();

		cage_triangles.clear();

		GREENphiCoordinates.clear();
		GREENpsiCoordinates.clear();
		GREENscalingFactors.clear();

		MVCCoordinates.clear();

		initial_cage_vertices.clear();
		initial_cage_triangles.clear();
		initial_cage_triangle_normals.clear();

		// convert cage TriQuad (cage_visu_quads and cage_visu_triangles) to triangles (cage_triangles) :
		for( unsigned int t = 0 ; t < cage_visu_triangles.size() ; ++t )
		{
			int _v1 = cage_visu_triangles[t][0] , _v2 = cage_visu_triangles[t][1] , _v3 = cage_visu_triangles[t][2];
			vector< int > _v;

			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );

			cage_triangles.push_back( _v );

			GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v2] - cage_vertices[_v1] , cage_vertices[_v3] - cage_vertices[_v1] ) );
		}
		for( unsigned int q = 0 ; q < cage_visu_quads.size() ; ++q )
		{
			int _v1 = cage_visu_quads[q][0] , _v2 = cage_visu_quads[q][1] , _v3 = cage_visu_quads[q][2] , _v4 = cage_visu_quads[q][3];
			vector< int > _v;

			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );

			cage_triangles.push_back( _v );

			GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v2] - cage_vertices[_v1] , cage_vertices[_v3] - cage_vertices[_v1] ) );

			_v.clear();
			_v.push_back( _v1 );
			_v.push_back( _v3 );
			_v.push_back( _v4 );

			cage_triangles.push_back( _v );

			GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v3] - cage_vertices[_v1] , cage_vertices[_v4] - cage_vertices[_v1] ) );
		}

		// cage normals :
		update_cage_triangle_normals();

		initial_cage_triangles.resize(cage_triangles.size());
		for( unsigned int i = 0 ; i < cage_triangles.size() ; i++ ){
			initial_cage_triangles[i] = cage_triangles[i];
		}

		initial_cage_vertices.resize(cage_vertices.size());
		for( unsigned int i = 0 ; i < cage_vertices.size() ; i++ ){
			initial_cage_vertices[i] = cage_vertices[i];
		}

		initial_cage_triangle_normals.resize(cage_triangle_normals.size());
		for( unsigned int i = 0 ; i < cage_triangle_normals.size() ; i++ ){
			initial_cage_triangle_normals[i] = cage_triangle_normals[i];
		}

		compute_max_sphere_radius();

		   // computeOutliers();

		ARAP.clear();
		ARAP.init( cage_vertices, cage_triangles );
	}

	void compute_cage_coordinates(){

		mesh_vertex_normals.clear();

		mesh_modified_vertices.clear();
		for( unsigned int v = 0 ; v < mesh_vertices.size() ; ++v )
			mesh_modified_vertices.push_back( mesh_vertices[ v ] );

		point_t bb , BB;
		getModelBBox( bb , BB );
		// compute cage coordinates :
		computeCoordinates( (BB-bb).norm() );

	}


	void update_mesh_vertex_normals()
	{
		mesh_vertex_normals.clear();
		mesh_vertex_normals.resize( mesh_vertices.size() , point_t(0,0,0) );

		for( unsigned int t = 0 ; t < mesh_triangles.size(); ++t )
		{
			point_t const & tri_normal = ( mesh_vertices[ mesh_triangles[t][1] ] - mesh_vertices[ mesh_triangles[t][0] ] ) %
						 ( mesh_vertices[ mesh_triangles[t][2] ] - mesh_vertices[ mesh_triangles[t][0] ] );

			mesh_vertex_normals[ mesh_triangles[t][0] ] += tri_normal;
			mesh_vertex_normals[ mesh_triangles[t][1] ] += tri_normal;
			mesh_vertex_normals[ mesh_triangles[t][2] ] += tri_normal;
		}

		for( unsigned int v = 0 ; v < mesh_vertices.size() ; ++v )
		{
			mesh_vertex_normals[ v ].normalize();
		}
	}

	void update_cage_triangle_normals()
	{
		cage_triangle_normals.clear();
		cage_triangle_normals.resize( cage_triangles.size() );
		for( unsigned int t = 0 ; t < cage_triangles.size(); ++t )
		{
			cage_triangle_normals[ t ] = ( cage_vertices[ cage_triangles[t][1] ] - cage_vertices[ cage_triangles[t][0] ] ) %
						 ( cage_vertices[ cage_triangles[t][2] ] - cage_vertices[ cage_triangles[t][0] ] );

			cage_triangle_normals[ t ].normalize();
		}

	}

	void compute_max_sphere_radius()
	{

		average_edge_halfsize = 0.;
		for( unsigned int t = 0 ; t < cage_triangles.size(); ++t )
		{
			for( int i = 0 ; i < 3 ; i ++ ){
			double elenght = (cage_vertices[ cage_triangles[t][(i+1)%3] ] - cage_vertices[ cage_triangles[t][i] ] ).norm();
			average_edge_halfsize += elenght;
			}
		}

		average_edge_halfsize = average_edge_halfsize/(2.*cage_triangles.size()*3.);

		build_sphere_list();
	}


	void update_cage_triangle_GREENscalingFactors()
	{
		for( unsigned int t = 0 ; t < cage_triangles.size() ; ++t )
		{
			GREENscalingFactors[ t ].computeScalingFactor( cage_vertices[ cage_triangles[t][1] ] - cage_vertices[ cage_triangles[t][0] ] ,
								   cage_vertices[ cage_triangles[t][2] ] - cage_vertices[ cage_triangles[t][0] ] );
		}
	}



	void saveDeformedModel( std::string const & filename )
	{
		std::ofstream myfile;
		myfile.open(filename.c_str());
		if (!myfile.is_open())
		{
			std::cout << filename << " cannot be opened" << std::endl;
			return;
		}

		myfile << "OFF" << std::endl;
		myfile << (mesh_modified_vertices.size()) << " " << (mesh_triangles.size()) << " 0" << std::endl;

		for( unsigned int v = 0 ; v < mesh_modified_vertices.size() ; ++v )
		{
			myfile << (mesh_modified_vertices[v]) << std::endl;
		}

		for( unsigned int t = 0 ; t < mesh_triangles.size() ; ++t )
		{
			myfile << "3 " << (mesh_triangles[t][0]) << " " << (mesh_triangles[t][0]) << " " << (mesh_triangles[t][0]) << std::endl;
		}

		myfile.close();
	}

	void saveCage( std::string const & filename )
	{
		std::ofstream myfile;
		myfile.open(filename.c_str());
		if (!myfile.is_open())
		{
			std::cout << filename << " cannot be opened" << std::endl;
			return;
		}

		myfile << "OFF" << std::endl;
		myfile << (cage_vertices.size()) << " " << (cage_visu_quads.size()+cage_visu_triangles.size()) << " 0" << std::endl;

		for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
		{
			myfile << (cage_vertices[v]) << std::endl;
		}

		for( unsigned int t = 0 ; t < cage_visu_triangles.size() ; ++t )
		{
			myfile << "3 " << (cage_visu_triangles[t][0]) << " " << (cage_visu_triangles[t][1]) << " " << (cage_visu_triangles[t][2]) << std::endl;
		}

		for( unsigned int t = 0 ; t < cage_visu_quads.size() ; ++t )
		{
			myfile << "4 " << (cage_visu_quads[t][0]) << " " << (cage_visu_quads[t][1]) << " " << (cage_visu_quads[t][2]) << " " << (cage_visu_quads[t][3]) << std::endl;
		}

		myfile.close();
	}


	void open_OFF_mesh( std::string const & filename  ,  point_t & bb  ,  point_t & BB )
	{
		std::ifstream myfile;
		myfile.open(filename.c_str());
		if (!myfile.is_open())
		{
			std::cout << filename << " cannot be opened" << std::endl;
			return;
		}

		std::string magic_s;

		myfile >> magic_s;

		if( magic_s != "OFF" )
		{
			std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
			myfile.close();
			exit(1);
		}



		int n_vertices , n_faces , dummy_int;
		myfile >> n_vertices >> n_faces >> dummy_int;


		mesh_vertices.clear();
		mesh_modified_vertices.clear();
		bb = point_t( FLT_MAX , FLT_MAX , FLT_MAX );
		BB = point_t( -FLT_MAX , -FLT_MAX , -FLT_MAX );
		for( int v = 0 ; v < n_vertices ; ++v )
		{
			float x , y , z;
			myfile >> x >> y >> z;
			mesh_vertices.push_back( point_t( x , y , z ) );
			mesh_modified_vertices.push_back( point_t( x , y , z ) );
			bb = min( bb , point_t( x , y , z ) );
			BB = max( BB , point_t( x , y , z ) );
		}

		mesh_triangles.clear();
		for( int f = 0 ; f < n_faces ; ++f )
		{
			int n_vertices_on_face;
			myfile >> n_vertices_on_face;
			if( n_vertices_on_face == 3 )
			{
			int _v1 , _v2 , _v3;
			vector< int > _v;
			myfile >> _v1 >> _v2 >> _v3;
			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );
			mesh_triangles.push_back( _v );
			}
			else if( n_vertices_on_face == 4 )
			{
			int _v1 , _v2 , _v3 , _v4;
			vector< int > _v;
			myfile >> _v1 >> _v2 >> _v3 >> _v4;
			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );
			mesh_triangles.push_back( _v );

			_v.clear();
			_v.push_back( _v1 );
			_v.push_back( _v3 );
			_v.push_back( _v4 );
			mesh_triangles.push_back( _v );
			}
			else
			{
			std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
			myfile.close();
			exit(1);
			}
		}
		reInitialize();
	}


	void open_OFF_cage( std::string const & filename )
	{
		std::ifstream myfile;
		myfile.open(filename.c_str());
		if (!myfile.is_open())
		{
			std::cout << filename << " cannot be opened" << std::endl;
			return;
		}

		std::string magic_s;

		myfile >> magic_s;

		if( magic_s != "OFF" )
		{
			std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
			myfile.close();
			exit(1);
		}



		int n_vertices , n_faces , dummy_int;
		myfile >> n_vertices >> n_faces >> dummy_int;

		cage_vertices.clear();

		for( int v = 0 ; v < n_vertices ; ++v )
		{
			float x , y , z;
			myfile >> x >> y >> z;
			cage_vertices.push_back( point_t( x , y , z ) );
		}

		cage_triangles.clear();
		cage_visu_triangles.clear();
		cage_visu_quads.clear();
		GREENscalingFactors.clear();
		for( int f = 0 ; f < n_faces ; ++f )
		{
			int n_vertices_on_face;
			myfile >> n_vertices_on_face;
			if( n_vertices_on_face == 3 )
			{
			int _v1 , _v2 , _v3;
			vector< int > _v;
			myfile >> _v1 >> _v2 >> _v3;
			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );

			cage_triangles.push_back( _v );

			GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v2] - cage_vertices[_v1] , cage_vertices[_v3] - cage_vertices[_v1] ) );

			// VISU :
			cage_visu_triangles.push_back( _v );
			}
			else if( n_vertices_on_face == 4 )
			{
			int _v1 , _v2 , _v3 , _v4;
			vector< int > _v;
			myfile >> _v1 >> _v2 >> _v3 >> _v4;
			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );

			cage_triangles.push_back( _v );

			GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v2] - cage_vertices[_v1] , cage_vertices[_v3] - cage_vertices[_v1] ) );

			_v.clear();
			_v.push_back( _v1 );
			_v.push_back( _v3 );
			_v.push_back( _v4 );

			cage_triangles.push_back( _v );

			GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v3] - cage_vertices[_v1] , cage_vertices[_v4] - cage_vertices[_v1] ) );

			// VISU :
			_v.clear();
			_v.push_back( _v1 );
			_v.push_back( _v2 );
			_v.push_back( _v3 );
			_v.push_back( _v4 );
			cage_visu_quads.push_back( _v );
			}
			else
			{
			std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
			myfile.close();
			exit(1);
			}
		}

		reInitialize();
	}

	void loadCageAndInitialize(const std::vector<point_t> & vertices , const std::vector<Triangle> & triangles , const std::vector<int> & envelopIndices )
	{

		clear();

		std::vector<bool> envelopVertices (vertices.size(), false);
		for( unsigned int i = 0 ; i < envelopIndices.size() ; i ++ ){
			const Triangle & T = triangles[ envelopIndices[i] ];
			for( int v = 0 ; v < 3 ; v++ ){
			envelopVertices[T.getVertex(v)] = true;
			}
		}

		std::map<unsigned int, unsigned int> VMap;
		unsigned int count = 0 ;


		for( unsigned int i = 0 ; i < vertices.size() ; i++ ){
			if( envelopVertices[i] ){
			cage_vertices.push_back( vertices[i] );
			VMap[i] = count++;
			} else {
			mesh_vertices.push_back( vertices[i] );
			}
		}

		for( unsigned int i = 0 ; i < envelopIndices.size() ; i ++ ){
			const Triangle & T = triangles[ envelopIndices[i] ];
			vector< int > _v;
			for(int v = 0 ; v < 3 ; v ++ )
			_v.push_back(VMap[T.getVertex(v)]);

			cage_visu_triangles.push_back( _v );
			addFace( _v[0] , _v[1] , _v[2] );
		}

		reInitialize();
		compute_cage_coordinates();
	}


	void loadCage(const std::vector<point_t> & vertices , const std::vector<Triangle> & triangles)
	{

		clear();

		cage_vertices = vertices;

		for( unsigned int i = 0 ; i < triangles.size() ; i ++ ){
			const Triangle & T = triangles[ i ];
			vector< int > _v;
			for(int v = 0 ; v < 3 ; v ++ )
			_v.push_back(T.getVertex(v));

			cage_visu_triangles.push_back( _v );
			addFace( _v[0] , _v[1] , _v[2] );
		}

		reInitialize();
	}

	void setBBCage(const point_t & _bb , const point_t & _BB)
	{

		float offset = (_BB - _bb).norm()*0.1;
		point_t BBMin = point_t (_bb[0] - offset, _bb[1] - offset, _bb[2] - offset);
		point_t BBMax = point_t (_BB[0] + offset, _BB[1] + offset, _BB[2] + offset);


		cage_vertices.clear();

		cage_vertices.push_back(BBMin);
		cage_vertices.push_back(point_t (BBMax[0], BBMin[1], BBMin[2]));
		cage_vertices.push_back(point_t (BBMax[0], BBMax[1], BBMin[2]));
		cage_vertices.push_back(point_t (BBMin[0], BBMax[1], BBMin[2]));

		cage_vertices.push_back(point_t (BBMin[0], BBMin[1], BBMax[2]));
		cage_vertices.push_back(point_t (BBMax[0], BBMin[1], BBMax[2]));
		cage_vertices.push_back(BBMax);
		cage_vertices.push_back(point_t (BBMin[0], BBMax[1], BBMax[2]));

		cage_triangles.clear();
		cage_visu_triangles.clear();
		cage_visu_quads.clear();
		GREENscalingFactors.clear();

		addFace(3,2,1,0);
		addFace(1,2,6,5);
		addFace(2,3,7,6);
		addFace(3,0,4,7);
		addFace(0,1,5,4);
		addFace(5,6,7,4);

		reInitialize();
	}

	void addFace(int _v1, int _v2, int _v3){

		vector< int > _v;

		_v.push_back( _v1 );
		_v.push_back( _v2 );
		_v.push_back( _v3 );

		cage_triangles.push_back( _v );

		GREENscalingFactors.push_back( GreenCoords::GreenScalingFactor< point_t >( cage_vertices[_v2] - cage_vertices[_v1] , cage_vertices[_v3] - cage_vertices[_v1] ) );

	}


	void addFace(int _v1, int _v2, int _v3, int _v4){

		addFace( _v1 , _v2, _v3 );

		addFace( _v1 , _v3, _v4 );

		// VISU :
		vector< int > _v;
		_v.push_back( _v1 );
		_v.push_back( _v2 );
		_v.push_back( _v3 );
		_v.push_back( _v4 );
		cage_visu_quads.push_back( _v );
	}


	void getCageBBox( point_t & bb , point_t & BB )
	{
		bb = point_t( FLT_MAX , FLT_MAX , FLT_MAX );
		BB = point_t( -FLT_MAX , -FLT_MAX , -FLT_MAX );
		for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
		{
			float x = cage_vertices[v][0];
			float y = cage_vertices[v][1];
			float z = cage_vertices[v][2];
			bb[0] = std::min( bb[0] , x );
			BB[0] = std::max( BB[0] , x );
			bb[1] = std::min( bb[1] , y );
			BB[1] = std::max( BB[1] , y );
			bb[2] = std::min( bb[2] , z );
			BB[2] = std::max( BB[2] , z );
		}
	}


	void getModelBBox( point_t & bb , point_t & BB )
	{
		bb = point_t( FLT_MAX , FLT_MAX , FLT_MAX );
		BB = point_t( -FLT_MAX , -FLT_MAX , -FLT_MAX );
		for( unsigned int v = 0 ; v < mesh_vertices.size() ; ++v )
		{
			float x = mesh_vertices[v][0];
			float y = mesh_vertices[v][1];
			float z = mesh_vertices[v][2];
			bb[0] = std::min( bb[0] , x );
			BB[0] = std::max( BB[0] , x );
			bb[1] = std::min( bb[1] , y );
			BB[1] = std::max( BB[1] , y );
			bb[2] = std::min( bb[2] , z );
			BB[2] = std::max( BB[2] , z );
		}
	}



	// This function gives you the index for the cage face you clicked on :  TODO
	int cageClickedFace( int x , int y , bool & isAQuad )
	{
		return -1;
	}



	inline void glVertex( BasicPoint const & p )
	{
		glVertex3f( p[0] , p[1] , p[2] );
	}
	inline void glNormal( BasicPoint const & p )
	{
		glNormal3f( p[0] , p[1] , p[2] );
	}

	void drawProblems(float scale){

		if(!problems) return ;
		for( unsigned int v = 0 ; v < cage_vertices.size() ; v++ ){
			point_t & center = cage_vertices[v];


			if( problematicVertices[ v ] )
			BasicGL::drawSphere( center[0], center[1], center[2], average_edge_halfsize*scale, 15, 15 );
		}


		for( unsigned int i = 0 ; i < cage_triangles.size() ; i++ ){

			if( problematicTriangles[ i ] ){
			glBegin(GL_TRIANGLES);
			for( unsigned int v = 0 ; v < 3 ; ++v ){
				glVertex( cage_vertices[ cage_triangles[i][v] ] );
			}
			glEnd();
			}
		}
	}


	void drawOrderedCage()
	{

		float modelview[16];
		glGetFloatv(GL_MODELVIEW_MATRIX , modelview);

		std::priority_queue< std::pair< float , int > , std::deque< std::pair< float , int > > , std::greater< std::pair< float , int > > > facesQueue;

		for (unsigned int t = 0 ; t < cage_visu_triangles.size() ; ++t )
		{
			point_t _center = (
				cage_vertices[ cage_visu_triangles[t][0] ]+
				cage_vertices[ cage_visu_triangles[t][1] ]+
				cage_vertices[ cage_visu_triangles[t][2] ]) / 3.f;
			facesQueue.push(
				std::make_pair(
					modelview[2] * _center[0] + modelview[6] * _center[1] + modelview[10] * _center[2] + modelview[14] ,
					t
					)
				);
		}

		for (unsigned int q = 0 ; q < cage_visu_quads.size() ; ++q )
		{
			point_t _center = (
				cage_vertices[ cage_visu_quads[q][0] ]+
				cage_vertices[ cage_visu_quads[q][1] ]+
				cage_vertices[ cage_visu_quads[q][2] ]+
				cage_vertices[ cage_visu_quads[q][3] ]) / 4.f;
			facesQueue.push(
				std::make_pair(
					modelview[2] * _center[0] + modelview[6] * _center[1] + modelview[10] * _center[2] + modelview[14] ,
					q + cage_visu_triangles.size()
					)
				);
		}


		while( ! facesQueue.empty() )
		{
			int face_index = facesQueue.top().second ;
			if( face_index >= (int)(cage_visu_triangles.size()) )
			{
			// Display quad number face_index - cage_visu_triangles.size() :
			face_index -= cage_visu_triangles.size();
			glBegin(GL_QUADS);
			for( unsigned int v = 0 ; v < 4 ; ++v )
				glVertex( cage_vertices[ cage_visu_quads[face_index][v] ] );
			glEnd();
			}
			else
			{
			// Display triangle number face_index :
			glBegin(GL_TRIANGLES);
			for( unsigned int v = 0 ; v < 3 ; ++v ){
				glVertex( cage_vertices[ cage_visu_triangles[face_index][v] ] );
			}
			glEnd();
			}

			facesQueue.pop();
		}
	}


	//  You  don't  really  need  that :
	void drawCage(  )
	{
		glBegin( GL_TRIANGLES );

		// Draw triangles :
		for (unsigned int t = 0 ; t < cage_visu_triangles.size() ; ++t )
		{
			for( unsigned int v = 0 ; v < 3 ; ++v )
			glVertex( cage_vertices[ cage_visu_triangles[t][v] ] );
		}
		glEnd();
		// Draw quads :
		glBegin( GL_QUADS );
		for (unsigned int q = 0 ; q < cage_visu_quads.size() ; ++q )
		{
			for( unsigned int v = 0 ; v < 4 ; ++v )
			glVertex( cage_vertices[ cage_visu_quads[q][v] ] );
		}
		glEnd();

	}


	//  You  don't  really  need  that :
	void drawBaseModel()
	{
		glEnable( GL_LIGHTING );
		glPolygonMode( GL_FRONT_AND_BACK , GL_FILL );
		glBegin( GL_TRIANGLES );
		for( unsigned int t = 0 ; t < mesh_triangles.size() ; ++t )
		{
			for( unsigned int v = 0 ; v < 3 ; ++v )
			{
			glNormal3f( mesh_vertex_normals[ mesh_triangles[t][v] ][0],
					mesh_vertex_normals[ mesh_triangles[t][v] ][1],
					mesh_vertex_normals[ mesh_triangles[t][v] ][2]);
			glVertex( mesh_vertices[ mesh_triangles[t][v] ] );
			}
		}
		glEnd();
	}


	//  You  don't  really  need  that :
	void drawModifiedModel()
	{
		glBegin( GL_TRIANGLES );
		for( unsigned int t = 0 ; t < mesh_triangles.size() ; ++t )
		{
			for( unsigned int v = 0 ; v < 3 ; ++v )
			{
			glNormal3f( mesh_vertex_normals[ mesh_triangles[t][v] ][0],
					mesh_vertex_normals[ mesh_triangles[t][v] ][1],
					mesh_vertex_normals[ mesh_triangles[t][v] ][2]);
			glVertex( mesh_modified_vertices[ mesh_triangles[t][v] ] );
			}
		}
		glEnd();
	}



	void drawCageSelectedVertices()
	{

		for( unsigned int v = 0 ; v < cage_vertices.size() ; v++ ){
			point_t & center = cage_vertices[v];

			bool large_sphere = true;
			if( cage_selected_vertices[ v ] ){
			glColor3f(0.8,0.,0.);
			} else if( cage_fixed_vertices[ v ] ) {
			glColor3f(0.,0.8,0.);
			} else {
			glColor3f(.8,0.8,0.8);
			large_sphere = false;
			}

			glPushMatrix();

			glTranslatef(center[0], center[1], center[2]);
			// draw the display list
			if(large_sphere)
			glCallList(sphere_index);
			else
			glCallList(small_sphere_index);

			glPopMatrix();
		}
	}

	void drawTriangulatedCageFaceNormals(float scale)
	{
		glBegin( GL_LINES );
		for( unsigned int t = 0 ; t < cage_triangles.size() ; ++t )
		{
			point_t const & center = ( cage_vertices[ cage_triangles[t][0] ] + cage_vertices[ cage_triangles[t][1] ] + cage_vertices[ cage_triangles[t][2] ] ) / 3.f;
			point_t normal = ( cage_vertices[ cage_triangles[t][1] ] - cage_vertices[ cage_triangles[t][0] ] ) % ( cage_vertices[ cage_triangles[t][2] ] - cage_vertices[ cage_triangles[t][0] ] );
			normal.normalize();

			glVertex( center );
			glVertex( center+normal*scale );
		}
		glEnd();
	}







	void addModelVertexAndComputeCoordinates( const std::vector<point_t> & points ){

		for(unsigned int i = 0 ; i < points.size() ; i ++){
			point_t deformed_point;

			addModelVertexAndComputeCoordinates(points[i], deformed_point);

		}
	}


	bool addModelVertexAndComputeCoordinates( const point_t & point, float error_max ){

		// !!!!!!!!!!!    MAKE SURE YOU CALLED  updateScalingFactors()  !!!!!!!!!!!!!!!!

		point_t up_position;

		addModelVertexAndComputeCoordinates(point, up_position);

		point_t diff = ( point - up_position );

		return true; //(  diff.isNull() || (diff.norm() < error_max) );

	}

	void addModelVertexAndComputeCoordinates( const point_t & point, point_t & pos ){

		// !!!!!!!!!!!    MAKE SURE YOU CALLED  updateScalingFactors()  !!!!!!!!!!!!!!!!
		int p_idx = mesh_vertices.size();

		mesh_vertices.push_back(point);

		GREENphiCoordinates.push_back( std::vector<double> (cage_vertices.size() , 0.f) );
		GREENpsiCoordinates.push_back(std::vector<double> (cage_triangles.size() , 0.f) );

		URAGO::computeCoordinatesOf3dPoint(
			mesh_vertices[ p_idx ] ,
			initial_cage_triangles ,
			initial_cage_vertices ,
			initial_cage_triangle_normals ,
			GREENphiCoordinates[ p_idx ],
			GREENpsiCoordinates[ p_idx ]);

		pos = point_t(0.,0.,0.);
		for( unsigned int vc = 0 ; vc < cage_vertices.size() ; ++vc )
			pos += GREENphiCoordinates[ p_idx ][ vc ] * cage_vertices[vc];
		for( unsigned int tc = 0 ; tc < cage_triangles.size() ; ++tc )
			pos += GREENpsiCoordinates[ p_idx ][ tc ] * GREENscalingFactors[ tc ].scalingFactor() * cage_triangle_normals[tc];

		mesh_modified_vertices.push_back(pos);

	}

	double computeDummy(std::vector<point_t> & positions){
		std::vector<double> dummyPhi(cage_vertices.size() , 0.f);
		std::vector<double> dummyPsi(cage_triangles.size() , 0.f);
		QTime timer;
		timer.start();
		unsigned int nEvals = 10000;
		for(unsigned int i = 0 ; i < nEvals ; i++){
			// !!!!!!!!!!!    MAKE SURE YOU CALLED  updateScalingFactors()  !!!!!!!!!!!!!!!!
			URAGO::computeCoordinatesOf3dPoint(
				positions[i] ,
				initial_cage_triangles ,
				initial_cage_vertices ,
				initial_cage_triangle_normals ,
				dummyPhi,
				dummyPsi);

			point_t pos = point_t(0.,0.,0.);
			for( unsigned int vc = 0 ; vc < cage_vertices.size() ; ++vc )
			pos += dummyPhi[ vc ] * cage_vertices[vc];
			for( unsigned int tc = 0 ; tc < cage_triangles.size() ; ++tc )
			pos += dummyPsi[ tc ] * GREENscalingFactors[ tc ].scalingFactor() * cage_triangle_normals[tc];
		}
		double res = (((double)(timer.elapsed()) / 1000.0) / nEvals);
		std::cout << "Evaluation took " << res << " s in average (evaluated on " << nEvals << " samples)" << std::endl;
		return res;
	}

	void getVertexInitialPosition( int p_idx ,  point_t & pos  )
	{
		if( p_idx < GREENphiCoordinates.size() ){
			////        !!!!!!!!!!!!!            the scaling factor of the original cage is 1.0    !!!!!!!!!!!!!!!!!!!!!!!!
			pos = point_t(0.,0.,0.);
			for( unsigned int vc = 0 ; vc < initial_cage_vertices.size() ; ++vc )
			pos += GREENphiCoordinates[ p_idx ][ vc ] * initial_cage_vertices[vc];
			for( unsigned int tc = 0 ; tc < initial_cage_triangles.size() ; ++tc )
			pos += GREENpsiCoordinates[ p_idx ][ tc ] * 1.0 * initial_cage_triangle_normals[tc];
		}

	}

	void computeGreenCoordinates( float modelDiameter )
	{
		if(mesh_vertices.size() == 0)
			return ;
		GREENphiCoordinates.clear();
		GREENpsiCoordinates.clear();
		GREENphiCoordinates.resize( mesh_vertices.size() );
		GREENpsiCoordinates.resize( mesh_vertices.size() );

		//        unsigned int count = 0;
		//        for (unsigned int i =0; i < mesh_vertices.size (); i++) {
		//            count += cage_vertices.size() + cage_triangles.size();
		//        }

		for (unsigned int i =0; i < mesh_vertices.size (); i++) {
			GREENphiCoordinates[i].clear();
			GREENpsiCoordinates[i].clear();
			GREENphiCoordinates[i].resize( cage_vertices.size() , 0.f );
			GREENpsiCoordinates[i].resize( cage_triangles.size() , 0.f );
		}
#pragma omp parallel for

		for( unsigned int p_idx = 0 ; p_idx < mesh_vertices.size() ; ++p_idx )
		{
			URAGO::computeCoordinatesOf3dPoint(
				mesh_vertices[ p_idx ] ,
				cage_triangles ,
				cage_vertices ,
				cage_triangle_normals ,
				GREENphiCoordinates[ p_idx ],
				GREENpsiCoordinates[ p_idx ]);
		}
	}

	void computeMVCCoordinates()
	{

		MVCCoordinates.clear();
		MVCCoordinates.resize( mesh_vertices.size() );

#pragma omp parallel for
		for( unsigned int p_idx = 0 ; p_idx < mesh_vertices.size() ; ++p_idx )
		{
			MVCCoords::computeMVCCoordinatesOf3dPoint(
				mesh_vertices[ p_idx ] ,
				cage_triangles ,
				cage_vertices ,
				cage_triangle_normals ,
				MVCCoordinates[ p_idx ]);
		}
	}

	void computeCoordinates( float modelDiameter )
	{
		if(deformationMethod == MVC)
			computeMVCCoordinates();
		else
			computeGreenCoordinates( modelDiameter );
	}
















	// You need to have the correct QGLContext activated for that !!!!!!!!
	// This function select the cage vertices drawn inside the QRect "zone"
	void select( QRectF const & zone, bool moving = true )
	{
	float modelview[16];
	glGetFloatv(GL_MODELVIEW_MATRIX , modelview);
	float projection[16];
	glGetFloatv(GL_PROJECTION_MATRIX , projection);


	for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
	{
		point_t const & p = cage_vertices[ v ];

		float x = modelview[0] * p[0] + modelview[4] * p[1] + modelview[8] * p[2] + modelview[12];
		float y = modelview[1] * p[0] + modelview[5] * p[1] + modelview[9] * p[2] + modelview[13];
		float z = modelview[2] * p[0] + modelview[6] * p[1] + modelview[10] * p[2] + modelview[14];
		float w = modelview[3] * p[0] + modelview[7] * p[1] + modelview[11] * p[2] + modelview[15];
		x /= w;
		y /= w;
		z /= w;
		w = 1.f;

		float xx = projection[0] * x + projection[4] * y + projection[8] * z + projection[12] * w;
		float yy = projection[1] * x + projection[5] * y + projection[9] * z + projection[13] * w;
		float ww = projection[3] * x + projection[7] * y + projection[11] * z + projection[15] * w;
		xx /= ww;
		yy /= ww;

		xx = ( xx + 1.f ) / 2.f;
		yy = 1.f - ( yy + 1.f ) / 2.f;

		if( zone.contains( xx , yy ) ){
		cage_selected_vertices[ v ] = moving;
		cage_fixed_vertices[ v ] = !moving;
		}
	}
	}


	int index_of_closest_point_in_sphere( const point_t & clicked, float radius ){

	int result = -1;
	float min_dist = FLT_MAX;
	for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
	{
		float dist = (cage_vertices[v] - clicked).norm();
		if( dist <= radius && dist < min_dist ){
		result = v;
		min_dist = dist;
		}
	}

	return result;
	}

	void select( const point_t & clicked, float scale )
	{
	int v = index_of_closest_point_in_sphere( clicked, scale*average_edge_halfsize );
	if( v >= 0 ){

		cage_selected_vertices[ v ] = !cage_selected_vertices[ v ];
		cage_fixed_vertices[ v ] = false;

	}

	}

	void make_fixed_handles( const point_t & clicked, float scale )
	{
	int v = index_of_closest_point_in_sphere( clicked, scale*average_edge_halfsize );
	if( v >= 0 ){
		bool fixed  = cage_fixed_vertices[ v ] ;
		cage_fixed_vertices[ v ] = !fixed;
		cage_selected_vertices[ v ] = fixed;

	}

	}

	// You need to have the correct QGLContext activated for that !!!!!!!!
	// This function unselect the cage vertices drawn inside the QRect "zone"
	void unselect( QRectF const & zone )
	{
	float modelview[16];
	glGetFloatv(GL_MODELVIEW_MATRIX , modelview);
	float projection[16];
	glGetFloatv(GL_PROJECTION_MATRIX , projection);


	for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
	{
		point_t const & p = cage_vertices[ v ];

		float x = modelview[0] * p[0] + modelview[4] * p[1] + modelview[8] * p[2] + modelview[12];
		float y = modelview[1] * p[0] + modelview[5] * p[1] + modelview[9] * p[2] + modelview[13];
		float z = modelview[2] * p[0] + modelview[6] * p[1] + modelview[10] * p[2] + modelview[14];
		float w = modelview[3] * p[0] + modelview[7] * p[1] + modelview[11] * p[2] + modelview[15];
		x /= w;
		y /= w;
		z /= w;
		w = 1.f;

		float xx = projection[0] * x + projection[4] * y + projection[8] * z + projection[12] * w;
		float yy = projection[1] * x + projection[5] * y + projection[9] * z + projection[13] * w;
		float ww = projection[3] * x + projection[7] * y + projection[11] * z + projection[15] * w;
		xx /= ww;
		yy /= ww;

		xx = ( xx + 1.f ) / 2.f;
		yy = 1.f - ( yy + 1.f ) / 2.f;

		if( zone.contains( xx , yy ) ){
		cage_selected_vertices[ v ] = false;
		cage_fixed_vertices[ v ] = false;
		}
	}
	}

	void clear_selection(){
	for(unsigned int v = 0 ; v < cage_selected_vertices.size() ; v++ ){
		cage_selected_vertices[ v ] = false;
		cage_fixed_vertices[ v ] = false;
	}
	}


	void select_all(){
	for(unsigned int v = 0 ; v < cage_selected_vertices.size() ; v++ ){
		if(!cage_fixed_vertices[ v ]){
		cage_selected_vertices[ v ] = true;
		}
	}
	}

	void unselect_all(){
	for(unsigned int v = 0 ; v < cage_selected_vertices.size() ; v++ ){
		cage_selected_vertices[ v ] = false;
	}
	}

	void fixe_all(){
	for(unsigned int v = 0 ; v < cage_selected_vertices.size() ; v++ ){
		if(!cage_selected_vertices[ v ]){
		cage_fixed_vertices[ v ] = true;
		}
	}
	}

	void unfixe_all(){
	for(unsigned int v = 0 ; v < cage_fixed_vertices.size() ; v++ ){
		cage_fixed_vertices[ v ] = false;
	}
	}

	// Once you're OK with the selection you made , call this function to compute the manipulator , and it will activate it :
	void computeManipulatorForSelection( RotationManipulator * manipulator )
	{
	manipulator->resetScales();

	manipulator->clear();

	int nb=0, nb_f = 0;
	qglviewer::Vec oo( 0.f , 0.f , 0.f );
	qglviewer::Vec oa( 0.f , 0.f , 0.f );
	for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
	{
		if( cage_selected_vertices[v] )
		{
		point_t const & p = cage_vertices[v];
		oo += qglviewer::Vec( p[0] , p[1] , p[2] );
		++nb;
		}

		if( cage_fixed_vertices[v] )
		{
		point_t const & p = cage_vertices[v];
		oa += qglviewer::Vec( p[0] , p[1] , p[2] );
		++nb_f;
		}
	}
	oo /= nb;
	oa /= nb_f;

	PCATools::PCASolver3f< qglviewer::Vec , qglviewer::Vec > solver;
	solver.setOrigine( oo );

	PCATools::PCASolver3f< qglviewer::Vec , qglviewer::Vec > fixed_solver;
	fixed_solver.setOrigine( oa );

	for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
	{
		if( cage_selected_vertices[v] )
		{
		point_t const & p = cage_vertices[v];
		solver.addPoint( qglviewer::Vec( p[0] , p[1] , p[2] ) );
		}
		if( cage_fixed_vertices[v] )
		{
		point_t const & p = cage_vertices[v];
		fixed_solver.addPoint( qglviewer::Vec( p[0] , p[1] , p[2] ) );
		}
	}

	solver.compute();
	fixed_solver.compute();

	manipulator->setOrigine( oo );
	manipulator->setRepX( solver.RepX() );
	manipulator->setRepY( solver.RepY() );
	manipulator->setRepZ( solver.RepZ() );


	manipulator->setAngularOrigine( oa );

	qglviewer::Vec a = oo - oa;
	a.normalize();
	manipulator->setAngularRepX( a );
	qglviewer::Vec b (-a.y , a.x, 0. );
	manipulator->setAngularRepY( b );
	qglviewer::Vec repz = qglviewer::Vec(a.y*b.z - a.z*b.y,
						 a.z*b.x - a.x*b.z,
						 a.x*b.y - a.y*b.x);
	repz.normalize();
	manipulator->setAngularRepZ( repz );

	manipulator->computeRepCoords();
	for( unsigned int v = 0 ; v < cage_vertices.size() ; ++v )
	{
		if( cage_selected_vertices[v] )
		{
		/// ! Changed ! ///
		//selected_vertices[v] = false;
		point_t const & p = cage_vertices[v];
		manipulator->addPoint( v , qglviewer::Vec( p[0] , p[1] , p[2] ) );
		}
	}

	manipulator->activate();

	ARAP.setHandles(get_cage_handles_vertices());
	}


	void setCageToPosition( const std::vector<point_t> & _cage_vertices )
	{
	unsigned int n_points = _cage_vertices.size();

	if(cage_vertices.size() != n_points) return;

	for( unsigned int i = 0 ; i < n_points ; ++i )
	{
		cage_vertices[ i ] = _cage_vertices[i];
	}

	update_cage_triangle_normals();

	//ARAP.init(cage_vertices, cage_triangles);
	updateModel();

	}


	// When you move your manipulator , it sends you a SIGNAL.
	// When it happens, call that function with the manipulator as the parameter, it will update everything :
	void cageChanged( RotationManipulator * manipulator )
	{
	unsigned int n_points = manipulator->n_points();
	qglviewer::Vec p;
	int idx;
	for( unsigned int i = 0 ; i < n_points ; ++i )
	{
		manipulator->getTransformedPoint( i , idx , p );

		cage_vertices[ idx ] = point_t( p[0] , p[1] , p[2] );
	}

	if(useARAP)
		ARAP.compute_deformation(cage_vertices);

	update_cage_triangle_normals();

	if( deformationMode == REALTIME )
	{
		updateModel();
	}
	}



	// When you release the mouse after moving the manipulator, it sends you a SIGNAL.
	// When it happens, call that function with the manipulator as the parameter, it will update everything :
	void manipulatorReleased()
	{
	if( deformationMode == INTERACTIVE )
	{
		updateModel();
	}
	}



	// You should never have to call that function on your own, but why not :
	void updateModel()
	{
	if( deformationMethod == GREEN )
	{
		update_cage_triangle_GREENscalingFactors();
		for( unsigned int v = 0 ; v < mesh_modified_vertices.size() ; ++v )
		{
		point_t pos(0,0,0);
		for( unsigned int vc = 0 ; vc < cage_vertices.size() ; ++vc )
			pos += GREENphiCoordinates[ v ][ vc ] * cage_vertices[vc];
		for( unsigned int tc = 0 ; tc < cage_triangles.size() ; ++tc )
			pos += GREENpsiCoordinates[ v ][ tc ] * GREENscalingFactors[ tc ].scalingFactor() * cage_triangle_normals[tc];

		mesh_modified_vertices[ v ] = pos;
		}
	}
	else if( deformationMethod == MVC )
	{
		for( unsigned int v = 0 ; v < mesh_modified_vertices.size() ; ++v )
		{
		point_t pos(0,0,0);
		for( unsigned int vc = 0 ; vc < MVCCoordinates[ v ].size() ; ++vc )
			pos += MVCCoordinates[ v ][ vc ].second * cage_vertices[ MVCCoordinates[ v ][ vc ].first ];

		mesh_modified_vertices[ v ] = pos;
		}
	}
	}
};







#endif // CAGEMANIPINTERFACE_H
