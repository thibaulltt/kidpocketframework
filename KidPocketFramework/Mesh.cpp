#include "Mesh.h"
#include "GLUtilityMethods.h"

#include "ShaderPool.h"
#include "ShaderInclude.h"


// Criteria
typedef Mesh_3::Facet_iterator Facet_iterator;
typedef Mesh_3::Cell_iterator Cell_iterator;
typedef Mesh_3::Vertex_handle Vertex_handle;
typedef Mesh_3::Cell_handle Cell_handle;
typedef K::Direction_3 Direction_3;
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Triangulation;
typedef Triangulation::Finite_facets_iterator Finite_facets_iterator;
typedef Triangulation::Finite_vertices_iterator Finite_vertices_iterator;
typedef Triangulation::Finite_cells_iterator Finite_cells_iterator;

Mesh::Mesh(){
    init();
}

Mesh::Mesh(const Mesh_3 & c3t3){

    init();

    const Triangulation & triangulation = c3t3.triangulation();

    std::map<Vertex_handle, int> VMap;
    int VCount = 0;

    vertices.clear();
    //    verticesDimensions.clear();
    vertices.resize(triangulation.number_of_vertices());
    //    verticesDimensions.resize(triangulation.number_of_vertices());

    int surf_v_count = 0;
    for(Finite_vertices_iterator vit = triangulation.finite_vertices_begin() ; vit != triangulation.finite_vertices_end() ; vit++){
        const Point_3 & point = vit->point();
        vertices[VCount] = BasicPoint(point.x(), point.y(), point.z());
        VMap[vit] = VCount++;
        if(c3t3.in_dimension(vit)==2)
            surf_v_count++;
    }

    std::cout << 100.*float(surf_v_count) / c3t3.triangulation().number_of_vertices() << " % of vertices on boundaries" << std::endl;

    for(Triangulation::Finite_facets_iterator fit = triangulation.finite_facets_begin() ; fit != triangulation.finite_facets_end() ; ++fit ){

        Facet mirror = triangulation.mirror_facet(*fit);

        if(c3t3.is_in_complex(fit->first, fit->second)){

            if(c3t3.is_in_complex(fit->first) && !triangulation.is_infinite(fit->first)){
                Cell_handle ch = fit->first;
                Subdomain_index si = c3t3.subdomain_index(ch);

                if(triangulation.is_infinite(mirror.first) || !c3t3.is_in_complex(mirror.first))
                    CGAL_envelop.push_back(triangles.size());

                triangles.push_back(Triangle(VMap[ch->vertex(indices[fit->second][0])],
                                             VMap[ch->vertex(indices[fit->second][1])] ,
                                             VMap[ch->vertex(indices[fit->second][2])] , si));

                triangles_subdomain_ids.push_back(si);

            }


            if(c3t3.is_in_complex(mirror.first) && !triangulation.is_infinite(mirror.first)){
                Cell_handle ch = mirror.first;
                Subdomain_index si = c3t3.subdomain_index(ch);

                if(triangulation.is_infinite(fit->first) || !c3t3.is_in_complex(fit->first))
                    CGAL_envelop.push_back(triangles.size());

                triangles.push_back(Triangle(VMap[ch->vertex(indices[mirror.second][0])],
                                             VMap[ch->vertex(indices[mirror.second][1])] ,
                                             VMap[ch->vertex(indices[mirror.second][2])]));

                triangles_subdomain_ids.push_back(si);
            }
        }

    }

    std::map<Cell_handle, int> CMap;
    int count = 0;
    for(Triangulation::Finite_cells_iterator cit = triangulation.finite_cells_begin() ; cit != triangulation.finite_cells_end() ; ++cit ){
        CMap[cit] = count++;
    }


    for(Triangulation::Finite_cells_iterator cit = triangulation.finite_cells_begin() ; cit != triangulation.finite_cells_end() ; ++cit ){
        Subdomain_index si = c3t3.subdomain_index(cit) ;
        Tetrahedron tet = Tetrahedron(VMap[cit->vertex(0)], VMap[cit->vertex(1)], VMap[cit->vertex(2)], VMap[cit->vertex(3)], si);
        for( int i = 0 ; i < 4 ; i ++ ){
            if(!triangulation.is_infinite(cit->neighbor(i))){
                tet.setNeighbor( i, CMap[cit->neighbor(i)] );
            }
        }
        tetrahedra.push_back(tet);
        tetrahedra_subdomain_ids.push_back(si);
    }

    //    envelopVertices.clear();
    //    envelopVertices.resize( vertices.size(), false );
    //
    //    surf_v_count = 0;
    //    for( unsigned int i = 0 ; i < CGAL_envelop.size() ; i ++ ){
    //        const Triangle & T = triangles[ CGAL_envelop[i] ];
    //        for( int v = 0 ; v < 3 ; v++ ){
    //            envelopVertices[T.getVertex(v)] = true;
    //        }
    //    }
    //
    //    for( unsigned int i = 0 ; i < envelopVertices.size() ; i ++ ){
    //
    //        if( envelopVertices[i] ){
    //            surf_v_count++;
    //        }
    //    }
    //
    //    std::cout << 100.*surf_v_count / c3t3.triangulation().number_of_vertices() << " % of vertices on envelop" << std::endl;

    vsize = vertices.size();

}


void Mesh::init(){

    indices[0][0] = 3; indices[0][1] = 1; indices[0][2] = 2;
    indices[1][0] = 3; indices[1][1] = 2; indices[1][2] = 0;
    indices[2][0] = 3; indices[2][1] = 0; indices[2][2] = 1;
    indices[3][0] = 2; indices[3][1] = 1; indices[3][2] = 0;

    cut = BasicPoint(0.,0.,0.),
    cutDirection = BasicPoint(1.,1.,1.);

    textureNb = 1;
    maxiComputed = false;

    normalDirection = 1;

    displayCount = 0;
    textureCreated = false;

}

void Mesh::initTextures(){

    clearTextures();

    findGoodTexSize(triangles.size()*3*3, textureWidth, textureHeight, false);

    GLint iMaxTexSize = 256;
    glGetIntegerv( GL_MAX_TEXTURE_SIZE, &iMaxTexSize );

    if( iMaxTexSize < textureWidth || iMaxTexSize < textureHeight )
        return;

    glGenTextures(1, &triangleVerticesTextureId);

    glBindTexture(GL_TEXTURE_2D, triangleVerticesTextureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    glGenTextures(1, &triangleNormalsTextureId);

    glBindTexture(GL_TEXTURE_2D, triangleNormalsTextureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    glBindTexture(GL_TEXTURE_2D, 0);

    // normal array
    GLfloat normalsArray[] = {0.,0.,1., 0.,0.,1., 0.,0.,1.};
    GLfloat verticesArray[] = {0.,0.,0., 1.,0.,0., 0.,1.,0.};
    GLfloat textureCoords[] = {0.,0., 1.,1., 2.,2.};
    GLushort trindices[] = {0,1,2};


    triangleVerticesVBOId = createVBO(verticesArray, 3*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);
    triangleNormalsVBOId = createVBO(normalsArray, 3*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);
    triangleTextureCoordsVBOId = createVBO(textureCoords, 2*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);
    triangleIndicesVBOId = createVBO(trindices, 3*sizeof(GLushort), GL_ELEMENT_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);

    if( false && tetrahedra.size() > 0 ){
        findGoodTexSize(tetrahedra.size()*3*3*4, tetTextureWidth, tetTextureHeight, false);

        GLint iMaxTexSize = 256;
        glGetIntegerv( GL_MAX_TEXTURE_SIZE, &iMaxTexSize );

        if( iMaxTexSize < tetTextureWidth || iMaxTexSize < tetTextureHeight )
            return;

        glGenTextures(1, &tetNormalsTextureId);

        glBindTexture(GL_TEXTURE_2D, tetNormalsTextureId);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

        glBindTexture(GL_TEXTURE_2D, 0);

        // normal array
        GLfloat tetnormalsArray[] = { 0.,0.,1., 0.,0.,1., 0.,0.,1.,
                                      0.,0.,1., 0.,0.,1., 0.,0.,1.,
                                      0.,0.,1., 0.,0.,1., 0.,0.,1.,
                                      0.,0.,1., 0.,0.,1., 0.,0.,1.};
        GLfloat tetverticesArray[] = {indices[0][0],indices[0][1],indices[0][2],
                                      indices[1][0],indices[1][1],indices[1][2],
                                      indices[2][0],indices[2][1],indices[2][2],
                                      indices[3][0],indices[3][1],indices[3][2]};
        GLfloat tettextureCoords[] = {0.,0., 1.,1., 2.,2.,3.,3.};
        GLushort tetindices[] = {0,1,2,3};


        tetVerticesVBOId = createVBO(tetverticesArray, 4*3*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);
        tetNormalsVBOId = createVBO(tetnormalsArray, 4*3*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);
        tetTextureCoordsVBOId = createVBO(tettextureCoords, 2*4*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);
        tetIndicesVBOId = createVBO(tetindices, 4*sizeof(GLushort), GL_ELEMENT_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);

    }
    GetOpenGLError();

    textureCreated = true;
}


void Mesh::computeBB(){

    BBMin = BasicPoint( FLT_MAX, FLT_MAX, FLT_MAX );
    BBMax = BasicPoint( -FLT_MAX, -FLT_MAX, -FLT_MAX );

    for( unsigned int i = 0 ; i < vertices.size() ; i ++ ){
        const BasicPoint & point = vertices[i];
        for( int v = 0 ; v < 3 ; v++ ){
            float value = point[v];
            if( BBMin[v] > value ) BBMin[v] = value;
            if( BBMax[v] < value ) BBMax[v] = value;
        }
    }

    radius = (BBMax - BBMin).norm();
}

void Mesh::updateModel(){

    initTextures();
    sortBySubdomainIndex();
    recomputeNormals();
    computeBB();
    std::cout << "Mesh : " << vertices.size() << " vertices, " << triangles.size() << " triangles " << std::endl;
}

Mesh::~Mesh(){

    clearTextures();

}

void Mesh::clearTextures(){

    if(textureCreated){
        deleteVBO( triangleVerticesVBOId);
        deleteVBO( triangleNormalsVBOId);
        deleteVBO( triangleIndicesVBOId);
        deleteVBO( triangleTextureCoordsVBOId);

        glDeleteTextures(1, &triangleVerticesTextureId);
        glDeleteTextures(1, &triangleNormalsTextureId);

        if( tetrahedra.size() == 0 ) {
            deleteVBO( tetVerticesVBOId);
            deleteVBO( tetNormalsVBOId);
            deleteVBO( tetIndicesVBOId);
            deleteVBO( tetTextureCoordsVBOId);

            glDeleteTextures(1, &tetNormalsTextureId);
        }
        textureCreated = false;
    }
}

void Mesh::clear(){


    offsets.clear();

    vertices.clear();
    envelopVertices.clear();
    triangles.clear();
    tetrahedra.clear();

    sortedTriangles.clear();
    sortedTetrahedra.clear();

    normals.clear();
    verticesNormals.clear();
    triangles_subdomain_ids.clear();
    tetrahedra_subdomain_ids.clear();

    CGAL_envelop.clear();;

    subdomain_indices.clear();;

    geodesics.clear();
    sellePoints.clear();
    significantGeodesics.clear();;

    sortedGeodesics.clear();;

    segmentation.clear();;


    clearTextures();
}

void Mesh::separateLabels(unsigned int l1, unsigned int l2 ){

    if( sortedTetrahedra[l1].size() > sortedTetrahedra[l2].size() ){
        separateLabels(l2, l1);
        return;
    }

    std::cout << "separate labels " << l1 << " and " << l2 << std::endl;
    std::vector<int> & tetindices = sortedTetrahedra[l1];
    std::map<int, int> VMap;
    std::map<int, int>::iterator vIt;
    for( unsigned int i = 0 ; i < tetindices.size() ; i++ ){
        Tetrahedron & tet = tetrahedra[tetindices[i]];
        for( int v = 0 ; v < 4 ; v++ ){
            int nid = tet.getNeighbor(v);
            if( nid >= 0){
                int s_id =  tetrahedra_subdomain_ids[nid];
                if( s_id == l2 ){

                    for( int j = 0 ; j < 3 ; j++ ){
                        int k = indices[v][j];
                        int vIndex = tet.getVertex(k);

                        vIt = VMap.find(vIndex);
                        if( vIt == VMap.end()){
                            VMap[vIndex] = vertices.size();
                            tet.setVertex(k, vertices.size());

                            vertices.push_back(vertices[vIndex]);

                        } else {
                            tet.setVertex(k, vIt->second);

                        }
                    }

                    tet.setNeighbor(v, -1);
                    tetrahedra[nid].setNeighbor(tetrahedra[nid].isNeighbor(tetindices[i]),-1);
                }
            }
        }

    }

    std::cout << vertices.size() - vsize << " new vertices "<< std::endl;
}

BasicPoint Mesh::computeBarycenter( unsigned int id ){
    BasicPoint barycenter (0.,0.,0.);

    Tetrahedron & tet = tetrahedra[id];
    for(int i = 0 ; i < 4; i++)
        barycenter += vertices[tet.getVertex(i)];
    return barycenter/4.;
}

void Mesh::segmentMesh(const std::vector<int> &labels){


    std::vector<BasicPoint> barycenters(tetrahedra.size());
    for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++)
        barycenters[i] = computeBarycenter(i);

    tetIds.clear();
    tetIds.resize(tetrahedra.size(), -1);
    std::vector<float> distances(tetrahedra.size(), FLT_MAX);

    std::priority_queue < LengthId, std::deque< LengthId > , std::greater<LengthId> > TetQueue;

    segHue.clear();
    for(unsigned int i = 0 ; i < labels.size() ; i ++){
        int label = labels[i];
        std::vector<int> & tetIndices = sortedTetrahedra[label];
        for(unsigned int j = 0 ; j < tetIndices.size() ; j++){
            TetQueue.push(LengthId(0., std::make_pair(tetIndices[j], label)));
        }
        segHue[label] = 0.99*i/labels.size();
    }

    while( !TetQueue.empty()){
        LengthId ltf =  TetQueue.top();
        TetQueue.pop ();

        unsigned int id = ltf.second.first;
        int flag = ltf.second.second;
        double dist = ltf.first;

        if(  tetIds[id] < 0 && dist < distances[id] ){

            distances[id] = dist;
            tetIds[id] = flag;

            const BasicPoint & pos = barycenters[id];

            for(unsigned int i = 0 ; i < 4 ; i++){
                int vId = tetrahedra[id].getNeighbor(i);
                if(vId >= 0 && tetrahedra_subdomain_ids [vId] > 0 ){
                    double newDist = dist + (barycenters[vId] - pos).norm();

                    if( newDist < distances[vId]){
                        TetQueue.push(LengthId(newDist, std::make_pair(vId, flag) ));
                    }
                }
            }
        }
    }

    vFlags.clear();
    vFlags.resize(vertices.size(), -1);

    for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++){
        if( tetrahedra_subdomain_ids[i] > 0 ){
            Tetrahedron & tet = tetrahedra[i];
            int tetFlag = tetIds[i];
            for(int j = 0 ; j < 4 ; j++){
                int vId = tet.getVertex(j);
                if( vFlags[vId] < 0 )
                    vFlags[vId] = tetFlag;
                //                else if( vFlags[vId] != tetFlag )
                //                    vFlags[vId] = 0;
            }

        }
    }
}

bool Mesh::isVisiblePoint( int i ){

    const BasicPoint & pos = vertices[i];
    float vis = dot( clippingNormal, pos - pointOnClipping );

    float cutVis[3];
    for( int i = 0 ; i < 3 ; i ++ )
        cutVis[i] = (pos[i] - cut[i])*cutDirection[i];

    if( vis < 0. || cutVis[0] < 0.|| cutVis[1] < 0.|| cutVis[2] < 0. )
        return false;
    return true;
}

bool Mesh::isVisibleTriangle(int i){
    const Triangle & t = triangles[i];
    for( int j = 0 ; j < 3 ; j++ ){
       if(!isVisiblePoint(t.getVertex(j)))
          return false;
    }
    return true;
}

bool Mesh::isVisibleTet(int i){

    int visCount = 0;
    for(unsigned int v = 0 ; v < 4 ; v++ )
        if( isVisiblePoint( tetrahedra[i].getVertex(v))  ) visCount++;

    return ( visCount != 0 && visCount != 4 );
}

void Mesh::findGoodTexSize( unsigned int uiNumValuesNeeded, int& iTexWidth, int& iTexHeight, bool bUseAlphaChannel )
{
    unsigned int uiFactor = ( bUseAlphaChannel ? 4 : 3 );
    int iRoot = ( int )sqrtf( ( float ) ( uiNumValuesNeeded / uiFactor ) );

    iTexWidth = iRoot + 1;
    iTexHeight = iRoot;
    if ( ( unsigned int ) ( iTexWidth * iTexHeight ) * uiFactor < uiNumValuesNeeded ) {
        ++iTexHeight;
    }
    if ( ( unsigned int ) ( iTexWidth * iTexHeight ) * uiFactor < uiNumValuesNeeded ) {
        ++iTexWidth;
    }

}


void Mesh::computeTexturesData(){

    if(!textureCreated){

        return;
    }

    int size = textureWidth*textureHeight*3;

    GLfloat * translation_data = new GLfloat[size];
    GLfloat * normals_data = new GLfloat[size];
    memset(translation_data, 0., sizeof(GLfloat)*size );
    memset(normals_data, 0., sizeof(GLfloat)*size );

    offsets.clear();

    int count = 0;
    int offset = 0;

    for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTriangles.begin() ; it != sortedTriangles.end() ; it ++ ){

        offsets[it->first] = offset*3;
        const std::vector<int> & T = it->second;
        for( unsigned int i = 0 ; i < T.size() ; i++ ){
            const Triangle & triangle = triangles[T[i]];
            for( int v = 0 ; v < 3 ; v++ ){
                const BasicPoint & vertex = vertices[triangle.getVertex(v)];
                const BasicPoint & normal = verticesNormals[triangle.getVertex(v)][it->first];

                for(int j = 0 ; j < 3 ; j++){
                    translation_data[count] = vertex[j];
                    normals_data[count++] = normal[j];
                }
            }
            offset++;
        }
    }


    glBindTexture(GL_TEXTURE_2D, triangleVerticesTextureId );

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 textureWidth,
                 textureHeight,
                 0,
                 GL_RGB,
                 GL_FLOAT,
                 translation_data);

    GetOpenGLError();

    delete [] translation_data;

    glBindTexture(GL_TEXTURE_2D, triangleNormalsTextureId);

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 textureWidth,
                 textureHeight,
                 0,
                 GL_RGB,
                 GL_FLOAT,
                 normals_data);

    glBindTexture(GL_TEXTURE_2D, 0);

    delete [] normals_data;

    GetOpenGLError();

    if( false && tetrahedra.size() > 0 ){

        int tetSize = tetTextureWidth*tetTextureHeight*3;

        GLfloat * tet_translation_data = new GLfloat[tetSize];
        GLfloat * tet_normals_data = new GLfloat[tetSize];
        memset(tet_translation_data, 0., sizeof(GLfloat)*tetSize );
        memset(tet_normals_data, 0., sizeof(GLfloat)*tetSize );

        tetOffsets.clear();

        count = 0;
        offset = 0;


        for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTetrahedra.begin() ; it != sortedTetrahedra.end() ; it ++ ){

            tetOffsets[it->first] = offset*3*4;
            const std::vector<int> & T = it->second;
            for( unsigned int i = 0 ; i < T.size() ; i++ ){
                const Tetrahedron & tet = tetrahedra[T[i]];
                for( int i = 0 ; i < 4 ; i++ ){
                    BasicPoint normal = cross(vertices[tet.getVertex(indices[i][1])] - vertices[tet.getVertex(indices[i][0])],
                                              vertices[tet.getVertex(indices[i][2])]- vertices[tet.getVertex(indices[i][0])]);
                    normal.normalize();
                    for( int j = 0 ; j < 3 ; j++ ){
                        int vId = tet.getVertex(indices[i][j]);
                        const BasicPoint & vertex = vertices[vId];

                        for(int v = 0 ; v < 3 ; v++){
                            translation_data[count] = vertex[v];
                            normals_data[count++] = normal[v];
                        }
                    }
                }
                offset++;
            }
        }

        glBindTexture(GL_TEXTURE_2D, tetVerticesTextureId );

        glTexImage2D(GL_TEXTURE_2D,
                     0, 0x8815,
                     tetTextureWidth,
                     tetTextureHeight,
                     0,
                     GL_RGB,
                     GL_FLOAT,
                     tet_translation_data);

        GetOpenGLError();

        delete [] tet_translation_data;

        glBindTexture(GL_TEXTURE_2D, tetNormalsTextureId);

        glTexImage2D(GL_TEXTURE_2D,
                     0, 0x8815,
                     tetTextureWidth,
                     tetTextureHeight,
                     0,
                     GL_RGB,
                     GL_FLOAT,
                     tet_normals_data);

        glBindTexture(GL_TEXTURE_2D, 0);

        delete [] tet_normals_data;

    }



}

int Mesh::computeTextureNumber(){
    //
    //    GLint iMaxTexSize = 256;
    //    glGetIntegerv( GL_MAX_TEXTURE_SIZE, &iMaxTexSize );
    //
    //    int size = iTexWidth*iTexHeight;
    //    if( size > iMaxTexSize ){
    //        float div = float(size) / float(iMaxTexSize);
    //        int number = int(div);
    //        if ( div - int(div) > 0. ) number++;
    //        return number;
    //    }

    return 1;
}
/************************************************************************************************
*
*        Distance field
*
************************************************************************************************/


void Mesh::computeMaxima(){

    std::vector< std::vector<unsigned int> > oneRing;

    collectOneRing(oneRing);

    geodesics.clear();
    sellePoints.clear();
    sortedGeodesics.clear();

    geodesics.resize(vertices.size());
    sellePoints.resize(vertices.size());
    sortedGeodesics.resize(vertices.size());

    float globaleMaxRatio = FLT_MIN;
    for( unsigned int i = 0 ; i < vertices.size() ; i ++ ){

        std::vector<float> distances(vertices.size(), FLT_MAX);
        std::vector<int> geodesicWay(vertices.size(), -1);

        std::priority_queue < LengthId, std::deque< LengthId > , std::greater<LengthId> > VertexQueue;

        distances[i] = 0.;

        const BasicPoint & position = vertices[i];


        for(unsigned int v = 0 ; v < oneRing[i].size() ; v++){

            unsigned int id = oneRing[i][v];
            VertexQueue.push(LengthId( (vertices[id] - position).norm(), std::make_pair(id, i)));
        }

        while(!VertexQueue.empty()){
            LengthId lvh =  VertexQueue.top();
            VertexQueue.pop ();

            unsigned int id = lvh.second.first;
            unsigned int idFrom = lvh.second.second;
            double length = lvh.first;

            if( length < distances[id] ){
                distances[id] = length;
                geodesicWay[id] = idFrom;
                const BasicPoint & pos = vertices[id];

                for(unsigned int v = 0 ; v < oneRing[id].size() ; v++){
                    unsigned int vId = oneRing[id][v];
                    double newLength = length + (vertices[vId] - pos).norm();
                    if(newLength < distances[vId]){
                        VertexQueue.push(LengthId(newLength, std::make_pair(vId, id) ));
                    }

                }
            }
        }

        //std::vector<float> ratios (vertices.size());
        float maxRatio = FLT_MIN;

        for( unsigned int v = 0 ; v < vertices.size() ; v ++ ){
            if( v != i ){
                float r = distances[v] / (position - vertices[v]).norm();

                if( distances[v] > radius*0.1 && r > maxRatio ){
                    geodesics[i] = v;
                    maxRatio = r;
                }
            }
        }

        sortedGeodesics[i] = RatioId(maxRatio, i);


        if(maxRatio > globaleMaxRatio){
            globaleMaxRatio = maxRatio;
            max_ratio = i;
        }


        float min = FLT_MAX;

        int vPrecId = geodesics[i];
        int vId = geodesicWay[vPrecId];
        const BasicPoint & v2Pos = vertices[geodesics[i]];
        float geodV1V2 = distances[geodesics[i]];

        float geodV2V = 0.;
        while( vPrecId != i && vId != i ) {

            geodV2V += (vertices[vPrecId] - vertices[vId]).norm();

            float r = (geodV1V2 - geodV2V) / (position - vertices[vId]).norm();

            r += geodV2V / (v2Pos - vertices[vId]).norm();

            if( r < min ){
                min = r;
                sellePoints[i] = vId;
            }
            vPrecId = vId;
            vId = geodesicWay[vPrecId];
        }


    }

    std::sort(sortedGeodesics.begin(), sortedGeodesics.end() , std::greater< RatioId >() );
    maxiComputed = true;
    classifyGeodesics();
}


bool Mesh::isGeodesicsSignificant(const std::vector< std::vector< GeoIdVertexType> > & verticesGeoClustering, unsigned int g0 ){

    unsigned int v1 = sortedGeodesics[g0].second;
    unsigned int v2 = geodesics[v1];
    unsigned int s = sellePoints[v1];

    const std::vector< GeoIdVertexType> & v1GeoClustering = verticesGeoClustering[v1];
    const std::vector< GeoIdVertexType> & v2GeoClustering = verticesGeoClustering[v2];
    const std::vector< GeoIdVertexType> & sGeoClustering = verticesGeoClustering[s];

    if( v1GeoClustering.size() == 0 || v2GeoClustering.size() == 0 || sGeoClustering.size() == 0 ) return true;

    std::vector< GeoIdVertexType>::const_iterator v1It = v1GeoClustering.begin();
    std::vector< GeoIdVertexType>::const_iterator v2It = v2GeoClustering.begin();


    for( unsigned int i = 0 ; i < v1GeoClustering.size() ; i++ ){
        unsigned int g1 = v1GeoClustering[i].first;
        int t1 = v1GeoClustering[i].second;
        for( unsigned int j = 0 ; j < v2GeoClustering.size() ; j++ ){
            unsigned int g2 = v2GeoClustering[j].first;
            int t2 = v2GeoClustering[j].second;
            for( unsigned int k = 0 ; k < sGeoClustering.size() ; k++ ){
                unsigned int gs = sGeoClustering[k].first;
                int ts = sGeoClustering[k].second;

                if( g1 != g2 || g1 != gs || g2 != gs ) continue;

                if( t1 == t2 ) continue;

                if( t1 == SELLE  ||  t2 == SELLE ) continue;

                if( ts != SELLE ) continue;

                return false;
            }
        }
    }
    return true;

#if 0


    while( v1It !=  v1GeoClustering.end() && v2It != v2GeoClustering.end() ){

        if( v1It->first > v2It->first)
            v2It++;
        else if( v1It->first < v2It->first)
            v1It++;

        if( v1It->first == v2It->first && v1It->second != v2It->second ){

            std::vector< GeoIdVertexType>::const_iterator it = sGeoClustering.begin();
            while( v1It !=  sGeoClustering.end() && it->first <= v1It->first ){
                if( it->first == v1It->first && it->second == SELLE )
                    return false;
                it++;
            }
            v1It++;
            v2It++;
        }
    }

    return true;
#endif
}

void Mesh::classifyGeodesics(){

    std::vector< std::vector<unsigned int> > oneRing;

    collectOneRing(oneRing);
    segmentation.clear();
    significantGeodesics.clear();
    std::vector< std::vector< GeoIdVertexType> > verticesGeodesicClustering(vertices.size());

    for( unsigned int i = 0 ; i < sortedGeodesics.size() ; i ++ ){
        std::vector<int> localTypes(vertices.size(), -1);
        if(isGeodesicsSignificant( verticesGeodesicClustering, i)){


            std::vector<float> distances(vertices.size(), FLT_MAX);

            std::priority_queue < DistanceVertexIdType, std::deque< DistanceVertexIdType > , std::greater<DistanceVertexIdType> > VertexQueue;

            unsigned int v1 = sortedGeodesics[i].second;
            unsigned int v2 = geodesics[v1];
            unsigned int s = sellePoints[v1];

            significantGeodesics.push_back(i);

            VertexQueue.push(DistanceVertexIdType( 0., std::make_pair(v1, GEOSTART)));
            VertexQueue.push(DistanceVertexIdType( 0., std::make_pair(v2, GEOEND)));
            VertexQueue.push(DistanceVertexIdType( 0., std::make_pair(s, SELLE)));

            BasicPoint dir1 = vertices[s] - vertices[v1] ;
            BasicPoint dir2 = vertices[s] - vertices[v2] ;

            dir1.normalize();
            dir2.normalize();

            while( !VertexQueue.empty()){
                DistanceVertexIdType lvh =  VertexQueue.top();
                VertexQueue.pop ();

                unsigned int id = lvh.second.first;
                int type = lvh.second.second;
                double length = lvh.first;

                if( localTypes[id] != -1 && localTypes[id] != type ) break;

                if(  localTypes[id] < 0 && length < distances[id] ){

                    distances[id] = length;
                    localTypes[id] = type;
                    verticesGeodesicClustering[id].push_back(GeoIdVertexType( i, type ));
                    const BasicPoint & pos = vertices[id];

                    for(unsigned int v = 0 ; v < oneRing[id].size() ; v++){
                        unsigned int vId = oneRing[id][v];

                        double newLength;
                        //
                        //                        if( type == GEOSTART )
                        //                            newLength = fabs(dot( dir1 , vertices[vId] - vertices[v1] ));
                        //                        else if( type == GEOEND )
                        //                            newLength = fabs(dot( dir2 , vertices[vId] - vertices[v2] ));
                        //                        else
                        newLength = length + (vertices[vId] - pos).norm();

                        if( newLength < distances[vId]){
                            VertexQueue.push(DistanceVertexIdType(newLength, std::make_pair(vId, type) ));
                        }

                    }
                }
            }

            segmentation.push_back(localTypes);
        }

    }

    std::cout<< "geod " << sortedGeodesics.size() << std::endl;
    std::cout<< "result " << significantGeodesics.size() << std::endl;

}


void Mesh::drawBoundariesVBO(bool solid, bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap  ){

    if(vertices.size() == 0) return;

    if(!textureCreated ){//|| subdomain_indices.size() == 1){
        drawBoundaries(meshColor, displayMap, colorMap);
        return;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glEnable(GL_TEXTURE_2D);


    GLTools::SetDiffuse(0., 0., 0., 1.);

    ShaderPool * shaderPool = ShaderPool::GetInstance();
    ShaderPackage * shaderPackage = shaderPool->GetSurfacePackage();
    ProgramObject * programObject= shaderPackage->GetProgram();

    //GPU start
    programObject->Begin();

    int iTextureSlot = 0;
    // uniform variables:


    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, triangleVerticesTextureId);

    programObject->SetUniform1i( "vertices_translations", iTextureSlot++ );

    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, triangleNormalsTextureId);

    programObject->SetUniform1i( "normals_translations", iTextureSlot++ );

    programObject->SetUniform3f("clippingPoint", pointOnClipping[0], pointOnClipping[1], pointOnClipping[2]);
    programObject->SetUniform3f("clippingNormal", clippingNormal[0], clippingNormal[1], clippingNormal[2]);
    programObject->SetUniform3f("cut", cut[0], cut[1], cut[2]);
    programObject->SetUniform3f("cutDirection", cutDirection[0], cutDirection[1], cutDirection[2]);
    //   std::cout << "translation texture " << programObject->GetUniformLocation("u_Translations") << std::endl;

    // before draw, specify vertex and index arrays with their offsets
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, triangleNormalsVBOId);
    glNormalPointer(GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, triangleVerticesVBOId);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, triangleIndicesVBOId);

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, triangleTextureCoordsVBOId);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);

    GetOpenGLError();

    glPushClientAttrib( GL_CLIENT_VERTEX_ARRAY_BIT );
    // start to render polygons
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);


    //   std::cout << "tWidth "<< tWidth << std::endl;
    programObject->SetUniform1i("width", textureWidth);

    programObject->SetUniform1i("solid", solid);
    programObject->SetUniform1i("visibility_check", visibility_check);
    programObject->SetUniform1i("normalDirection", normalDirection);

    std::map<Subdomain_index, bool>::const_iterator itVis;
    std::map<Subdomain_index, QColor>::const_iterator itCol;


    QColor color;



    for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTriangles.begin(); it != sortedTriangles.end() ; it ++  ){

        Subdomain_index si = it->first;
        programObject->SetUniform1i("offset", offsets[si]);

        if(displayMap[si]){
            color = QColor(0,0,0);
            if(meshColor){
                itCol = colorMap.find(si);
                if( itCol == colorMap.end() )
                    color.setHsvF(0.5, 1.,1.);
                else
                    color =itCol->second;

            } else
                color = QColor(0,0,0);
            GLTools::SetDiffuse(color.redF(), color.greenF(), color.blueF());

            glDrawElementsInstancedEXT( GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, 0, it->second.size() );

        }

    }

    glDisableClientState(GL_VERTEX_ARRAY);	// disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);	// disable normal arrays
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    //    glDisableVertexAttribArray( translation );

    glPopClientAttrib();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    programObject->End();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);

    for ( int i=0; i<iTextureSlot; ++i ) {
        glActiveTexture( GL_TEXTURE0 + i );
        glDisable( GL_TEXTURE_2D );
    }
    glActiveTexture( GL_TEXTURE0 );

}

void Mesh::drawTetrahedraVBO(bool solid, bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap ){

    if(vertices.size() == 0 || tetrahedra.size() == 0 ) return;

    if(!textureCreated){
        return;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glEnable(GL_TEXTURE_2D);


    GLTools::SetDiffuse(0., 0., 0.);

    ShaderPool * shaderPool = ShaderPool::GetInstance();
    ShaderPackage * shaderPackage = shaderPool->GetSurfacePackage();
    ProgramObject * programObject= shaderPackage->GetProgram();

    //GPU start
    programObject->Begin();

    int iTextureSlot = 0;
    // uniform variables:


    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, triangleVerticesTextureId);

    programObject->SetUniform1i( "vertices_translations", iTextureSlot++ );

    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, tetNormalsTextureId);

    programObject->SetUniform1i( "normals_translations", iTextureSlot++ );

    programObject->SetUniform3f("clippingPoint", pointOnClipping[0], pointOnClipping[1], pointOnClipping[2]);
    programObject->SetUniform3f("clippingNormal", clippingNormal[0], clippingNormal[1], clippingNormal[2]);

    //   std::cout << "translation texture " << programObject->GetUniformLocation("u_Translations") << std::endl;

    // before draw, specify vertex and index arrays with their offsets
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, tetNormalsVBOId);
    glNormalPointer(GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, tetVerticesVBOId);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, tetIndicesVBOId);

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, tetTextureCoordsVBOId);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);

    GetOpenGLError();

    glPushClientAttrib( GL_CLIENT_VERTEX_ARRAY_BIT );
    // start to render polygons
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);


    //   std::cout << "tWidth "<< tWidth << std::endl;
    programObject->SetUniform1i("width", textureWidth);

    programObject->SetUniform1i("solid", solid);
    programObject->SetUniform1i("visibility_check", visibility_check);
    std::map<Subdomain_index, bool>::const_iterator itVis;
    std::map<Subdomain_index, QColor>::const_iterator itCol;

    const Subdomain_index & si_max = subdomain_indices.back();

    QColor color;



    for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTriangles.begin(); it != sortedTriangles.end() ; it ++  ){

        Subdomain_index si = it->first;
        programObject->SetUniform1i("offset", offsets[si]);

        if(displayMap[si]){

            if(meshColor){
                itCol = colorMap.find(si);
                if( itCol == colorMap.end() )
                    color.setHsvF(0.5, 1.,1.);
                else
                    color =itCol->second;

                GLTools::SetDiffuse(color.redF(), color.greenF(), color.blueF());
            }

            glDrawElementsInstancedEXT( GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, 0, it->second.size() );

        }

    }

    glDisableClientState(GL_VERTEX_ARRAY);	// disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);	// disable normal arrays
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    //    glDisableVertexAttribArray( translation );

    glPopClientAttrib();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    programObject->End();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);

    for ( int i=0; i<iTextureSlot; ++i ) {
        glActiveTexture( GL_TEXTURE0 + i );
        glDisable( GL_TEXTURE_2D );
    }
    glActiveTexture( GL_TEXTURE0 );

}


void Mesh::drawEnvelop(){

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glColor3f(0.5, 0.1, 0.);
    glBegin (GL_TRIANGLES);
    for( unsigned int i = 0 ; i < CGAL_envelop.size() ; i ++ ){

        const Triangle & triangle = triangles[CGAL_envelop[i]];
        glNormal( normals[CGAL_envelop[i]] );
        for(int v = 0 ; v < 3 ; v++)
            glVertex( vertices[triangle.getVertex(v)] );
    }

    glEnd();
}

void Mesh::draw(){

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glBegin(GL_TRIANGLES);
    for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTriangles.begin(); it != sortedTriangles.end() ; it ++  ){

        Subdomain_index si = it->first;

        const std::vector<int> & sortedIds = it->second;
        for( unsigned int i = 0 ; i < sortedIds.size() ; i++ ){
            if(isVisibleTriangle(sortedIds[i]))
            glTriangle( sortedIds[i],si);
        }
    }
    glEnd();
}

GLuint Mesh::createVBO(const void* data, int dataSize, GLenum target, GLenum usage)
{
    GLuint id = 0;  // 0 is reserved, glGenBuffersARB() will return non-zero id if success

    glGenBuffersARB(1, &id);                        // create a vbo
    glBindBufferARB(target, id);                    // activate vbo id to use
    glBufferDataARB(target, dataSize, data, usage); // upload data to video card

    // check data size in VBO is same as input array, if not return 0 and delete VBO
    int bufferSize = 0;
    glGetBufferParameterivARB(target, GL_BUFFER_SIZE_ARB, &bufferSize);
    if(dataSize != bufferSize)
    {
        glDeleteBuffersARB(1, &id);
        id = 0;
        std::cout << "[createVBO()] Data size is mismatch with input array\n";
    }

    return id;      // return VBO id
}



///////////////////////////////////////////////////////////////////////////////
// destroy a VBO
// If VBO id is not valid or zero, then OpenGL ignores it silently.
///////////////////////////////////////////////////////////////////////////////
void Mesh::deleteVBO(const GLuint _vboId)
{
    glDeleteBuffersARB(1, &_vboId);
}

void Mesh::drawVertices(std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap)
{

    if(vertices.size() == 0)
        return;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glDisable(GL_LIGHTING);
    glColor3f(0., 0., 0.);

    glEnable(GL_CLIP_PLANE0);
    glBegin (GL_POINTS);
    for( unsigned int i = 0 ; i < vertices.size() ; i++ ){

        int id = vFlags[i];
        QColor color (0, 0, 0);
        if(id > 0)
            color.setHsvF(segHue[id], 1.,1.);
        if( i >= vsize )
            color.setRgb(255, 255, 0);
        glColor3f(color.redF(), color.greenF(), color.blueF());
        glVertex(vertices[i]);

    }
    glEnd();


    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);
}

void Mesh::drawTetrahedraDebug(bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap)
{

    if(tetrahedra.size() == 0)
        return;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glColor3f(0., 0., 0.);

    glEnable(GL_CLIP_PLANE0);

    glBegin (GL_TRIANGLES);
    for( unsigned int i = 0 ; i < tetrahedra.size() ; i++ ){
        if(tetrahedra_subdomain_ids[i]>0){
            int id = tetIds[i];
            QColor color;
            color.setHsvF(segHue[id], 1.,1.);
            glColor3f(color.redF(), color.greenF(), color.blueF());
            //if(isVisibleTet(i))
            glTetrahedron(i);
        }

    }
    glEnd();

    GLTools::SetDiffuse(0., 0., 0., 1.);
    glColor3f(0., 0., 0.);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);
}

void Mesh::drawTetrahedra(bool solid, bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap)
{

    if(tetrahedra.size() == 0)
        return;

    if(tetIds.size()>0){
        //drawTetrahedraDebug(meshColor, displayMap, colorMap);
        drawVertices( displayMap, colorMap);
        return;
    }

    drawBoundariesVBO(solid, meshColor, displayMap, colorMap);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glColor3f(0., 0., 0.);
    glBegin (GL_TRIANGLES);

    for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTetrahedra.begin(); it != sortedTetrahedra.end() ; it ++  ){

        Subdomain_index si = it->first;

        if(displayMap[si]){

            glColor3f(0., 0., 0.);
            if(meshColor){
                if( subdomain_indices.size() == 1 ){
                    glColor3f(1.,0.9,0.9);
                } else {
                    QColor & color = colorMap[si];
                    glColor3f(color.redF(), color.greenF(), color.blueF());
                }
            }

            const std::vector<int> & sortedIds = it->second;
            for( unsigned int i = 0 ; i < sortedIds.size() ; i++ ){
                int id = sortedIds[i];
                if(!visibility_check || isVisibleTet(id))
                    glTetrahedron(id);
            }
        }
    }

    glEnd();


    GLTools::SetDiffuse(0., 0., 0., 1.);
    glColor3f(0., 0., 0.);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);
}

void Mesh::updateMesh(const std::vector<BasicPoint> &insideVertices, const std::vector<BasicPoint> &envelop){

    int insideCount = 0 ;
    int envelopCount = 0 ;

    for(unsigned int i = 0 ; i < vertices.size() ; i++){
        if( envelopVertices[i] )
            vertices [ i ] = envelop [ envelopCount++ ];
        else
            vertices [ i ] = insideVertices [ insideCount++ ];
    }

}

void Mesh::sortBySubdomainIndex(){

    sortedTriangles.clear();
    sortedTetrahedra.clear();

    for(unsigned int t = 0 ; t < triangles.size() ; t++){
        sortedTriangles[ triangles_subdomain_ids[t] ].push_back(t);
    }

    for(unsigned int t = 0 ; t < tetrahedra.size() ; t++){
        sortedTetrahedra[ tetrahedra_subdomain_ids[t] ].push_back(t);
    }

    bool noZero = true;
    subdomain_indices.clear();
    for(std::map<int, std::vector<int> >::iterator it = sortedTriangles.begin(); it != sortedTriangles.end() ; it ++  ){
        if( it->first == 0 ) noZero = false;
        subdomain_indices.push_back(it->first);
    }
    //if(noZero) subdomain_indices.push_back(0);
    std::sort(subdomain_indices.begin() , subdomain_indices.end());
}


void Mesh::addToMesh(const std::vector<BasicPoint> & V, const std::vector<Triangle> & T, Subdomain_index si){

    BasicPointMapIndex VHMap;
    for( unsigned int i = 0 ; i < vertices.size() ; i++ )
        VHMap[vertices[i]] = i;


    std::map<int , int> VMap;
    for( unsigned int i = 0 ; i< V.size() ; i++ ){
        BasicPointMapIndex::iterator it = VHMap.find(V[i]);
        if( it != VHMap.end() ){
            VMap[i] = it->second;
        } else {
            VMap[i] = vertices.size();
            vertices.push_back(V[i]);
        }
    }

    for( unsigned int i = 0 ; i < T.size() ; i++ ){
        const Triangle t = T[i];
        triangles.push_back(Triangle(VMap[t.getVertex(0)], VMap[t.getVertex(1)], VMap[t.getVertex(2)], si));
        triangles_subdomain_ids.push_back(si);
    }
}


void Mesh::recomputeNormals () {

    computeFacetsNormals();
    computeVerticesNormals(true);
    computeTexturesData();
    computeBB();

}

void Mesh::computeFacetsNormals(){

    normals.clear();

    for(unsigned int i = 0 ; i < triangles.size() ; i++){
        normals.push_back(computeFacetNormal(i));
    }

}

BasicPoint Mesh::computeFacetNormal( int id ){

    const Triangle & t = triangles[id];
    BasicPoint normal = cross(vertices[t.getVertex (1)] - vertices[t.getVertex (0)], vertices[t.getVertex (2)]- vertices[t.getVertex (0)]);
    normal.normalize();
    return normal;

}


void Mesh::computeVerticesNormals(unsigned int normWeight){

    verticesNormals.resize(vertices.size());

    for( std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTriangles.begin() ; it != sortedTriangles.end() ; it++ ){

        const std::vector<int> & T = it->second;
        Subdomain_index si = it->first;
        for (unsigned int i = 0 ; i < T.size(); i++){

            const Triangle & t = triangles[T[i]];
            for (unsigned int  j = 0; j < 3; j++) {
                unsigned int vj = t.getVertex (j);
                float w = 1.0; // uniform weights
                BasicPoint e0 = vertices[t.getVertex ((j+1)%3)] - vertices[vj];
                BasicPoint e1 = vertices[t.getVertex ((j+2)%3)] - vertices[vj];

                if (normWeight == 1) { // area weight
                    w = cross (e0, e1).norm() / 2.0;
                } else if (normWeight == 2) { // angle weight
                    e0.normalize ();
                    e1.normalize ();
                    w = (2.0 - (dot(e0, e1) + 1.0)) / 2.0;
                }
                if (w <= 0.0)
                    continue;

                std::map<Subdomain_index, BasicPoint>::iterator it = verticesNormals[vj].find(si);

                const BasicPoint & normal = normals[T[i]];
                if( it == verticesNormals[vj].end() ){
                    verticesNormals[vj][si] = normal * w;
                } else {
                    it->second = it->second + normal * w;
                }

            }
        }

    }

    for(unsigned int i = 0 ; i < verticesNormals.size() ; i++){
        for(std::map<Subdomain_index, BasicPoint>::iterator it = verticesNormals[i].begin(); it != verticesNormals[i].end() ; it++){
            it->second.normalize();
        }
    }
}

void Mesh::collectOneRing (std::vector< std::vector<unsigned int> > & oneRing) const {
    oneRing.resize (vertices.size ());
    for (unsigned int i = 0; i < triangles.size (); i++) {
        const Triangle & ti = triangles[i];
        for (unsigned int j = 0; j < 3; j++) {
            unsigned int vj = ti.getVertex (j);
            for (unsigned int k = 1; k < 3; k++) {
                unsigned int vk = ti.getVertex ((j+k)%3);
                if (std::find (oneRing[vj].begin (), oneRing[vj].end (), vk) == oneRing[vj].end ())
                    oneRing[vj].push_back (vk);
            }
        }
    }
}

void Mesh::glTriangle(unsigned int i, Subdomain_index si){

    //glNormal(normals[i]);

    const Triangle & t = triangles[i];
    for( int j = 0 ; j < 3 ; j++ ){

            glNormal(verticesNormals[t.getVertex(j)][si]*normalDirection);
            glVertex(vertices[t.getVertex(j)]);

    }

}

void Mesh::glTetrahedron(unsigned int i){

    const Tetrahedron & t = tetrahedra[i];

    for( int i = 0 ; i < 4 ; i++ ){
        BasicPoint normal = cross(vertices[t.getVertex(indices[i][1])] - vertices[t.getVertex(indices[i][0])],
                                  vertices[t.getVertex(indices[i][2])]- vertices[t.getVertex(indices[i][0])]);
        normal.normalize();
        glNormal(normal*normalDirection);
        for( int j = 0 ; j < 3 ; j++ ){
            glVertex(vertices[t.getVertex(indices[i][j])]);
        }
    }
}

void Mesh::sortFaces( FacesQueue & facesQueue ){
    float modelview[16];
    glGetFloatv(GL_MODELVIEW_MATRIX , modelview);


    for (unsigned int t = 0 ; t < triangles.size() ; ++t )
    {
        BasicPoint _center = (
                vertices[ triangles[t].getVertex(0) ]+
                vertices[ triangles[t].getVertex(1) ]+
                vertices[ triangles[t].getVertex(2) ]) / 3.f;
        facesQueue.push( std::make_pair( modelview[2] * _center[0] + modelview[6] * _center[1] + modelview[10] * _center[2] + modelview[14] , t ) );
    }

}

void Mesh::drawBoundaries(bool meshColor, const std::map<Subdomain_index, bool> & displayMap, const std::map<Subdomain_index, QColor> & colorMap )
{

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glColor3f(0., 0., 0.);
    glBegin (GL_TRIANGLES);

    std::map<Subdomain_index, bool>::const_iterator itVis;
    std::map<Subdomain_index, QColor>::const_iterator itCol;



    QColor color;
    int count = 0;
    for(std::map<Subdomain_index, std::vector<int> >::iterator it = sortedTriangles.begin(); it != sortedTriangles.end() ; it ++ , count++ ){

        Subdomain_index si = it->first;

        bool visible = true;
        itVis = displayMap.find(si);
        if( itVis != displayMap.end() ) visible = itVis->second;

        if(visible){
            color = QColor(0,0,0);
            if(meshColor){
                if( subdomain_indices.size() == 1 ){
                    glColor3f(1.,0.9,0.9);
                } else {
                    itCol = colorMap.find(si);
                    if( itCol == colorMap.end() ) color.setHsvF(0.98*count/subdomain_indices.size(), 1.,1.);
                    else color =itCol->second;

                    glColor3f(color.redF(), color.greenF(), color.blueF());
                }
            }

            const std::vector<int> & sortedIds = it->second;
            for( unsigned int i = 0 ; i < sortedIds.size() ; i++ ){
                if(isVisibleTriangle(sortedIds[i]))
                    glTriangle( sortedIds[i],si);
            }
        }
    }

    glEnd();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);

}

void Mesh::drawMaxima(float radius){

    if( sortedGeodesics.size() == 0 ) return;

    glEnable(GL_LIGHTING);
    glEnable( GL_DEPTH_TEST );

    unsigned int id = sortedGeodesics[displayCount].second;

    BasicPoint center;
    if( max_ratio == id ){
        glColor3f(0.f,1.f,0.5f);
        center = vertices[id];
        BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );

        glColor3f(0.f,1.f,0.f);
        center = vertices[geodesics[id]];
        BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );

    } else {
        glColor3f(1.f,0.f,0.f);
        center = vertices[id];
        BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );

        glColor3f(0.f,0.f,1.f);
        center = vertices[geodesics[id]];
        BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );
    }
    glColor3f(1.f,1.f,0.f);
    center = vertices[sellePoints[id]];
    BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );

    glDisable( GL_DEPTH_TEST );
    glEnable(GL_LIGHTING);

}


void Mesh::drawSignificantGeodesics(float radius, bool segmentationDisplay){

    if( significantGeodesics.size() == 0 ) return;

    glEnable(GL_LIGHTING);
    glEnable( GL_DEPTH_TEST );


    //for( unsigned int i = 0 ; i < significantGeodesics.size() ; i++ ){
    unsigned int id = sortedGeodesics[significantGeodesics[displayCount]].second;

    BasicPoint center;
    //    if( max_ratio == id ){
    //        glColor3f(0.f,1.f,0.5f);
    //        center = vertices[id];
    //        BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );
    //        //glVertex(center);
    //
    //        glColor3f(0.f,1.f,0.f);
    //        center = vertices[geodesics[id]];
    //        BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );
    //        //glVertex(center);
    //
    //    } else {
    glColor3f(1.f,0.f,0.f);
    center = vertices[id];
    BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );
    //glVertex(center);

    glColor3f(0.f,0.f,1.f);
    center = vertices[geodesics[id]];
    BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );
    //glVertex(center);
    //    }
    glColor3f(1.f,1.f,0.f);
    center = vertices[sellePoints[id]];
    BasicGL::drawSphere( center[0], center[1], center[2], radius, 15, 15 );
    //glVertex(center);

    if(segmentationDisplay){
        glPointSize(5.);
        glBegin(GL_POINTS);
        for(unsigned int i = 0 ; i < segmentation[displayCount].size() ; i++){

            int segId = segmentation[displayCount][i];
            if( segId == GEOSTART )
                glColor3f(1.f,0.f,0.f);
            if( segId == GEOEND )
                glColor3f(0.f,0.f,1.f);
            if( segId == SELLE )
                glColor3f(1.f,1.f,0.f);
            if( segId >= 0 ){
                center = vertices[i];
                glVertex(center);
            }
        }
        glEnd();

    }
    glDisable( GL_DEPTH_TEST );
    glEnable(GL_LIGHTING);

}
