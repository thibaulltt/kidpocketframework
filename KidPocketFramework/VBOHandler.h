#ifndef VBOHANDLER_H
#define VBOHANDLER_H
#include "OpenGLHeader.h"

class VBOHandler
{
public:
    VBOHandler();

    void findGoodTexSize( unsigned int uiNumValuesNeeded, unsigned int& iTexWidth, unsigned int& iTexHeight, bool bUseAlphaChannel );

    GLuint createVBO(const void* data, int dataSize, GLenum target, GLenum usage);

    void deleteVBO(const GLuint _vboId);
};

#endif // VBOHANDLER_H
