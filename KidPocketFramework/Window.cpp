#include "Window.h"

#include <QDockWidget>
#include <QFormLayout>
#include <QFileDialog>
#include <QScrollArea>
#include <QRadioButton>
#include <QStatusBar>
#include <QColorDialog>
#include <QDialogButtonBox>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>

Window::Window()
{
    if (this->objectName().isEmpty())
        this->setObjectName("window");
    this->resize(929, 891);
    
    viewer = new MesherViewer(this);
    
    QWidget * mesherWidget = new QWidget(this);
    
    QGridLayout * mesherLayout = new QGridLayout();
    
    mesherLayout->addWidget(viewer, 0, 1, 1, 1);
    
    mesherWidget->setLayout(mesherLayout);
    
    QGroupBox * viewerGroupBox = new QGroupBox("Viewer");
    
    QGridLayout * gridLayout = new QGridLayout(viewerGroupBox);
    gridLayout->setObjectName("gridLayout");
    
    gridLayout->addWidget(mesherWidget, 0, 1, 1, 1);
    
    viewerGroupBox->setLayout(gridLayout);
    
    this->setCentralWidget(viewerGroupBox);
    
    initDisplayDockWidgets();
    initToolsDockWidgets();
    initInfoDockWidget();
    //  initQualityHistogramDockWidget();
    initActions ();
    initMenus ();
    initToolBars ();
    
    this->setWindowTitle("KidPocket Framework");
    
}

void Window::initToolsDockWidgets(){
    
    mesherDockWidget = new QDockWidget("Tools");
    
    QWidget * widget = new QWidget();
    QVBoxLayout * widgetVLayout = new QVBoxLayout(widget);
    
    QGroupBox * modeGroupBox = new QGroupBox ("Mode");
    QVBoxLayout * modeLayout = new QVBoxLayout (modeGroupBox);
    modeComboBox = new QComboBox (modeGroupBox);
    modeComboBox->addItem ("Meshing");
    modeComboBox->addItem ("Deformation");
    //modeComboBox->addItem ("Image Deformation");
    //modeComboBox->addItem ("Direct deformation");
    //modeComboBox->addItem ("Selection");
    connect (modeComboBox, SIGNAL (activated (int)),
             this, SLOT (setMode (int)));
    modeLayout->addWidget (modeComboBox);
    
    //  modeLayout->addStretch(0);
    widgetVLayout->addWidget (modeGroupBox);
    
    QGroupBox * imageOffset = new QGroupBox("Image Offset");
    
    QFormLayout * offsetFLayout = new QFormLayout(imageOffset);
    
    offsetSpinBox = new QSpinBox();
    offsetSpinBox->setValue(1);
    offsetSpinBox->setRange(-50, 100 );
    
    useSegmentationCheckBox = new QCheckBox("Use segmentation");
    offsetFLayout->addWidget(useSegmentationCheckBox);
    
    QPushButton * offsetPushButton = new QPushButton("Update offset");
    offsetFLayout->addRow(offsetSpinBox, offsetPushButton);
    connect(offsetPushButton, SIGNAL(clicked()), this, SLOT(updateGridOffset()));
    
    widgetVLayout->addWidget(imageOffset);
    
    contents = new QTabWidget();
    
    QWidget * meshingTab = new QWidget();
    
    QVBoxLayout * meshingTabLayout = new QVBoxLayout();
    
    meshingPushButton = new QPushButton("Mesh");
    meshingPushButton->setEnabled(false);
    connect(meshingPushButton, SIGNAL(clicked()), this, SLOT(CGALMeshing()));
    
    meshingTabLayout->addWidget(meshingPushButton);
    
    //meshingTabLayout->addWidget(createOptimizationGroupBox());
    
    QGroupBox * offsetGroupBox = new QGroupBox("Image Cage");
    
    QVBoxLayout * offsetVLayout = new QVBoxLayout(offsetGroupBox);
    
    QPushButton * generateCagePushButton = new QPushButton("Generate Offset Surface");
    connect (generateCagePushButton, SIGNAL(clicked()), this, SLOT(generateCage()));
    //computeCoordinates->setEnabled(false);
    offsetVLayout->addWidget(generateCagePushButton);
    
    QPushButton * simplifyCagePushButton = new QPushButton("Simplify Surface");
    connect (simplifyCagePushButton, SIGNAL(clicked()), this, SLOT(simplifyCage()));
    offsetVLayout->addWidget(simplifyCagePushButton);
    
    QPushButton * useCagePushButton = new QPushButton("Use Surface as Cage");
    connect (useCagePushButton, SIGNAL(clicked()), viewer, SLOT(useSurfaceAsCage()));
    offsetVLayout->addWidget(useCagePushButton);
    
    QPushButton * useSurfaceAsMeshPushButton = new QPushButton("Use Surface as Mesh");
    connect (useSurfaceAsMeshPushButton, SIGNAL(clicked()), viewer, SLOT(useSurfaceAsMesh()));
    offsetVLayout->addWidget(useSurfaceAsMeshPushButton);
    
    QPushButton * saveSurfacePusheButton = new QPushButton("Save surface");
    connect (saveSurfacePusheButton, SIGNAL(clicked()), this, SLOT(saveSurface()));
    offsetVLayout->addWidget(saveSurfacePusheButton);
    
    QCheckBox * displaySurfaceCheckBox = new QCheckBox("Display surface");
    connect (displaySurfaceCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplaySurface(bool)));
    displaySurfaceCheckBox->setChecked(true);
    offsetVLayout->addWidget(displaySurfaceCheckBox);
    
    meshingTabLayout->addWidget(offsetGroupBox);
    
    QPushButton * applyTrilinearInterpolationPushB = new QPushButton("Apply trilinear interpolation");
    connect (applyTrilinearInterpolationPushB, SIGNAL(clicked()), viewer, SLOT(applyTrilinearInterpolation()));
    offsetVLayout->addWidget(applyTrilinearInterpolationPushB);
    
    QPushButton * separateHandsPushButton = new QPushButton("Separate Labels");
    connect(separateHandsPushButton, SIGNAL(clicked()), this, SLOT(separateHands()));
    meshingTabLayout->addWidget(separateHandsPushButton);
    
    
    QPushButton * changeLabelValuePushButton = new QPushButton("Change Label Value");
    connect(changeLabelValuePushButton, SIGNAL(clicked()), this, SLOT(setLabelToZero()));
    meshingTabLayout->addWidget(changeLabelValuePushButton);
    
    /*
    labelNbSpinBox = new QSpinBox();
    meshingTabLayout->addWidget(labelNbSpinBox);
    labelNbSpinBox->setRange( 1 , indexToLabel.size() );
    
    QPushButton * segmentMeshPushButton = new QPushButton("Mesh segmentation");
    connect(segmentMeshPushButton, SIGNAL(clicked()), this, SLOT(segmentation()));
    meshingTabLayout->addWidget(segmentMeshPushButton);
*/
    meshingTabLayout->addStretch(0);
    meshingTab->setLayout(meshingTabLayout);
    
    contents->addTab(meshingTab, "Meshing");
    
    QWidget * deformationTab = new QWidget();
    QVBoxLayout * defVLayout = new QVBoxLayout();
    deformationTab->setLayout(defVLayout);
    
    QFrame * defFrame = new QFrame();
    
    QVBoxLayout * deformationTabLayout = new QVBoxLayout();
    defFrame->setLayout(deformationTabLayout);
    
    QScrollArea * defScrollArea = new QScrollArea(deformationTab);
    defScrollArea->setWidget(defFrame);
    
    defScrollArea->setWidgetResizable(true);
    defScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    
    defVLayout->addWidget(defScrollArea);
    
    
    QGroupBox * useARAPGroupBox = new QGroupBox("ARAP");
    useARAPGroupBox->setCheckable(true);
    useARAPGroupBox->setChecked(true);
    
    connect (useARAPGroupBox, SIGNAL(clicked(bool)), viewer, SLOT(setUseARAP(bool)));
    
    QFormLayout * arapFormLayout = new QFormLayout(useARAPGroupBox);
    
    QSpinBox * arapSpinBox = new QSpinBox();
    arapSpinBox->setValue( 5 );
    connect (arapSpinBox, SIGNAL(valueChanged(int)), viewer, SLOT(setARAPIteration(int)));
    
    arapFormLayout->addRow("Iteration number", arapSpinBox);
    
    deformationTabLayout->addWidget(useARAPGroupBox);
    
    /*
    QPushButton * saveGridInfo = new QPushButton("Save Grid info");
    connect(saveGridInfo, SIGNAL(clicked()), this, SLOT(saveGridInfo()));
    
    deformationTabLayout->addWidget(saveGridInfo);
    
    QPushButton * useEnvelopCagePushButton = new QPushButton("Use envelop as Cage");
    connect (useEnvelopCagePushButton, SIGNAL(clicked()), this, SLOT(useCGALEnvelopAsCage()));
    //computeCoordinates->setEnabled(false);
    
    deformationTabLayout->addWidget(useEnvelopCagePushButton);
*/
    
    QGroupBox * trMeshingGroupBox = new QGroupBox("Transfert Mesh");
    trMeshingGroupBox->setCheckable(true);
    trMeshingGroupBox->setChecked(false);
    QVBoxLayout * trMeshingFromImgLayout = new QVBoxLayout(trMeshingGroupBox);
    connect(trMeshingGroupBox, SIGNAL(clicked(bool)), viewer, SLOT(setUseTransferMesh(bool)));
    
    QPushButton * openTransfertMeshPushButton = new QPushButton("Open transfert Mesh");
    connect(openTransfertMeshPushButton, SIGNAL(clicked()), this, SLOT(openTransferMesh()));
    trMeshingFromImgLayout->addWidget(openTransfertMeshPushButton);
    
    QPushButton * generateTrMesh = new QPushButton("Mesh From Img");
    
    smoothTransferMeshCheckBox = new QCheckBox("Smooth mesh");
    trMeshingFromImgLayout->addWidget(smoothTransferMeshCheckBox);
    
    trMeshingFromImgLayout->addWidget(generateTrMesh);
    
    connect(generateTrMesh, SIGNAL(clicked()), this, SLOT(transferMeshGeneration()));
    
    QPushButton * generateTrMeshAdaptiveSize = new QPushButton("Mesh motion adaptive");
    
    trMeshingFromImgLayout->addWidget(generateTrMeshAdaptiveSize);
    connect(generateTrMeshAdaptiveSize, SIGNAL(clicked()), this, SLOT(meshAdaptiveSize()));
    
    QPushButton * intiTrMeshPushButton = new QPushButton("Reset Transfer Mesh");
    connect(intiTrMeshPushButton, SIGNAL(clicked()), viewer, SLOT(initializeTransferMesh()));
    trMeshingFromImgLayout->addWidget(intiTrMeshPushButton);
    
    QPushButton * useAsMeshPushButton = new QPushButton("Use Transfer Mesh");
    connect(useAsMeshPushButton, SIGNAL(clicked()), this, SLOT(useAsMesh()));
    trMeshingFromImgLayout->addWidget(useAsMeshPushButton);
    
    QCheckBox * drawTransferTetCheckBox = new QCheckBox("Draw Transfer Mesh");
    trMeshingFromImgLayout->addWidget(drawTransferTetCheckBox);
    connect(drawTransferTetCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayTransferTets(bool)));
    
    QCheckBox * drawTMOutliersCheckBox = new QCheckBox("Draw TM outliers");
    trMeshingFromImgLayout->addWidget(drawTMOutliersCheckBox);
    connect(drawTMOutliersCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayOutliers(bool)));

    minimalMemoryCheckBox = new QCheckBox("Minimal Memory use");
    minimalMemoryCheckBox->setChecked(true);
    trMeshingFromImgLayout->addWidget(minimalMemoryCheckBox);
    
    QPushButton * applyDeformationToImagePushButton = new QPushButton("Apply Deformation to image");
    connect(applyDeformationToImagePushButton, SIGNAL(clicked()), this, SLOT(updateGridWithTransferMesh()));
    trMeshingFromImgLayout->addWidget(applyDeformationToImagePushButton);
    
    deformationTabLayout->addWidget(trMeshingGroupBox);
    
    computeCoordinates = new QPushButton("Initialize Cage");
    connect (computeCoordinates, SIGNAL(clicked()), this, SLOT(initializeCage()));
    //computeCoordinates->setEnabled(false);
    
    deformationTabLayout->addWidget(computeCoordinates);
    
    QPushButton * rescaleCagePushButton = new QPushButton("Rescale Cage");
    connect (rescaleCagePushButton, SIGNAL(clicked()), viewer, SLOT(rescaleCage()));
    deformationTabLayout->addWidget(rescaleCagePushButton);
    
    QPushButton * rescaleSurfacePushButton = new QPushButton("Rescale Surface");
    connect (rescaleSurfacePushButton, SIGNAL(clicked()), viewer, SLOT(rescaleSurface()));
    
    //deformationTabLayout->addWidget(rescaleSurfacePushButton);
    
    
    
    
    QPushButton * computeProblemButton = new QPushButton("Compute Potential Problems");
    connect (computeProblemButton, SIGNAL(clicked()), viewer, SLOT(locateCageProblems()));
    
    deformationTabLayout->addWidget(computeProblemButton);
    
    QCheckBox * displayCageProblemsCheckBox = new QCheckBox("Display Cage Potential Problems");
    connect (displayCageProblemsCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayCageProblems(bool)));
    
    deformationTabLayout->addWidget(displayCageProblemsCheckBox);
    
    
    
    QPushButton * CheckForOutliersButton = new QPushButton("Check Outliers");
    connect (CheckForOutliersButton, SIGNAL(clicked()), viewer, SLOT(checkForOutliers()));
    
    deformationTabLayout->addWidget(CheckForOutliersButton);
    
    QPushButton * IgnoreOutliersButton = new QPushButton("Ignore Outliers");
    connect (IgnoreOutliersButton, SIGNAL(clicked()), viewer, SLOT(ignoreOutliers()));
    
    // deformationTabLayout->addWidget(IgnoreOutliersButton);
    
    
    /*
    QGroupBox * rasterizeGroupBox = new QGroupBox("Rasterization type");
    QVBoxLayout * rasterizeLayout = new QVBoxLayout(rasterizeGroupBox);
    QComboBox * rasterTypeComboBox = new QComboBox (rasterizeGroupBox);
    rasterTypeComboBox->addItem ("Vector Field");
    rasterTypeComboBox->addItem ("Nearest Neighbor");
    rasterTypeComboBox->addItem ("Dilatation");
    
    rasterizeLayout->addWidget(rasterTypeComboBox);
    
    connect (rasterTypeComboBox, SIGNAL (activated (int)),
             this, SLOT (setRasterMode (int)));
             
    neighborhoodSizeSpinBox = new QSpinBox();
    rasterizeLayout->addWidget(neighborhoodSizeSpinBox);
    neighborhoodSizeSpinBox->setValue(2);
    
    gridResolutionSizeSpinBox = new QDoubleSpinBox();
    rasterizeLayout->addWidget(gridResolutionSizeSpinBox);
    gridResolutionSizeSpinBox->setValue(1.);
    gridResolutionSizeSpinBox->setSingleStep(0.1);
    QPushButton * rasterizePushButton = new QPushButton("Update");
    rasterizeLayout->addWidget(rasterizePushButton);
    connect (rasterizePushButton, SIGNAL(clicked()), this, SLOT(rasterize()));
    
    deformationTabLayout->addWidget (rasterizeGroupBox);
    */

    QPushButton * compareVolumeHistogramPButton = new QPushButton("Compare volumes");
    deformationTabLayout->addWidget(compareVolumeHistogramPButton);
    connect (compareVolumeHistogramPButton, SIGNAL(clicked()), this, SLOT(compareVolumes()));
    
    QPushButton * computeVolumeHistogramPButton = new QPushButton("Compute volume diff");
    deformationTabLayout->addWidget(computeVolumeHistogramPButton);
    connect (computeVolumeHistogramPButton, SIGNAL(clicked()), this, SLOT(computeVolumeHistogram()));
    
    QPushButton * computeHistogramsButton = new QPushButton("Compute Conformal error");
    deformationTabLayout->addWidget(computeHistogramsButton);
    connect (computeHistogramsButton, SIGNAL(clicked()), this, SLOT(computeHistograms()));
    /*
    QPushButton * saveHistogramsButton = new QPushButton("Save distortion");
    deformationTabLayout->addWidget(saveHistogramsButton);
    connect (saveHistogramsButton, SIGNAL(clicked()), this, SLOT(exportDistortion()));
*/
    deformationTabLayout->addStretch(0);
    contents->addTab(deformationTab, "Deformation");
    
    /*
    QWidget * tetMeshTab = new QWidget();
    QVBoxLayout * tetMeshVLayout = new QVBoxLayout(tetMeshTab);
    
    QTabWidget * transferMeshingTab = new QTabWidget();
    QWidget * trMeshingTab = new QWidget();
    
    QVBoxLayout * trMeshingTabLayout = new QVBoxLayout(trMeshingTab);
    
    meshingTab->setLayout(meshingTabLayout);
    
    QPushButton * cageMeshingPushButton = new QPushButton("Mesh cage");
    connect(cageMeshingPushButton, SIGNAL(clicked()), this, SLOT(cageTetMeshing()));
    trMeshingTabLayout->addWidget(cageMeshingPushButton);
    
    QFormLayout * distributionFormLayout = new QFormLayout();
    
    
    pointNbSpinBox = new QSpinBox();
    
    distributionFormLayout->addRow("Point Nb", pointNbSpinBox);
    
    pointNbSpinBox->setRange(0,100000);
    pointNbSpinBox->setValue(1000);
    
    radiusDoubleSpinBox = new QDoubleSpinBox();
    
    radiusDoubleSpinBox->setRange(0.01, 1.);
    radiusDoubleSpinBox->stepBy(0.01);
    distributionFormLayout->addRow("Radius", radiusDoubleSpinBox);
    
    trMeshingTabLayout->addLayout(distributionFormLayout);
    
    QPushButton * updateSamplingPushButton = new QPushButton("Update Sampling");
    connect(updateSamplingPushButton, SIGNAL(clicked()), this, SLOT(pointDistribution()));
    trMeshingTabLayout->addWidget(updateSamplingPushButton);
    
    fitToGridCheckBox = new QCheckBox("Fit Sampling To Grid");
    trMeshingTabLayout->addWidget(fitToGridCheckBox);
    
    QPushButton * recomputeTMeshPushButton = new QPushButton("Recompute Transfer Mesh");
    connect(recomputeTMeshPushButton, SIGNAL(clicked()), this, SLOT(recomputeTransferMesh()));
    trMeshingTabLayout->addWidget(recomputeTMeshPushButton);
    
   // transferMeshingTab->addTab(trMeshingTab, "Mesh from Point Distrib.");




    QComboBox * distortionComboBox = new QComboBox ();
    distortionComboBox->addItem ("Cell based");
    distortionComboBox->addItem ("Vertex based");
    
    connect (distortionComboBox, SIGNAL (activated (int)), viewer, SLOT (setDistortionType (int)));
    tetMeshVLayout->addWidget(distortionComboBox);
    
    QPushButton * computeDistortionsPushButton = new QPushButton("Compute Mesh Distortion");
    connect(computeDistortionsPushButton, SIGNAL(clicked()), viewer, SLOT(computeDistortions()));
    tetMeshVLayout->addWidget(computeDistortionsPushButton);
    
    QComboBox * samplingComboBox = new QComboBox ();
    samplingComboBox->addItem ("Edge split");
    samplingComboBox->addItem ("Insert at barycenter");
    
    connect (samplingComboBox, SIGNAL (activated (int)), viewer, SLOT (setSamplingType (int)));
    tetMeshVLayout->addWidget(samplingComboBox);
    
    QPushButton * splitCellsPushButton = new QPushButton("Adapt Sampling");
    connect(splitCellsPushButton, SIGNAL(clicked()), viewer, SLOT(adaptSampling()));
    tetMeshVLayout->addWidget(splitCellsPushButton);
    
    QPushButton * setToInitPosPushButton = new QPushButton("Transfert mesh to init");
    connect(setToInitPosPushButton, SIGNAL(clicked()), viewer, SLOT(setToInit()));
    tetMeshVLayout->addWidget(setToInitPosPushButton);
    
    QPushButton * setToCurrentPushButton = new QPushButton("Set to current state");
    connect(setToCurrentPushButton, SIGNAL(clicked()), viewer, SLOT(setToCurrent()));
    tetMeshVLayout->addWidget(setToCurrentPushButton);
    
    QCheckBox * drawBoundingBoxCheckBox = new QCheckBox("Draw Cage BB");
    tetMeshVLayout->addWidget(drawBoundingBoxCheckBox);
    connect(drawBoundingBoxCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayCageBB(bool)));
    
    QCheckBox * drawSamplingCheckBox = new QCheckBox("Draw Poisson Sampling");
    tetMeshVLayout->addWidget(drawSamplingCheckBox);
    connect(drawSamplingCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplaySampling(bool)));
    
    QCheckBox * drawCageTetsCheckBox = new QCheckBox("Draw Cage Tetrahedrization");
    tetMeshVLayout->addWidget(drawCageTetsCheckBox);
    connect(drawCageTetsCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayCageTets(bool)));
    
    QCheckBox * drawDistortionCheckBox = new QCheckBox("Draw Distortion");
    tetMeshVLayout->addWidget(drawDistortionCheckBox);
    connect(drawDistortionCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayDistortions(bool)));
    
    QCheckBox * drawPointsCheckBox = new QCheckBox("Draw added points");
    tetMeshVLayout->addWidget(drawPointsCheckBox);
    connect(drawPointsCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayPoints(bool)));
    
    QCheckBox * drawEdgesCheckBox = new QCheckBox("Draw edges");
    tetMeshVLayout->addWidget(drawEdgesCheckBox);
    connect(drawEdgesCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayEdges(bool)));
    
    tetMeshVLayout->addStretch(0);
    contents->addTab(tetMeshTab, "Tetrahedral Mesh");
*/
    
    widgetVLayout->addWidget(contents);
    mesherDockWidget->setWidget(widget);
    
    this->addDockWidget(Qt::RightDockWidgetArea, mesherDockWidget);
}

void Window::updateGridWithTransferMesh(){
    viewer->updateGridWithTransferMesh(minimalMemoryCheckBox->isChecked());
}

QGroupBox * Window::createOptimizationGroupBox(){
    QGroupBox * optimizationGroupBox = new QGroupBox("Optimization");
    QVBoxLayout * optimizationLayout = new QVBoxLayout(optimizationGroupBox);
    
    QPushButton * exudePushButton = new QPushButton("Exude Slivers");
    connect(exudePushButton, SIGNAL(clicked()), this, SLOT(exude_image_mesh()));
    optimizationLayout->addWidget(exudePushButton);
    
    QPushButton * pertubPushButton = new QPushButton("Perturbe Slivers");
    connect(pertubPushButton, SIGNAL(clicked()), this, SLOT(perturb_image_mesh()));
    optimizationLayout->addWidget(pertubPushButton);
    
    QPushButton * lloydSmoothingPushButton = new QPushButton("Lloyd smoothing");
    connect(lloydSmoothingPushButton, SIGNAL(clicked()), this, SLOT(lloydSmoothing_image_mesh()));
    optimizationLayout->addWidget(lloydSmoothingPushButton);
    
    QPushButton * odtSmoothingPushButton = new QPushButton("Odt smoothing");
    connect(odtSmoothingPushButton, SIGNAL(clicked()), this, SLOT(odtSmoothing_image_mesh()));
    optimizationLayout->addWidget(odtSmoothingPushButton);
    
    return optimizationGroupBox;
    
}

QGroupBox * Window::createTrOptimizationGroupBox(){
    QGroupBox * optimizationGroupBox = new QGroupBox("Optimization");
    QVBoxLayout * optimizationLayout = new QVBoxLayout(optimizationGroupBox);
    
    QPushButton * exudePushButton = new QPushButton("Exude Slivers");
    connect(exudePushButton, SIGNAL(clicked()), this, SLOT(exude_transfer_mesh()));
    optimizationLayout->addWidget(exudePushButton);
    
    QPushButton * pertubPushButton = new QPushButton("Perturbe Slivers");
    connect(pertubPushButton, SIGNAL(clicked()), this, SLOT(perturb_transfer_mesh()));
    optimizationLayout->addWidget(pertubPushButton);
    
    QPushButton * lloydSmoothingPushButton = new QPushButton("Lloyd smoothing");
    connect(lloydSmoothingPushButton, SIGNAL(clicked()), this, SLOT(lloydSmoothing_transfer_mesh()));
    optimizationLayout->addWidget(lloydSmoothingPushButton);
    
    QPushButton * odtSmoothingPushButton = new QPushButton("Odt smoothing");
    connect(odtSmoothingPushButton, SIGNAL(clicked()), this, SLOT(odtSmoothing_transfer_mesh()));
    optimizationLayout->addWidget(odtSmoothingPushButton);
    
    return optimizationGroupBox;
    
}

void Window::initInfoDockWidget(){
    QDockWidget *  infoDockWidget = new QDockWidget("Info");
    
    QWidget * contents = new QWidget();
    
    QVBoxLayout * contentLayout = new QVBoxLayout();
    
    displayLabel = new QLabel();

    contentLayout->addWidget (displayLabel);
    distortionLabel = new QLabel();
    contentLayout->addWidget(distortionLabel);
    elongationLabel = new QLabel();
    contentLayout->addWidget(elongationLabel);
    contents->setLayout(contentLayout);
    volumeLabel = new QLabel();
    contentLayout->addWidget(volumeLabel);
    contents->setLayout(contentLayout);

    infoDockWidget->setWidget(contents);
    
    this->addDockWidget(Qt::LeftDockWidgetArea, infoDockWidget);
}


void Window::initQualityHistogramDockWidget(){
    QDockWidget *  qualityHistDockWidget = new QDockWidget("Info");
    
    QWidget * contents = new QWidget();
    
    QVBoxLayout * contentLayout = new QVBoxLayout();
    
    displayLabel = new QLabel();
    
    contentLayout->addWidget (displayLabel);
    distortionLabel = new QLabel();
    contentLayout->addWidget(distortionLabel);
    elongationLabel = new QLabel();
    contentLayout->addWidget(elongationLabel);
    contents->setLayout(contentLayout);
    volumeLabel = new QLabel();
    contentLayout->addWidget(volumeLabel);
    contents->setLayout(contentLayout);
    
    qualityHistDockWidget->setWidget(contents);
    
    this->addDockWidget(Qt::LeftDockWidgetArea, qualityHistDockWidget);
}

/*
void Window::updateInfo(){
    displayLabel->setPixmap(viewer->getQualityHistogram());
}
*/

void Window::computeVolumeHistogram(){

    volumeLabel->setPixmap(viewer->getVolumeHistogram());
    
    
}

void Window::compareVolumes(){
    QString selectedFilter, openFileNameLabel;
    QString fileFilter = "Known Filetypes (*.dim *.nii);;IMA (*.dim);;NIFTI (*.nii)";
    
    QString fileName1 = QFileDialog::getOpenFileName(this,
                                                     tr("Select an input 3D image"),
                                                     openFileNameLabel,
                                                     fileFilter,
                                                     &selectedFilter);
    
    // In case of Cancel
    if ( fileName1.isEmpty() ) {
        return;
    }
    
    QString fileName2 = QFileDialog::getOpenFileName(this,
                                                     tr("Select an input 3D image"),
                                                     openFileNameLabel,
                                                     fileFilter,
                                                     &selectedFilter);
    
    // In case of Cancel
    if ( fileName2.isEmpty() ) {
        return;
    }
    
    volumeLabel->setPixmap(viewer->compareVolumeLossWithGrid(fileName1, fileName2));
    
}

void Window::computeHistograms(){

    QPixmap eMap, dMap;
    viewer->computeHistograms(dMap, eMap);
    
    distortionLabel->setPixmap(dMap);
    elongationLabel->setPixmap(eMap);
}

void Window::exportDistortion(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save distortion info as ", "./", "VTK (*.vtk)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    viewer->export_distortion_vtk(fileName.toStdString());
    
}

void Window::rasterize(){
    viewer->rasterizeGrid(neighborhoodSizeSpinBox->value(), gridResolutionSizeSpinBox->value());
}

void Window::initDisplayDockWidgets(){
    displayDockWidget = new QDockWidget("Display");
    
    QWidget * contents = new QWidget();
    QVBoxLayout * cVLayout = new QVBoxLayout(contents);
    
    QFrame * disFrame = new QFrame();
    
    QVBoxLayout * contentLayout = new QVBoxLayout();
    disFrame->setLayout(contentLayout);
    
    QScrollArea * disScrollArea = new QScrollArea(contents);
    disScrollArea->setWidget(disFrame);
    
    disScrollArea->setWidgetResizable(true);
    disScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    
    cVLayout->addWidget(disScrollArea);
    
    drawMeshGroupBox = new QGroupBox("Draw Mesh");
    drawMeshGroupBox->setCheckable(true);
    drawMeshGroupBox->setChecked(false);
    connect(drawMeshGroupBox, SIGNAL(clicked(bool)), this, SLOT(setDisplayMesh(bool)));
    
    
    QVBoxLayout * drawMeshLayout = new QVBoxLayout(drawMeshGroupBox);
    QCheckBox * drawTetCheckBox = new QCheckBox("Tetrahedron");
    connect(drawTetCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayTetrahedra(bool)));
    
    drawTetCheckBox->setChecked(false);
    drawMeshLayout->addWidget(drawTetCheckBox);
    
    contentLayout->addWidget(drawMeshGroupBox);
    
    drawImageGroupBox = new QGroupBox("Draw Image");
    drawImageGroupBox->setCheckable(true);
    drawImageGroupBox->setChecked(false);
    connect(drawImageGroupBox, SIGNAL(clicked(bool)), this, SLOT(setDisplayImage(bool)));
    
    QVBoxLayout * drawImageLayout = new QVBoxLayout(drawImageGroupBox);
    QRadioButton * drawImageNiceRadio = new QRadioButton("Draw Voxels");
    QRadioButton * drawImagePointsRadio = new QRadioButton("Draw Points");
    
    connect(drawImageNiceRadio, SIGNAL(clicked(bool)), viewer, SLOT(drawNiceVoxels(bool)));
    
    drawImagePointsRadio->setChecked(true);
    drawImageLayout->addWidget(drawImagePointsRadio);
    drawImageLayout->addWidget(drawImageNiceRadio);
    
    contentLayout->addWidget(drawImageGroupBox);
    

    drawSegmentationGroupBox = new QGroupBox("Draw Segmentation");
    drawSegmentationGroupBox->setCheckable(true);
    drawSegmentationGroupBox->setChecked(false);
    connect(drawSegmentationGroupBox, SIGNAL(clicked(bool)), this, SLOT(setDisplaySegmentation(bool)));

    QVBoxLayout * drawSegmentationLayout = new QVBoxLayout(drawSegmentationGroupBox);
    QRadioButton * drawSegmentationNiceRadio = new QRadioButton("Draw Voxels");
    QRadioButton * drawSegmentationPointsRadio = new QRadioButton("Draw Points");

    connect(drawSegmentationNiceRadio, SIGNAL(clicked(bool)), viewer, SLOT(drawSegmentationNiceVoxels(bool)));

    drawSegmentationPointsRadio->setChecked(true);
    drawSegmentationLayout->addWidget(drawSegmentationPointsRadio);
    drawSegmentationLayout->addWidget(drawSegmentationNiceRadio);

    contentLayout->addWidget(drawSegmentationGroupBox);

    displayImageGroupBox = new QGroupBox("Label display", this);

    QVBoxLayout * segIVLayout = new QVBoxLayout(displayImageGroupBox);

    QFrame * segIFrame = new QFrame();

    segIGridLayout = new QGridLayout(segIFrame);

    QScrollArea * displayIScrollArea = new QScrollArea(displayImageGroupBox);
    displayIScrollArea->setWidget(segIFrame);

    displayIScrollArea->setWidgetResizable(true);
    displayIScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    segIVLayout->addWidget(displayIScrollArea);
    signalIMapper = new QSignalMapper(displayImageGroupBox);

    connect(signalIMapper, SIGNAL(mapped(const int &)), this, SLOT(setIVisibility(const int &)));

    QGridLayout * segIGridKayout = new QGridLayout();

    QPushButton *selectIPushButton = new QPushButton("Select All", displayImageGroupBox);
    segIGridKayout->addWidget(selectIPushButton, 0, 0, 1, 1);

    QPushButton *discardIPushButton = new QPushButton("Discard All", displayImageGroupBox);
    segIGridKayout->addWidget(discardIPushButton, 0, 1, 1, 1);

    segIVLayout->addLayout(segIGridKayout);

    segIVLayout->addStretch(0);
    connect(discardIPushButton, SIGNAL(clicked()), this, SLOT(discardIAll()));
    connect(selectIPushButton, SIGNAL(clicked()), this, SLOT(selectIAll()));

    connect(viewer, SIGNAL(setImageLabels()), this, SLOT(setImageLabels()));
    contentLayout->addWidget(displayImageGroupBox);
    //displayImageGroupBox->setVisible(false);
    
    displayGroupBox = new QGroupBox("Label display", this);
    
    QVBoxLayout * segVLayout = new QVBoxLayout(displayGroupBox);
    
    QFrame * segFrame = new QFrame();
    
    segGridLayout = new QGridLayout(segFrame);
    
    QScrollArea * displayScrollArea = new QScrollArea(displayGroupBox);
    displayScrollArea->setWidget(segFrame);
    
    displayScrollArea->setWidgetResizable(true);
    displayScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    
    segVLayout->addWidget(displayScrollArea);
    signalMapper = new QSignalMapper(displayGroupBox);
    
    connect(signalMapper, SIGNAL(mapped(const int &)), this, SLOT(setVisibility(const int &)));
    
    QGridLayout * segGridKayout = new QGridLayout();
    
    QPushButton *selectPushButton = new QPushButton("Select All", displayGroupBox);
    segGridKayout->addWidget(selectPushButton, 0, 0, 1, 1);
    
    QPushButton *discardPushButton = new QPushButton("Discard All", displayGroupBox);
    segGridKayout->addWidget(discardPushButton, 0, 1, 1, 1);
    
    segVLayout->addLayout(segGridKayout);
    
    segVLayout->addStretch(0);
    connect(discardPushButton, SIGNAL(clicked()), this, SLOT(discardAll()));
    connect(selectPushButton, SIGNAL(clicked()), this, SLOT(selectAll()));
    
    connect(viewer, SIGNAL(setMeshedLabels()), this, SLOT(setMeshedLabels()));
    
    contentLayout->addWidget(displayGroupBox);
    
    /*
    QPushButton * recomputeColorsPushButton = new QPushButton("Recompute Colors");
    contentLayout->addWidget(recomputeColorsPushButton);
    connect(recomputeColorsPushButton, SIGNAL(clicked()), viewer, SLOT(recomputeColors()));
    
    
    QCheckBox * drawBinaryColor = new QCheckBox("Draw Binary");
    contentLayout->addWidget(drawBinaryColor);
    connect(drawBinaryColor, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayBC(bool)));
    
    QCheckBox * drawIndexDependantColors = new QCheckBox("Draw Index Dependant");
    contentLayout->addWidget(drawIndexDependantColors);
    connect(drawIndexDependantColors, SIGNAL(clicked(bool)), viewer, SLOT(setIndexDependantColors(bool)));
   */
    
    QPushButton * openColorsPB = new QPushButton("Open Color File", this);
    contentLayout->addWidget(openColorsPB);
    connect(openColorsPB, SIGNAL(clicked()), this, SLOT(openColors()));

    contentLayout->addWidget(getCuttingPlaneGroupBox(this));
    
    QPushButton * updatePushButton = new QPushButton("Update");
    contentLayout->addWidget(updatePushButton);
    connect(updatePushButton, SIGNAL(clicked()), viewer, SLOT(updateGridDisplay()));

    QCheckBox * drawBBCheckBox = new QCheckBox("Draw BoundingBox");
    contentLayout->addWidget(drawBBCheckBox);
    connect(drawBBCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayBB(bool)));
    
    
    
    QPushButton * invertNormalsPushButton = new QPushButton("Invert Normals");
    contentLayout->addWidget(invertNormalsPushButton);
    connect(invertNormalsPushButton, SIGNAL(clicked()), viewer, SLOT(invertNormals()));
    
    QGroupBox * displayCageGroupBox = new QGroupBox ("Draw Cage");
    displayCageGroupBox->setCheckable(true);
    displayCageGroupBox->setChecked(true);
    connect (displayCageGroupBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayCage(bool)));
    
    QVBoxLayout * displayVLayout = new QVBoxLayout(displayCageGroupBox);
    
    QRadioButton * displayCageRadioButton = new QRadioButton("Display Wire Cage");
    connect (displayCageRadioButton, SIGNAL(toggled(bool)), viewer, SLOT(setDisplayWireCage(bool)));
    displayCageRadioButton->setChecked(false);
    displayVLayout->addWidget(displayCageRadioButton);
    
    QRadioButton * displayWireCageRadioButton = new QRadioButton("Display Transparent Cage");
    displayWireCageRadioButton->setChecked(true);
    displayVLayout->addWidget(displayWireCageRadioButton);
    
    QLabel * sphereRadiusLabel = new QLabel("Sphere radius");
    displayVLayout->addWidget(sphereRadiusLabel);
    sphereRadiusSpinBox = new QDoubleSpinBox();
    sphereRadiusSpinBox->setSingleStep(0.01);
    sphereRadiusSpinBox->setValue(1.);
    sphereRadiusSpinBox->setDecimals( 2 );
    connect (sphereRadiusSpinBox, SIGNAL(valueChanged(double)), viewer, SLOT(setSphereScale(double)));
    
    displayVLayout->addWidget(sphereRadiusSpinBox);
    
    QLabel * manipulatorScaleLabel = new QLabel("Manipulator scale");
    displayVLayout->addWidget(manipulatorScaleLabel);
    QDoubleSpinBox * manipulatorScaleSpinBox = new QDoubleSpinBox();
    manipulatorScaleSpinBox->setSingleStep(0.01);
    manipulatorScaleSpinBox->setValue(1.);
    manipulatorScaleSpinBox->setDecimals( 2 );
    connect (manipulatorScaleSpinBox, SIGNAL(valueChanged(double)), viewer, SLOT(setManipulatorScale(double)));
    
    displayVLayout->addWidget(manipulatorScaleSpinBox);
    
    QCheckBox * displayCageNormalsCheckBox = new QCheckBox("Display Cage Normals");
    connect (displayCageNormalsCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayCageNormals(bool)));
    
    displayVLayout->addWidget(displayCageNormalsCheckBox);
    
    contentLayout->addWidget(displayCageGroupBox);
    
    QPushButton * saveCameraPushButton = new QPushButton("Save camera");
    contentLayout->addWidget(saveCameraPushButton);
    connect(saveCameraPushButton, SIGNAL(clicked()), this, SLOT(saveCamera()));
    
    QPushButton * loadCameraPushButton = new QPushButton("Load camera");
    contentLayout->addWidget(loadCameraPushButton);
    connect(loadCameraPushButton, SIGNAL(clicked()), this, SLOT(openCamera()));
    
    /*
      
    QPushButton * selectBackPushButton = new QPushButton("Select Background Color");
    contentLayout->addWidget(selectBackPushButton);
    connect(selectBackPushButton, SIGNAL(clicked()), this, SLOT(changeBackgroundColor()));
*/
    QCheckBox * drawModelCheckBox = new QCheckBox("Draw Objectif mesh");
    contentLayout->addWidget(drawModelCheckBox);
    connect(drawModelCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDrawModel(bool)));
    
    /*
    QCheckBox * useMovingLightCheckBox = new QCheckBox("Use moving light");
    contentLayout->addWidget(useMovingLightCheckBox);
    connect(useMovingLightCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(useMovingLight(bool)));
    */
    
    contentLayout->addStretch(0);
    displayDockWidget->setWidget(contents);
    
    this->addDockWidget(Qt::LeftDockWidgetArea, displayDockWidget);
}

void Window::openColors(){

    QString selectedFilter, openFileNameLabel;
    QString fileFilter = "Known Filetypes (*.txt );;TXT (*.txt)";

    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an color file"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);

    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }

    statusBar()->showMessage("Opening color...");
    if(fileName.endsWith(".txt")){
        viewer->openColors(fileName);
        statusBar()->showMessage("Color file opened");
    }
}

QGroupBox * Window::getCuttingPlaneGroupBox(QWidget * parent){
    QGroupBox * groupBox = new QGroupBox("groupBox", parent);
    
    groupBox->setCheckable(true);
    groupBox->setChecked(false);
    connect(groupBox, SIGNAL(clicked(bool)), viewer, SLOT(setClippingTest(bool)));
    
    groupBox->setTitle("Cutting plane");
    groupBox->setMaximumSize(QSize(16777215, 200));
    
    QGridLayout * cuttingPlaneGridLayout = new QGridLayout(groupBox);
    xHSlider = new QSlider(groupBox);
    xHSlider->setObjectName("xHSlider");
    xHSlider->setOrientation(Qt::Horizontal);
    
    cuttingPlaneGridLayout->addWidget(xHSlider, 1, 0, 1, 1);
    
    yHSlider = new QSlider(groupBox);
    yHSlider->setObjectName("yHSlider");
    yHSlider->setOrientation(Qt::Horizontal);
    
    cuttingPlaneGridLayout->addWidget(yHSlider, 3, 0, 1, 1);
    
    zHSlider = new QSlider(groupBox);
    zHSlider->setObjectName("zHSlider");
    zHSlider->setMaximum(1);
    zHSlider->setOrientation(Qt::Horizontal);
    
    cuttingPlaneGridLayout->addWidget(zHSlider, 5, 0, 1, 1);
    
    QPushButton * invertXPushButton = new QPushButton("invert", groupBox);
    cuttingPlaneGridLayout->addWidget(invertXPushButton, 1, 1, 1, 1);
    
    QPushButton * invertYPushButton = new QPushButton("invert", groupBox);
    cuttingPlaneGridLayout->addWidget(invertYPushButton, 3, 1, 1, 1);
    
    QPushButton * invertZPushButton = new QPushButton("invert", groupBox);
    cuttingPlaneGridLayout->addWidget(invertZPushButton, 5, 1, 1, 1);
    
    QLabel * labelCutX = new QLabel("x cut position", groupBox);
    cuttingPlaneGridLayout->addWidget(labelCutX, 0, 0, 1, 1);
    
    QLabel * labelCutY = new QLabel("y cut position", groupBox);
    cuttingPlaneGridLayout->addWidget(labelCutY, 2, 0, 1, 1);
    
    QLabel * labelCutZ = new QLabel("z cut position", groupBox);
    cuttingPlaneGridLayout->addWidget(labelCutZ, 4, 0, 1, 1);
    
    QCheckBox * displayXCut = new QCheckBox("display", groupBox);
    cuttingPlaneGridLayout->addWidget(displayXCut, 0, 1, 1, 1);
    
    QCheckBox * displayYCut = new QCheckBox("display", groupBox);
    cuttingPlaneGridLayout->addWidget(displayYCut, 2, 1, 1, 1);
    
    QCheckBox * displayZCut = new QCheckBox("display", groupBox);
    cuttingPlaneGridLayout->addWidget(displayZCut, 4, 1, 1, 1);
    
    connect(xHSlider, SIGNAL(valueChanged(int)), viewer, SLOT(setXCut(int)));
    connect(yHSlider, SIGNAL(valueChanged(int)), viewer, SLOT(setYCut(int)));
    connect(zHSlider, SIGNAL(valueChanged(int)), viewer, SLOT(setZCut(int)));
    
    connect(invertXPushButton, SIGNAL(clicked()), viewer, SLOT(invertXCut()));
    connect(invertYPushButton, SIGNAL(clicked()), viewer, SLOT(invertYCut()));
    connect(invertZPushButton, SIGNAL(clicked()), viewer, SLOT(invertZCut()));
    
    connect(displayXCut, SIGNAL(clicked(bool)), viewer, SLOT(setXCutDisplay(bool)));
    connect(displayYCut, SIGNAL(clicked(bool)), viewer, SLOT(setYCutDisplay(bool)));
    connect(displayZCut, SIGNAL(clicked(bool)), viewer, SLOT(setZCutDisplay(bool)));
    
    connect(viewer, SIGNAL(setMaxCutPlanes(int,int,int)), this, SLOT(setMaxCutPlanes(int,int,int)));
    
    return groupBox;
}

void Window::saveCamera(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save camera file as ", "./data/", "Data (*.txt)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(!fileName.endsWith(".txt"))
        fileName.append(".txt");
    
    viewer->saveCamera(fileName);
}

void Window::openCamera(){
    
    QString selectedFilter, openFileNameLabel;
    
    QString fileFilter = "Known Filetypes (*.txt);; Data (*.txt)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input camera"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    viewer->openCamera(fileName);
}

void Window::changeBackgroundColor(){
    QColor color = QColorDialog::getColor(Qt::white, this);
    if (color.isValid()) {
        viewer->setBackgroundColor(color);
        viewer->setWhite();
    }
    
}

void Window::setMaxCutPlanes( int x, int y , int z ){
    xHSlider->setRange(0,x);
    yHSlider->setRange(0,y);
    zHSlider->setRange(0,z);
}

void Window::setRasterMode( int mode ){
    RasterType rt;
    if( mode == 0 ){
        rt = VECTOR_FIELD;
    } else if( mode == 1 ) {
        rt = NEAREST_NEIGHBOR;
    } else if( mode == 2 ) {
        rt = DILATATION;
    }
    viewer->setRasterType(rt);
}

void Window::setDisplayMesh(bool visible){
    DisplayType dT = NONE;
    displayImageGroupBox->setVisible(false);
    drawImageGroupBox->setChecked(false);
    if(visible) {
        dT = MESH;
    }
    drawMeshGroupBox->setChecked(visible);
    displayGroupBox->setVisible(visible);
    viewer->setDisplayType(dT);
    
}

void Window::setDisplayImage(bool visible){
    DisplayType dT = NONE;
    displayGroupBox->setVisible(false);
    drawMeshGroupBox->setChecked(false);
    if(visible) {
        dT = IMAGE;
    }
    drawImageGroupBox->setChecked(visible);
    displayImageGroupBox->setVisible(visible);
    viewer->setDisplayType(dT);
    
}

//void Window::setDisplayImage(bool visible){
//    DisplayType dT = NONE;
//    displayGroupBox->setVisible(false);
//    drawMeshGroupBox->setChecked(false);
//    if(visible) {
//        dT = SEGMENTATION;
//    }
//    drawImageGroupBox->setChecked(visible);
//    displayImageGroupBox->setVisible(visible);
//    viewer->setDisplayType(dT);
//
//}

void Window::saveMeshes(){
    QString folderName = QFileDialog::getExistingDirectory(this , tr("Folder for the meshes"), "./data/");
    
    // In case of Cancel
    if ( folderName.isEmpty() ) {
        return;
    }
    
    viewer->saveMeshes(folderName);
    
}

void Window::saveTransferMesh(){
    
    QString fileName = QFileDialog::getSaveFileName(this, "Save transfer mesh file as ", "./data/", "Data (*.mesh)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(!fileName.endsWith(".mesh"))
        fileName.append(".mesh");
    
    viewer->saveTransferMesh(fileName);
    
}

void Window::saveMesh(){
    
    QString fileName = QFileDialog::getSaveFileName(this, "Save mesh file as ", "./data/", "Data (*.mesh);;Off (*.off)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(fileName.endsWith(".mesh"))
        viewer->saveMESH(fileName);
    else if(fileName.endsWith(".off") )
        viewer->saveOFF(fileName);
    else if( fileName.endsWith(".vrml")){
        viewer->saveVRML(fileName);
    }else {
        fileName.append(".mesh");
        viewer->saveMESH(fileName);
    }
    
    
    
}

void Window::saveSurface(){
    
    QString fileName = QFileDialog::getSaveFileName(this, "Save surface as ", "./data/", "Data (*.off)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(!fileName.endsWith(".off")) fileName.append(".off");
    
    viewer->saveSurface(fileName);
    
}

void Window::saveEnvelop(){
    
    QString fileName = QFileDialog::getSaveFileName(this, "Save envelop as ", "./data/", "Data (*.off)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(!fileName.endsWith(".off")) fileName.append(".off");
    
    viewer->saveCGALEnvelop(fileName);
    
}

void Window::saveCage(){
    
    QString fileName = QFileDialog::getSaveFileName(this, "Save cage file as ", "./data/", "Data (*.off)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(!fileName.endsWith(".off")) fileName.append(".off");
    
    viewer->saveCage(fileName);
    
}

void Window::open3DImage(){
    
    QString selectedFilter, openFileNameLabel;
    QString fileFilter = "Known Filetypes (*.dim *.nii);;IMA (*.dim);;NIFTI (*.nii)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input 3D image"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    statusBar()->showMessage("Opening 3D image...");
    
    viewer->openImage(fileName, loadFR());
    
    set3DImageDisplay();
    statusBar()->showMessage("3D image opened");
}

void Window::openImageSegmentation(){
    QString selectedFilter, openFileNameLabel;
    QString fileFilter = "Known Filetypes (*.dim *.nii);;IMA (*.dim);;NIFTI (*.nii)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input segmentation"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    statusBar()->showMessage("Opening segmentation...");
    
    viewer->openSegmentation(fileName);
    
    statusBar()->showMessage("Segmentation opened");
}


void Window::openModel(){
    
    QString selectedFilter, openFileNameLabel;
    
    QString fileFilter = "Known Filetypes (*.off);;OFF (*.off)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an model mesh"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(fileName.endsWith(".off")) {
        viewer->openModel(fileName);
    }
    
}

void Window::openTransferMesh(){
    
    QString selectedFilter, openFileNameLabel;
    
    QString fileFilter = "Known Filetypes (*.mesh);; MESH (*.mesh)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input transfer mesh"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(fileName.endsWith(".mesh"))  {
        viewer->openTransferMESH(fileName);
    }
    
}

void Window::openMesh(){
    
    QString selectedFilter, openFileNameLabel;
    
#if 1
    QString fileFilter = "Known Filetypes (*.obj *.mesh *.off);;OBJ (*.obj);;MESH (*.mesh);;OFF (*.off)";
#else
    
    QString fileFilter = "Known Filetypes (*.mesh *.off);;MESH (*.mesh);;OFF (*.off)";
#endif
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input mesh"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    if(fileName.endsWith(".off")) {
        viewer->openOFF(fileName);
    }else if(fileName.endsWith(".mesh"))  {
        viewer->openMESH(fileName);
    } else if(fileName.endsWith(".obj")) {
        viewer->openOBJ(fileName);
    }
    
    setMeshDisplay();
}

void Window::openMeshes(){
    QString folderName = QFileDialog::getExistingDirectory(this , tr("Folder containing the meshes"), "./data/");
    
    // In case of Cancel
    if ( folderName.isEmpty() ) {
        return;
    }
    
    QDir dir(folderName);
    
    statusBar()->showMessage("Opening " + dir.dirName() + " meshes...");
    
    QStringList filters(QString("*.off"));
    
    QFileInfoList list = dir.entryInfoList(filters);
    if( list.size() > 0 ){
        viewer->openOFFFiles(list);
    } else {
        filters = QStringList(QString("*.obj"));
        
        list = dir.entryInfoList(filters);
        if( list.size() > 0 )
            viewer->openOBJFiles(list);
    }
    
    statusBar()->showMessage(dir.dirName() + " meshes opened");
    setMeshDisplay();
}


void Window::useCGALEnvelopAsCage(){
    setMode(MESH_DEFORMATION);
    modeComboBox->setCurrentIndex(MESH_DEFORMATION);
    statusBar()->showMessage(QString("Loading cage..."));
    viewer->useCGALEnvelopAsCage();;
    sphereRadiusSpinBox->setValue(viewer->getSphereScale());
    statusBar()->showMessage(QString("Cage computed..."));
    computeCoordinates->setEnabled(true);
}

void Window::openCage(){
    QString selectedFilter, openFileNameLabel;
    QString fileFilter = "Known Filetypes (*.off );;OFF (*.off)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input cage"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    setMode(MESH_DEFORMATION);
    modeComboBox->setCurrentIndex(MESH_DEFORMATION);
    statusBar()->showMessage(QString("Cage opening..."));
    viewer->openCage(fileName);
    sphereRadiusSpinBox->setValue(viewer->getSphereScale());
    statusBar()->showMessage(QString("Cage opened..."));
    computeCoordinates->setEnabled(true);
}

void Window::openFinalCage(){
    QString selectedFilter, openFileNameLabel;
    QString fileFilter = "Known Filetypes (*.off );;OFF (*.off)";
    
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select an input cage"),
                                                    openFileNameLabel,
                                                    fileFilter,
                                                    &selectedFilter);
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    statusBar()->showMessage(QString("Cage final cage..."));
    viewer->openFinalCage(fileName);
    statusBar()->showMessage(QString("Cage opened"));
    computeCoordinates->setEnabled(true);
}

void Window::initActions () {
    initFileActions ();
}

void Window::initFileActions () {
    QAction * fileOpen3DImageAction = new QAction (QPixmap ("./Icons/fileopenimage.png"), "Open 3D Image", this);
    fileOpen3DImageAction->setShortcut (tr ("Ctrl+I"));
    connect (fileOpen3DImageAction, SIGNAL (triggered ()) , this, SLOT (open3DImage()));
    
    QAction * fileOpenSegmentationAction = new QAction (QPixmap ("./Icons/fileopensegmentation.png"), "Open Segmentation", this);
    connect (fileOpenSegmentationAction, SIGNAL (triggered ()) , this, SLOT (openImageSegmentation()));
    
    QAction * fileSaveEnvelopAction = new QAction (QPixmap ("./Icons/filesaveenvelope.png"), "Save Envelop", this);
    fileSaveEnvelopAction->setShortcut (tr ("Ctrl+E"));
    connect (fileSaveEnvelopAction, SIGNAL (triggered ()) , this, SLOT (saveEnvelop()));
    
    QAction * fileOpenMeshAction = new QAction (QPixmap ("./Icons/fileopenmesh.png"), "Open Mesh", this);
    fileOpenMeshAction->setShortcut (tr ("Ctrl+O"));
    connect (fileOpenMeshAction, SIGNAL (triggered ()) , this, SLOT (openMesh ()));
    
    QAction * fileOpenMeshesAction = new QAction (QPixmap ("./Icons/fileopenmeshes.png"), "Open Meshes", this);
    fileOpenMeshesAction->setShortcut (tr ("Ctrl+M"));
    connect (fileOpenMeshesAction, SIGNAL (triggered ()) , this, SLOT (openMeshes ()));
    
    QAction * fileSaveMeshAction = new QAction (QPixmap ("./Icons/filesavemesh.png"), "Save Mesh", this);
    fileSaveMeshAction->setShortcut (tr ("Ctrl+S"));
    connect (fileSaveMeshAction, SIGNAL (triggered ()) , this, SLOT (saveMesh ()));
    
    QAction * fileSaveMeshesAction = new QAction (QPixmap ("./Icons/filesavemeshes.png"), "Save Meshes", this);
    fileSaveMeshesAction->setShortcut (tr ("Ctrl+Shift+S"));
    connect (fileSaveMeshesAction, SIGNAL (triggered ()) , this, SLOT (saveMeshes ()));
    
    QAction * fileOpenCageAction = new QAction (QPixmap ("./Icons/fileopencage.png"), "Open Cage", this);
    fileOpenCageAction->setShortcut (tr ("Ctrl+Shift+O"));
    connect (fileOpenCageAction, SIGNAL (triggered ()) , this, SLOT (openCage ()));
    
    QAction * fileOpenFinalCageAction = new QAction (QPixmap ("./Icons/fileopencage.png"), "Open Final Cage", this);
    fileOpenFinalCageAction->setShortcut (tr ("Ctrl+Alt+Shift+O"));
    connect (fileOpenFinalCageAction, SIGNAL (triggered ()) , this, SLOT (openFinalCage ()));
    
    QAction * fileSaveCageAction = new QAction (QPixmap ("./Icons/filesavecage.png"), "Save Cage", this);
    fileSaveCageAction->setShortcut (tr ("Ctrl+Alt+S"));
    connect (fileSaveCageAction, SIGNAL (triggered ()) , this, SLOT (saveCage ()));
    
    QAction * fileSaveGridAction = new QAction (QPixmap ("./Icons/filesaveimage.png"), "Save Grid", this);
    fileSaveGridAction->setShortcut (tr ("Ctrl+Alt+S"));
    connect (fileSaveGridAction, SIGNAL (triggered ()) , this, SLOT (saveGrid ()));
    
    QAction * fileOpenModelAction = new QAction (QPixmap ("./Icons/fileopen.png"), "Open Model", this);
    connect (fileOpenModelAction, SIGNAL (triggered ()) , this, SLOT (openModel ()));
    
    QAction * fileSaveSegmentationAction = new QAction (QPixmap ("./Icons/filesavesegmentation.png"), "Save Segmentation", this);
    connect (fileSaveSegmentationAction, SIGNAL (triggered ()) , this, SLOT (saveSegmentation()));

    QAction * automaticSeparationAction = new QAction (QPixmap ("./Icons/fileopen.png"), "Automatic Separation", this);
    connect (automaticSeparationAction, SIGNAL (triggered ()) , viewer, SLOT (automaticSeparation ()));

    QAction * fileActiveSelectionAction = new QAction (QPixmap ("./Icons/fileopen.png"), "Active Selection", this);
    connect (fileActiveSelectionAction, SIGNAL (triggered ()) , viewer, SLOT (setActiveSelection ()));

    QAction * fileActiveGridSelectionAction = new QAction (QPixmap ("./Icons/fileopen.png"), "Active Selection", this);
    connect (fileActiveGridSelectionAction, SIGNAL (triggered ()) , viewer, SLOT (setActiveGridSelection ()));


    QAction * fileQuitAction = new QAction (QPixmap("./Icons/exit.png"), "Quit", this);
    fileQuitAction->setShortcut (tr ("Ctrl+Q"));
    connect (fileQuitAction, SIGNAL (triggered()) , qApp, SLOT (closeAllWindows()));
    
    fileActionGroup = new QActionGroup (this);
    fileActionGroup->addAction (fileOpen3DImageAction);
    fileActionGroup->addAction(fileOpenSegmentationAction);
    fileActionGroup->addAction (fileSaveEnvelopAction);
    fileActionGroup->addAction (fileOpenMeshAction);
    fileActionGroup->addAction (fileOpenMeshesAction);
    fileActionGroup->addAction (fileSaveMeshAction);
    fileActionGroup->addAction (fileSaveMeshesAction);
    fileActionGroup->addAction (fileOpenCageAction);
    fileActionGroup->addAction (fileSaveCageAction);
    fileActionGroup->addAction (fileOpenFinalCageAction);
    fileActionGroup->addAction (fileSaveGridAction);
    // fileActionGroup->addAction (fileOpenModelAction);
    fileActionGroup->addAction (fileSaveSegmentationAction);
    fileActionGroup->addAction (automaticSeparationAction);
    fileActionGroup->addAction (fileActiveSelectionAction);
    fileActionGroup->addAction (fileActiveGridSelectionAction);
    fileActionGroup->addAction (fileQuitAction);
    
}


void Window::initMenus () {
    QMenu * fileMenu = menuBar ()->addMenu (tr ("File"));;
    fileMenu->addActions (fileActionGroup->actions ());
    
    QMenu * helpMenu = menuBar ()->addMenu (tr ("Help"));
    QAction *  helpAction = new QAction ("Control", this);
    helpAction->setShortcut (tr ("Ctrl+H"));
    connect (helpAction, SIGNAL (activated ()), viewer, SLOT (help ()));
    helpMenu->addAction (helpAction);
}

void Window::initToolBars () {
    fileToolBar = new QToolBar (this);
    fileToolBar->addActions (fileActionGroup->actions ());
    fileToolBar->setIconSize(QSize(50,50));
    addToolBar (fileToolBar);
}


void Window::addImageLabels(const std::map<Subdomain_index, QColor> & colors){
    
    std::map<unsigned char, QColor> textColorMap = viewer->getTextColorMap();
    indexIToLabel.clear();
    std::vector<Subdomain_index> subdomain_indices;
    viewer->getImageSubdomainIndices(subdomain_indices);
    int i = 0;
    for (std::map<Subdomain_index, QColor>::const_iterator it = colors.begin(); it != colors.end() ; ++it , i++){
        std::cout <<"Window:: si "<< it->first << std::endl;
        indexIToLabel.push_back(it->first);

        QColor color = it->second;
        if(textColorMap.find(subdomain_indices[it->first]) == textColorMap.end() )
            std::cout << "Not found !! " <<std::endl;
        else
            color = textColorMap[subdomain_indices[it->first]];

        if(labelICheckButtons.size() > i ){
            imageLabels[i]->setPalette(color);
            imageLabels[i]->setVisible(true);
            labelICheckButtons[i]->setVisible(true);
        } else {

            QLabel * labelText  = new QLabel(QString::number(subdomain_indices[it->first]));
            segIGridLayout->addWidget(labelText, i, 0, 1, 1);

            QCheckBox * labelICheckBox = new QCheckBox(displayImageGroupBox);
            segIGridLayout->addWidget(labelICheckBox, i, 2, 1, 1);

            QLabel * labelColor = new QLabel(displayImageGroupBox);
            labelColor->setPalette(color);
            labelColor->setAutoFillBackground(true);

            segIGridLayout->addWidget(labelColor, i, 1, 1, 1);

            imageLabels.push_back(labelColor);
            labelICheckButtons.push_back(labelICheckBox);

            signalIMapper->setMapping(labelICheckBox, i);
            connect(labelICheckBox, SIGNAL(clicked()), signalIMapper, SLOT (map()));
        }
    }

    if( (unsigned int)labelICheckButtons.size() > colors.size() ){
        for( int b = colors.size() ; b < labelICheckButtons.size() ; b++ ){
            labelICheckButtons[b]->setVisible(false);
            imageLabels[b]->setVisible(false);
        }
    }

    displayImageGroupBox->adjustSize();
    
}

void Window::addSegmentationLabels(const std::map<Subdomain_index, QColor> & colors){

    indexIToLabel.clear();
    std::vector<Subdomain_index> subdomain_indices;
    viewer->getSegmentationSubdomainIndices(subdomain_indices);
    int i = 0;
    for (std::map<Subdomain_index, QColor>::const_iterator it = colors.begin(); it != colors.end() ; ++it , i++){
        indexIToLabel.push_back(it->first);
        QColor color = it->second;

        if(labelICheckButtons.size() > i ){
            imageLabels[i]->setPalette(color);
            imageLabels[i]->setVisible(true);
            labelICheckButtons[i]->setVisible(true);
        } else {

            QLabel * labelText  = new QLabel(QString::number(subdomain_indices[it->first]));
            segIGridLayout->addWidget(labelText, i, 0, 1, 1);

            QCheckBox * labelICheckBox = new QCheckBox(displayImageGroupBox);
            segIGridLayout->addWidget(labelICheckBox, i, 2, 1, 1);

            QLabel * labelColor = new QLabel(displayImageGroupBox);
            labelColor->setPalette(color);
            labelColor->setAutoFillBackground(true);

            segIGridLayout->addWidget(labelColor, i, 1, 1, 1);

            imageLabels.push_back(labelColor);
            labelICheckButtons.push_back(labelICheckBox);

            signalIMapper->setMapping(labelICheckBox, i);
            connect(labelICheckBox, SIGNAL(clicked()), signalIMapper, SLOT (map()));
        }
    }

    if( (unsigned int)labelICheckButtons.size() > colors.size() ){
        for( int b = colors.size() ; b < labelICheckButtons.size() ; b++ ){
            labelICheckButtons[b]->setVisible(false);
            imageLabels[b]->setVisible(false);
        }
    }

    displayImageGroupBox->adjustSize();

}

void Window::addMeshedLabels(const std::map<Subdomain_index, QColor> & colors){
    
    indexToLabel.clear();

    int i = 0;
    for (std::map<Subdomain_index, QColor>::const_iterator it = colors.begin(); it != colors.end() ; ++it , i++){
        indexToLabel.push_back(it->first);
        QColor color = it->second;
        
        if(labelCheckButtons.size() > i ){
            meshLabels[i]->setPalette(color);
            meshLabels[i]->setVisible(true);
            labelCheckButtons[i]->setVisible(true);
        } else {
            QLabel * labelText  = new QLabel(QString::number(it->first));
            segGridLayout->addWidget(labelText, i, 0, 1, 1);
            
            QCheckBox * labelCheckBox = new QCheckBox( displayGroupBox);
            segGridLayout->addWidget(labelCheckBox, i, 2, 1, 1);
            
            QLabel * labelColor = new QLabel(displayGroupBox);
            labelColor->setPalette(color);
            labelColor->setAutoFillBackground(true);
            
            segGridLayout->addWidget(labelColor, i, 1, 1, 1);
            
            meshLabels.push_back(labelColor);
            labelCheckButtons.push_back(labelCheckBox);
            
            signalMapper->setMapping(labelCheckBox, i);
            connect(labelCheckBox, SIGNAL(clicked()), signalMapper, SLOT (map()));
        }
    }
    
    if( (unsigned int)labelCheckButtons.size() > colors.size() ){
        for( int b = colors.size() ; b < labelCheckButtons.size() ; b++ ){
            labelCheckButtons[b]->setVisible(false);
            meshLabels[b]->setVisible(false);
        }
    }
    
    displayGroupBox->adjustSize();
    
}

void Window::setMeshedLabels(){
    
    const std::map<Subdomain_index, QColor> & colorMap = viewer->getColorMap();
    
    addMeshedLabels(colorMap);
    selectAll();
    
    //labelNbSpinBox->setRange(1, indexToLabel.size());
    
}

void Window::setImageLabels(){
    
    const std::map<Subdomain_index, QColor> & colorMap = viewer->getIColorMap();
    
    addImageLabels(colorMap);
    selectIAll();
    
}

void Window::setSegmentationLabels(){

    const std::map<Subdomain_index, QColor> & colorMap = viewer->getSColorMap();

    addSegmentationLabels(colorMap);
    selectSAll();

}
void Window::initializeCage(){
    statusBar()->showMessage("Computing coordinates...");
    viewer->initializeCage();
    statusBar()->showMessage("Ready for deformations");
}

void Window::setVisibility(int i){
    if(i < (int)indexToLabel.size())
        viewer->setVisibility(indexToLabel.at(i), labelCheckButtons[i]->isChecked());
}

void Window::selectAll(){
    for (int i = 0;i<labelCheckButtons.size();i++){
        if(!labelCheckButtons[i]->isChecked()){
            labelCheckButtons[i]->setChecked(true);
        }
        setVisibility(i);
    }
}

void Window::setIVisibility(int i){
    if(i < (int)indexIToLabel.size())
        viewer->setIVisibility(indexIToLabel.at(i), labelICheckButtons[i]->isChecked());
}



void Window::selectIAll(){
    for (int i = 0;i<labelICheckButtons.size();i++){
        if(!labelICheckButtons[i]->isChecked()){
            labelICheckButtons[i]->setChecked(true);
        }
        setIVisibility(i);
    }
}

void Window::discardIAll(){
    for (int i = 0;i<labelICheckButtons.size();i++){
        if(labelICheckButtons[i]->isChecked()){
            labelICheckButtons[i]->setChecked(false);
        }
    }
    viewer->discardIAll();
}

void Window::setSVisibility(int i){
    if(i < (int)indexSToLabel.size())
        viewer->setSVisibility(indexSToLabel.at(i), labelSCheckButtons[i]->isChecked());
}



void Window::selectSAll(){
    for (int i = 0;i<labelSCheckButtons.size();i++){
        if(!labelSCheckButtons[i]->isChecked()){
            labelSCheckButtons[i]->setChecked(true);
        }
        setSVisibility(i);
    }
}

void Window::discardSAll(){
    for (int i = 0;i<labelSCheckButtons.size();i++){
        if(labelSCheckButtons[i]->isChecked()){
            labelSCheckButtons[i]->setChecked(false);
        }
    }
    viewer->discardSAll();
}

void Window::discardAll(){
    for (int i = 0;i<labelCheckButtons.size();i++){
        if(labelCheckButtons[i]->isChecked()){
            labelCheckButtons[i]->setChecked(false);
        }
    }
    viewer->discardAll();
}

void Window::setMode(int mode){
    viewer->setMode(Mode(mode));
    if(mode > 0)
        contents->setCurrentIndex(1);
}
double Window::get_approximate(double d, int precision, int& decimals)
{
    if ( d<0 ) { return 0; }
    
    double i = std::pow(10.,precision-1);
    
    decimals = 0;
    while ( d > i*10 ) { d = d/10.; ++decimals; }
    while ( d < i ) { d = d*10.; --decimals; }
    
    return std::floor(d)*std::pow(10.,decimals);
}



void Window::trMeshGenerationFromMesh(){
    
    double diag = viewer->diagonal_length();
    
    double angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue,
            ratioSpinBoxValue, cellSizeSpinBoxValue ;
    
    if(meshingDialog(diag, angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue ))
        viewer->meshTransfer(angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue, true, smoothTransferMeshCheckBox->isChecked() );
    
}

void Window::transferMeshGeneration(){
    
    double diag = viewer->getSceneRadius();
    
    std::cout << diag << std::endl;
    double angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue,
            ratioSpinBoxValue, cellSizeSpinBoxValue ;
    
    if(meshingDialog(diag, angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue ))
        viewer->meshTransfer(angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue, false, smoothTransferMeshCheckBox->isChecked() );

    //  viewer->meshTransfer(30., 20., 2., 3, 35, false, smoothTransferMeshCheckBox->isChecked() );
}


void Window::meshAdaptiveSize(){
    
    viewer->computeDistortions();
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Motion adaptive parameters"));
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * meshingGroupBox = new QGroupBox("Odt parameters");
    dilogLayout->addWidget(meshingGroupBox);
    
    QVBoxLayout * odtVLayout = new QVBoxLayout(meshingGroupBox);
    
    QFormLayout * itLayout = new QFormLayout();
    
    QDoubleSpinBox * sizeMul = new QDoubleSpinBox(meshingGroupBox);
    sizeMul->setDecimals(2);
    sizeMul->setMaximum(1);
    sizeMul->setValue(0.5);
    sizeMul->setSingleStep(0.01);
    sizeMul->setMinimum(0.01);
    
    itLayout->addRow("Size", sizeMul);
    
    QDoubleSpinBox * percentage = new QDoubleSpinBox();
    percentage->setDecimals(2);
    percentage->setMinimum(0.01);
    percentage->setMaximum(1);
    percentage->setValue(1./5.);
    percentage->setSingleStep(0.01);
    
    itLayout->addRow("Reduce %",percentage);
    
    odtVLayout->addLayout(itLayout);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return;

    viewer->meshTransferAdativeSize(sizeMul->value(), percentage->value(), smoothTransferMeshCheckBox->isChecked());

    //   viewer->meshTransferAdativeSize(0.5, 0.3, smoothTransferMeshCheckBox->isChecked());
}

void Window::CGALMeshing(){
    
    double diag = viewer->diagonal_length();
    
    double angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue,
            ratioSpinBoxValue, cellSizeSpinBoxValue ;
    
    if(meshingDialog(diag, angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue ))
        viewer->mesh3DImage(angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue );
    
    setMeshDisplay();
    meshingPushButton->setEnabled(true);
    drawImageGroupBox->setEnabled(true);
    
    //updateInfo();
}

void Window::cageTetMeshing(){
    
    double diag = viewer->diagonal_length();
    
    double angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue,
            ratioSpinBoxValue, cellSizeSpinBoxValue ;
    
    if(meshingDialog(diag, angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue, ratioSpinBoxValue, cellSizeSpinBoxValue ))
        viewer->generateCageTetMesh(angleSpinBoxValue, sizeSpinBoxValue, approximationSpinBoxValue,
                                    ratioSpinBoxValue, cellSizeSpinBoxValue );
    
}

bool Window::loadFR(){

    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Load Full Res ?"));

    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);

    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));

    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();

    if(i == QDialog::Rejected)
        return false;

    return true;
}

bool Window::meshingDialog(double diag, double &angleSpinBoxValue, double &sizeSpinBoxValue, double &approximationSpinBoxValue,
                           double &ratioSpinBoxValue, double &cellSizeSpinBoxValue ){
    
    int decimals = 0;
    double sizing_default = get_approximate(diag * 0.05, 2, decimals);
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("CGAL Meshing parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    QGroupBox * CGALMeshingGroupBox = new QGroupBox();
    dilogLayout->addWidget(CGALMeshingGroupBox);
    
    CGALMeshingGroupBox->setObjectName("CGALMeshingGroupBox");
    CGALMeshingGroupBox->setTitle("CGAL meshing options");
    
    QVBoxLayout * CGALVLayout = new QVBoxLayout(CGALMeshingGroupBox);
    
    QGroupBox * facetCriterionGroupBox = new QGroupBox();
    facetCriterionGroupBox->setObjectName("facetCriterionGroupBox");
    facetCriterionGroupBox->setTitle("Facet criterion");
    
    QFormLayout * FacetFLayout = new QFormLayout(facetCriterionGroupBox);
    
    QLabel * angleLabel = new QLabel("angle", CGALMeshingGroupBox);
    QDoubleSpinBox * angleSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    double angle = FACET_ANGLE;
    angleSpinBox->setValue(angle);
    angleSpinBox->setMaximum(30);
    
    FacetFLayout->setWidget(0, QFormLayout::LabelRole, angleLabel);
    FacetFLayout->setWidget(0, QFormLayout::FieldRole, angleSpinBox);
    
    QLabel * sizeLabel = new QLabel("size", CGALMeshingGroupBox);
    QDoubleSpinBox * sizeSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    
    sizeSpinBox->setDecimals(-decimals+2);
    sizeSpinBox->setSingleStep(std::pow(10.,decimals));
    sizeSpinBox->setRange(diag * 10e-6, // min
                          diag); // max
    sizeSpinBox->setValue(sizing_default); // default value
    
    FacetFLayout->setWidget(1, QFormLayout::LabelRole, sizeLabel);
    FacetFLayout->setWidget(1, QFormLayout::FieldRole, sizeSpinBox);
    
    QLabel * approximationLabel = new QLabel("approximation", CGALMeshingGroupBox);
    QDoubleSpinBox * approximationSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    
    double approx_default = get_approximate(diag * 0.005, 2, decimals);
    approximationSpinBox->setDecimals(-decimals+2);
    approximationSpinBox->setSingleStep(std::pow(10.,decimals));
    approximationSpinBox->setRange(diag * 10e-7, // min
                                   diag); // max
    approximationSpinBox->setValue(approx_default);
    
    
    FacetFLayout->setWidget(2, QFormLayout::LabelRole, approximationLabel);
    FacetFLayout->setWidget(2, QFormLayout::FieldRole, approximationSpinBox);
    
    facetCriterionGroupBox->setLayout(FacetFLayout);
    
    CGALVLayout->addWidget(facetCriterionGroupBox);
    
    QGroupBox * cellCriterionGroupBox = new QGroupBox();
    cellCriterionGroupBox->setObjectName("cellCriterionGroupBox");
    cellCriterionGroupBox->setTitle("cell criterion");
    
    QFormLayout * cellFLayout = new QFormLayout(cellCriterionGroupBox);
    
    QLabel * ratioLabel = new QLabel("Radius-edge ratio", CGALMeshingGroupBox);
    QDoubleSpinBox * ratioSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    double ratio = CELL_RATIO;
    ratioSpinBox->setValue(ratio);
    
    cellFLayout->setWidget(0, QFormLayout::LabelRole, ratioLabel);
    cellFLayout->setWidget(0, QFormLayout::FieldRole, ratioSpinBox);
    
    QLabel * cellSizeLabel = new QLabel("cellSize", CGALMeshingGroupBox);
    QDoubleSpinBox * cellSizeSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    
    cellSizeSpinBox->setDecimals(-decimals+2);
    cellSizeSpinBox->setSingleStep(std::pow(10.,decimals));
    cellSizeSpinBox->setRange(diag * 10e-6, // min
                              diag); // max
    cellSizeSpinBox->setValue(2*sizing_default); // default value
    
    cellFLayout->setWidget(1, QFormLayout::LabelRole, cellSizeLabel);
    cellFLayout->setWidget(1, QFormLayout::FieldRole, cellSizeSpinBox);
    
    cellCriterionGroupBox->setLayout(cellFLayout);
    
    CGALVLayout->addWidget(cellCriterionGroupBox);
    
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return false;
    
    angleSpinBoxValue = angleSpinBox->value();
    sizeSpinBoxValue = sizeSpinBox->value();
    approximationSpinBoxValue = approximationSpinBox->value();
    ratioSpinBoxValue = ratioSpinBox->value();
    cellSizeSpinBoxValue = cellSizeSpinBox->value();
    
    return true;
}

void Window::pointDistribution(){
    
    viewer->generatePoissonSampling(pointNbSpinBox->value(), radiusDoubleSpinBox->value());
}

void Window::useAsMesh(){
    viewer->useTransferAsMesh();
    setMeshDisplay();
}

bool Window::exude( double & sliverBoundValue, double & maxTimeValue ){
    
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Exudation parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * exudationGroupBox = new QGroupBox("Exudation parameters");
    dilogLayout->addWidget(exudationGroupBox);
    
    QFormLayout * itLayout = new QFormLayout(exudationGroupBox);
    
    QDoubleSpinBox * sliverBound = new QDoubleSpinBox();
    sliverBound->setDecimals(1);
    sliverBound->setMaximum(180);
    sliverBound->setValue(8);
    itLayout->addRow("Sliver Bound", sliverBound);
    
    
    QDoubleSpinBox * maxTime = new QDoubleSpinBox(exudationGroupBox);
    maxTime->setDecimals(1);
    maxTime->setMaximum(9999);
    maxTime->setValue(60);
    
    itLayout->addRow("Time Limit", maxTime);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return false;
    
    sliverBoundValue = sliverBound->value();
    maxTimeValue = maxTime->value();
    
    return true;
}


void Window::exude_image_mesh(){
    
    double sliverBound, maxTime;
    
    if( exude( sliverBound, maxTime) ){
        viewer->exude( sliverBound, maxTime );
        //updateInfo();
    }
}

void Window::exude_transfer_mesh(){
    
    double sliverBound, maxTime;
    //
    //    if( exude( sliverBound, maxTime) ){
    //        viewer->exude_transfer_mesh( sliverBound, maxTime );
    //        //updateInfo();
    //    }
}

bool Window::perturb( double & maxTimeValue, double & sliverBoundValue ){
    
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Perturbation parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * exudationGroupBox = new QGroupBox("Exudation parameters");
    dilogLayout->addWidget(exudationGroupBox);
    
    QFormLayout * itLayout = new QFormLayout(exudationGroupBox);
    
    QDoubleSpinBox * sliverBound = new QDoubleSpinBox();
    sliverBound->setDecimals(1);
    sliverBound->setMaximum(180);
    sliverBound->setValue(8);
    itLayout->addRow("Sliver Bound", sliverBound);
    
    
    QDoubleSpinBox * maxTime = new QDoubleSpinBox(exudationGroupBox);
    maxTime->setDecimals(1);
    maxTime->setMaximum(9999);
    maxTime->setValue(60);
    
    itLayout->addRow("Time Limit", maxTime);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return false;
    
    maxTimeValue = maxTime->value();
    sliverBoundValue = sliverBound->value();
    
    return true;
}

void Window::perturb_image_mesh(){
    
    double sliverBound, maxTime;
    
    if( perturb( maxTime, sliverBound ) ){
        viewer->perturb(maxTime, sliverBound);
        //updateInfo();
    }
}

void Window::perturb_transfer_mesh(){
    
    double sliverBound, maxTime;
    
    if( perturb( maxTime, sliverBound) ){
        viewer->perturb_transfer_mesh(maxTime, sliverBound);
        //updateInfo();
    }
}


bool Window::lloydSmoothing( double &maxTimeValue, double &maxIterationValue, double &convergenceRatioValue, double &freezeRatioValue){
    
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Lloyd-smoothing parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * lloydGroupBox = new QGroupBox("Lloyd parameters");
    dilogLayout->addWidget(lloydGroupBox);
    
    QVBoxLayout * lloydVLayout = new QVBoxLayout(lloydGroupBox);
    
    QFormLayout * itLayout = new QFormLayout();
    
    QDoubleSpinBox * maxTime = new QDoubleSpinBox(lloydGroupBox);
    maxTime->setDecimals(1);
    maxTime->setMaximum(9999);
    maxTime->setValue(60);
    
    itLayout->addRow("Time Limit", maxTime);
    
    
    QSpinBox * maxIterationNb = new QSpinBox();
    maxIterationNb->setMaximum(200);
    maxIterationNb->setValue(100);
    
    itLayout->addRow("Max iteration", maxIterationNb );
    
    QDoubleSpinBox * convergenceRatio = new QDoubleSpinBox();
    convergenceRatio->setDecimals(4);
    convergenceRatio->setMinimum(0.0001);
    convergenceRatio->setMaximum(1);
    convergenceRatio->setSingleStep(0.01);
    
    itLayout->addRow("Convergence ratio",convergenceRatio);
    
    QDoubleSpinBox * freezeRatio = new QDoubleSpinBox();
    freezeRatio->setDecimals(4);
    freezeRatio->setMinimum(0);
    freezeRatio->setMaximum(1);
    freezeRatio->setSingleStep(0.001);
    freezeRatio->setValue(0);
    
    itLayout->addRow("Freeze ratio", freezeRatio);
    
    namespace cgpd = CGAL::parameters::default_values;
    convergenceRatio->setValue(cgpd::lloyd_convergence_ratio);
    freezeRatio->setValue(cgpd::lloyd_freeze_ratio);
    
    lloydVLayout->addLayout(itLayout);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return false;
    
    maxTimeValue = maxTime->value();
    maxIterationValue = maxIterationNb->value() ;
    convergenceRatioValue = convergenceRatio->value();
    freezeRatioValue = freezeRatio->value();
    
    return true;
}

void Window::lloydSmoothing_image_mesh(){
    
    
    double maxTimeValue;
    double maxIterationValue;
    double convergenceRatioValue;
    double freezeRatioValue;
    if(lloydSmoothing(maxTimeValue, maxIterationValue, convergenceRatioValue, freezeRatioValue)){
        viewer->lloyd_optimize_mesh_3(maxTimeValue, maxIterationValue , convergenceRatioValue, freezeRatioValue);
        //updateInfo();
    }
}

void Window::lloydSmoothing_transfer_mesh(){
    
    double maxTimeValue;
    double maxIterationValue;
    double convergenceRatioValue;
    double freezeRatioValue;
    if(lloydSmoothing(maxTimeValue, maxIterationValue, convergenceRatioValue, freezeRatioValue)){
        viewer->lloyd_optimize_transfer_mesh(maxTimeValue, maxIterationValue , convergenceRatioValue, freezeRatioValue);
        //updateInfo();
    }
    
}

bool Window::odtSmoothing( double &maxTimeValue, double &maxIterationValue, double &convergenceRatioValue, double &freezeRatioValue){
    
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Odt-smoothing parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * odtGroupBox = new QGroupBox("Odt parameters");
    dilogLayout->addWidget(odtGroupBox);
    
    QVBoxLayout * odtVLayout = new QVBoxLayout(odtGroupBox);
    
    QFormLayout * itLayout = new QFormLayout();
    
    QDoubleSpinBox * maxTime = new QDoubleSpinBox(odtGroupBox);
    maxTime->setDecimals(1);
    maxTime->setMaximum(9999);
    maxTime->setValue(60);
    
    itLayout->addRow("Time Limit", maxTime);
    
    QSpinBox * maxIterationNb = new QSpinBox();
    maxIterationNb->setMaximum(200);
    maxIterationNb->setValue(100);
    
    itLayout->addRow("Max iteration", maxIterationNb );
    
    QDoubleSpinBox * convergenceRatio = new QDoubleSpinBox();
    convergenceRatio->setDecimals(4);
    convergenceRatio->setMinimum(0.0001);
    convergenceRatio->setMaximum(1);
    convergenceRatio->setSingleStep(0.01);
    
    itLayout->addRow("Convergence ratio",convergenceRatio);
    
    QDoubleSpinBox * freezeRatio = new QDoubleSpinBox();
    freezeRatio->setDecimals(4);
    freezeRatio->setMinimum(0);
    freezeRatio->setMaximum(1);
    freezeRatio->setSingleStep(0.001);
    freezeRatio->setValue(0);
    
    itLayout->addRow("Freeze ratio", freezeRatio);
    
    
    namespace cgpd = CGAL::parameters::default_values;
    convergenceRatio->setValue(cgpd::odt_convergence_ratio);
    freezeRatio->setValue(cgpd::odt_freeze_ratio);
    
    odtVLayout->addLayout(itLayout);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return false;
    
    maxTimeValue = maxTime->value();
    maxIterationValue = maxIterationNb->value() ;
    convergenceRatioValue = convergenceRatio->value();
    freezeRatioValue = freezeRatio->value();
    
    return true;
}

void Window::odtSmoothing_image_mesh(){
    
    double maxTimeValue;
    double maxIterationValue;
    double convergenceRatioValue;
    double freezeRatioValue;
    if(lloydSmoothing(maxTimeValue, maxIterationValue, convergenceRatioValue, freezeRatioValue)){
        //double time_limit, double max_iteration_number, double convergence, double freeze_bound
        viewer->odt_optimize_mesh_3(maxTimeValue, maxIterationValue , convergenceRatioValue, freezeRatioValue);
        //updateInfo();
    }
}

void Window::odtSmoothing_transfer_mesh(){
    
    double maxTimeValue;
    double maxIterationValue;
    double convergenceRatioValue;
    double freezeRatioValue;
    if(lloydSmoothing(maxTimeValue, maxIterationValue, convergenceRatioValue, freezeRatioValue)){
        viewer->odt_optimize_transfer_mesh(maxTimeValue, maxIterationValue , convergenceRatioValue, freezeRatioValue);
        //updateInfo();
    }
}

void Window::setLabelToZero(){
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Change label value"));
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * labelsGroupBox = new QGroupBox("Change label value");
    dilogLayout->addWidget(labelsGroupBox);
    
    QVBoxLayout * labelsVLayout = new QVBoxLayout(labelsGroupBox);
    
    QComboBox * l1ComboxBox = new QComboBox();
    QComboBox * l2ComboxBox = new QComboBox();
    for( unsigned int i = 0 ; i < indexToLabel.size() ; i++ ){
        l1ComboxBox->addItem(QString::number(indexToLabel[i]));
        l2ComboxBox->addItem(QString::number(indexToLabel[i]));
    }
    labelsVLayout->addWidget(new QLabel("From"));
    labelsVLayout->addWidget(l1ComboxBox);
    
    labelsVLayout->addWidget(new QLabel("To"));
    labelsVLayout->addWidget(l2ComboxBox);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected || l1ComboxBox->count() == 0 || l2ComboxBox->count() == 0)
        return;
    
    int l1 = indexToLabel[l1ComboxBox->currentIndex()];
    int l2 = indexToLabel[l2ComboxBox->currentIndex()];
    if(l1!=l2)
        viewer->changeLabelValue( l1,l2);
    
}

void Window::separateHands(){
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Label separation"));
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * labelsGroupBox = new QGroupBox("Label separation");
    dilogLayout->addWidget(labelsGroupBox);
    
    QVBoxLayout * labelsVLayout = new QVBoxLayout(labelsGroupBox);
    
    QComboBox * l1ComboxBox = new QComboBox();
    QComboBox * l2ComboxBox = new QComboBox();
    for( unsigned int i = 0 ; i < indexToLabel.size() ; i++ ){
        l1ComboxBox->addItem(QString::number(indexToLabel[i]));
        l2ComboxBox->addItem(QString::number(indexToLabel[i]));
    }
    
    labelsVLayout->addWidget(l1ComboxBox);
    labelsVLayout->addWidget(l2ComboxBox);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected || l1ComboxBox->count() == 0 || l2ComboxBox->count() == 0)
        return;
    
    int l1 = indexToLabel[l1ComboxBox->currentIndex()];
    int l2 = indexToLabel[l2ComboxBox->currentIndex()];
    if(l1!=l2)
        viewer->separateLabels( l1,l2);
    
}

void Window::segmentation(){
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Mesh segmentation"));
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    
    QGroupBox * labelsGroupBox = new QGroupBox("Mesh segmentation");
    dilogLayout->addWidget(labelsGroupBox);
    
    QVBoxLayout * labelsVLayout = new QVBoxLayout(labelsGroupBox);
    
    int labelNb = labelNbSpinBox->value();
    std::vector<QComboBox *> lComboBoxes(labelNb);
    for( int i = 0 ; i < labelNb ; i ++ ){
        lComboBoxes[i] = new QComboBox();
        
        for( unsigned int j = 0 ; j < indexToLabel.size() ; j++ )
            lComboBoxes[i]->addItem(QString::number(indexToLabel[j]));
        
        labelsVLayout->addWidget(lComboBoxes[i]);
        
    }
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected )
        return;
    
    std::vector<int> labels(labelNb);
    for( int i = 0 ; i < labelNb ; i ++ ){
        labels[i] = indexToLabel[lComboBoxes[i]->currentIndex()];
    }
    
    viewer->segmentMesh(labels);
    
}

void Window::saveSizingFieldAsGrid(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save sizing field info as ", "./", "Nifti (*.nii);;VTK (*.vtk)");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    
    if(fileName.endsWith(".vtk"))
        viewer->export_sizing_field_vtk(fileName.toStdString());
    else {
        if(!fileName.endsWith(".nii"))
            fileName.append(".nii");
        viewer->saveSizingFieldAsGrid(fileName);
    }
}

void Window::saveGridInfo(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save grid info as ", "./");
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    viewer->saveGridInfo(fileName.toStdString());
}


void Window::saveGrid(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save grid as ", "./", "(*.dim *.nii *.vtk);;IMA (*.dim);;NIFTI (*.nii);;VTK (*.vtk)" );
    
    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }
    
    viewer->saveGrid(fileName);
}

void Window::saveSegmentation(){
    std::cout << "save seg" << std::endl;
    QString fileName = QFileDialog::getSaveFileName(this, "Save segmentation as ", "./", "(*.dim *.nii *.vtk);;IMA (*.dim);;NIFTI (*.nii);;VTK (*.vtk)" );

    // In case of Cancel
    if ( fileName.isEmpty() ) {
        return;
    }

    viewer->saveSegmentation(fileName);
}

void Window::set3DImageDisplay(){
    contents->setCurrentIndex(MESHING);
    meshingPushButton->setEnabled(true);
    modeComboBox->setCurrentIndex(MESHING);
    setDisplayImage(true);
    //computeCoordinates->setEnabled(false);
}

void Window::setMeshDisplay(){
    contents->setCurrentIndex(MESH_DEFORMATION);
    meshingPushButton->setEnabled(false);
    modeComboBox->setCurrentIndex(MESH_DEFORMATION);
    setDisplayMesh(true);
    //computeCoordinates->setEnabled(false);
}

void Window::updateGridOffset(){
    viewer->setGridOffset(offsetSpinBox->value(), useSegmentationCheckBox->checkState());
    
}

void Window::recomputeTransferMesh(){
    viewer->recomputeTransferMesh(fitToGridCheckBox->isChecked());
}

void Window::generateCage(){
    double diag = viewer->diagonal_length();
    int decimals = 0;
    double sizing_default = get_approximate(diag * 0.05, 2, decimals);
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Cage creation parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    QGroupBox * CGALMeshingGroupBox = new QGroupBox("Cage creation options");
    dilogLayout->addWidget(CGALMeshingGroupBox);
    
    QVBoxLayout * CGALVLayout = new QVBoxLayout(CGALMeshingGroupBox);
    
    QGroupBox * facetCriterionGroupBox = new QGroupBox();
    facetCriterionGroupBox->setObjectName("facetCriterionGroupBox");
    facetCriterionGroupBox->setTitle("Facet criterion");
    
    QFormLayout * FacetFLayout = new QFormLayout(facetCriterionGroupBox);
    
    QLabel * angleLabel = new QLabel("angle", CGALMeshingGroupBox);
    QDoubleSpinBox * angleSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    double angle = FACET_ANGLE;
    angleSpinBox->setValue(angle);
    angleSpinBox->setMaximum(30);
    
    FacetFLayout->setWidget(0, QFormLayout::LabelRole, angleLabel);
    FacetFLayout->setWidget(0, QFormLayout::FieldRole, angleSpinBox);
    
    QLabel * sizeLabel = new QLabel("size", CGALMeshingGroupBox);
    QDoubleSpinBox * sizeSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    
    sizeSpinBox->setDecimals(-decimals+2);
    sizeSpinBox->setSingleStep(std::pow(10.,decimals));
    sizeSpinBox->setRange(diag * 10e-6, // min
                          diag); // max
    sizeSpinBox->setValue(sizing_default); // default value
    
    FacetFLayout->setWidget(1, QFormLayout::LabelRole, sizeLabel);
    FacetFLayout->setWidget(1, QFormLayout::FieldRole, sizeSpinBox);
    
    QLabel * approximationLabel = new QLabel("approximation", CGALMeshingGroupBox);
    QDoubleSpinBox * approximationSpinBox = new QDoubleSpinBox(CGALMeshingGroupBox);
    
    double approx_default = get_approximate(diag * 0.005, 2, decimals);
    approximationSpinBox->setDecimals(-decimals+2);
    approximationSpinBox->setSingleStep(std::pow(10.,decimals));
    approximationSpinBox->setRange(diag * 10e-7, // min
                                   diag); // max
    approximationSpinBox->setValue(approx_default);
    
    
    FacetFLayout->setWidget(2, QFormLayout::LabelRole, approximationLabel);
    FacetFLayout->setWidget(2, QFormLayout::FieldRole, approximationSpinBox);
    
    facetCriterionGroupBox->setLayout(FacetFLayout);
    
    CGALVLayout->addWidget(facetCriterionGroupBox);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return;
    
    viewer->generateSurface(angleSpinBox->value() , sizeSpinBox->value(), approximationSpinBox->value());
    
}

void Window::simplifyCage(){
    
    int edgeCount = viewer->getSurfaceHalfEdgeCount();
    
    if(edgeCount == 0)
        return;
    
    QDialog * dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Cage simplification parameters"));
    
    
    QVBoxLayout * dilogLayout = new QVBoxLayout(dialog);
    QGroupBox * simplificationCriterionGroupBox = new QGroupBox("Cage simplification options");
    dilogLayout->addWidget(simplificationCriterionGroupBox);
    
    QFormLayout * simplificationFLayout = new QFormLayout(simplificationCriterionGroupBox);
    
    QLabel * label = new QLabel("Half edge count", simplificationCriterionGroupBox);
    QSpinBox * edgeCountSpinBox = new QSpinBox(simplificationCriterionGroupBox);
    edgeCountSpinBox->setRange(0, edgeCount);
    edgeCountSpinBox->setValue(edgeCount);
    
    simplificationFLayout->addRow(label, edgeCountSpinBox);
    
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
    
    QObject::connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
    QObject::connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
    
    dilogLayout->addWidget(buttonBox);
    int i = dialog->exec();
    
    if(i == QDialog::Rejected)
        return;
    
    viewer->simplifySurface(edgeCountSpinBox->value());
}
