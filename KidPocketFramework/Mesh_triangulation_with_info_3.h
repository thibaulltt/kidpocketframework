// Copyright (c) 2006-2009 INRIA Sophia-Antipolis (France).
// All rights reserved.
//
// This file is part of CGAL (www.cgal.org); you may redistribute it under
// the terms of the Q Public License version 1.0.
// See the file LICENSE.QPL distributed with CGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $URL: svn+ssh://scm.gforge.inria.fr/svn/cgal/branches/CGAL-3.5-branch/Mesh_3/include/CGAL/Mesh_triangulation_3.h $
// $Id: Mesh_triangulation_3.h 51094 2009-08-06 13:11:07Z stayeb $
//
//
// Author(s)     : Laurent Rineau, Stephane Tayeb


#ifndef CGAL_MESH_TRIANGULATION_WITH_INFO_3_H
#define CGAL_MESH_TRIANGULATION_WITH_INFO_3_H

#include <CGAL/Kernel_traits.h>

#include <CGAL/Regular_triangulation_3.h>
#include <CGAL/Regular_triangulation_euclidean_traits_3.h>

#include "Mesh_vertex_base_with_info_3.h"
#include "Mesh_cell_base_with_info_3.h"

#include <CGAL/Mesh_triangulation_3.h>

namespace CGAL {


// Struct Mesh_triangulation_3
//
template<class MD, typename VI, typename CI, class K=typename Kernel_traits<MD>::Kernel>
struct Mesh_triangulation_with_info_3
{
private:
  typedef typename details::Mesh_geom_traits_generator<K>::type Geom_traits;
  typedef Mesh_vertex_base_with_info_3<VI,Geom_traits, MD>                   Vertex_base;
  typedef Mesh_cell_base_with_info_3<CI, Geom_traits, MD>                     Cell_base;
  typedef Triangulation_data_structure_3<Vertex_base,Cell_base> Tds;
  typedef Regular_triangulation_3<Geom_traits, Tds>             Triangulation;

public:
  typedef Triangulation type;
  typedef type Type;
};  // end struct Mesh_triangulation_3


}  // end namespace CGAL

#endif // CGAL_MESH_TRIANGULATION_WITH_INFO_3_H

