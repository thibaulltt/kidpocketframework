#ifndef CAGECREATOR_H
#define CAGECREATOR_H

#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Gray_level_image_3.h>
#include "BasicPoint.h"
#include "Triangle.h"

// default triangulation for Surface_mesher
typedef CGAL::Surface_mesh_default_triangulation_3 Surface_Tr;

typedef Surface_Tr::Geom_traits GT;
typedef CGAL::Gray_level_image_3<GT::FT, GT::Point_3> Gray_level_image;

class CageCreator
{
public:
    CageCreator( const CGAL::Image_3 & image , int greyLevel );

    std::vector<BasicPoint> vertices;
    std::vector<Triangle> triangles;
};

#endif // CAGECREATOR_H
