#include "Histogram.h"
#include <QPainter>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include "limits.h"
#include "float.h"

Histogram::Histogram()
{
}

void Histogram::build_histogram( std::vector<float> & histo_data )
{
    // Create an histogram_ and display it
    const int height = 200;
    const int top_margin = 5;
    const int left_margin = 20;
    const int width = 402;
    const int cell_width = 360./histo_data.size();
    const int text_margin = 3;
    const int text_height = 20;

    int drawing_size = height-top_margin*2;

    histogram_ = QPixmap(width,height+text_height);
    histogram_.fill(QColor(192,192,192));

    QPainter painter(&histogram_);
    painter.setPen(Qt::black);
    painter.setBrush(QColor(128,128,128));
    //painter.setFont(QFont("Arial", 30));

    // Get maximum value (to normalize)
    float max_perc = -FLT_MAX;
    float min_perc = FLT_MAX;
    for ( std::vector<float>::iterator it = histo_data.begin(), end = histo_data.end() ;
    it != end ; ++it )
    {
        max_perc = (std::max)(max_perc,*it);
        min_perc = (std::min)(min_perc,*it);
    }


    float diff = std::max(max_perc, (float)0.) + fabs(std::min(min_perc, (float)0.)) ;

    int drawing_height = height*(std::max(max_perc/(diff), (float)0.))+top_margin*2;



    // colored histogram
    int j = 1;

    // draw
    int i=left_margin;
    for ( std::vector<float>::iterator it = histo_data.begin(), end = histo_data.end() ;
    it != end ; ++it, i+=cell_width )
    {
        int line_height = static_cast<int>(ceil((static_cast<double>(drawing_size)) * (*it)/diff) + .5);

        painter.fillRect(i,
                         drawing_height+top_margin-std::max(line_height, 0),
                         cell_width,
                         abs(line_height),
                         get_label_color(j++));
    }

    // draw bottom horizontal line
    painter.setPen(Qt::blue);

    painter.drawLine(QPoint(left_margin, drawing_height + top_margin),
                     QPoint(left_margin + static_cast<int>(histo_data.size())*cell_width,
                            drawing_height + top_margin));


}


void Histogram::build_histogram( std::vector<int> & histo_data , double min_value, double max_value )
{
    // Create an histogram_ and display it
    const int height = 160;
    const int top_margin = 5;
    const int left_margin = 20;
    const int drawing_height = height-top_margin*2;
    const int width = 402;
    const int cell_width = (width - 2*left_margin)/histo_data.size();
    const int text_margin = 3;
    const int text_height = 20;

    histogram_ = QPixmap(width,height+text_height);
    histogram_.fill(QColor(192,192,192));

    QPainter painter(&histogram_);
    painter.setPen(Qt::black);
    painter.setBrush(QColor(128,128,128));
    //painter.setFont(QFont("Arial", 30));

    // Get maximum value (to normalize)
    double interval = fabs(max_value - min_value);
    double step = interval/histo_data.size();

    int max_size = 0;
    int m_id = 0;
    int count = 0;
    for ( std::vector<int>::iterator it = histo_data.begin(), end = histo_data.end() ;
    it != end ; ++it, ++count)
    {
        max_size = (std::max)(max_size,*it);
        if(max_size == *it) m_id = count;
    }

    // colored histogram
    int j = 0;

    // draw
    int i=left_margin;
    for ( std::vector<int>::iterator it = histo_data.begin(), end = histo_data.end() ;
    it != end ; ++it, i+=cell_width )
    {
        int line_height = static_cast<int>( round(static_cast<double>(drawing_height) *
                                                      static_cast<double>(*it)/static_cast<double>(max_size)));

        painter.fillRect(i,
                         drawing_height+top_margin-line_height,
                         cell_width,
                         line_height,
                         get_histogram_color(j++));
    }

    // draw bottom horizontal line
    painter.setPen(Qt::blue);

    painter.drawLine(QPoint(left_margin, drawing_height + top_margin),
                     QPoint(left_margin + static_cast<int>(histo_data.size())*cell_width,
                            drawing_height + top_margin));


    // draw min value and max value

    const double max_nb_value = min_value + m_id*step;


    const int tr_width = 20;
    const int min_tr_pos = static_cast<int>( left_margin );
    const int max_tr_pos = static_cast<int>(
           histo_data.size()*cell_width + left_margin );
    const int max_nb_pos = static_cast<int>(
            m_id*cell_width + left_margin );
    const int tr_y = drawing_height + top_margin + text_margin;

    painter.setPen(get_histogram_color(min_value));
    QRect min_text_rect (min_tr_pos - tr_width/2., tr_y, tr_width, text_height);
    QString text;
    text.setNum(min_value,'f',1);
    painter.drawText(min_text_rect, Qt::AlignCenter, text);

    painter.setPen(get_histogram_color(max_value));
    QRect max_text_rect (max_tr_pos - tr_width/2., tr_y, tr_width, text_height);
    text.setNum(max_value,'f',1);
    painter.drawText(max_text_rect, Qt::AlignCenter, text);

    painter.setPen(get_histogram_color(max_nb_value));
    QRect max_nb_rect (max_nb_pos- tr_width/2., tr_y, tr_width, text_height);
    text.setNum(max_nb_value,'f',1);
    painter.drawText(max_nb_rect, Qt::AlignCenter, text);
}

void Histogram::build_histogram( std::vector<int> & histo_data , double min_value, double max_value, int m_id, int max_size )
{
    // Create an histogram_ and display it
    const int height = 160;
    const int top_margin = 5;
    const int left_margin = 20;
    const int drawing_height = height-top_margin*2;
    const int width = 402;
    const int cell_width = (width - 2*left_margin)/histo_data.size();
    const int text_margin = 3;
    const int text_height = 20;

    histogram_ = QPixmap(width,height+text_height);
    histogram_.fill(QColor(192,192,192));

    QPainter painter(&histogram_);
    painter.setPen(Qt::black);
    painter.setBrush(QColor(128,128,128));
    //painter.setFont(QFont("Arial", 30));

    // Get maximum value (to normalize)
    double interval = fabs(max_value - min_value);
    double step = interval/histo_data.size();

    // colored histogram
    int j = 0;

    // draw
    int i=left_margin;
    for ( std::vector<int>::iterator it = histo_data.begin(), end = histo_data.end() ;
    it != end ; ++it, i+=cell_width )
    {
        int line_height = static_cast<int>( round(static_cast<double>(drawing_height) *
                                                      static_cast<double>(*it)/static_cast<double>(max_size)));

        painter.fillRect(i,
                         drawing_height+top_margin-line_height,
                         cell_width,
                         line_height,
                         get_histogram_color(j++));
    }

    // draw bottom horizontal line
    painter.setPen(Qt::blue);

    painter.drawLine(QPoint(left_margin, drawing_height + top_margin),
                     QPoint(left_margin + static_cast<int>(histo_data.size())*cell_width,
                            drawing_height + top_margin));


    // draw min value and max value

    const double max_nb_value = min_value + m_id*step;


    const int tr_width = 20;
    const int min_tr_pos = static_cast<int>( left_margin );
    const int max_tr_pos = static_cast<int>(
           histo_data.size()*cell_width + left_margin );
    const int max_nb_pos = static_cast<int>(
            m_id*cell_width + left_margin );
    const int tr_y = drawing_height + top_margin + text_margin;

    painter.setPen(get_histogram_color(min_value));
    QRect min_text_rect (min_tr_pos - tr_width/2., tr_y, tr_width, text_height);
    QString text;
    text.setNum(min_value,'f',1);
    painter.drawText(min_text_rect, Qt::AlignCenter, text);

    painter.setPen(get_histogram_color(max_value));
    QRect max_text_rect (max_tr_pos - tr_width/2., tr_y, tr_width, text_height);
    text.setNum(max_value,'f',1);
    painter.drawText(max_text_rect, Qt::AlignCenter, text);

    painter.setPen(get_histogram_color(max_nb_value));
    QRect max_nb_rect (max_nb_pos- tr_width/2., tr_y, tr_width, text_height);
    text.setNum(max_nb_value,'f',1);
    painter.drawText(max_nb_rect, Qt::AlignCenter, text);
}

QColor Histogram::get_label_color(const int v) const
{
    if(v>= colors.size()){ return QColor(0,0,0); }
    if(colors.size()>0)     { return colors[v];}

}

QColor Histogram::get_histogram_color(const double v) const
{
    return QColor(36,133,252);
    if ( v < 5 )       { return Qt::red; }
    else if ( v < 10 )      { return QColor(215,108,0); }
    else if ( v < 15 )      { return QColor(138,139,0); }
    else if ( v < 165 )     { return Qt::darkGreen; }
    else if ( v < 170 )     { return QColor(138,139,1); }
    else if ( v < 175 )     { return QColor(215,108,0); }
    else /* 175<v<=180 */   { return Qt::red; }
}
