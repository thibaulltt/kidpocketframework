#ifndef CELLINFO_H
#define CELLINFO_H
#include "BasicPoint.h"

class CellInfo
{
public:
    CellInfo():_index(0), _in_complex(false), _distortion_laplacian(0.) {
       for(int i = 0 ; i < 3; i++ )
        _basis_def.push_back(BasicPoint(0.,0.,0.));
    }
    ~CellInfo(){}

    void set_index(int i){ assert(i >= 0); _index = i; }
    int index(){return _index; }

    void set_in_complex(bool in_c){ _in_complex = in_c; }
    bool in_complex(){ return _in_complex; }

    void set_is_cage_inlier(bool in_c){ _cage_inlier = in_c; }
    bool cage_inlier(){ return _cage_inlier; }

    void set_basis_def(const std::vector<BasicPoint> & _b_def ){ assert(_b_def.size() == 3); _basis_def = _b_def; }
    std::vector<BasicPoint> & basis_def(){ return _basis_def; }
    const std::vector<BasicPoint> & basis_def ()const{ return _basis_def; }

    void set_distortion_laplacian( float _d ){ _distortion_laplacian = _d; }
    float distortion_laplacian(){ return _distortion_laplacian; }

protected:
    int _index;
    bool _in_complex;
    bool _cage_inlier;
    std::vector<BasicPoint> _basis_def;
    float _distortion_laplacian;
};

#endif // CELLINFO_H

