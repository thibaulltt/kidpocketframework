#include <iostream>
#include <fstream>

#include "Texture.h"

#include <algorithm>

Texture::Texture()
{
    init();
    initTexture();

}

Texture::~Texture(){
    if(textureCreated)
        deleteTexture();
}

void Texture::init(){

    /***********************************************************************/
    //Default values
    /***********************************************************************/
    //x, y, z cutting plane for the 3D texture
    xCutPosition = 1.;
    yCutPosition = 1.;
    zCutPosition = 1.;

    //x, y, z cut direction
    xCutDirection = 1.;
    yCutDirection = 1.;
    zCutDirection = 1.;

    xCutDisplay = false;
    yCutDisplay = false;
    zCutDisplay = false;

    //Set texture to cube of size 1.
    xMax = 1.;
    yMax = 1.;
    zMax = 1.;

    gridSize = 0;

    d[0] = 0;
    d[1] = 0;
    d[2] = 0;

    n[0] = 0;
    n[1] = 0;
    n[2] = 0;

    textureCreated = false;

}

void Texture::initTexture(){
    /***********************************************************************/

    glGenTextures(1, &textureId);

    glBindTexture(GL_TEXTURE_3D, textureId);

    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    // glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


void Texture::deleteTexture(){
    glDeleteTextures( 1, &textureId);
}

void Texture::draw(){

    if(!textureCreated)
        return;


    shaderPool = ShaderPool::GetInstance();
    shaderPackage = shaderPool->GetVolumePackage();
    programObject = shaderPackage->GetProgram();

    glDisable(GL_DEPTH);
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_3D);
    // glEnable(GL_BLEND);

    glPolygonMode( GL_FRONT_AND_BACK , GL_FILL );

    //GPU start
    programObject->Begin();

    /***********************************************************************/
    //Parameters given to the shader
    /***********************************************************************/
    programObject->SetUniform1f("xCutPosition", xCutPosition);
    programObject->SetUniform1f("yCutPosition", yCutPosition);
    programObject->SetUniform1f("zCutPosition", zCutPosition);

    programObject->SetUniform1i("xCutDirection", xCutDirection);
    programObject->SetUniform1i("yCutDirection", yCutDirection);
    programObject->SetUniform1i("zCutDirection", zCutDirection);

    programObject->SetUniform1f("xMax", xMax);
    programObject->SetUniform1f("yMax", yMax);
    programObject->SetUniform1f("zMax", zMax);
    /***********************************************************************/

    int iTextureSlot = 0;
    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_3D, textureId);

    programObject->SetUniform1i("Mask", iTextureSlot++);

    //Drawing the cube on which the texture is mapped
    drawCube();

    //Draw quads to fill the cuts
    fillCut();

    programObject->End();
    //GPU end

    glBindTexture(GL_TEXTURE_3D, 0);
    glDisable(GL_TEXTURE_3D);

    glBindTexture(GL_TEXTURE_2D, 0);

    drawBoundingBox();

    drawCutPlanes();

    glEnable(GL_DEPTH);
    glEnable(GL_LIGHTING);

    //  glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_3D);


    for ( int i=0; i<iTextureSlot; ++i ) {
        glActiveTexture( GL_TEXTURE0 + i );
        glDisable( GL_TEXTURE_3D );
    }
    glActiveTexture( GL_TEXTURE0 );


}

void Texture::fillCut(){

    glBegin(GL_QUADS);
    // Right face
    glVertex3f( xCutPosition, 0.0f, 0.0f);	// Bottom Right Of The Texture and Quad
    glVertex3f( xCutPosition, yMax, 0.0f);	// Top Right Of The Texture and Quad
    glVertex3f( xCutPosition, yMax, zMax);	// Top Left Of The Texture and Quad
    glVertex3f( xCutPosition, 0.0f, zMax);	// Bottom Left Of The Texture and Quad

    // Front Face
    glVertex3f(0.0f, 0.0f, zCutPosition);	// Bottom Left Of The Texture and Quad
    glVertex3f(xMax, 0.0f, zCutPosition);	// Bottom Right Of The Texture and Quad
    glVertex3f(xMax, yMax, zCutPosition);	// Top Right Of The Texture and Quad
    glVertex3f(0.0f, yMax, zCutPosition);	// Top Left Of The Texture and Quad

    // Top Face
    glVertex3f(0.0f, yCutPosition, 0.0f);	// Top Left Of The Texture and Quad
    glVertex3f(0.0f, yCutPosition, zMax);	// Bottom Left Of The Texture and Quad
    glVertex3f(xMax, yCutPosition, zMax);	// Bottom Right Of The Texture and Quad
    glVertex3f(xMax, yCutPosition, 0.0f);	// Top Right Of The Texture and Quad

    glEnd();
}

void Texture::drawCube(){
    glBegin(GL_QUADS);

    glVertex3f(0.0f, 0.0f, 0.0f);	// Bottom Right Of The Texture and Quad
    glVertex3f(0.0f, yMax, 0.0f);	// Top Right Of The Texture and Quad
    glVertex3f(xMax, yMax, 0.0f);	// Top Left Of The Texture and Quad
    glVertex3f(xMax, 0.0f, 0.0f);	// Bottom Left Of The Texture and Quad
    // Bottom Face
    glVertex3f(0.0f, 0.0f, 0.0f);	// Top Right Of The Texture and Quad
    glVertex3f(xMax, 0.0f, 0.0f);	// Top Left Of The Texture and Quad
    glVertex3f(xMax, 0.0f, zMax);	// Bottom Left Of The Texture and Quad
    glVertex3f(0.0f, 0.0f, zMax);	// Bottom Right Of The Texture and Quad
    // Left Face
    glVertex3f(0.0f, 0.0f, 0.0f);	// Bottom Left Of The Texture and Quad
    glVertex3f(0.0f, 0.0f, zMax);	// Bottom Right Of The Texture and Quad
    glVertex3f(0.0f, yMax, zMax);	// Top Right Of The Texture and Quad
    glVertex3f(0.0f, yMax, 0.0f);	// Top Left Of The Texture and Quad
    // Right face
    glVertex3f(xMax, 0.0f, 0.0f);	// Bottom Right Of The Texture and Quad
    glVertex3f(xMax, yMax, 0.0f);	// Top Right Of The Texture and Quad
    glVertex3f(xMax, yMax, zMax);	// Top Left Of The Texture and Quad
    glVertex3f(xMax, 0.0f,  zMax);	// Bottom Left Of The Texture and Quad

    // Front Face
    glVertex3f(0.0f, 0.0f, zMax);	// Bottom Left Of The Texture and Quad
    glVertex3f(xMax, 0.0f, zMax);	// Bottom Right Of The Texture and Quad
    glVertex3f(xMax, yMax, zMax);	// Top Right Of The Texture and Quad
    glVertex3f(0.0f,  yMax,  zMax);	// Top Left Of The Texture and Quad

    // Top Face
    glVertex3f(0.0f,  yMax, 0.0f);	// Top Left Of The Texture and Quad
    glVertex3f(0.0f, yMax,  zMax);	// Bottom Left Of The Texture and Quad
    glVertex3f(xMax, yMax, zMax);	// Bottom Right Of The Texture and Quad
    glVertex3f(xMax, yMax, 0.0f);	// Top Right Of The Texture and Quad
    glEnd();
}


void Texture::drawCutPlanes(){

    double x = xCutPosition + xCutDirection*.001;
    double y = yCutPosition + yCutDirection*.001;
    double z = zCutPosition + zCutDirection*.001;

    glColor4f(1.0,0.,0.,0.25);
    glBegin(GL_QUADS);

    if(xCutDisplay){
        // Right face
        glVertex3f( x, 0.0f, 0.0f);	// Bottom Right Of The Texture and Quad
        glVertex3f( x, yMax, 0.0f);	// Top Right Of The Texture and Quad
        glVertex3f( x, yMax, zMax);	// Top Left Of The Texture and Quad
        glVertex3f( x, 0.0f, zMax);	// Bottom Left Of The Texture and Quad
    }

    if(zCutDisplay){
        // Front Face
        glVertex3f(0.0f, 0.0f, z);	// Bottom Left Of The Texture and Quad
        glVertex3f(xMax, 0.0f, z);	// Bottom Right Of The Texture and Quad
        glVertex3f(xMax, yMax, z);	// Top Right Of The Texture and Quad
        glVertex3f(0.0f, yMax, z);	// Top Left Of The Texture and Quad
    }

    if(yCutDisplay){
        // Top Face
        glVertex3f(0.0f, y, 0.0f);	// Top Left Of The Texture and Quad
        glVertex3f(0.0f, y, zMax);	// Bottom Left Of The Texture and Quad
        glVertex3f(xMax, y, zMax);	// Bottom Right Of The Texture and Quad
        glVertex3f(xMax, y, 0.0f);	// Top Right Of The Texture and Quad
    }
    glEnd();


}

void Texture::drawBoundingBox(){

    glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(1.f,0.f,0.f);
    drawCube();
    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

}

void Texture::build(const std::vector<unsigned char> & data, const std::vector<unsigned char> & labels,
                    unsigned int & nx , unsigned int & ny , unsigned int & nz,
                    float & dx , float & dy , float & dz,
                    std::map<unsigned char, QColor> & labelsToColor){

    if(textureCreated)
        deleteTexture();

    initTexture();

//    _labels = labels;
//    for(unsigned int i = 0 ; i < labels.size() ; i ++)
//        _labelIDMap[labels[i]] = i;

    n[0] = nx; n[1] = ny; n[2] = nz;
    d[0] = dx; d[1] = dy; d[2] = dz;
    std::cout << "(nx,dx) = ( " << n[0] << " ; " << d[0] << " ) "<< std::endl;
    std::cout << "(ny,dy) = ( " << n[1] << " ; " << d[1] << " ) "<< std::endl;
    std::cout << "(nz,dz) = ( " << n[2] << " ; " << d[2] << " ) "<< std::endl;

    xMax = double(n[0])*d[0];
    yMax = double(n[1])*d[1];
    zMax = double(n[2])*d[2];

    minD = dx;
    minD = std::min(dy, minD); minD = std::min(dz, minD);

    gridSize = n[0]*n[1]*n[2];

    std::cout <<"(xMax, yMax, zMax) =  ( " << xMax << " ; " << yMax << " ; " << zMax << " ) "<< std::endl;

    xCutPosition = xMax;
    yCutPosition = yMax;
    zCutPosition = zMax;

    unsigned char * texels = new unsigned char[n[0]*n[1]*n[2]*4];

    unsigned int max_id = 0 ;

    for(unsigned int i = 0 ; i < labels.size() ; i ++){
        max_id = std::max(static_cast<unsigned int>(labels[i]), max_id);
        std::cout <<"Label value " << static_cast<unsigned int>(labels[i]) << std::endl;
    }

    //    std::vector<QColor> labelsToColor(max_id+1, QColor(0.,0.,0.));
    //    for(std::map<unsigned char, QColor>::const_iterator it = lToColor.begin(); it != lToColor.end(); ++it){
    //        labelsToColor[(unsigned int)it->first] = it->second;
    //
    //    }

    Vmin[0] = 3000; Vmin[1] = 3000; Vmin[2] = 3000;
    Vmax[0] = 0; Vmax[1] = 0; Vmax[2] = 0;


    for (unsigned int i=0; i<n[0]; i++){
        for (unsigned int j=0; j<n[1]; j++){
            for (unsigned int k=0; k<n[2]; k++){

                unsigned int indice = k*n[0]*n[1]*4 + j*n[0]*4 +i*4;

                unsigned int indice1 = k*n[0]*n[1] + j*n[0] +i;

                int greyValue = static_cast<int>(data[indice1]);

                if( greyValue > 0 ){

                    if( i < Vmin[0] ) Vmin[0]=i;
                    if( j < Vmin[1] ) Vmin[1]=j;
                    if( k < Vmin[2] ) Vmin[2]=k;

                    if( i > Vmax[0] ) Vmax[0]=i;
                    if( j > Vmax[1] ) Vmax[1]=j;
                    if( k > Vmax[2] ) Vmax[2]=k;
                }

                //                        std::cout << (unsigned int)greyValue << std::endl;
                QColor color = labelsToColor[greyValue];

                texels[indice ] = color.red();
                texels[indice +1] = color.green();
                texels[indice +2] = color.blue();
                texels[indice +3] = greyValue;

//                std::cout << color.red()/255. << " -- "<<
//                             color.green()/255. << " -- "<<
//                             color.blue()/255. << " -- "<<
//                             greyValue << std::endl;
            }
        }
    }

    std::cout <<"Label nb: " << labels.size() << std::endl;

    glBindTexture(GL_TEXTURE_3D, textureId);

    glTexImage3D(GL_TEXTURE_3D,
                 0,
                 GL_RGBA,
                 n[0],
            n[1],
            n[2],
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            texels);


    delete [] texels;

    textureCreated = true;
}


void Texture::setXCut(int _xCut){
    xCut = 1.-double(_xCut)/n[0];
    xCutPosition = xMax*xCut;
}

void Texture::setYCut(int _yCut){
    yCut = 1.- double(_yCut)/n[1];
    yCutPosition = yMax*yCut;
}

void Texture::setZCut(int _zCut){
    zCut = 1.0-double(_zCut)/n[2];
    zCutPosition = zMax*zCut;
}

void Texture::clear(){

    if( textureCreated )
        glDeleteTextures(1, &textureId);

    init();

}

