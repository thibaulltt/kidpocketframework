#ifndef MESH_H
#define MESH_H
#include "OpenGLHeader.h"
#include "BasicPoint.h"
#include "Triangle.h"
#include "Tetrahedron.h"

#include "CGALIncludes.h"
#include <queue>
#include <QColor>

class Mesh
{
public:

	Mesh();
	~Mesh();

	Mesh(const Mesh_3 & c3t3);
	void addToMesh(const std::vector<BasicPoint> & V, const std::vector<Triangle> & T, Subdomain_index si);

	std::vector<BasicPoint> & getVertices(){return vertices;}
	const std::vector<BasicPoint> & getVertices()const {return vertices;}

	std::vector<Triangle> & getTriangles(){return triangles;}
	const std::vector<Triangle> & getTriangles()const {return triangles;}

	std::vector<Tetrahedron> & getTetrahedra(){return tetrahedra;}
	const std::vector<Tetrahedron> & getTetrahedra()const {return tetrahedra;}

	std::vector<Subdomain_index> & getTriangles_subdomain_ids(){return triangles_subdomain_ids;}
	const std::vector<Subdomain_index> & getTriangles_subdomain_ids()const {return triangles_subdomain_ids;}

	std::vector<Subdomain_index> & getTetrahedra_subdomain_ids(){return tetrahedra_subdomain_ids;}
	const std::vector<Subdomain_index> & getTetrahedra_subdomain_ids()const {return tetrahedra_subdomain_ids;}

	std::map <Subdomain_index, std::vector<int> > & getSortedTriangles(){return sortedTriangles;}
	const std::map <Subdomain_index, std::vector<int> > & getSortedTriangles() const {return sortedTriangles;}

	std::map <Subdomain_index, std::vector<int> > & getSortedTetrahedra(){return sortedTetrahedra;}
	const std::map <Subdomain_index, std::vector<int> > & getSortedTetrahedra() const {return sortedTetrahedra;}

	std::vector<int> & getCGAL_envelop(){return CGAL_envelop;}
	const std::vector<int> & getCGAL_envelop() const {return CGAL_envelop;}

	std::vector<Subdomain_index> & getSubdomainIndices( ){return subdomain_indices;}
	const std::vector<Subdomain_index> & getSubdomainIndices( ) const{return subdomain_indices;}

	std::vector<bool> & getEnvelopVertices( ){return envelopVertices;}
	const std::vector<bool> & getEnvelopVertices( ) const{return envelopVertices;}

	void drawEnvelop();
	void draw();
	void drawMaxima(float radius);
	void drawSignificantGeodesics(float radius, bool segmentation);
	void drawBoundaries(bool meshColor, const std::map<Subdomain_index, bool> & displayMap, const std::map<Subdomain_index, QColor> & colorMap);
	void drawBoundariesVBO(bool solid, bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap);
	void drawTetrahedraVBO(bool solid, bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap );
	void drawTetrahedra(bool solid, bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap);
	void drawTetrahedraDebug(bool meshColor, std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap);
	void drawVertices(std::map<Subdomain_index, bool> & displayMap, std::map<Subdomain_index, QColor> & colorMap);
	void updateMesh(const std::vector<BasicPoint> & insideVertices, const std::vector<BasicPoint> & envelopVertices);

	void recomputeNormals();
	void invertNormals(){ normalDirection *=-1; }
	void updateModel();

	void clear();

	const BasicPoint & getBBMin(){return BBMin;}
	const BasicPoint & getBBMax(){return BBMax;}

	void setClipeEquation(const BasicPoint & clipN, const BasicPoint & pointN){ clippingNormal = clipN; clippingNormal.normalize(); pointOnClipping = pointN; }
	void setOrthongonalCut(const BasicPoint & _cut, const BasicPoint & _cutDirection){ cut = _cut; cutDirection = _cutDirection; }

	typedef std::priority_queue< std::pair< float , int > , std::deque< std::pair< float , int > > , std::greater< std::pair< float , int > > > FacesQueue;

	enum VertexType{ GEOSTART = 0 , GEOEND = 1 , SELLE = 2 };

	typedef std::pair<unsigned int, int> GeoIdVertexType;

	typedef std::pair<float, std::pair<unsigned int, unsigned int> > LengthId;
	typedef std::pair<float, GeoIdVertexType > DistanceVertexIdType;
	typedef std::pair<float, unsigned int> RatioId;

	void computeMaxima();
	void classifyGeodesics();

	//void incrementDisplayCount(){ if (displayCount < ratioMins.size()-1) displayCount++;  }
	void incrementDisplayCount(){ if (displayCount < int(segmentation.size())-1) displayCount++;  }
	void decrementDisplayCount(){ if (displayCount > 0 ) displayCount--;  }

	void separateLabels(unsigned int l1, unsigned int l2 );
	void segmentMesh(const std::vector<int> & labels);

	std::vector<int> tetIds;
	std::map<int, float> segHue;
	std::vector<int> vFlags;

	void set_visibility_check(bool _v_check){visibility_check = _v_check; }

	inline void glVertex( BasicPoint const & p )
	{
		glVertex3f( p[0] , p[1] , p[2] );
	}
	inline void glNormal( BasicPoint const & p )
	{
		glNormal3f( p[0] , p[1] , p[2] );
	}
protected:
	void init();
	void initTextures();
	bool isGeodesicsSignificant(const std::vector< std::vector< GeoIdVertexType> > & verticesGeoClustering, unsigned int g0 );
	void computeBB();

	void collectOneRing (std::vector<std::vector<unsigned int> > & oneRing) const;

	BasicPoint computeBarycenter( unsigned int i );

	void sortBySubdomainIndex();
	void initVBOData();

	void computeFacetsNormals();
	BasicPoint computeFacetNormal(int t);

	void computeVerticesNormals(unsigned int weight);


	void glTriangle(unsigned int i, Subdomain_index si);
	void glTetrahedron(unsigned int i);

	GLuint createVBO(const void* data, int dataSize, GLenum target, GLenum usage);
	void deleteVBO(const GLuint vboId);

	void sortFaces( FacesQueue & facesQueue );

	void computeTexturesData();
	void clearTextures();
	void findGoodTexSize( unsigned int uiNumValuesNeeded, int& iTexWidth, int& iTexHeight, bool bUseAlphaChannel );
	bool isVisiblePoint( int i );
	bool isVisibleTet(int i );
	bool isVisibleTriangle(int i);
	int computeTextureNumber();
	int textureWidth;
	int textureHeight;

	int tetTextureWidth;
	int tetTextureHeight;

	int indices[4][3];

	int textureNb;

	GLuint triangleVerticesVBOId;
	GLuint triangleNormalsVBOId;
	GLuint triangleIndicesVBOId;
	GLuint triangleTextureCoordsVBOId;

	GLuint triangleVerticesTextureId;
	GLuint triangleNormalsTextureId;


	GLuint tetVerticesVBOId;
	GLuint tetNormalsVBOId;
	GLuint tetIndicesVBOId;
	GLuint tetTextureCoordsVBOId;

	GLuint tetVerticesTextureId;
	GLuint tetNormalsTextureId;

	BasicPoint clippingNormal;
	BasicPoint pointOnClipping;

	BasicPoint cut;
	BasicPoint cutDirection;

	std::map<Subdomain_index, int > offsets;
	std::map<Subdomain_index, int > tetOffsets;

	std::vector <BasicPoint> vertices;
	std::vector <int> verticesDimensions;
	int vsize;
	std::vector <bool> envelopVertices;
	std::vector <Triangle> triangles;
	std::vector <Tetrahedron> tetrahedra;

	std::map <Subdomain_index, std::vector<int> > sortedTriangles;
	std::map <Subdomain_index, std::vector<int> > sortedTetrahedra;

	std::vector<BasicPoint> normals;
	std::vector<std::map<Subdomain_index, BasicPoint> > verticesNormals;
	std::vector<Subdomain_index> triangles_subdomain_ids;
	std::vector<Subdomain_index> tetrahedra_subdomain_ids;

	std::vector<int> CGAL_envelop;

	std::vector<Subdomain_index> subdomain_indices;

	BasicPoint BBMin;
	BasicPoint BBMax;
	float radius;

	std::vector< unsigned int > geodesics;
	std::vector<unsigned int> sellePoints;
	std::vector<unsigned int> significantGeodesics;

	std::vector< RatioId > sortedGeodesics;

	std::vector< std::vector<int> > segmentation;
	unsigned int max_ratio;

	int displayCount;

	bool maxiComputed;

	bool textureCreated;
	int normalDirection;

	bool visibility_check;

};

#endif // MESH_H
