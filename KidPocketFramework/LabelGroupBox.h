#ifndef LABELGROUPBOX_H
#define LABELGROUPBOX_H

#define LABEL_NB 256
#define Index int

#include <QGroupBox>
#include <QSignalMapper>
#include <QVector>
#include <QCheckBox>
#include <QLabel>
#include <map>
#include <QGridLayout>

//*****************************************************************************************************//
//  LabelGroupBox class                                                                                //
//*****************************************************************************************************//
// Creates a label list visibility box                                                                 //
// Each ligne correspond to a label: text Qlabel, color QLabel, visibility status checkbox             //
//*****************************************************************************************************//
// Usage:                                                                                              //
// Create class                                                                                        //
// Then setLabels(), with or without colors                                                            //
// To update colors use setColors()                                                                    //
// Connect signals to get user input information                                                       //
//      visibilityValueChanged(unsigned int i, bool value);                                            //
//      selectAllPushButton();                                                                         //
//      discardAllPushButton();                                                                        //
// Visibility and labels can be accessed with public functions                                         //
//*****************************************************************************************************//

class LabelGroupBox : public QGroupBox
{
    Q_OBJECT

    struct LabelQData {
        QCheckBox * visibilityCheckBox;
        QLabel * textQLabel;
        QLabel * colorQLabel;
    };

public:
    LabelGroupBox(QWidget *parent = nullptr);
    LabelGroupBox(const QString &title, QWidget *parent = nullptr);

    //Setting the labels, building the list of QData
    void setLabels(const std::vector<Index> &labels);
    void setLabels(const std::vector<Index> &labels, const std::vector<QColor> &colors );

    //Update label colors
    void setColors(const std::vector<QColor> &colors);
    void setColors(const std::vector<Index> &labels, const std::vector<QColor> &colors);

    //Getter for the ith label
    Index label( unsigned int i );

    //Getter for the visibility status of a label
    bool getVisibility( unsigned int i );
    bool getVisibilityFromLabel( Index i );

signals:
    //To connect to the class using the groupbox
    void visibilityValueChanged(unsigned int i, bool value);
    void selectAllPushButton();
    void discardAllPushButton();

protected:
    //Setting the base structure ready to add label list
    void init();
    //Compute a set of color for a given number of labels
    void computeDefaultColors( std::vector<QColor> &colors );

    //Mapper to get a signal per label
    QSignalMapper * m_signalMapper;

    //List of label information
    std::vector<LabelQData> m_QDataList;
    std::vector<QColor> m_colors;
    std::vector<Index> m_labels;

    //Mapping from label if to list of label index
    std::map<Index, unsigned int> m_labels_id;

    QGridLayout * m_gridLayout;

protected slots:
    //Local slots used to be redirected as signals
    void setVisibility(int i);
    void selectAll();
    void discardAll();
};

#endif // LABELGROUPBOX_H
