#ifndef HEXAHEDRON_H
#define HEXAHEDRON_H

#include "BasicPoint.h"

#include <map>
#include <vector>

// -------------------------------------------------
// Intermediate QuadFace structure for hashed adjacency
// -------------------------------------------------

struct QuadFace {
public:
    inline QuadFace ( unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3 ) {
        std::vector<int> vertices (4); vertices[0] = v0; vertices[1] = v1; vertices[2] = v2;vertices[3] = v3;
        std::sort( vertices.begin(), vertices.end() );
        v[0] = vertices[0]; v[1] = vertices[1]; v[2] = vertices[2]; v[3] = vertices[3];
    }

    inline QuadFace (const QuadFace & f) { v[0] = f.v[0]; v[1] = f.v[1]; v[2] = f.v[2]; v[3] = f.v[3]; }
    inline virtual ~QuadFace () {}
    inline QuadFace & operator= (const QuadFace & f) { v[0] = f.v[0]; v[1] = f.v[1]; v[2] = f.v[2]; v[3] = f.v[3]; return (*this); }
    inline bool operator== (const QuadFace & f) { return (v[0] == f.v[0] && v[1] == f.v[1] && v[2] == f.v[2] && v[3] == f.v[3]); }
    inline bool operator< (const QuadFace & f) { return (v[0] < f.v[0] ||
                                                    (v[0] == f.v[0] && v[1] < f.v[1]) ||
                                                    (v[0] == f.v[0] && v[1] == f.v[1] && v[2] < f.v[2]) ||
                                                    (v[0] == f.v[0] && v[1] == f.v[1] && v[2] == f.v[2] && v[3] < f.v[3])); }
    inline bool contains (unsigned int i) const { return (v[0] == i || v[1] == i || v[2] == i || v[3] == i); }
    inline unsigned int getVertex (unsigned int i) const { return v[i]; }
    unsigned int v[3];

};

struct compareQuadFace {
    inline bool operator()(const QuadFace f1, const QuadFace f2) const {
        if (f1.v[0] < f2.v[0] ||
           (f1.v[0] == f2.v[0] && f1.v[1] < f2.v[1]) ||
           (f1.v[0] == f2.v[0] && f1.v[1] == f2.v[1] && f1.v[2] < f2.v[2]) ||
           (f1.v[0] == f2.v[0] && f1.v[1] == f2.v[1] && f1.v[2] == f2.v[2] && f1.v[3] < f2.v[3]))
            return true;
        return false;
    }
};

typedef std::map<QuadFace, unsigned int, compareQuadFace> QuadFaceMapIndex;

class Hexahedron {
public:
  inline Hexahedron(){}
  inline Hexahedron (unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3,
                     unsigned int v4, unsigned int v5, unsigned int v6, unsigned int v7, int _label = -1) { init (v0, v1, v2, v3, v4, v5, v6, v7, _label); }
  inline Hexahedron (unsigned int * vp, int _label = -1) { init (vp[0], vp[1], vp[2], vp[3], vp[4], vp[5], vp[6], vp[7], _label); }
  inline Hexahedron (unsigned int * vp, int * np, int _label = -1) { init (vp, np, _label); }
  inline Hexahedron (const Hexahedron & it) { for(int i = 0 ; i < 6 ; i++) { v[i] = it.v[i]; neighbors[i] = it.neighbors[i]; } v[6] = it.v[6]; v[7] = it.v[7]; label = it.label; }
  inline virtual ~Hexahedron () {}

  inline Hexahedron & operator= (const Hexahedron & it) { for(int i = 0 ; i < 6 ; i++) { v[i] = it.v[i]; neighbors[i] = it.neighbors[i]; } v[6] = it.v[6]; v[7] = it.v[7]; label = it.label; return (*this); }
  inline bool operator== (const Hexahedron & _t) const { for(int j=0; j<8; j++) if(v[j]!=_t.v[j]) return false; return true; }

  inline unsigned int getVertex (unsigned int i) const { return v[i]; }
  inline int getNeighbor (unsigned int i) { return neighbors[i]; }
  inline int getLabel () const { return label; }

  inline bool contains (unsigned int vertex) const { for(int j=0; j<8; j++) if(v[j] == vertex) return true; return false;}
  inline bool hasNeighbor (unsigned int hexahedron) const { for(int j=0; j<6; j++) if(neighbors[j] == hexahedron) return true; return false;}
  inline int indexOf (unsigned int vertex) const { for(int j=0; j<8; j++) if(v[j]==vertex){ return j;} return -1; }
  inline int indexOfNeighbor (unsigned int hexahedron) const { for(int j=0; j<6; j++) if(neighbors[j] == hexahedron) return j; return -1;}

  inline void setVertex (unsigned int i, unsigned int vertex) { v[i] = vertex; }
  inline void setNeighbor (unsigned int i, int hexahedron) { neighbors[i] = hexahedron; }
  inline void setLabel(unsigned int _label) {label = _label;}

  inline bool replace(unsigned int vertex, unsigned int by){ for(int j=0; j<8; j++) if(v[j]==vertex){ v[j]=by; return true;} return false;}
  inline void decrement(unsigned int i){ for(int j=0; j<8; j++) if(v[j]>i) v[j]-=1; }

protected:
  inline void init ( unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3,
                     unsigned int v4, unsigned int v5, unsigned int v6, unsigned int v7, int _label = -1 ) {
      v[0] = v0; v[1] = v1; v[2] = v2; v[3] = v3; v[4] = v4; v[5] = v5; v[6] = v6; v[7] = v7; label = _label;
  }
  inline void init ( unsigned int * vp, int * np, int _label = -1 ) {
      for(int i = 0 ; i < 6 ; i++) { v[i] = vp[i]; neighbors[i] = np[i]; }
      v[6] = vp[6]; v[7] = vp[7];
      label = _label;
  }
private:
  int neighbors[4];
  unsigned int v[4];
  int label;

};

class Quad {
public:
  inline Quad(){}
  inline Quad ( Hexahedron * hexahedron, unsigned int f, int ln ) { _hexahedron = hexahedron; _f = f; label = std::make_pair(_hexahedron->getLabel(), ln); }
  inline Quad (const Quad & q) { _hexahedron = q._hexahedron; _f = q._f; label = q.label; }
  inline virtual ~Quad () {}

  inline Quad & operator= (const Quad & q) {  _hexahedron = q._hexahedron; _f = q._f; label = q.label; return (*this); }
  inline bool operator== (const Quad & _q) const { return ( (*_hexahedron) == (*_q.hexahedron()) && _f == _q._f); }

  inline unsigned f(){ return _f; }
  const Hexahedron * hexahedron() const { return _hexahedron; }
  Hexahedron * hexahedron(){ return _hexahedron; }
  const std::pair< int , int > & getLabel(){ return label; }

private:
  unsigned int _f;
  Hexahedron * _hexahedron;
  std::pair< int , int > label;

};

extern std::ostream & operator<< (std::ostream & output, const Hexahedron & t);


#endif // HEXAHEDRON_H
