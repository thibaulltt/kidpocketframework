TEMPLATE = app
TARGET = kidPocketFramework
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
DEPENDPATH += ./include
DISTFILES += *.png
QT *= xml \
    opengl

#QMAKE_CC = clang
#QMAKE_CXX = clang++

CONFIG += qt \
    opengl \
    xml \
    warn_on \
    console \
    thread \
    release
REP_EXT = ../external
EXT_DIR = ../external
INCLUDEPATH = $${REP_EXT}/compiled/boost/include/boost_1_65 \
    $${REP_EXT}/boost-1.65/boost \
    $${REP_EXT}/compiled/cgal/include \
    $${REP_EXT}/CGAL-4.10/include \
    /usr/include/nifti \
    /home/thibault/git/libQGLViewer-2.7.2 \
    ./CageDeform \
    ./GLSL
HEADERS += Window.h \
    MesherViewer.h \
    CageDeform/PCATools.h \
    CageDeform/CageManipInterface.h \
    CageDeform/CageCoordinates.h \
    CageDeform/GLUtilityMethods.h \
    CageDeform/RectangleSelection.h \
    CageDeform/BasicPoint.h \
    CageDeform/RotationManipulator.h \
    Tetrahedron.h \
    Triangle.h \
    VoxelGrid.h \
    RegularBspTree.h \
    Voxel.h \
    Mesh.h \
    CGALIncludes.h \
    Hexahedron.h \
    GLSL/VertexShader.h \
    GLSL/ShaderPool.h \
    GLSL/ShaderObject.h \
    GLSL/ShaderInclude.h \
    GLSL/ProgramObject.h \
    GLSL/OpenGLHeader.h \
    GLSL/HelperFunctions.h \
    GLSL/GeometryShader.h \
    GLSL/FragmentShader.h \
    GLSL/BufferObject.h \
    AsRigidAsPossible.h \
    Edge.h \
    Mesh_complex_3.h \
    Mesh_triangulation_with_info_3.h \
    Mesh_3_image_3_domain.h \
    make_mesh_3_from_labeled_triangulation_3.h \
    SurfaceCreator.h \
    Histogram.h \
    TetMeshCreator.h \
    Mesh_vertex_base_with_info_3.h \
    Mesh_cell_base_with_info_3.h \
    CellInfo.h \
    TetraLRISolver.h \
    CholmodLSStruct.h \
    SeparationInfo.h \
    Texture.h \
    VBOHandler.h \
    VisuMesh.h \
    Vec3D.h
SOURCES += Window.cpp \
    MesherViewer.cpp \
    Main.cpp \
    CageDeform/GLUtilityMethods.cpp \
    VoxelGrid.cpp \
    Mesh.cpp \
    GLSL/VertexShader.cpp \
    GLSL/ShaderPool.cpp \
    GLSL/ShaderObject.cpp \
    GLSL/ProgramObject.cpp \
    GLSL/HelperFunctions.cpp \
    GLSL/GeometryShader.cpp \
    GLSL/FragmentShader.cpp \
    GLSL/BufferObject.cpp \
    AsRigidAsPossible.cpp \
    SurfaceCreator.cpp \
    Histogram.cpp \
    TetMeshCreator.cpp \
    TetraLRISolver.cpp \
    Texture.cpp \
    VBOHandler.cpp \
    VisuMesh.cpp

LIBS = -L$${REP_EXT}/compiled/boost/lib \
    -L$${REP_EXT}/compiled/cgal/lib \
    -L/home/thibault/git/libQGLViewer-2.7.2/QGLViewer \
    -lboost_system-gcc92-mt-d-1_65 \
    -lboost_thread-gcc92-mt-d-1_65 \
    -lCGAL \
    -lCGAL_Core \
    -lCGAL_ImageIO \
    -lQGLViewer-qt5 \
    -lGLU \
    -lGLEW \
    -lniftiio \
    -lznz \
    -lz \
    -lmpfr \
    -lgmp \
    -lgmpxx \
    -lmd4c \
    -lgsl \
    -lblas \
    -lcblas \
    -lomp \
    -lcholmod \
    -lamd \
    -lcolamd \
    -lccolamd \
    -lcamd \
    -llapack \
    -lm
QMAKE_CXXFLAGS = -frounding-math \
    -DNDEBUG \
    -fno-strict-aliasing -w
#    -std=c++14
QMAKE_CXXFLAGS_DEBUG += -ggdb
QMAKE_CXXFLAGS_RELEASE += -fopenmp
QMAKE_CFLAGS_RELEASE += -fopenmp

# ------------------------------ for CHOLMOD : ------------------------------#
#QMAKE_LIBDIR = $${EXT_DIR}/libcholmod/CHOLMOD/Lib \ # QMAKE_LIBDIR += $${EXT_DIR}/libcholmod/CHOLMOD/Lib \
#    $${EXT_DIR}/libcholmod/AMD/Lib \
#    $${EXT_DIR}/libcholmod/COLAMD/Lib \
#    $${EXT_DIR}/libcholmod/CCOLAMD/Lib \
#    $${EXT_DIR}/libcholmod/CAMD/Lib
#LIBS += -lcholmod \
#    -lamd \
#    -lcolamd \
#    -lccolamd \
#    -lcamd \
#    -llapack \
#    -lm

