#ifndef SURFACECREATOR_H
#define SURFACECREATOR_H
#include "OpenGLHeader.h"

#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Gray_level_image_3.h>
#include "CGAL/Polyhedron_3.h"
#include "BasicPoint.h"
#include "Triangle.h"

// default triangulation for Surface_mesher
typedef CGAL::Surface_mesh_default_triangulation_3 Surface_Tr;

typedef Surface_Tr::Geom_traits GT;
typedef CGAL::Gray_level_image_3<GT::FT, GT::Point_3> Gray_level_image;

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;

class SurfaceCreator
{
public:
    SurfaceCreator( const CGAL::Image_3 & image , int greyLevel, float angle, float size, float approximation, BasicPoint offset =  BasicPoint (0.,0.,0.) );

    SurfaceCreator(){}

    void draw();
    void simplify( unsigned int ratio );

    void getMesh( std::vector<BasicPoint> & vertices, std::vector<Triangle> & triangles );

    unsigned int getHalfEdgeCount(){return polyhedron.size_of_halfedges ();}


    inline void glVertex( BasicPoint const & p )
    {
        glVertex3f( p[0] , p[1] , p[2] );
    }
    inline void glNormal( BasicPoint const & p )
    {
        glNormal3f( p[0] , p[1] , p[2] );
    }
protected:

    void recomputeVertexNormals();

    Polyhedron polyhedron;
    std::vector<BasicPoint> normals;
};

#endif // CAGECREATOR_H
