To use KidPocketFramework, install the following libraries: 

* niftiio 2.0
http://sourceforge.net/projects/niftilib/files/nifticlib/
Download 
nifticlib-2.0.0.tar.gz  

In nifticlib diretory:
cmake .
make 
sudo make install

* CGAL-3.7 
http://www.cgal.org/download.html
In CGAL diretory:
cmake .
make 
sudo make install

* QGlViewer

sudo apt-get install libqglviewer-qt4-dev

* blas

sudo apt-get install libblas-dev

* gsl 

sudo apt-get install libgsl0-dev

* glut

sudo apt-get install freeglut3-dev

Usage :

qmake-qt4 
make 
./kidPocketFramework

For more information press H in the application.