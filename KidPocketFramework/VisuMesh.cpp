// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend
//
// Copyright(C) 2007-2009
// Tamy Boubekeur
//
// All rights reserved.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
// for more details.
//
// --------------------------------------------------------------------------

#include "VisuMesh.h"
#include "GLUtilityMethods.h"
#include <algorithm>
#include <climits>

#define GL_R32UI 0x8236
#define GL_RED_INTEGER 0x8D94

using namespace std;

void VisuMesh::clear () {
    clearTopology ();
    clearGeometry ();
}

void VisuMesh::clearGeometry () {
    vertices.clear ();
}

void VisuMesh::clearTopology () {
    tetrahedra.clear ();
}

void VisuMesh::init(){

    indices[0][0] = 3; indices[0][1] = 1; indices[0][2] = 2;
    indices[1][0] = 3; indices[1][1] = 2; indices[1][2] = 0;
    indices[2][0] = 3; indices[2][1] = 0; indices[2][2] = 1;
    indices[3][0] = 2; indices[3][1] = 1; indices[3][2] = 0;

    textureCreated = false;
    built = false;

    cut = BasicPoint(0.,0.,0.);
    cutDirection = BasicPoint(1.,1.,1.);

    c_incr = 0;
}

void VisuMesh::updateVisuMesh(){
    clearTextures();
    collectNeighbors();
    initTextures();
    computeTexturesData();

}

void VisuMesh::collectNeighbors(){

    std::cout << " collectNeighbors " << std::endl;

    glGenTextures(1, &tetNeighborsTextureId);

    glBindTexture(GL_TEXTURE_2D, tetNeighborsTextureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    glBindTexture(GL_TEXTURE_2D, 0);

    //    glGenTextures(1, &tetNeighborsNbTextureId);
    //
    //    glBindTexture(GL_TEXTURE_2D, tetNeighborsNbTextureId);
    //
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);
    //
    //    glBindTexture(GL_TEXTURE_2D, 0);


    int neighborsNb = tetrahedra.size()*4;
    unsigned int neighborTHeight;
    unsigned int neighborNbTHeight;

    neighbors.clear();
    neighbors.resize( tetrahedra.size() );

    //std::vector< std::vector< int> > vNeighbors ( vertices.size() );

    std::map< Face , std::pair<int,int> > adjacent_faces;

    for(  int i = 0 ; i < tetrahedra.size() ; i ++ ){
        neighbors[i].clear();
        neighbors[i].resize(4, -1);
    }

    for(  int i = 0 ; i < tetrahedra.size() ; i ++ ){
        for( int v = 0 ; v < 4 ; v++ ){
            //vNeighbors[ tetrahedra[i].getVertex(v) ].push_back( i );
            Face face = Face( tetrahedra[i].getVertex( indices[v][0] ),
                    tetrahedra[i].getVertex( indices[v][1] ),
                    tetrahedra[i].getVertex( indices[v][2] ) );
            std::map< Face , std::pair<int,int> >::iterator it = adjacent_faces.find(face);
            if( it == adjacent_faces.end() ){
                adjacent_faces[face] = std::make_pair(i,v);
            } else {
                neighbors[i][v] = it->second.first;
                neighbors[it->second.first][it->second.second] = i;
            }
        }
    }


    /*
    for( unsigned int i = 0 ; i < tetrahedra.size() ; i ++ ){
        for( int v = 0 ; v < 4 ; v++ ){
            const std::vector<unsigned int> & v_neigh_indices = vNeighbors[ tetrahedra[i].getVertex(v) ];
            for( unsigned int j = 0 ; j < v_neigh_indices.size() ; j ++ ){
                if( v_neigh_indices[j] != i && std::find( neighbors[i].begin(), neighbors[i].end(), v_neigh_indices[j] ) == neighbors[i].end() ){
                   // if(neighbors[i].size() < 5 ){
                        neighbors[i].push_back( v_neigh_indices[j] );
                        neighborsNb ++;
                   // }
                }
            }
        }
    }
    */
    //  vNeighbors.clear();

    //    for( unsigned int i = 0 ; i < tetrahedra.size() ; i ++ ){
    //
    //        const Tetrahedron & current_tetrahedron = tetrahedra[i];
    //        std::vector<int> & current_neighbors = neighbors[i];
    //        std::map<int, bool> visited;
    //        std::vector<int> new_neighbors;

    //        for( unsigned int j = 0 ; j < direct_neighbors[i].size() ; j++ ){
    //            unsigned int c_neigh_id = direct_neighbors[i][j];
    //            new_neighbors.push_back( c_neigh_id );
    //            neighborsNb++;
    //            for( unsigned int k = 0 ; k < direct_neighbors[c_neigh_id].size(); k++ ){
    //                unsigned int next_neigh_id = direct_neighbors[c_neigh_id][k];
    //                if( next_neigh_id != i ){
    //                    if( tetrahedra[next_neigh_id].contains(current_tetrahedron.getVertex(0)) ||
    //                            tetrahedra[next_neigh_id].contains(current_tetrahedron.getVertex(1)) ||
    //                            tetrahedra[next_neigh_id].contains(current_tetrahedron.getVertex(2)) ||
    //                            tetrahedra[next_neigh_id].contains(current_tetrahedron.getVertex(3))){
    //
    //                        if( std::find( new_neighbors.begin(), new_neighbors.end(), next_neigh_id ) == new_neighbors.end() ){
    //                            new_neighbors.push_back( next_neigh_id );
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        for( unsigned int j = 0 ; j < current_neighbors.size() ; j++ ){
    //
    //            if( std::find( new_neighbors.begin(), new_neighbors.end(), current_neighbors[j] ) == new_neighbors.end() ){
    //                new_neighbors.push_back( current_neighbors[j] );
    //            }
    //        }
    //
    //        if(current_neighbors.size() != new_neighbors.size()){
    //            std::cout << "Oups" << std::endl;
    //            return ;
    //        }
    //        current_neighbors.clear();
    //        neighbors[i] = std::vector<int>(new_neighbors);
    //
    //    }

    vboHandler.findGoodTexSize(neighborsNb*3, neighborTWidth, neighborTHeight, false);
    int n_nb_size = neighborTWidth*neighborTHeight*3;

    GLfloat * n_data = new GLfloat[n_nb_size];
    memset(n_data, 0., sizeof(GLfloat)*n_nb_size );


    //    vboHandler.findGoodTexSize(tetrahedra.size()*3, neighborNbTWidth, neighborNbTHeight, false);
    //    int size = neighborNbTWidth*neighborNbTHeight*3;
    //
    //    GLfloat * n_nb_data = new GLfloat[size];
    //    memset(n_nb_data, 0., sizeof(GLfloat)*size );

    int count = 0;
    int current_count = 0;
    for( unsigned int i = 0 ; i < neighbors.size() ; i ++ ){

        // n_nb_data[ i*3 ] = neighbors[i].size();
        // n_nb_data[ i*3 +1 ] = current_count;
        current_count += neighbors[i].size();
        for( unsigned int j = 0 ; j < neighbors[i].size() ; j ++ ){
            n_data[count] = neighbors[i][j];
            count += 3;
        }
    }

    //    glBindTexture(GL_TEXTURE_2D, tetNeighborsNbTextureId);
    //
    //
    //    glTexImage2D(GL_TEXTURE_2D,
    //                 0, 0x8815,
    //                 neighborNbTWidth,
    //                 neighborNbTHeight,
    //                 0,
    //                 GL_RGB,
    //                 GL_FLOAT,
    //                 n_nb_data);

    glBindTexture(GL_TEXTURE_2D, tetNeighborsTextureId);

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 neighborTWidth,
                 neighborTHeight,
                 0,
                 GL_RGB,
                 GL_FLOAT,
                 n_data);

    glBindTexture(GL_TEXTURE_2D, 0);

    GetOpenGLError();

    std::cout << " collectNeighbors end : " << tetrahedra.size() << std::endl;
}

void VisuMesh::initTextures(){

    vboHandler.findGoodTexSize(tetrahedra.size()*12*3, textureWidth, textureHeight, false);

    GLint iMaxTexSize = 256;
    glGetIntegerv( GL_MAX_TEXTURE_SIZE, &iMaxTexSize );

    if( iMaxTexSize <= (int)textureWidth || iMaxTexSize <= (int)textureWidth )
        return;

    glGenTextures(1, &verticesCoordTextureId);

    glBindTexture(GL_TEXTURE_2D, verticesCoordTextureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &verticesNormalsTextureId);

    glBindTexture(GL_TEXTURE_2D, verticesNormalsTextureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &texture3DCoordId);

    glBindTexture(GL_TEXTURE_2D, texture3DCoordId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);


    int size = textureWidth*textureHeight*3;

    GLfloat * texture_coord_data = new GLfloat[size];
    memset(texture_coord_data, 0., sizeof(GLfloat)*size );

    int count = 0 ;
    for(unsigned int i = 0 ; i < tetrahedra.size() ; i++){

        const Tetrahedron & tetrahedron = tetrahedra[i];

        for( int f = 0 ; f < 4 ; f++ ){

            for( int v = 0 ; v < 3 ; v++ ){

                const BasicPoint & textureCoord = textureCoords[tetrahedron.getVertex(indices[f][v])];

                for(int j = 0 ; j < 3 ; j++){
                    texture_coord_data[count++] = textureCoord[j];
                }
            }
        }

    }

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 textureWidth,
                 textureHeight,
                 0,
                 GL_RGB,
                 GL_FLOAT,
                 texture_coord_data);

    glBindTexture(GL_TEXTURE_2D, 0);

    GetOpenGLError();

    float v0 [3] = { -1, -1,  1 };
    float v1 [3] = { -1,  1, -1 };
    float v2 [3] = {  1, -1, -1 };
    float v3 [3] = {  1,  1,  1 };

    // vertex coords array
    GLfloat vertices[] = {v3[0],v3[1],v3[2],  v1[0],v1[1],v1[2],  v2[0],v2[1],v2[2],        // v3-v1-v2
                          v3[0],v3[1],v3[2],  v2[0],v2[1],v2[2],  v1[0],v1[1],v1[2],        // v3-v2-v1
                          v3[0],v3[1],v3[2],  v0[0],v0[1],v0[2],  v1[0],v1[1],v1[2],        // v3-v0-v1
                          v2[0],v2[1],v2[2],  v1[0],v1[1],v1[2],  v0[0],v0[1],v0[2]};       // v2-v1-v0


    verticesVBOId = vboHandler.createVBO(vertices, 12*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);


    // Tetra ///////////////////////////////////////////////////////////////////////
    //    v0----- v
    //   /       /|
    //  v ------v3|
    //  | |     | |
    //  | |v ---|-|v2
    //  |/      |/
    //  v1------v


    // normal array
    GLfloat normals[] = {v3[0],v3[1],v3[2],  v1[0],v1[1],v1[2],  v2[0],v2[1],v2[2],        // v3-v1-v2
                         v3[0],v3[1],v3[2],  v2[0],v2[1],v2[2],  v1[0],v1[1],v1[2],        // v3-v2-v1
                         v3[0],v3[1],v3[2],  v0[0],v0[1],v0[2],  v1[0],v1[1],v1[2],        // v3-v0-v1
                         v2[0],v2[1],v2[2],  v1[0],v1[1],v1[2],  v0[0],v0[1],v0[2]};       // v2-v1-v0

    normalsVBOId = vboHandler.createVBO(normals, 12*3*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);

    // index array of vertex array for glDrawElements()
    // Notice the indices are listed straight from beginning to end as exactly
    // same order of vertex array without hopping, because of different normals at
    // a shared vertex. For this case, glDrawArrays() and glDrawElements() have no
    // difference.
    GLushort indices[] = {0,1,2,
                          3,4,5,
                          6,7,8,
                          9,10,11};

    indicesVBOId = vboHandler.createVBO(indices, 12*sizeof(GLushort), GL_ELEMENT_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);

    GLfloat textureCoords[] = {0.,0.,1. ,1. ,2. ,2. ,
                               3.,3.,4. ,4. ,5. ,5. ,
                               6.,6.,7. ,7. ,8. ,8. ,
                               9.,9.,10.,10.,11.,11.};


    textureCoordsVBOId = vboHandler.createVBO(textureCoords, 12*2*sizeof(GLfloat), GL_ARRAY_BUFFER_ARB, GL_STATIC_DRAW_ARB);

    textureCreated = true;
}

void VisuMesh::buildVisibilityTexture( std::map<unsigned char, bool> & visibility ){
          //  std::cout << "Building" << std::endl;



    if( !built ){
        //glDeleteTextures(1, &visibilityTextureId);

        glGenTextures(1, &visibilityTextureId);

        glBindTexture(GL_TEXTURE_2D, visibilityTextureId);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
#if 1

    unsigned int max_id = 0;
    for( std::map<unsigned char, bool>::iterator it = visibility.begin(); it != visibility.end(); ++it){
        max_id = std::max( (unsigned int)it->first, max_id);
         //std::cout << "Data " << (unsigned int)it->first<< " out of " << visibility.size() <<std::endl;
    }


    int siNb = max_id;
    unsigned int visibilityTHeight;

    vboHandler.findGoodTexSize(siNb*3, visibilityTWitdth, visibilityTHeight, false);
    int v_nb_size = visibilityTWitdth*visibilityTHeight*3;

    GLfloat * v_data = new GLfloat[v_nb_size];
    memset(v_data, 0., sizeof(GLfloat)*v_nb_size );

    for( std::map<unsigned char, bool>::iterator it = visibility.begin(); it != visibility.end(); ++it){
        unsigned int c_id = static_cast<unsigned int>(it->first);
        if(it->second){
//            std::cout << c_id << " visible "<< std::endl;
            v_data[c_id*3] = 100.;
            v_data[c_id*3+1] = 100.;
            v_data[c_id*3+2] = 100.;
        }
//        else {
//            std::cout << c_id << " invisible "<< std::endl;
//        }
    }

    glBindTexture(GL_TEXTURE_2D, visibilityTextureId);

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 visibilityTWitdth,
                 visibilityTHeight,
                 0,
                 GL_RGB,
                 GL_FLOAT,
                 v_data);

    glBindTexture(GL_TEXTURE_2D, 0);

    GetOpenGLError();
#else
    unsigned int visibilityTHeight;

    unsigned int max_id = 0;
    for( std::map<unsigned char, bool>::iterator it = visibility.begin(); it != visibility.end(); ++it){
        max_id = std::max( (unsigned int)it->first, max_id);
         std::cout << "Data " << (unsigned int)it->first<< " out of " << visibility.size() <<std::endl;
    }


    int si_nb = max_id*3;

    vboHandler.findGoodTexSize(si_nb*3, visibilityTWitdth, visibilityTHeight, false);
    int si_nb_size = si_nb*3;

    unsigned char * data = new unsigned char[si_nb_size];
    memset(data, (unsigned char)0, sizeof(unsigned char)*si_nb_size );

    for( std::map<unsigned char, bool>::iterator it = visibility.begin(); it != visibility.end(); ++it){
        unsigned int c_id = static_cast<unsigned int>(it->first);
        if(it->second){
            std::cout << c_id << " visible "<< std::endl;
            data[c_id*3] = (unsigned char)100;
            data[c_id*3+1] = (unsigned char)100;
            data[c_id*3+2] = (unsigned char)100;
        } else {
            std::cout << c_id << " invisible "<< std::endl;
        }
    }



    glBindTexture(GL_TEXTURE_2D, visibilityTextureId);

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 visibilityTWitdth,
                 visibilityTHeight,
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 data);

    glBindTexture(GL_TEXTURE_2D, 0);

    GetOpenGLError();

#endif

    built = true;
}

void VisuMesh::computeTexturesData(){

    if(!textureCreated){
        return;
    }

    std::cout << "texture data" << std::endl;
    int size = textureWidth*textureHeight*3;

    GLfloat * translation_data = new GLfloat[size];
    memset(translation_data, 0., sizeof(GLfloat)*size );



    unsigned int normalTHeight;

    int s = tetrahedra.size()*4;
    vboHandler.findGoodTexSize(s*4, normalTWidth, normalTHeight, true);

    int normal_size = normalTWidth*normalTHeight*4;
    GLfloat * normals_data = new GLfloat[normal_size];
    memset(normals_data, 0., sizeof(GLfloat)*normal_size );



    int count = 0 ;
    int n_count = 0;
    for(unsigned int i = 0 ; i < tetrahedra.size() ; i++){

        const Tetrahedron & tetrahedron = tetrahedra[i];

        for( int f = 0 ; f < 4 ; f++ ){

            BasicPoint normal = cross(vertices[tetrahedron.getVertex(indices[f][1])] - vertices[tetrahedron.getVertex(indices[f][0])],
                    vertices[tetrahedron.getVertex(indices[f][2])] - vertices[tetrahedron.getVertex(indices[f][0])]);

            normal.normalize();
            float val = 1./dot( vertices[tetrahedra[i].getVertex(f)] - vertices[tetrahedra[i].getVertex( (f+1)%4)] , normal );

            // std::cout << val << std::endl;
            for( int v = 0 ; v < 3 ; v++ ){

                const BasicPoint & position = vertices[tetrahedra[i].getVertex(indices[f][v])];

                for(int j = 0 ; j < 3 ; j++){
                    translation_data[count] = position[j];
                    count++;
                }
            }

            for(int j = 0 ; j < 3 ; j++){
                normals_data[n_count] = normal[j];
                n_count++;
            }

            normals_data[n_count] = val;
            n_count++;
        }
    }

    glBindTexture(GL_TEXTURE_2D, verticesCoordTextureId );

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8815,
                 textureWidth,
                 textureHeight,
                 0,
                 GL_RGB,
                 GL_FLOAT,
                 translation_data);

    GetOpenGLError();

    delete [] translation_data;

    glBindTexture(GL_TEXTURE_2D, verticesNormalsTextureId);
    GetOpenGLError();

    glTexImage2D(GL_TEXTURE_2D,
                 0, 0x8814,
                 normalTWidth,
                 normalTHeight,
                 0,
                 GL_RGBA,
                 GL_FLOAT,
                 normals_data);

    GetOpenGLError();

    glBindTexture(GL_TEXTURE_2D, 0);
    GetOpenGLError();

    delete [] normals_data;

    GetOpenGLError();

}

void VisuMesh::clearTextures(){

    if(textureCreated){
        vboHandler.deleteVBO( verticesVBOId);
        vboHandler.deleteVBO( normalsVBOId);
        vboHandler.deleteVBO( indicesVBOId);
        vboHandler.deleteVBO( textureCoordsVBOId);

        glDeleteTextures(1, &verticesCoordTextureId);
        glDeleteTextures(1, &verticesNormalsTextureId);
        glDeleteTextures(1, &textureCoordsVBOId);
        glDeleteTextures(1, &tetNeighborsTextureId);
        glDeleteTextures(1, &tetNeighborsNbTextureId);
        glDeleteTextures(1, &visibilityTextureId);

        textureCreated = false;
    }
}


void VisuMesh::drawTetrahedra(){

#if 1
    //glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin( GL_TRIANGLES );

    glColor4f(0., 0.,0.,1.);

    for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++ ){
        const Tetrahedron & tetrahedron = tetrahedra[i];
        for( int f = 0 ; f < 4 ; f++ ){

            BasicPoint normal = cross(vertices[tetrahedron.getVertex(indices[f][1])] - vertices[tetrahedron.getVertex(indices[f][0])],
                    vertices[tetrahedron.getVertex(indices[f][2])] - vertices[tetrahedron.getVertex(indices[f][0])]);
            normal.normalize();

            glNormal3f(normal[0], normal[1], normal[2]);

            BasicPoint position[3] = {vertices[tetrahedron.getVertex(indices[f][0])],
                                      vertices[tetrahedron.getVertex(indices[f][1])],
                                      vertices[tetrahedron.getVertex(indices[f][2])]};
            glVertex3f(position[0][0], position[0][1], position[0][2]);
            glVertex3f(position[1][0], position[1][1], position[1][2]);
            glVertex3f(position[2][0], position[2][1], position[2][2]);


        }
    }
    glEnd();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBegin( GL_TRIANGLES );

    glColor4f(0.8, 0.,0.,1.);

    for(unsigned int i = 0 ; i < tetrahedra.size() ; i ++ ){
        const Tetrahedron & tetrahedron = tetrahedra[i];
        for( int f = 0 ; f < 4 ; f++ ){

            BasicPoint normal = cross(vertices[tetrahedron.getVertex(indices[f][1])] - vertices[tetrahedron.getVertex(indices[f][0])],
                    vertices[tetrahedron.getVertex(indices[f][2])] - vertices[tetrahedron.getVertex(indices[f][0])]);
            normal.normalize();

            glNormal3f(normal[0], normal[1], normal[2]);

            BasicPoint position[3] = {vertices[tetrahedron.getVertex(indices[f][0])],
                                      vertices[tetrahedron.getVertex(indices[f][1])],
                                      vertices[tetrahedron.getVertex(indices[f][2])]};
            glVertex3f(position[0][0], position[0][1], position[0][2]);
            glVertex3f(position[1][0], position[1][1], position[1][2]);
            glVertex3f(position[2][0], position[2][1], position[2][2]);
        }
    }
    glEnd();
#else
    if(!textureCreated) return ;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glEnable(GL_TEXTURE_2D);


    GLTools::SetDiffuse(0., 0., 0., 1.);

    shaderPool = ShaderPool::GetInstance();
    ShaderPackage * shaderPackage = shaderPool->GetTetrahedraPackage();
    ProgramObject * programObject= shaderPackage->GetProgram();

    //GPU start
    programObject->Begin();

    int iTextureSlot = 0;
    // uniform variables:


    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, verticesCoordTextureId);

    programObject->SetUniform1i( "vertices_translations", iTextureSlot++ );

    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, verticesNormalsTextureId);

    programObject->SetUniform1i( "normals_translations", iTextureSlot++ );

    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, texture3DCoordId);

    programObject->SetUniform1i( "texture_coordinates", iTextureSlot++ );

    programObject->SetUniform3f("cut", cut[0], cut[1], cut[2]);
    programObject->SetUniform3f("cutDirection", cutDirection[0], cutDirection[1], cutDirection[2]);


    //    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    //    // ...bind
    //    glBindTexture(GL_TEXTURE_2D, tetNeighborsNbTextureId);

    //    programObject->SetUniform1i( "neighbors_nb", iTextureSlot++ );

    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, tetNeighborsTextureId);

    programObject->SetUniform1i( "neighbors", iTextureSlot++ );

    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_3D, gridTexture->getTextureId());

    programObject->SetUniform1i("Mask", iTextureSlot++);

    /*
    programObject->SetUniform3f("clippingPoint", pointOnClipping[0], pointOnClipping[1], pointOnClipping[2]);
    programObject->SetUniform3f("clippingNormal", clippingNormal[0], clippingNormal[1], clippingNormal[2]);
    programObject->SetUniform3f("cut", cut[0], cut[1], cut[2]);
    programObject->SetUniform3f("cutDirection", cutDirection[0], cutDirection[1], cutDirection[2]);
*/

    //programObject->SetUniform3f("cam", cam[0], cam[1], cam[2]);

    // before draw, specify vertex and index arrays with their offsets
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, normalsVBOId);
    glNormalPointer(GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, verticesVBOId);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indicesVBOId);

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, textureCoordsVBOId);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);

    GetOpenGLError();

    glPushClientAttrib( GL_CLIENT_VERTEX_ARRAY_BIT );
    // start to render polygons
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);


    //   std::cout << "tWidth "<< tWidth << std::endl;
    programObject->SetUniform1i("width", textureWidth);
    programObject->SetUniform1i("neighbor_width", neighborTWidth);
    programObject->SetUniform1i("neighbor_nb_width", neighborNbTWidth);

    programObject->SetUniform1f("dx", gridTexture->dx());
    programObject->SetUniform1f("dy", gridTexture->dy());
    programObject->SetUniform1f("dz", gridTexture->dz());

    programObject->SetUniform1f("diffuseRef", 0.8f);
    programObject->SetUniform1f("specRef", 1.5f);
    programObject->SetUniform1f("shininess", 128.0f);
    //    programObject->SetUniform1i("solid", solid);
    //    programObject->SetUniform1i("visibility_check", visibility_check);
    //    programObject->SetUniform1i("normalDirection", normalDirection);


    GLTools::SetDiffuse(1., 0., 0.);
    glColor3f(1.,1.,1.);
    glDrawElementsInstancedEXT( GL_TRIANGLES, 12, GL_UNSIGNED_SHORT, GL_BUFFER_OFFSET(0), tetrahedra.size() );


    glDisableClientState(GL_VERTEX_ARRAY);	// disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);	// disable normal arrays
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    //    glDisableVertexAttribArray( translation );

    glPopClientAttrib();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    programObject->End();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);

    for ( int i=0; i<iTextureSlot; ++i ) {
        glActiveTexture( GL_TEXTURE0 + i );
        glDisable( GL_TEXTURE_2D );
        glDisable( GL_TEXTURE_3D );
    }
    glActiveTexture( GL_TEXTURE0 );
#endif
}

void VisuMesh::draw( const qglviewer::Vec & cam ) {

    if(!textureCreated) return ;



    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glEnable(GL_TEXTURE_2D);


    GLTools::SetDiffuse(0., 0., 0., 1.);

    shaderPool = ShaderPool::GetInstance();
    ShaderPackage * shaderPackage = shaderPool->GetTrMeshPackage();
    ProgramObject * programObject= shaderPackage->GetProgram();

    //GPU start
    programObject->Begin();

    int iTextureSlot = 0;
    // uniform variables:


    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, verticesCoordTextureId);

    programObject->SetUniform1i( "vertices_translations", iTextureSlot++ );

    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, verticesNormalsTextureId);

    programObject->SetUniform1i( "normals_translations", iTextureSlot++ );

    glActiveTexture( GL_TEXTURE0 + iTextureSlot );

    // ...bind
    glBindTexture(GL_TEXTURE_2D, visibilityTextureId);

    programObject->SetUniform1i( "visibility_texture", iTextureSlot++ );

    glActiveTexture( GL_TEXTURE0 + iTextureSlot );

    // ...bind
    glBindTexture(GL_TEXTURE_2D, texture3DCoordId);

    programObject->SetUniform1i( "texture_coordinates", iTextureSlot++ );

    programObject->SetUniform3f("cut", cut[0], cut[1], cut[2]);
    programObject->SetUniform3f("cutDirection", cutDirection[0], cutDirection[1], cutDirection[2]);
    programObject->SetUniform3f("clippingPoint", pointOnClipping[0], pointOnClipping[1], pointOnClipping[2]);
    programObject->SetUniform3f("clippingNormal", clippingNormal[0], clippingNormal[1], clippingNormal[2]);

    //    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    //    // ...bind
    //    glBindTexture(GL_TEXTURE_2D, tetNeighborsNbTextureId);

    //    programObject->SetUniform1i( "neighbors_nb", iTextureSlot++ );
    //
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_2D, tetNeighborsTextureId);

    programObject->SetUniform1i( "neighbors", iTextureSlot++ );

    // uniform variables:
    glActiveTexture( GL_TEXTURE0 + iTextureSlot );
    // ...bind
    glBindTexture(GL_TEXTURE_3D, gridTexture->getTextureId());

    programObject->SetUniform1i("Mask", iTextureSlot++);

    /*
    programObject->SetUniform3f("clippingPoint", pointOnClipping[0], pointOnClipping[1], pointOnClipping[2]);
    programObject->SetUniform3f("clippingNormal", clippingNormal[0], clippingNormal[1], clippingNormal[2]);
    programObject->SetUniform3f("cut", cut[0], cut[1], cut[2]);
    programObject->SetUniform3f("cutDirection", cutDirection[0], cutDirection[1], cutDirection[2]);
*/

    programObject->SetUniform3f("cam", cam[0], cam[1], cam[2]);

    // before draw, specify vertex and index arrays with their offsets
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, normalsVBOId);
    glNormalPointer(GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ARRAY_BUFFER_ARB, verticesVBOId);
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indicesVBOId);

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, textureCoordsVBOId);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);

    GetOpenGLError();

    glPushClientAttrib( GL_CLIENT_VERTEX_ARRAY_BIT );
    // start to render polygons
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);


    //   std::cout << "tWidth "<< tWidth << std::endl;
    programObject->SetUniform1i("width", textureWidth);
    programObject->SetUniform1i("neighbor_width", neighborTWidth);
    programObject->SetUniform1i("normal_width", normalTWidth);
    programObject->SetUniform1i("visibility_width", visibilityTWitdth);

    programObject->SetUniform1f("dx", gridTexture->dx());
    programObject->SetUniform1f("dy", gridTexture->dy());
    programObject->SetUniform1f("dz", gridTexture->dz());

    programObject->SetUniform1f("diffuseRef", 0.8f);
    programObject->SetUniform1f("specRef", 1.5f);
    programObject->SetUniform1f("shininess", 128.0f);
    //    programObject->SetUniform1i("solid", solid);
    //    programObject->SetUniform1i("visibility_check", visibility_check);
    //    programObject->SetUniform1i("normalDirection", normalDirection);


    GLTools::SetDiffuse(1., 0., 0.);
    glColor3f(1.,1.,1.);
    glDrawElementsInstancedEXT( GL_TRIANGLES, 12, GL_UNSIGNED_SHORT, GL_BUFFER_OFFSET(0), tetrahedra.size() );


    glDisableClientState(GL_VERTEX_ARRAY);	// disable vertex arrays
    glDisableClientState(GL_NORMAL_ARRAY);	// disable normal arrays
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    //    glDisableVertexAttribArray( translation );

    glPopClientAttrib();

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    programObject->End();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH);

    for ( int i=0; i<iTextureSlot; ++i ) {
        glActiveTexture( GL_TEXTURE0 + i );
        glDisable( GL_TEXTURE_2D );
        glDisable( GL_TEXTURE_3D );
    }
    glActiveTexture( GL_TEXTURE0 );

    glUseProgram(0);
    //  drawTetrahedra();
}
