#ifndef TETRALRISOLVER_H
#define TETRALRISOLVER_H

#include "CGALIncludes.h"
#include "CellInfo.h"
#include "Mesh_triangulation_with_info_3.h"

#include "gsl/gsl_linalg.h"

#include "CholmodLSStruct.h"

typedef CGAL::Mesh_triangulation_with_info_3<Mesh_domain, int, CellInfo, K>::type Triangulation;
class TetraLRISolver
{
private:
    // Cholmod stuff:
    CholmodLSStruct BasisSolver , VerticesSolver;

    // additional structure:
    // edges are needed for the setup of VerticesSolver.
    std::vector< std::pair< Triangulation::Vertex_handle ,  Triangulation::Vertex_handle > >    edges;

    // verts structure.
    std::vector< Triangulation::Vertex_handle >     verts_mapping_from_solver_to_mesh;
    std::vector< Point_3 >                          verts_initial_positions;
    std::vector< int >                              verts_mapping_from_mesh_to_solver;
    std::vector< int >                              verts_constraints;

    std::vector<Triangulation::Cell_handle>         tetrahedra;
    std::vector<int>                                handle_tetrahedra;
    std::vector<int>                                unknown_tetrahedra;

    void collect_edges( const Triangulation & triangulation , const std::vector<Triangulation::Cell_handle> & _tetrahedra );

    void collect_vertices( const std::vector<Triangulation::Cell_handle> & _tetrahedra , unsigned int nb_of_mesh_vertices );
    void collect_constraints_vertices( const std::vector<Triangulation::Cell_handle> & tetrahedra, const std::vector<int> & unknown_tets, const std::vector<bool> & unknown_vertices );

public:
    TetraLRISolver()
    {
        BasisSolver.start();
        VerticesSolver.start();
    }

    void load_tetrahedra( const Triangulation & triangulation ,
                          const std::vector<Triangulation::Cell_handle> & _tetrahedra,
                          const std::vector<int> & unknown_tets, const std::vector<int> & handle_tets ,
                          const std::vector<bool> & unknown_vertices );
    void update_constraints( const Triangulation & triangulation, const std::vector<bool> & _outlier_vertices );
    void clear();


};

#endif // TETRALRISOLVER_H
