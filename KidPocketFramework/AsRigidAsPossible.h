#ifndef ASRIGIDASPOSSIBLE_H
#define ASRIGIDASPOSSIBLE_H
#include "GLUtilityMethods.h"
#include "cholmod.h"

#include "BasicPoint.h"
#include "Edge.h"
#include "Triangle.h"
#include "gsl/gsl_linalg.h"

class AsRigidAsPossible
{
public:
    AsRigidAsPossible();

    ~AsRigidAsPossible();
    void init( const std::vector<BasicPoint> & _vertices, const std::vector< Triangle > & _triangles);
    void init( const std::vector<BasicPoint> & _vertices, const std::vector< std::vector <int> > & _triangles );

    void setVertices( const std::vector<BasicPoint> & _vertices );

    inline std::vector<BasicPoint> & getVertices(){ return vertices; }
    inline const std::vector<BasicPoint> getVertices() const { return vertices; }

    inline std::vector< bool > & getHandles(){ return handles; }
    inline const std::vector< bool > & getHandles() const { return handles; }

    void setHandles(const std::vector< bool > & _handles);
    void compute_deformation(std::vector<BasicPoint> & positions);

    void setIterationNb(int itNb){ iterationNb = itNb; }
    void draw();
    void clear();
protected:

    void compute_product_and_sum( gsl_matrix * R, const BasicPoint & point, BasicPoint & result );
    void compute_S( gsl_matrix * S , unsigned int vi, const std::vector<BasicPoint> & pdef);
    void compute_R( gsl_matrix * R , gsl_matrix * U, gsl_matrix * V );
    float compute_determinant( gsl_matrix * M );
    void singular_value_decomposition( gsl_matrix * matrix, gsl_matrix * U, gsl_matrix * V );
    void factorize_cholmod_A_system();void add_A_coeff( const int row , const int col , const double value );
    void update_A_coeff( const int i , const double value );
    void set_b_value( const int i , const BasicPoint & value );
    cholmod_dense* solve_cholmod();
    void allocates_cholmod_A_and_b(  );
    void fill_cholmod_A(  );
    void setDefaultRotations();

    int constrainedNb;
    // PARTIE CHOLMOD , ininteressante //
    int _rows;
    int _cols;
    cholmod_triplet *_triplet;
    int *_rowPtrA;
    int *_colPtrA;
    double *_valuePtrA;

    cholmod_sparse *_At;

    cholmod_factor *_L;

    cholmod_dense *_b;
    double *_valuePtrB;

    cholmod_common _c;

    int _nb_non_zeros_in_A;

    int step;

    bool computeCotangent;
    bool computeWithHandles;

    bool data_loaded;

    /////////////////////////////////////
    int iterationNb;
    std::vector< BasicPoint > vertices;
    CotangentWeights edgesWeightMap;
    std::map<Edge, BasicPoint, compareEdge> bij;
    std::vector< bool > handles;
    std::vector< std::vector<unsigned int> > oneRing;
    std::vector<gsl_matrix *> R;
    std::vector<float> sumWij;

};

#endif // ASRIGIDASPOSSIBLE_H
