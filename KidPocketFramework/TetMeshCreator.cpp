#include "TetMeshCreator.h"


// Sizing field
struct Motion_adaptive_sizing_field
{
    typedef ::FT FT;

    typedef Mesh_domain::Index Index;

    float value;

    int xdim, ydim;
    BasicPoint _d;

    std::vector<float> * size_field;
    FT operator()(const Point_3& p, const int, const Index&) const
    {

        int i = (int) (p.x()/_d[0]);
        int j= (int) (p.y()/_d[1]);
        int k = (int)(p.z()/_d[2]);

        int index = i + j*xdim + k*xdim*ydim;

        float v = 1.;
        if(index >= 0 && index < size_field->size()){
            v = size_field->at(index);
        }
        return value * v;
    }
};



TetMeshCreator::TetMeshCreator(CMInterface< BasicPoint > * cageInterface, VoxelGrid * voxel_grid) : cageInterface(cageInterface), voxel_grid(voxel_grid) {

    BBMax = voxel_grid->getWorldCoordinate(voxel_grid->xdim()-1, voxel_grid->ydim()-1, voxel_grid->zdim()-1);
    BBMin = BasicPoint(0.,0.,0.);


    dataLoaded = true;
    indices[0][0] = 3; indices[0][1] = 1; indices[0][2] = 2;
    indices[1][0] = 3; indices[1][1] = 2; indices[1][2] = 0;
    indices[2][0] = 3; indices[2][1] = 0; indices[2][2] = 1;
    indices[3][0] = 2; indices[3][1] = 1; indices[3][2] = 0;

    distortionType = CELL_BASED;
    samplingType = EDGE_MID_POINT;

    transfer_mesh = new Triangulation();

    outlierChecked = false;
    visibility_check = true;
    isAutomaticSeparation = false;
}

void TetMeshCreator::computeInitialTetBB( const Triangulation::Cell_handle & ch, BasicPoint & bb_min, BasicPoint & bb_max ){

    bb_min = BasicPoint( FLT_MAX, FLT_MAX, FLT_MAX);
    bb_max = BasicPoint(-FLT_MAX,-FLT_MAX,-FLT_MAX);
    for(unsigned int i = 0 ; i < 4 ; i ++ ){
        const Point_3 & point = initial_vertices_positions[ch->vertex(i)->info()];
        for(unsigned int j = 0 ; j < 3 ; j ++ ){
            bb_min[j] = std::min(double(bb_min[j]), point[j]);
            bb_max[j] = std::max(double(bb_max[j]), point[j]);
        }
    }

}

void TetMeshCreator::computeDeformedTetBB( const Triangulation::Cell_handle & ch, BasicPoint & bb_min, BasicPoint & bb_max  ){

    bb_min = BasicPoint( FLT_MAX, FLT_MAX, FLT_MAX);
    bb_max = BasicPoint(-FLT_MAX,-FLT_MAX,-FLT_MAX);
    for(unsigned int i = 0 ; i < 4 ; i ++ ){
        const Point_3 & point = ch->vertex(i)->point();
        for(unsigned int j = 0 ; j < 3 ; j ++ ){
            if(bb_min[j] > point[j]) bb_min[j] = point[j];
            if(bb_max[j] < point[j]) bb_max[j] = point[j];
        }
    }


}

void TetMeshCreator::clear(){

    clear_triangulations();

    from_poly_c3t3 = C3t3_with_info();
    c3t3_with_info = C3t3_with_info();

    size_field.clear();
    points.clear();


    BBMin = BasicPoint(0.,0.,0.);
    BBMax = BasicPoint(0.,0.,0.);

    _r = 0.;

    dataLoaded = false;

    defStart = Triangulation::Cell_handle();

    computationTime = 0.;


}


void TetMeshCreator::clear_triangulations(){

    if( transfer_mesh->number_of_vertices() > 0 ){
        transfer_mesh->clear();
    }

    vertices_normals.clear();
    initial_vertices_positions.clear();

    defStart = Triangulation::Cell_handle();
    initPointBaryCoords.clear();

    vertices_gradient.clear();

    while (!vertex_error_queue.empty() )
        vertex_error_queue.pop();

    while (!cell_error_queue.empty() )
        cell_error_queue.pop();

    outlierChecked = false;
    outlier_vertices.clear();

    suggestion.clear();

    vertices_normals.clear();

    tetraLRISolver.clear();

    cageInterface->clear_mesh_info();

}


void TetMeshCreator::createFromPolygon(double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize){

    Input_Polyhedron polyhedron;
    surfaceToPolyhedron(cageInterface->get_cage_vertices(), cageInterface->get_cage_triangles(), polyhedron );

    // Create domain
    Polyhedral_mesh_domain domain(polyhedron);


    // Mesh criteria
    Triangulation_mesh_criteria criteria (facet_angle=facetAngle, facet_size=facetSize, facet_distance=facetApproximation,
                                          cell_radius_edge_ratio=cellRatio, cell_size=cellSize );

    // Mesh generation
    from_poly_c3t3 = CGAL::make_mesh_3<C3t3_with_info>(domain, criteria);
    //
    //    // Output
    //    std::ofstream medit_file("out.mesh");
    //    from_poly_c3t3.output_to_medit(medit_file);


}

// You need to have the correct QGLContext activated for that !!!!!!!!
// This function select the cage vertices drawn inside the QRect "zone"
void TetMeshCreator::select( const Separation_info & separation_info, std::vector<bool> & selected_vertices  )
{

    selected_vertices.clear();
    selected_vertices.resize (transfer_mesh->number_of_vertices(), false );

    for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end() ; ++vit)
    {
        if( c3t3_with_info.in_dimension(vit) == 2 ){
            BasicPoint const & p = to_basic_point(vit->point());

            float x = separation_info.modelview[0] * p[0] + separation_info.modelview[4] * p[1] + separation_info.modelview[8] * p[2] + separation_info.modelview[12];
            float y = separation_info.modelview[1] * p[0] + separation_info.modelview[5] * p[1] + separation_info.modelview[9] * p[2] + separation_info.modelview[13];
            float z = separation_info.modelview[2] * p[0] + separation_info.modelview[6] * p[1] + separation_info.modelview[10] * p[2] + separation_info.modelview[14];
            float w = separation_info.modelview[3] * p[0] + separation_info.modelview[7] * p[1] + separation_info.modelview[11] * p[2] + separation_info.modelview[15];
            x /= w;
            y /= w;
            z /= w;
            w = 1.f;

            float xx = separation_info.projection[0] * x + separation_info.projection[4] * y + separation_info.projection[8] * z + separation_info.projection[12] * w;
            float yy = separation_info.projection[1] * x + separation_info.projection[5] * y + separation_info.projection[9] * z + separation_info.projection[13] * w;
            float ww = separation_info.projection[3] * x + separation_info.projection[7] * y + separation_info.projection[11] * z + separation_info.projection[15] * w;
            xx /= ww;
            yy /= ww;

            xx = ( xx + 1.f ) / 2.f;
            yy = 1.f - ( yy + 1.f ) / 2.f;

            if( separation_info.zone.contains( xx , yy ) ){
                selected_vertices[ vit->info() ] = true;
            }
        }
    }
}

void TetMeshCreator::addNewSeparation( const Separation_info & separation_info ){
    if( transfer_mesh->number_of_vertices() == 0 ) return ;
    std::vector<bool> selected_vertices ;
    select(separation_info, selected_vertices);
    separateLabels(separation_info, selected_vertices);
    separation_information.push_back(separation_info);
}

void TetMeshCreator::surfaceToPolyhedron(const std::vector<BasicPoint> & vertices, const std::vector< std::vector<int> > & triangles, Input_Polyhedron & polyhedron ){

    std::vector<Point_3> P;
    for(unsigned int i = 0 ; i < vertices.size() ; i ++ ){
        P.push_back(to_point_3(vertices[i]));
    }

    for(unsigned int i = 0 ; i < triangles.size() ; i ++ ){
        const std::vector<int> & triangle = triangles[i];
        polyhedron.make_triangle(P[triangle[0]], P[triangle[1]], P[triangle[2]]);
    }

}

void TetMeshCreator::clearPointDistribution(){
    points.clear();
}

void TetMeshCreator::createPoissonPointDistribution(unsigned int pointNb, float r){

    if( r > _r || pointNb < points.size())
        clearPointDistribution();

    _r = r;

    BasicPoint scale = (BBMax-BBMin);

    float radius = scale[0];
    std::min(radius, scale[1]);
    std::min(radius, scale[2]);

    radius = (radius/2.)*r;

    while( points.size() < pointNb ){

        BasicPoint point = BasicPointM::random(1.);
        for( int i = 0 ; i < 3 ; i ++ )
            point[i] = fabs(point[i]* scale[i]);

        point += BBMin;

        Voxel grid_coords = voxel_grid->getGridCoordinate(point);

        if( voxel_grid->is_in_offseted_domains(grid_coords.i(), grid_coords.j(), grid_coords.k())){
            bool keep = true;
            for( unsigned int i = 0 ; i < points.size() ; i ++ ){
                if((point - points[i]).norm() < radius){
                    keep = false;
                    break;
                }
            }

            if(keep) points.push_back(point);
        }
    }

}

bool TetMeshCreator::isPointInCageTetMesh(const Point_3 & point, Triangulation::Cell_handle & ch, Triangulation::Cell_handle start){
    Triangulation::Locate_type locate_type;

    int li, lj;
    ch = from_poly_c3t3.triangulation().locate(point, locate_type, li, lj, start);

    if(locate_type ==  Triangulation::CELL ) {
        if( from_poly_c3t3.is_in_complex(ch) ) {
            return true;
        }
    } else if( locate_type == Triangulation::VERTEX ){
        if( from_poly_c3t3.in_dimension(ch->vertex(li)) == 3 ) return true;
    } else if( locate_type == Triangulation::EDGE ) {
        if( from_poly_c3t3.in_dimension(ch->vertex(li)) == 3 || from_poly_c3t3.in_dimension(ch->vertex(lj)) == 3 ) return true;
    }else if( locate_type == Triangulation::FACET ) {
        for(int i = 0 ; i < 3 ; i ++ )
            if( from_poly_c3t3.in_dimension(ch->vertex(indices[li][i])) == 3 ) return true;
    }

    return false;
}

bool TetMeshCreator::isPointInCageTetMesh(const BasicPoint & point, Triangulation::Cell_handle start){
    Triangulation::Cell_handle ch;
    return isPointInCageTetMesh(to_point_3(point), ch, start);
}

bool TetMeshCreator::isPointInCageTetMesh(const Point_3 & point, Triangulation::Cell_handle start){
    Triangulation::Cell_handle ch;
    return isPointInCageTetMesh(point, ch, start);
}

void TetMeshCreator::checkAndMarkOutliers(){
    std::cout << "Checking for ouliers" << std::endl;

    BasicPoint bb, BB;
    cageInterface->getCageBBox(bb, BB);

    float error_max = (BB - bb).norm()/1000.;
    outlier_vertices.clear();
    outlier_vertices.resize(transfer_mesh->number_of_vertices(), false);
    bool outlierDetected = false;
    int outlierCount = 0;
    for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end() ; ++vit){
        int id = vit->info();
        BasicPoint position = to_basic_point(initial_vertices_positions[id]);
        BasicPoint up_position;
        //std::cout << "id " << id << std::endl;
        cageInterface->getVertexInitialPosition(id, up_position);
        BasicPoint diff = ( position - up_position );
        if ( ! diff.isNull() && (diff.norm() > error_max) ){
            //  std::cout << "f " << f << std::endl ;
            outlier_vertices[id] = true;
            outlierDetected = true;
            outlierCount++;
        }
    }

    std::cout << outlierCount << " ouliers marked" << std::endl;

    if(!outlierDetected) outlier_vertices.clear();
    else {
        for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
            cit->info().set_is_cage_inlier(true);
            for( int i = 0 ; i < 4 ; i++ ){
                if( outlier_vertices[cit->vertex(i)->info()] ){
                    cit->info().set_is_cage_inlier(false);
                    break;
                }
            }
        }
        initializeLRISolver();
    }

    outlierChecked = true;

    std::cout << "Solver initialized" << std::endl;
}

bool TetMeshCreator::isPointInCell( const Point_3 & p, const Triangulation::Cell_handle & c, Triangulation::Locate_type & lt, int & li, int & lj){

    Geom_traits::Tetrahedron_3 tetrahedron( c->vertex(0)->point(), c->vertex(1)->point(), c->vertex(2)->point(), c->vertex(3)->point() );
    return !tetrahedron.has_on_unbounded_side(p);

    // Stores the results of the 4 orientation tests.  It will be used
    // at the end to decide if p lies on a face/edge/vertex/interior.
    CGAL::Orientation o[4];

    const Point_3 &p0 = c->vertex(0)->point();
    const Point_3 &p1 = c->vertex(1)->point();
    const Point_3 &p2 = c->vertex(2)->point();
    const Point_3 &p3 = c->vertex(3)->point();

    // We know that the 4 vertices of c are positively oriented.
    // So, in order to test if p is seen outside from one of c's facets,
    // we just replace the corresponding point by p in the orientation
    // test.  We do this using the array below.
    const Point_3* pts[4] = { &(p0),
                              &(p1),
                              &(p2),
                              &(p3)};



    for (int i=0; i < 4; i++) {
        // We temporarily put p at i's place in pts.
        const Point_3* backup = pts[i];
        pts[i] = &p;
        o[i] = transfer_mesh->geom_traits().orientation_3_object()(*pts[0], *pts[1], *pts[2], *pts[3]);
        if ( o[i] == CGAL::NEGATIVE ) {
            pts[i] = backup;
            return false;
        }
    }


    int sum = ( o[0] == CGAL::COPLANAR )
            + ( o[1] == CGAL::COPLANAR )
            + ( o[2] == CGAL::COPLANAR )
            + ( o[3] == CGAL::COPLANAR );
    switch (sum) {
    case 0:
    {
        lt = Triangulation::CELL;
        break;
    }
    case 1:
    {
        lt = Triangulation::FACET;
        li = ( o[0] == CGAL::COPLANAR ) ? 0 :
                                          ( o[1] == CGAL::COPLANAR ) ? 1 :
                                                                       ( o[2] == CGAL::COPLANAR ) ? 2 : 3;
        break;
    }
    case 2:
    {
        lt = Triangulation::EDGE;
        li = ( o[0] != CGAL::COPLANAR ) ? 0 :
                                          ( o[1] != CGAL::COPLANAR ) ? 1 : 2;
        lj = ( o[li+1] != CGAL::COPLANAR ) ? li+1 :
                                             ( o[li+2] != CGAL::COPLANAR ) ? li+2 : li+3;
        CGAL_triangulation_assertion(collinear( p,
                                                c->vertex( li )->point(),
                                                c->vertex( lj )->point()));
        break;
    }
    case 3:
    {
        lt = Triangulation::VERTEX;
        li = ( o[0] != CGAL::COPLANAR ) ? 0 :
                                          ( o[1] != CGAL::COPLANAR ) ? 1 :
                                                                       ( o[2] != CGAL::COPLANAR ) ? 2 : 3;
        break;
    }
    }

}

bool TetMeshCreator::locate( const Point_3 & p, Triangulation::Locate_type & lt, int & li, int & lj, Triangulation::Cell_handle & ch, Triangulation::Cell_handle start, std::vector<bool> & visited ){

    // Make sure we continue from here with a finite cell.
    if ( start == Triangulation::Cell_handle() )
        start = transfer_mesh->infinite_cell();

    int ind_inf;
    if ( start->has_vertex(transfer_mesh->infinite_vertex(), ind_inf) )
        start = start->neighbor(ind_inf);

    if( !is_in_complex(start) ){
        for( int i = 0 ; i < 4 ; i ++ ){
            if( is_in_complex(start->neighbor(i)) ){
                start = start->neighbor(i) ;
                break;
            }
        }
    }

    assert(is_in_complex(start));


    std::queue<Triangulation::Cell_handle> cell_list;

    for(unsigned int i = 0 ; i < visited.size(); i++){
        visited[i] = false ;
    }

    cell_list.push(start);
    visited[start->info().index()] = true;
    
    while( !cell_list.empty() ){

        const Triangulation::Cell_handle & c = cell_list.front();
        cell_list.pop();

        if( isPointInCell(p, c, lt, li, lj) ){
            ch = c;
            return true;
        }

        for( int i = 0 ; i < 4 ; i++ ){
            if( is_in_complex( c->neighbor(i) ) && ! visited[c->neighbor(i)->info().index()]){
                visited[c->neighbor(i)->info().index()] = true;
                cell_list.push(c->neighbor(i));
            }
        }
    }

    return false;
}

Triangulation::Cell_handle TetMeshCreator::locate(const Point_3 & p, Triangulation::Locate_type & lt, int & li, int & lj, Triangulation::Cell_handle start )
// returns the (finite or infinite) cell p lies in
// starts at cell "start"
// if lt == OUTSIDE_CONVEX_HULL, li is the
// index of a facet separating p from the rest of the triangulation
// in dimension 2 :
// returns a facet (Cell_handle,li) if lt == FACET
// returns an edge (Cell_handle,li,lj) if lt == EDGE
// returns a vertex (Cell_handle,li) if lt == VERTEX
// if lt == OUTSIDE_CONVEX_HULL, li, lj give the edge of c
// separating p from the rest of the triangulation
// lt = OUTSIDE_AFFINE_HULL if p is not coplanar with the triangulation
{


    // Make sure we continue from here with a finite cell.
    if ( start == Triangulation::Cell_handle() )
        start = transfer_mesh->infinite_cell();

    int ind_inf;
    if ( start->has_vertex(transfer_mesh->infinite_vertex(), ind_inf) )
        start = start->neighbor(ind_inf);


    if( !is_in_complex(start) ){
        for( int i = 0 ; i < 4 ; i ++ ){
            if( is_in_complex(start->neighbor(i)) ){
                start = start->neighbor(i) ;
                break;
            }
        }
    }


    CGAL_triangulation_precondition( start != Triangulation::Cell_handle() );
    CGAL_triangulation_precondition( ! start->has_vertex(transfer_mesh->infinite_vertex()) );
    CGAL_triangulation_precondition( is_in_complex( start ) );
    // We implement the remembering visibility/stochastic walk.

    // Remembers the previous cell to avoid useless orientation tests.
    Triangulation::Cell_handle previous = Triangulation::Cell_handle();
    Triangulation::Cell_handle c = start;

    // Stores the results of the 4 orientation tests.  It will be used
    // at the end to decide if p lies on a face/edge/vertex/interior.
    CGAL::Orientation o[4];

    // Now treat the cell c.
try_next_cell:

    const Point_3 &p0 = c->vertex(0)->point();
    const Point_3 &p1 = c->vertex(1)->point();
    const Point_3 &p2 = c->vertex(2)->point();
    const Point_3 &p3 = c->vertex(3)->point();

    // We know that the 4 vertices of c are positively oriented.
    // So, in order to test if p is seen outside from one of c's facets,
    // we just replace the corresponding point by p in the orientation
    // test.  We do this using the array below.
    const Point_3* pts[4] = { &(p0),
                              &(p1),
                              &(p2),
                              &(p3)};

    // For the remembering stochastic walk,
    // we need to start trying with a random index :
    int i = rng.get_bits<2>();
    // For the remembering visibility walk (Triangulation only), we don't :
    // int i = 0;

    for (int j=0; j != 4; ++j, i = (i+1)&3) {
        Triangulation::Cell_handle next = c->neighbor(i);

        if (previous == next) {

            o[i] = CGAL::POSITIVE;

            continue;
        }
        // We temporarily put p at i's place in pts.
        const Point_3* backup = pts[i];
        pts[i] = &p;
        o[i] = transfer_mesh->geom_traits().orientation_3_object()(*pts[0], *pts[1], *pts[2], *pts[3]);
        if ( o[i] != CGAL::NEGATIVE ) {

            pts[i] = backup;

            continue;
        }
        if ( next->has_vertex(transfer_mesh->infinite_vertex(), li) ) {
            // We are outside the convex hull.
            lt = Triangulation::OUTSIDE_CONVEX_HULL;
            return next;
        }

        if ( !is_in_complex(next) ) {
            // We are outside the convex hull.
            lt = Triangulation::CELL;
            return next;
        }

        previous = c;
        c = next;
        goto try_next_cell;
    }


    int sum = ( o[0] == CGAL::COPLANAR )
            + ( o[1] == CGAL::COPLANAR )
            + ( o[2] == CGAL::COPLANAR )
            + ( o[3] == CGAL::COPLANAR );
    switch (sum) {
    case 0:
    {
        lt = Triangulation::CELL;
        break;
    }
    case 1:
    {
        lt = Triangulation::FACET;
        li = ( o[0] == CGAL::COPLANAR ) ? 0 :
                                          ( o[1] == CGAL::COPLANAR ) ? 1 :
                                                                       ( o[2] == CGAL::COPLANAR ) ? 2 : 3;
        break;
    }
    case 2:
    {
        lt = Triangulation::EDGE;
        li = ( o[0] != CGAL::COPLANAR ) ? 0 :
                                          ( o[1] != CGAL::COPLANAR ) ? 1 : 2;
        lj = ( o[li+1] != CGAL::COPLANAR ) ? li+1 :
                                             ( o[li+2] != CGAL::COPLANAR ) ? li+2 : li+3;
        CGAL_triangulation_assertion(collinear( p,
                                                c->vertex( li )->point(),
                                                c->vertex( lj )->point()));
        break;
    }
    case 3:
    {
        lt = Triangulation::VERTEX;
        li = ( o[0] != CGAL::COPLANAR ) ? 0 :
                                          ( o[1] != CGAL::COPLANAR ) ? 1 :
                                                                       ( o[2] != CGAL::COPLANAR ) ? 2 : 3;
        break;
    }
    }

    return c;

}

bool TetMeshCreator::isPointInTransferTetMesh(const Point_3 & point, Triangulation::Cell_handle & ch, Triangulation::Cell_handle start ){
    Triangulation::Locate_type locate_type;

    int li, lj;
    ch = transfer_mesh->locate(point, locate_type, li, lj, start);
    if(locate_type ==  Triangulation::CELL ) {
        return true;
    } else if( locate_type == Triangulation::FACET ){
        if(is_in_complex(ch))
            return true;
        else if( is_in_complex(ch->neighbor(li)) ){
            ch = ch->neighbor(li);
            return true;
        }
    } else if( locate_type == Triangulation::EDGE ){
        if(is_in_complex(ch))
            return true;

        Triangulation::Edge edge (ch, li,lj);
        Triangulation::Cell_circulator cell_circulator = transfer_mesh->incident_cells(edge);
        Triangulation::Cell_circulator done(cell_circulator);

        do{
            if(is_in_complex(cell_circulator)){
                ch = cell_circulator;
                return true;
            }
        }while (++cell_circulator != done);

    }  else if( locate_type == Triangulation::VERTEX ){

    }
    return false;
}

bool TetMeshCreator::isPointInTransferTetMesh(const BasicPoint & point, Triangulation::Cell_handle start){
    Triangulation::Cell_handle ch;
    return isPointInTransferTetMesh(to_point_3(point), ch, start);
}

bool TetMeshCreator::isPointInTransferTetMesh(const Point_3 & point, Triangulation::Cell_handle start){
    Triangulation::Cell_handle ch;
    return isPointInTransferTetMesh(point, ch, start);
}

void TetMeshCreator::computeErrorField(){
    std::cout << "compute error Field " << std::endl;
    error_field.clear();
    error_field.resize(size_field.size(), 0.);
    
    std::vector<int> c_count (transfer_mesh->number_of_vertices(), 0);
    std::vector<float> vertices_average_error (transfer_mesh->number_of_vertices(), 0);
    std::vector<float> vol_sum (transfer_mesh->number_of_vertices(), 0.);
    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){

        float cit_error = cit->info().distortion_laplacian();
        float volume = computeCellVolume(cit);

        for(int i = 0 ; i < 4; i ++){
            int id = cit->vertex(i)->info();

            vertices_average_error[id] += cit_error*volume;
            c_count[id]++;
            vol_sum[id] += volume;
        }
    }


    for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end() ; ++vit){
        int id = vit->info();
        if( c_count[id] > 0 ){
            vertices_average_error[id] /= vol_sum[id];
        }
    }

    c_count.clear();

    computeContiniousError( vertices_average_error );
    
}

void TetMeshCreator::computeContiniousError( const std::vector<float> & vertices_average_error , int iterations){

    std::cout << "compute error " << std::endl;
    QTime t;
    t.start();
    set_transfer_mesh_to_initial_state();


    Triangulation::Cell_handle start = defStart;

    min_size_value = FLT_MAX;
    max_size_value = -FLT_MAX;
    float average = 0.;
    int count;
    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
        BasicPoint tet_bb_min, tet_bb_max;

        computeInitialTetBB(cit, tet_bb_min, tet_bb_max);


        Voxel vmin = voxel_grid->getGridCoordinate(tet_bb_min);
        Voxel vmax = voxel_grid->getGridCoordinate(tet_bb_max);

        // if(result.isInGrid(vmin) && result.isInGrid(vmax)){

        for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
            for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
                for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){

                    BasicPoint position = voxel_grid->getWorldCoordinate(i,j,k);

                    GRID_TYPE value = voxel_grid->value(i,j,k);

                    if( value >= VoxelGrid::BACKGROUND_GRID_VALUE ){

                        float ld[4];
                        computeBarycentricCoordinates(cit, position, ld[0], ld[1], ld[2], ld[3]);

                        if(ld[0] >= 0 && ld[0] <=1. && ld[1] >= 0 && ld[1] <=1. && ld[2] >= 0 && ld[2] <=1. && ld[3] >= 0 && ld[3] <=1. ){

                            float v = 0.;
                            for(int n = 0 ; n < 4 ; n++)
                                v+= ld[n]*vertices_average_error[cit->vertex(n)->info()];
                            error_field[voxel_grid->getGridIndex(i,j,k)] = v;
                            // std::cout << pos << std::endl;

                            average+= v;
                            count ++;
                        }
                    }
                }

            }
        }
    }
    average /= count;
    std::cout << "Average " << average << std::endl;
    for( unsigned int i = 0 ; i < size_field.size() ; i ++ ){
        if( voxel_grid->is_in_offseted_domains(i) ){
            size_field[i] *= pow(average / std::max(error_field[i], average), iterations/3. );
            // std::cout <<error_field[i] << std::endl;
        }
    }

    std::cout << "Time elapsed for sizing field computation: " << t.elapsed() << " ms" << std::endl;
    std::cout << "Sizing field values: " << min_size_value <<std::endl;
}
void TetMeshCreator::computeContiniousSizeField( const std::vector<float> & vertices_average_sizing ){

    std::cout << "computeSizeField() " << std::endl;
    QTime t;
    t.start();
    set_transfer_mesh_to_initial_state();

    
    Triangulation::Cell_handle start = defStart;

    min_size_value = FLT_MAX;
    max_size_value = -FLT_MAX;

#if 1
    std::vector<Voxel> to_average;
    std::vector<char> visited(size_field.size(), 0);
    int startY = 0, incrY = 1;
    int startZ = 0, incrZ = 1;
    for( unsigned int i = 0 ; i < voxel_grid->xdim() ; i ++ ){
        for( int j = startY ; j >= 0 && j < voxel_grid->ydim() ; j += incrY ){
            for( int k = startZ ; k >= 0 && k < voxel_grid->zdim() ; k += incrZ ){
                BasicPoint position = voxel_grid->getWorldCoordinate(i,j,k);

                Triangulation::Cell_handle ch;

                if(voxel_grid->is_in_offseted_domains(i,j,k) ){
                    if(locatePointInComplex(position, ch, start)){
                        float bary[4];
                        computeBarycentricCoordinates(ch, position, bary[0], bary[1], bary[2], bary[3]);

                        float v = 0.;

                        for( int k = 0 ; k < 4 ; k++ ){
                            v += bary[k]* vertices_average_sizing[ch->vertex(k)->info()];
                        }

                        int id = voxel_grid->getGridIndex(i,j,k);
                        size_field[id] *= v;
                        min_size_value = std::min(size_field[id], min_size_value);
                        max_size_value = std::max(size_field[id], max_size_value);
                        start = ch;
                        visited[id] = 1;

                    } else {
                        to_average.push_back(Voxel(i,j,k));
                    }
                }
            }
            incrZ *= -1;
            if(startZ == 0) startZ = voxel_grid->zdim() - 1;
            else startZ = 0;
        }
        incrY *= -1;
        if(startY == 0) startY = voxel_grid->ydim() - 1;
        else startY = 0;
    }

    for( unsigned int i = 0 ; i < to_average.size(); i++ ){
        Voxel & voxel = to_average[i];

        Voxel vmin, vmax;
        voxel_grid->getNeighborhoodBorns(voxel, vmin, vmax, 1 );

        float sum = 0.;
        int count = 0;
        for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
            for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
                for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){
                    int id = voxel_grid->getGridIndex(i,j,k);
                    if((voxel.i() != i || voxel.j() != j || voxel.k() != k) && visited[id] ==1){
                        count++;
                        sum += size_field[id];
                    }
                }
            }
        }
        if( count > 0 ) {
            int id = voxel_grid->getGridIndex(voxel);
            size_field[id] = sum /count;
            min_size_value = std::min(size_field[id], min_size_value);
            max_size_value = std::max(size_field[id], max_size_value);
        }

    }
    std::cout << "Time elapsed for sizing field computation: " << t.elapsed() << " ms" << std::endl;
    std::cout << "Sizing field values: " << min_size_value << " - " << max_size_value << std::endl;
#else

    std::vector<char> visited(size_field.size(), 0);

    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
        BasicPoint tet_bb_min, tet_bb_max;

        computeInitialTetBB(cit, tet_bb_min, tet_bb_max);
        //        Voxel vmin (std::max(0, int(bb_min[0]), std::max(0, int(bb_min[1]), std::max(0, int(bb_min[2]));
        //        Voxel vmax (std::min(result.xdim()-1, int(bb_max[0]), std::min(result.ydim()-1, int(bb_max[1]), std::max(result.zdim()-1, int(bb_max[2]));


        Voxel vmin = voxel_grid->getGridCoordinate(tet_bb_min);
        Voxel vmax = voxel_grid->getGridCoordinate(tet_bb_max);


        for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
            for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
                for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){

                    BasicPoint position = voxel_grid->getWorldCoordinate(i,j,k);
                    int g_id = voxel_grid->getGridIndex(i,j,k);
                    if(voxel_grid->is_in_offseted_domains(i,j,k) && visited[g_id] == 0){

                        float ld[4];
                        computeBarycentricCoordinates(cit, position, ld[0], ld[1], ld[2], ld[3]);
                        if(ld[0] >= 0 && ld[0] <=1. && ld[1] >= 0 && ld[1] <=1. && ld[2] >= 0 && ld[2] <=1. && ld[3] >= 0 && ld[3] <=1. ){
                            float v = 0.;

                            for( int k = 0 ; k < 4 ; k++ ){
                                v += ld[k]* vertices_average_sizing[cit->vertex(k)->info()];
                            }

                            //                    float min_coord = bary[0];
                            //                    int id_min = 0;
                            //                    for(int i = 1; i < 4; i++){
                            //                        if(min_coord > bary[i]){
                            //                            id_min = i;
                            //                            min_coord = bary[i];
                            //                        }
                            //                    }



                            size_field[g_id] *= v;
                            min_size_value = std::min(size_field[g_id], min_size_value);
                            max_size_value = std::max(size_field[g_id], max_size_value);

                            visited[g_id] = 1;
                        }
                    }
                }

            }
        }
    }
    visited.clear();
#endif

    std::cout << "computeSizeField()::end " << std::endl;
}

GRID_TYPE TetMeshCreator::value(int i){
    GRID_TYPE v = voxel_grid->value(i);

    if( v >= VoxelGrid::BACKGROUND_GRID_VALUE ){
        float s = size_field[i];

        s = (s-min_size_value) / (max_size_value - min_size_value);

        v = GRID_TYPE(1 + s*100);
    }

    return v;

}

void TetMeshCreator::computeSizeField(){

    std::cout << "computeSizeField() " << std::endl;
    set_transfer_mesh_to_initial_state();
    
    Triangulation::Cell_handle start = defStart;
    min_size_value = FLT_MAX;
    max_size_value = -FLT_MAX;

    int startY = 0, incrY = 1;
    int startZ = 0, incrZ = 1;
    for( unsigned int i = 0 ; i < voxel_grid->xdim() ; i ++ ){
        for( int j = startY ; j >= 0 && j < voxel_grid->ydim() ; j += incrY ){
            for( int k = startZ ; k >= 0 && k < voxel_grid->zdim() ; k += incrZ ){
                BasicPoint position = voxel_grid->getWorldCoordinate(i,j,k);

                Triangulation::Cell_handle ch;

                if(voxel_grid->is_in_offseted_domains(i,j,k) && locatePointInComplex(position, ch, start)){
                    float v = 1.;
                    if( ch->info().distortion_laplacian() > refinement_limit )
                        v = 0.5;

                    int id = voxel_grid->getGridIndex(i,j,k);

                    size_field[id] *= v;

                    min_size_value = std::min(size_field[id], min_size_value);
                    max_size_value = std::max(size_field[id], max_size_value);

                    start = ch;
                    // si = 1;
                }
            }
            incrZ *= -1;
            if(startZ == 0) startZ = voxel_grid->zdim() - 1;
            else startZ = 0;
        }
        incrY *= -1;
        if(startY == 0) startY = voxel_grid->ydim() - 1;
        else startY = 0;
    }

    std::cout << "computeSizeField()::end " << std::endl;
}


void TetMeshCreator::transferMeshFromImgAdaptativeSize(Image & image, double sizeMul, double percentage, bool optimize){

    initializeErrorQueue();

    QTime t;
    t.start();

    int count = 0;



    bool use_sizing_field = true;
    if(use_sizing_field){
        std::vector<float> vertices_average_sizing(transfer_mesh->number_of_vertices(), 0.);
        std::vector<int> c_count(transfer_mesh->number_of_vertices(), 0);


        while(!cell_error_queue.empty()){

            std::pair< float, Triangulation::Cell_handle > distCell = cell_error_queue.top();
            cell_error_queue.pop();

            float v = 1.;
            if(int(transfer_mesh->number_of_cells()*percentage) > count){
                refinement_limit = distCell.first;
                count ++;
                v=sizeMul;
            }

            for(int i = 0 ; i < 4; i ++){
                int id = distCell.second->vertex(i)->info();
                vertices_average_sizing[id] += v;
                c_count[id]++;
            }

        }

        std::cout<< "Objectif gradient " << refinement_limit << std::endl ;

        for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end() ; ++vit){
            int id = vit->info();
            if( c_count[id] > 0 ){
                vertices_average_sizing[id] /= c_count[id];
            }
        }
        c_count.clear();

        computeContiniousSizeField( vertices_average_sizing );

        vertices_average_sizing.clear();
    } else {
        computeErrorField();
    }

    Mesh_domain domain(image);
    // Mesh criteria
    Motion_adaptive_sizing_field size;

    size.size_field = &size_field;

    size.xdim = voxel_grid->xdim();
    size.ydim = voxel_grid->ydim();

    for(int i = 0; i < 3; i++){
        size._d[i] = voxel_grid->d(i);
    }

    size.value = initial_sizing_field.cell_value;
    Triangulation_mesh_criteria criteria (facet_angle=initial_sizing_field.angle, facet_size=initial_sizing_field.facet_value, facet_distance=initial_sizing_field.approximation,
                                          cell_radius_edge_ratio=initial_sizing_field.ratio, cell_size=size );

    // Meshing
    c3t3_with_info = CGAL::make_mesh_3<C3t3_with_info>(domain, criteria, no_exude());


    std::cout << "Adaptive mesh generated: " << c3t3_with_info.triangulation().number_of_vertices() << " vertices - " << c3t3_with_info.number_of_cells() << "  cells" << std::endl;
    if(optimize){
        std::cout << "Optimizing" << std::endl;
        CGAL::lloyd_optimize_mesh_3(c3t3_with_info, domain, 60., 100, 0.02, 0.01);
    }
    CGAL::exude_mesh_3(c3t3_with_info, 10, 120.);
    //  CGAL::exude_mesh_3(c3t3_with_info, 10, 120.);
    
    updateMeshFromC3t3(c3t3_with_info);

    if(isAutomaticSeparation){
        automaticSeparation();
    } else {
        for( unsigned int i = 0 ; i < separation_information.size() ; i ++ ){
            std::vector<bool> selected_vertices;
            select(separation_information[i], selected_vertices);
            separateLabels(separation_information[i], selected_vertices);
        }
    }
    for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin(); vit != transfer_mesh->finite_vertices_end() ; ++vit){
        const Point_3 & point = vit->point();
        BasicPoint defPosition;

        cageInterface->addModelVertexAndComputeCoordinates(to_basic_point(point), defPosition);
        //vit->set_point(to_point_3(defPosition));
    }
    std::cout <<"Compute motion adaptive mesh " << t.elapsed() << " ms " << std::endl;
    computationTime += t.elapsed();
    checkAndMarkOutliers();

    const std::vector<BasicPoint> & V = cageInterface->get_mesh_modified_vertices();
    if (V.size() == transfer_mesh->number_of_vertices()){
        for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){

            const BasicPoint & point = V[vit->info()];
            vit->set_point(to_point_3(point));
        }
    }

    computeBasisTransforms();

    updateVertices();

    std::cout << "v " << transfer_mesh->number_of_vertices() << " - " << transfer_mesh->number_of_cells() << std::endl;
}
void TetMeshCreator::exude(){
    CGAL::exude_mesh_3(c3t3_with_info, 10, 120);
}

void TetMeshCreator::automaticSeparation(  ){

    std::vector<bool> selected_vertices(transfer_mesh->number_of_vertices(), true);

    Separation_info s_i = Separation_info();
    s_i.labels.first = 4; s_i.labels.second = 9; separateLabels( s_i, selected_vertices );
    s_i.labels.first = 4; s_i.labels.second = 10; separateLabels( s_i, selected_vertices );
    s_i.labels.first = 4; s_i.labels.second = 11; separateLabels( s_i, selected_vertices );

    s_i.labels.first = 5; s_i.labels.second = 10; separateLabels( s_i, selected_vertices );
    s_i.labels.first = 5; s_i.labels.second = 11; separateLabels( s_i, selected_vertices );


    s_i.labels.first = 4; s_i.labels.second = 9; separateLabels( s_i, selected_vertices );
    s_i.labels.first = 4; s_i.labels.second = 10; separateLabels( s_i, selected_vertices );
    s_i.labels.first = 4; s_i.labels.second = 14; separateLabels( s_i, selected_vertices );

    s_i.labels.first = 8; s_i.labels.second = 10; separateLabels( s_i, selected_vertices );
    s_i.labels.first = 8; s_i.labels.second = 14; separateLabels( s_i, selected_vertices );

}

void TetMeshCreator::separateLabels( const Separation_info & separation_info, const std::vector<bool> & selected_vertices ){

#if 0
    std::vector<BasicPoint> vertices ( transfer_mesh->number_of_vertices() );
    std::vector<int> V ( transfer_mesh->number_of_vertices() ) ;
    std::vector<int> duplicate ( transfer_mesh->number_of_vertices(), -1 ) ;

    std::vector<bool> duplicate_cell(transfer_mesh->number_of_finite_cells(), false);
    bool to_update = false;


    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        const Point_3 & point = vit->point();
        vertices[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
        V[vit->info()] = vit->info();
    }

    int count = 0 ;
    for( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit, count++ ){
        cit->info().set_index(count);
    }

    //Mark vertices to be duplicated
    for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end() ; ++fit ){
        Surface_index si = c3t3_with_info.surface_index(*fit);

        char sep_info_ids[3];

        for( int i = 0 ; i < 3 ; i++ ){
            sep_info_ids[i] = selected_vertices[fit->first->vertex( indices[fit->second][i] )->info()];
        }

        bool to_separate = false;
        //Mark facet to be duplicated
        if( sep_info_ids[1] >= 0 && sep_info_ids[0] == sep_info_ids[1] && sep_info_ids[1] == sep_info_ids[2]   ){
            if( separation_information[sep_info_ids[1]].labels.first == si.first && separation_information[sep_info_ids[1]].labels.second == si.second ){
                //if( si.first != 0 ){
                std::cout << si.first << " - " << si.second << std::endl;
                C3t3_with_info::Facet facet = *fit;

                if( c3t3_with_info.subdomain_index(facet.first) != si.first )
                    facet = transfer_mesh->mirror_facet(facet);

                for( int i = 0 ; i < 3 ; i++ ){
                    const Triangulation::Vertex_handle vh = facet.first->vertex( indices[facet.second][i] );
                    //if( vh->info() == V[ vh->info() ] ){
                    V[ vh->info() ] = vertices.size();
                    const Point_3 & point = vh->point();
                    vertices.push_back(BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z())));
                    //}
                }

                duplicate_cell[ facet.first->info().index() ] = true;
                to_separate = true;
                to_update = true;
            }
        }

        //        if(to_separate){
        //            for( int i = 0 ; i < 3 ; i++ )
        //                duplicate[fit->first->vertex( indices[fit->second][i] )->info()] = 0;
        //        }
    }

    if( to_update ){

        //        for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        //            const Point_3 & point = vit->point();
        //            vertices[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
        //
        //            if( duplicate [vit->info()] < 0 ){
        //                duplicate [vit->info()] = vit->info();
        //            } else {
        //                duplicate[ vit->info() ] = vertices.size();
        //                const Point_3 & point = vit->point();
        //                vertices.push_back(BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z())));
        //            }
        //        }

        std::vector<Tetrahedron> tetrahedra;
        std::vector<Subdomain_index> tet_ids;

        for( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit ){
            //            bool duplicate_n = false;
            //
            //            if(!c3t3_with_info.is_in_complex(cit)){
            //                for( int i = 0 ; i < 4 ; i ++ ){
            //                    if(!transfer_mesh->is_infinite(cit->neighbor(i)))
            //                        duplicate_n = duplicate_cell[cit->neighbor(i)->info().index()];
            //                    if(duplicate_n)
            //                        break;
            //                }
            //            }

            if( duplicate_cell[cit->info().index()] ){//|| duplicate_n ){
                tetrahedra.push_back(Tetrahedron( V[cit->vertex(0)->info()],
                                     V[cit->vertex(1)->info()],
                        V[cit->vertex(2)->info()],
                        V[cit->vertex(3)->info()]));
            } else {
                tetrahedra.push_back(Tetrahedron( cit->vertex(0)->info(),
                                                  cit->vertex(1)->info(),
                                                  cit->vertex(2)->info(),
                                                  cit->vertex(3)->info()));
            }
            tet_ids.push_back(c3t3_with_info.subdomain_index(cit));

        }

        selected_vertices.clear();
        V.clear();
        duplicate_cell.clear();
        duplicate.clear();
        clear_triangulations();

        Triangulated_mesh_domain my_domain (vertices, tetrahedra, tet_ids);

        c3t3_with_info= CGAL::make_mesh_3_from_labeled_triangulation_3<C3t3_with_info>(my_domain);
        updateMeshFromC3t3(c3t3_with_info);
    }
#else

    std::cout << "Separate separation_info.labels " << separation_info.labels.first << " and " << separation_info.labels.second << std::endl;

    std::vector<BasicPoint> vertices ( transfer_mesh->number_of_vertices() );
    std::vector<int> V ( transfer_mesh->number_of_vertices() ) ;

    std::vector<bool> to_duplicate (transfer_mesh->number_of_vertices(), false);

    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        const Point_3 & point = vit->point();
        vertices[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
        V[vit->info()] = vit->info();
    }


    bool label_connected = false;
    for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end() ; ++fit ){

        Surface_index si = c3t3_with_info.surface_index(*fit);


        if( separation_info.labels.first == si.first && separation_info.labels.second == si.second ){

            C3t3_with_info::Facet facet = *fit;
            if( c3t3_with_info.subdomain_index(fit->first) != separation_info.labels.first )
                facet = transfer_mesh->mirror_facet(*fit);

            for( int i = 0 ; i < 3 ; i++ ){
                const Triangulation::Vertex_handle vh = facet.first->vertex( indices[facet.second][i] );
                if( selected_vertices[ vh->info() ] )
                    to_duplicate[ vh->info() ] = true;

                //}
            }

            label_connected = true;
        }
    }
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        if(to_duplicate[vit->info()]){
            V[ vit->info() ] = vertices.size();
            const Point_3 & point = vit->point();
            vertices.push_back(BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z())));
        }
    }
    if( label_connected ){
        std::vector<Tetrahedron> tetrahedra;
        std::vector<Subdomain_index> tet_ids;
        for( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit ){
            bool neighbor_duplicate = false;
            if(c3t3_with_info.subdomain_index(cit) == 0 )
                for(int i = 0 ; i < 4 ; i++){
                    if( c3t3_with_info.subdomain_index( cit->neighbor(i) ) == separation_info.labels.first ){
                        neighbor_duplicate = true;
                        break;
                    }
                }
            if( c3t3_with_info.subdomain_index(cit) == separation_info.labels.first || neighbor_duplicate ){
                tetrahedra.push_back(Tetrahedron( V[cit->vertex(0)->info()],
                                     V[cit->vertex(1)->info()],
                        V[cit->vertex(2)->info()],
                        V[cit->vertex(3)->info()]));

            }
            else
            {
                tetrahedra.push_back(Tetrahedron( cit->vertex(0)->info(),
                                                  cit->vertex(1)->info(),
                                                  cit->vertex(2)->info(),
                                                  cit->vertex(3)->info()));
            }

            tet_ids.push_back(c3t3_with_info.subdomain_index(cit));
        }

        V.clear();
        clear_triangulations();

        Triangulated_mesh_domain my_domain (vertices, tetrahedra, tet_ids);

        c3t3_with_info= CGAL::make_mesh_3_from_labeled_triangulation_3<C3t3_with_info>(my_domain);
        updateMeshFromC3t3(c3t3_with_info);

    }
#endif

}

void TetMeshCreator::separateLabels( std::pair<Subdomain_index, Subdomain_index> labels){

    std::cout << "Separate labels " << labels.first << " and " << labels.second << std::endl;

    std::vector<BasicPoint> vertices ( transfer_mesh->number_of_vertices() );
    std::vector<int> V ( transfer_mesh->number_of_vertices() ) ;

    std::vector<bool> to_duplicate (transfer_mesh->number_of_vertices(), false);

    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        const Point_3 & point = vit->point();
        vertices[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
        V[vit->info()] = vit->info();
    }


    bool label_connected = false;
    for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end() ; ++fit ){

        Surface_index si = c3t3_with_info.surface_index(*fit);
        if( labels.first == si.first && labels.second == si.second ){

            C3t3_with_info::Facet facet = *fit;
            if( c3t3_with_info.subdomain_index(fit->first) != labels.first )
                facet = transfer_mesh->mirror_facet(*fit);

            for( int i = 0 ; i < 3 ; i++ ){
                const Triangulation::Vertex_handle vh = facet.first->vertex( indices[facet.second][i] );
                to_duplicate[ vh->info() ] = true;

                //}
            }

            label_connected = true;
        }
    }
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        if(to_duplicate[vit->info()]){
            V[ vit->info() ] = vertices.size();
            const Point_3 & point = vit->point();
            vertices.push_back(BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z())));
        }
    }
    if( label_connected ){
        std::vector<Tetrahedron> tetrahedra;
        std::vector<Subdomain_index> tet_ids;
        for( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit ){
            bool neighbor_duplicate = false;
            if(c3t3_with_info.subdomain_index(cit) == 0 )
                for(int i = 0 ; i < 4 ; i++){
                    if( c3t3_with_info.subdomain_index( cit->neighbor(i) ) == labels.first ){
                        neighbor_duplicate = true;
                        break;
                    }
                }
            if( c3t3_with_info.subdomain_index(cit) == labels.first || neighbor_duplicate ){
                tetrahedra.push_back(Tetrahedron( V[cit->vertex(0)->info()],
                                     V[cit->vertex(1)->info()],
                        V[cit->vertex(2)->info()],
                        V[cit->vertex(3)->info()]));

            }
            else
            {
                tetrahedra.push_back(Tetrahedron( cit->vertex(0)->info(),
                                                  cit->vertex(1)->info(),
                                                  cit->vertex(2)->info(),
                                                  cit->vertex(3)->info()));
            }

            tet_ids.push_back(c3t3_with_info.subdomain_index(cit));
        }

        V.clear();
        clear_triangulations();

        Triangulated_mesh_domain my_domain (vertices, tetrahedra, tet_ids);

        c3t3_with_info= CGAL::make_mesh_3_from_labeled_triangulation_3<C3t3_with_info>(my_domain);
        updateMeshFromC3t3(c3t3_with_info);

    }
}

void TetMeshCreator::transferMeshFromImg(Image & image, double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize, bool optimize){


    std::cout << "Meshing from image "  << facetAngle << " - " << facetSize << " - " << facetApproximation << " - " << cellRatio  << " - " << cellSize << std::endl;
    QTime t;
    t.start();
    separation_information.clear();

    computationTime = 0.;
    clear_triangulations();
    size_field.clear();
    size_field.resize(voxel_grid->size(),1);
    Mesh_domain domain(image);
    // Mesh criteria

    initial_sizing_field.cell_value = cellSize;
    initial_sizing_field.facet_value = facetSize;
    initial_sizing_field.angle = facetAngle;
    initial_sizing_field.approximation = facetApproximation;
    initial_sizing_field.ratio = cellRatio;

    Triangulation_mesh_criteria criteria(facet_angle=facetAngle, facet_size=facetSize, facet_distance=facetApproximation,
                                         cell_radius_edge_ratio=cellRatio, cell_size=cellSize );

    // Meshing
    if(optimize){
        c3t3_with_info = CGAL::make_mesh_3<C3t3_with_info>(domain, criteria, CGAL::parameters::no_features(),
                                                           CGAL::parameters::lloyd(60., 100, 0.02, 0.01),
                                                           CGAL::parameters::odt(60., 100, 0.02, 0.01),
                                                           CGAL::parameters::perturb(),
                                                           CGAL::parameters::exude());
    } else {

        c3t3_with_info = CGAL::make_mesh_3<C3t3_with_info>(domain, criteria, CGAL::parameters::no_features(),
                                                           CGAL::parameters::no_lloyd(),
                                                           CGAL::parameters::no_odt(),
                                                           CGAL::parameters::perturb(),
                                                           CGAL::parameters::exude());

    }
    std::cout << "Meshing done, " << t.elapsed() << std::endl;


    //  CGAL::lloyd_optimize_mesh_3(c3t3_with_info, domain, 60., 100, 0.02, 0.01);
    //CGAL::perturb_mesh_3(c3t3_with_info, domain, 120.);
    //CGAL::exude_mesh_3(c3t3_with_info, 10, 120.);
    //computationTime += t.elapsed();
    std::cout << "Mesh generated in " << t.elapsed() << " ms" << std::endl;


    updateMeshFromC3t3(c3t3_with_info);

    if(isAutomaticSeparation){
        automaticSeparation();
    }
    computeVerticesNormals();
    //checkIfPointsAreInsideTrMesh();
    std::cout << "v " << transfer_mesh->number_of_vertices() << " - " << transfer_mesh->number_of_cells() << std::endl;
}

void TetMeshCreator::transferMeshFromPolyhedron(const std::vector<BasicPoint> & vertices, const std::vector<Triangle > & triangles,
                                                double facetAngle, double facetSize, double facetApproximation, double cellRatio, double cellSize){

    Input_Polyhedron  polyhedron;
    std::vector<Point_3> P;
    for(unsigned int i = 0 ; i < vertices.size() ; i ++ ){
        P.push_back(to_point_3(vertices[i]));
    }

    for(unsigned int i = 0 ; i < triangles.size() ; i ++ ){
        const Triangle & triangle = triangles[i];
        polyhedron.make_triangle(P[triangle.getVertex(0)], P[triangle.getVertex(1)], P[triangle.getVertex(2)]);
    }


    // Create domain
    Polyhedral_mesh_domain domain(polyhedron);

    // Mesh criteria
    Motion_adaptive_sizing_field size;
    size.size_field = &size_field;

    size.xdim = voxel_grid->xdim();
    size.ydim = voxel_grid->ydim();

    for(int i = 0; i < 3; i++){
        size._d[i] = voxel_grid->d(i);
    }

    size.value = cellSize;

    Triangulation_mesh_criteria criteria (facet_angle=facetAngle, facet_size=facetSize, facet_distance=facetApproximation,
                                          cell_radius_edge_ratio=cellRatio, cell_size=size );

    // Meshing
    c3t3_with_info = CGAL::make_mesh_3<C3t3_with_info>(domain, criteria, no_exude());

    CGAL::lloyd_optimize_mesh_3(c3t3_with_info, domain, 60., 100, 0.02, 0.01);
    CGAL::perturb_mesh_3(c3t3_with_info, domain, 120.);
    CGAL::exude_mesh_3(c3t3_with_info, 10, 120.);

    updateMeshFromC3t3(c3t3_with_info);

    separation_information.clear();
    computationTime = 0.;
    //checkIfPointsAreInsideTrMesh();
    std::cout << "v " << transfer_mesh->number_of_vertices() << " - " << transfer_mesh->number_of_cells() << std::endl;
}

void TetMeshCreator::computeTransferMeshDistortion(std::vector<int> & values, float & min_value, float & max_value){


}

void TetMeshCreator::updatePositions(std::vector< std::pair<Voxel, GRID_TYPE > > & outside){

    std::vector<BasicPoint> corrected_def_positions(transfer_mesh->number_of_vertices());

    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        corrected_def_positions[vit->info()] = to_basic_point(vit->point());
    }

    set_transfer_mesh_to_initial_state();

    outside.clear();

    const std::vector<Voxel> & voxels = voxel_grid->getVoxels();
    std::vector<BasicPoint> & positions = voxel_grid->getPositions();

    Triangulation::Cell_handle start = defStart;
    for(unsigned int v=0; v < voxels.size(); v++){
        const Voxel & voxel = voxels[v];
        BasicPoint position = voxel_grid->getWorldCoordinate(voxel.i(),voxel.j(),voxel.k());

        if(voxel_grid->is_in_offseted_domains(voxel.i(),voxel.j(),voxel.k()) ){

            Triangulation::Cell_handle ch;
            if(locatePointInComplex(position, ch, start)){
                float bary[4];
                computeBarycentricCoordinates(ch, position, bary[0], bary[1], bary[2], bary[3]);

                positions[v] = BasicPoint(0.,0.,0.);

                for( int k = 0 ; k < 4 ; k++ ){
                    positions[v] += bary[k]* corrected_def_positions[ch->vertex(k)->info()];
                }


            } else {
                int id = voxel_grid->getGridIndex(voxel.i(),voxel.j(),voxel.k());
                //outside.push_back( std::make_pair(voxel, voxel_grid->value(voxel.i(),voxel.j(),voxel.k())) );
                voxel_grid->setValue(id, VoxelGrid::DEFAULT_GRID_VALUE);
            }


        }
    }
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        vit->set_point(to_point_3(corrected_def_positions[vit->info()]));
    }
}


void TetMeshCreator::updateMesh(const std::vector<BasicPoint> & vertices , const std::vector<Tetrahedron> & tetrahedra, const std::vector<Subdomain_index> & tet_ids ){

    clear_triangulations();
    size_field.clear();
    size_field.resize(voxel_grid->size(),1.);

    Triangulated_mesh_domain my_domain (vertices, tetrahedra, tet_ids);

    c3t3_with_info= CGAL::make_mesh_3_from_labeled_triangulation_3<C3t3_with_info>(my_domain);
    updateMeshFromC3t3(c3t3_with_info);

    initializeCage();

}
void TetMeshCreator::updateMeshFromC3t3( C3t3_with_info & c3t3 ){

    vertices_normals.clear();
    initial_vertices_positions.clear();

    defStart = Triangulation::Cell_handle();
    initPointBaryCoords.clear();

    vertices_gradient.clear();

    while (!vertex_error_queue.empty() )
        vertex_error_queue.pop();

    while (!cell_error_queue.empty() )
        cell_error_queue.pop();

    outlierChecked = false;
    outlier_vertices.clear();

    suggestion.clear();

    vertices_normals.clear();

    tetraLRISolver.clear();

    cageInterface->clear_mesh_info();

    transfer_mesh = &c3t3.triangulation();

    int inum = 0;
    bool to_init = true;
    //update vertices indices
    for ( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit, inum++)
    {
        vit->info() = initial_vertices_positions.size();
        initial_vertices_positions.push_back(Point_3(vit->point()));

    }

    inum = 0;

    for ( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit, inum++  )
    {
        cit->info().set_index(inum);
        if(c3t3.is_in_complex(cit)){
            add_to_complex(cit);
            defStart = cit;
        }
    }

    //    computeVerticesNormals();
    //    computeBB();
}


void TetMeshCreator::createFromPointDistribution( ){
    if(points.size() == 0) return ;
    clear_triangulations();

    //Triangulation triangulation;
    Triangulation point_distribution_triangulation;
    for(unsigned int i = 0 ; i < points.size() ; i++){
        point_distribution_triangulation.insert(to_point_3(points[i]));
    }

    std::vector<BasicPoint> V(point_distribution_triangulation.number_of_vertices());
    std::vector<Tetrahedron> Tet;
    std::vector<Triangulated_mesh_domain::Subdomain_index> tet_ids;

    int count = 0;
    for(Triangulation::Finite_vertices_iterator vit = point_distribution_triangulation.finite_vertices_begin();
        vit != point_distribution_triangulation.finite_vertices_end() ; ++vit, count++){
        vit->info() = count;
        const Point_3 & point = vit->point();
        V[count] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
    }

    for( Triangulation::Finite_cells_iterator cit = point_distribution_triangulation.finite_cells_begin() ; cit != point_distribution_triangulation.finite_cells_end(); ++cit ){

        Triangulated_mesh_domain::Subdomain_index si = 1;
        Tet.push_back(Tetrahedron( cit->vertex(0)->info(),
                                   cit->vertex(1)->info(),
                                   cit->vertex(2)->info(),
                                   cit->vertex(3)->info()));

        tet_ids.push_back(si);
    }


    Triangulated_mesh_domain my_domain (V, Tet, tet_ids);

    c3t3_with_info= CGAL::make_mesh_3_from_labeled_triangulation_3<C3t3_with_info>(my_domain);
    updateMeshFromC3t3(c3t3_with_info);

    // finished

}

void TetMeshCreator::createFromPointDistributionFittedToPoints( ){

    if(points.size() == 0) return ;
    clear_triangulations();

    //Triangulation triangulation;
    Triangulation point_distribution_triangulation;
    for(unsigned int i = 0 ; i < points.size() ; i++){
        point_distribution_triangulation.insert(to_point_3(points[i]));
    }

    std::cout << "Point distribution " << point_distribution_triangulation.number_of_vertices() << " " << point_distribution_triangulation.number_of_cells() << std::endl ;
    std::vector<BasicPoint> V(point_distribution_triangulation.number_of_vertices());
    std::vector<Tetrahedron> Tet;
    std::vector<Triangulated_mesh_domain::Subdomain_index> tet_ids;

    initial_vertices_positions.clear();
    initial_vertices_positions.resize(V.size());

    int count = 0;
    for(Triangulation::Finite_vertices_iterator vit = point_distribution_triangulation.finite_vertices_begin();
        vit != point_distribution_triangulation.finite_vertices_end() ; ++vit, count++){
        vit->info() = count;
        const Point_3 & point = vit->point();
        initial_vertices_positions[count] = Point_3(point);
        V[count] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
    }

    for( Triangulation::Finite_cells_iterator cit = point_distribution_triangulation.finite_cells_begin() ; cit != point_distribution_triangulation.finite_cells_end(); ++cit ){

        Triangulated_mesh_domain::Subdomain_index si = 0;
        Tet.push_back(Tetrahedron( cit->vertex(0)->info(),
                                   cit->vertex(1)->info(),
                                   cit->vertex(2)->info(),
                                   cit->vertex(3)->info()));
        if(is_tet_in_complex(cit)){
            si = 1;
        }
        tet_ids.push_back(si);
    }


    Triangulated_mesh_domain my_domain (V, Tet, tet_ids);

    c3t3_with_info= CGAL::make_mesh_3_from_labeled_triangulation_3<C3t3_with_info>(my_domain);
    updateMeshFromC3t3(c3t3_with_info);


}

void TetMeshCreator::set_transfer_mesh_to_initial_state(){

    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        vit->set_point(initial_vertices_positions[vit->info()]);
    }
}

void TetMeshCreator::getEnvelope(std::vector<BasicPoint> & V, std::vector<Triangle> & T){
    V.clear();

    std::map<int, int> VMap;
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        if(c3t3_with_info.in_dimension(vit) == 2){
            const Point_3 & point = vit->point();
            VMap[vit->info()] = V.size();
            V.push_back(BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z())));
        }
    }


    T.clear();
    for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end(); ++fit ){

        Triangulation::Facet mirror_facet  = transfer_mesh->mirror_facet(*fit);
        Triangulation::Cell_handle current_cell = fit->first ;



        if(c3t3_with_info.is_in_complex(current_cell) ){
            T.push_back(Triangle( VMap[fit->first->vertex(indices[fit->second][0])->info()],
                    VMap[fit->first->vertex(indices[fit->second][1])->info()],
                    VMap[fit->first->vertex(indices[fit->second][2])->info()]));
        } else {
            T.push_back(Triangle( VMap[mirror_facet.first->vertex(indices[mirror_facet.second][0])->info()],
                    VMap[mirror_facet.first->vertex(indices[mirror_facet.second][1])->info()],
                    VMap[mirror_facet.first->vertex(indices[mirror_facet.second][2])->info()]));
        }
    }
}

void TetMeshCreator::getMesh(std::vector<BasicPoint> & V, std::vector<Triangle> & T, std::vector<Subdomain_index> & tr_si,
                             std::vector<Tetrahedron> & Tet, std::vector<Subdomain_index> & tet_si){

    V.clear();
    V.resize(transfer_mesh->number_of_vertices());
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        const Point_3 & point = vit->point();
        V[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
    }


    T.clear();
    tr_si.clear();
    for( Triangulation::Finite_facets_iterator fit = transfer_mesh->finite_facets_begin() ; fit != transfer_mesh->finite_facets_end(); ++fit ){

        Triangulation::Facet mirror_facet  = transfer_mesh->mirror_facet(*fit);
        Triangulation::Cell_handle current_cell = fit->first ;

        bool current_in = is_in_complex(current_cell);
        bool mirror_in = is_in_complex(mirror_facet.first);

        if(current_in && !mirror_in ){
            T.push_back(Triangle( fit->first->vertex(indices[fit->second][0])->info(),
                    fit->first->vertex(indices[fit->second][1])->info(),
                    fit->first->vertex(indices[fit->second][2])->info()));
            tr_si.push_back( c3t3_with_info.subdomain_index(fit->first) );
        } else if( mirror_in && !current_in){
            T.push_back(Triangle( mirror_facet.first->vertex(indices[mirror_facet.second][0])->info(),
                    mirror_facet.first->vertex(indices[mirror_facet.second][1])->info(),
                    mirror_facet.first->vertex(indices[mirror_facet.second][2])->info()));
            tr_si.push_back( c3t3_with_info.subdomain_index(mirror_facet.first) );
        }
    }



    Tet.clear();
    tet_si.clear();
    for ( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin() ; cit != c3t3_with_info.cells_end() ; ++cit  ){

        Tet.push_back(Tetrahedron( cit->vertex(0)->info(),
                                   cit->vertex(1)->info(),
                                   cit->vertex(2)->info(),
                                   cit->vertex(3)->info()));

        tet_si.push_back(c3t3_with_info.subdomain_index(cit));
    }

}

void TetMeshCreator::getMesh(std::vector<BasicPoint> & V, std::vector<Tetrahedron> & Tet){

    V.clear();
    V.resize(transfer_mesh->number_of_vertices());
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        const Point_3 & point = vit->point();
        V[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
    }


    Tet.clear();

    for ( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin() ; cit != c3t3_with_info.cells_end() ; ++cit  ){


        Tet.push_back(Tetrahedron( cit->vertex(0)->info(),
                                   cit->vertex(1)->info(),
                                   cit->vertex(2)->info(),
                                   cit->vertex(3)->info()));
    }

}

void TetMeshCreator::computeVerticesNormals(){

    int normWeight = 2;
    vertices_normals.clear();
    vertices_normals.resize(transfer_mesh->number_of_vertices(), Geom_traits::Vector_3(0.,0.,0.));

    for (C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin(); fit != c3t3_with_info.facets_end(); ++fit){
        Triangulation::Cell_handle ch = fit->first;

        const Point_3 & pa = ch->vertex(indices[fit->second][0])->point();
        const Point_3 & pb = ch->vertex(indices[fit->second][1])->point();
        const Point_3 & pc = ch->vertex(indices[fit->second][2])->point();
        Geom_traits::Vector_3 n = CGAL::cross_product(pb - pa, pc -pa);

        float n_dir = 1;
        if(!c3t3_with_info.is_in_complex(ch)) n_dir = -1.;
        n = n_dir*n / CGAL::sqrt(n*n);

        vector<int> v;
        for (unsigned int j = 1; j < 4; j++)
            v.push_back((fit->second+j)%4);

        for (unsigned int  j = 0; j < 3; j++) {

            Triangulation::Vertex_handle vh = ch->vertex(v[j]);
            const Point_3 & pos = vh->point();

            double w = 1.0; // uniform weights

            Geom_traits::Vector_3 e0 = static_cast<Point_3>(ch->vertex (v[(j+1)%3])->point()) - pos;
            Geom_traits::Vector_3 e1 = static_cast<Point_3>(ch->vertex (v[(j+2)%3])->point()) - pos;

            if (normWeight == 1) { // area weight
                Geom_traits::Vector_3 cp = CGAL::cross_product(e0, e1);
                w = cp*cp / 2.0;
            } else if (normWeight == 2) { // angle weight
                e0 = e0/(CGAL::sqrt(e0*e0));
                e1 = e1/(CGAL::sqrt(e1*e1));
                w = (2.0 - (e0*e1 + 1.0)) / 2.0;
            }
            if (w <= 0.0)
                continue;

            vertices_normals[ vh->info()] = vertices_normals[ vh->info()] +  n* w;

        }

    }



    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin(); vit != transfer_mesh->finite_vertices_end(); ++vit)
    {
        if(c3t3_with_info.in_dimension(vit) == 2 ){
            const Geom_traits::Vector_3 & n = vertices_normals[ vit->info()];
            vertices_normals[ vit->info()] = n /CGAL::sqrt(n*n);
        }
    }
}

void TetMeshCreator::computeBB(){

    BBMin = BasicPoint( FLT_MAX, FLT_MAX, FLT_MAX);
    BBMax = BasicPoint(-FLT_MAX,-FLT_MAX,-FLT_MAX);

    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin(); vit != transfer_mesh->finite_vertices_end(); ++vit)
    {
        const Point_3 & point = vit->point();
        for(unsigned int j = 0 ; j < 3 ; j ++ ){
            if(BBMin[j] > point[j]) BBMin[j] = point[j];
            if(BBMax[j] < point[j]) BBMax[j] = point[j];
        }
    }
}

void TetMeshCreator::initializeCage(){
    std::cout << "Computing TM cage coordinates" << std::endl;

    cageInterface->clear_mesh_info();

    std::vector< BasicPoint > & ipoints = cageInterface->get_mesh_vertices();
    ipoints.clear();
    ipoints.resize(transfer_mesh->number_of_vertices());

    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        const Point_3 & point = vit->point();
        ipoints[vit->info()] = BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
    }

    //TODO Compute while checking outlier_vertices and marking vertices
    QTime t;
    t.start();
    cageInterface->compute_cage_coordinates();

    computationTime += t.elapsed();
    std::cout << t.elapsed() << " ms to compute cage coordinates" << std::endl;

    checkAndMarkOutliers();
    updateVertices();
}

void TetMeshCreator::initializeLRISolver(){

    std::vector<Triangulation::Cell_handle> tetrahedra;

    std::vector<int> handle_tetrahedra;
    std::vector<int> unknown_tetrahedra;

    tetrahedra.clear();
    handle_tetrahedra.clear();
    unknown_tetrahedra.clear();

    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
        unsigned int index = tetrahedra.size();
        if(cit->info().cage_inlier()){
            for( int i = 0 ; i < 4 ; i++ ){
                const Triangulation::Cell_handle & neighbor = cit->neighbor(i);
                if(c3t3_with_info.is_in_complex(neighbor) && !neighbor->info().cage_inlier()){
                    handle_tetrahedra.push_back(index);
                    cit->info().set_index(index);
                    tetrahedra.push_back(cit);
                    break;
                }
            }
        } else {
            cit->info().set_index(index);
            tetrahedra.push_back(cit);
            unknown_tetrahedra.push_back(index);
        }
    }

    tetraLRISolver.clear();
    tetraLRISolver.load_tetrahedra(*transfer_mesh, tetrahedra, unknown_tetrahedra, handle_tetrahedra, outlier_vertices);
}

void TetMeshCreator::updateVertices( vector<BasicPoint> & points ){

    if (points.size() == transfer_mesh->number_of_vertices())

        for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
            points[vit->info()] = to_basic_point(vit->point());
        }

}

void TetMeshCreator::updateVertices( ){

    //    if(!outlierChecked)
    //        checkAndMarkOutliers();
#if 1

    QTime t;
    t.start();
    const std::vector<BasicPoint> & V = cageInterface->get_mesh_modified_vertices();
    if (V.size() == transfer_mesh->number_of_vertices()){
        for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){

            const BasicPoint & point = V[vit->info()];
            vit->set_point(to_point_3(point));
        }
    }
    computationTime += t.elapsed();
    std::cout << t.elapsed() << " ms to update positions" << std::endl;

    if( outlier_vertices.size() > 0 ){
        std::cout << "Solving tetraLRISolver" << std::endl;
        computeBasisTransforms();

        tetraLRISolver.update_constraints(*transfer_mesh, outlier_vertices);

    }

    std::cout << t.elapsed() << " ms to update outliers" << std::endl;

#else
    if( outlier_vertices.size() > 0 ){
        if (V.size() == transfer_mesh->number_of_vertices()){
            for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){

                if( !outlier_vertices[vit->info()] ){
                    const BasicPoint & point = V[vit->info()];
                    vit->set_point(to_point_3(point));
                }else {

                    std::vector<Triangulation::Vertex_handle> neighbors;
                    transfer_mesh->finite_incident_vertices(vit, std::back_inserter(neighbors));
                    float sumW = 0.;

                    Geom_traits::Vector_3 def(0.,0.,0.);

                    const Point_3 & p0 = initial_vertices_positions[vit->info()];

                    for( unsigned int i = 0 ; i < neighbors.size() ; i++ ){
                        Triangulation::Vertex_handle & n = neighbors[i];
                        if( !outlier_vertices[n->info()]  && hasInComplexCell(vit,n)){

                            Geom_traits::Vector_3 dist = initial_vertices_positions[n->info()] - p0;
                            float w = 1. /(dist*dist);
                            sumW += w;
                            def = def + ( initial_vertices_positions[n->info()] - to_point_3(V[n->info()]) )* w;

                        }
                    }

                    if( sumW > 0 ){
                        std::cout << "one done" << std::endl;
                        def = def/sumW;

                        def = Geom_traits::Vector_3(p0.x(), p0.y(), p0.z()) - def;
                        vit->set_point(Point_3(def.x(), def.y(), def.z()));
                    } else {
                        std::cout << "no neighborhood not enough" << std::endl;
                    }
                }
            }
        }

    } else {


        if (V.size() == transfer_mesh->number_of_vertices()){
            for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){

                const BasicPoint & point = V[vit->info()];
                vit->set_point(to_point_3(point));
            }
        }
    }
#endif
    computeVerticesNormals();
    computeBB();
}

void TetMeshCreator::updateOutOfComplexCells(){
    Triangulation tmpMesh(*transfer_mesh);

    transfer_mesh->clear();

    Builder builder(*transfer_mesh, true);
    builder.begin_triangulation(3);
    // add the vertices to the incremental builder object and store the

    //C3T3 c3t3;


    int inum = 0;
    std::vector<Triangulation::Vertex_handle> VMap(tmpMesh.number_of_vertices());
    // Insert points and set their index and dimension
    for ( Triangulation::Finite_vertices_iterator vit = tmpMesh.finite_vertices_begin() ; vit != tmpMesh.finite_vertices_end(); ++vit, inum++)
    {
        Triangulation::Vertex_handle vh = builder.add_vertex();
        const Point_3 & p = vit->point();

        vh->set_point(p);
        vh->info() = vit->info();
        VMap[vit->info()] = vh;

    }

    //C3T3 c3t3;

    // Insert points and set their index and dimension
    std::map<Triangulation::Cell_handle, bool> CMap;
    for ( Triangulation::Finite_cells_iterator cit = tmpMesh.finite_cells_begin() ; cit != tmpMesh.finite_cells_end() ; ++cit  )
    {
        if(is_in_complex(cit)){
            Triangulation::Cell_handle ch = builder.add_cell(VMap[cit->vertex(0)->info()],VMap[cit->vertex(1)->info()], VMap[cit->vertex(2)->info()], VMap[cit->vertex(3)->info()]);

            ch->info() = cit->info();
            CMap[ch] = ch->info().in_complex();
            defStart = ch;
        }
    }


    // finished
    builder.end_triangulation();

    int count = 0;
    for( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end(); ++cit , count ++){

        if(CMap.find(cit) == CMap.end())
            cit->info().set_in_complex(false);
        cit->info().set_index(count);
    }

}
inline bool TetMeshCreator::is_in_complex(const Triangulation::Cell_handle & ch){
    if(transfer_mesh->is_infinite(ch))
        return false;
    return ch->info().in_complex();
}

inline void TetMeshCreator::add_to_complex(const Triangulation::Cell_handle & ch){
    ch->info().set_in_complex(true);
}
//
//inline void TetMeshCreator::remove_from_complex(const Triangulation::Cell_handle & ch){
//
//    if(transfer_mesh->is_cell(ch) && !transfer_mesh->is_infinite(ch) ){
//        ch->info() = false;
//    }
//
//}

inline BasicPoint TetMeshCreator::to_basic_point(const Point_3 & point){
    return BasicPoint(CGAL::to_double(point.x()), CGAL::to_double(point.y()), CGAL::to_double(point.z()));
}

//
//bool TetMeshCreator::computeBaryCoords(Triangulation::Cell_handle & ch, const BasicPoint & point, float & ld0, float & ld1, float & ld2, float & ld3 ){
//
//
//    for(Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit){
//
//        BasicPoint p1 = to_basic_point(cit->vertex(0)->point());
//        BasicPoint p2 = to_basic_point(cit->vertex(1)->point());
//        BasicPoint p3 = to_basic_point(cit->vertex(2)->point());
//        BasicPoint p4 = to_basic_point(cit->vertex(3)->point());
//
//        float a = p1[0] - p4[0]; float b = p2[0] - p4[0]; float c = p3[0] - p4[0];
//        float d = p1[1] - p4[1]; float e = p2[1] - p4[1]; float f = p3[1] - p4[1];
//        float g = p1[2] - p4[2]; float h = p2[2] - p4[2]; float k = p3[2] - p4[2];
//
//        float det = a*(e*k - h*f) + b*(g*f - d*k) + c*(d*h - g*e);
//
//        float A = e*k - f*h; float D = c*h - b*k; float G = b*f - c*e;
//        float B = f*g - d*k; float E = a*k - c*g; float H = c*d - a*f;
//        float C = d*h - e*g; float F = g*b - a*h; float K = a*e - b*d;
//
//        ld0 = ((point[0] - p4[0])*A + (point[1] - p4[1])*D + (point[2] - p4[2])*G )/ det;
//        ld1 = ((point[0] - p4[0])*B + (point[1] - p4[1])*E + (point[2] - p4[2])*H )/ det;
//        ld2 = ((point[0] - p4[0])*C + (point[1] - p4[1])*F + (point[2] - p4[2])*K )/ det;
//        ld3 = 1 - ld0 - ld1 - ld2;
//
//        if( ld0 >= 0. && ld1 >= 0. && ld2 >= 0. && ld0 <= 1. && ld1 <= 1. && ld2 <= 1. ){
//            ch = cit;
//            return true;
//        }
//
//
//    }
//    return false;
//}

void TetMeshCreator::computeBarycentricCoordinates(const Triangulation::Cell_handle & ch , const BasicPoint & point, float & ld0, float & ld1, float & ld2, float & ld3 ){

    BasicPoint p1 = to_basic_point(ch->vertex(0)->point());
    BasicPoint p2 = to_basic_point(ch->vertex(1)->point());
    BasicPoint p3 = to_basic_point(ch->vertex(2)->point());
    BasicPoint p4 = to_basic_point(ch->vertex(3)->point());

    float a = p1[0] - p4[0]; float b = p2[0] - p4[0]; float c = p3[0] - p4[0];
    float d = p1[1] - p4[1]; float e = p2[1] - p4[1]; float f = p3[1] - p4[1];
    float g = p1[2] - p4[2]; float h = p2[2] - p4[2]; float k = p3[2] - p4[2];

    float det = a*(e*k - h*f) + b*(g*f - d*k) + c*(d*h - g*e);

    float A = e*k - f*h; float D = c*h - b*k; float G = b*f - c*e;
    float B = f*g - d*k; float E = a*k - c*g; float H = c*d - a*f;
    float C = d*h - e*g; float F = g*b - a*h; float K = a*e - b*d;

    ld0 = ((point[0] - p4[0])*A + (point[1] - p4[1])*D + (point[2] - p4[2])*G )/ det;
    ld1 = ((point[0] - p4[0])*B + (point[1] - p4[1])*E + (point[2] - p4[2])*H )/ det;
    ld2 = ((point[0] - p4[0])*C + (point[1] - p4[1])*F + (point[2] - p4[2])*K )/ det;
    ld3 = 1 - ld0 - ld1 - ld2;



}

void TetMeshCreator::computePseudoRasterDeformation(VoxelGrid & result){

    std::cout << "Compute deformation " << result.xdim() << " - " << result.ydim() << " - " << result.zdim() << std::endl;
    QTime t;
    t.start();
    result.clearToDefault();

    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
        BasicPoint tet_bb_min, tet_bb_max;

        computeDeformedTetBB(cit, tet_bb_min, tet_bb_max);


        Voxel vmin = result.getGridCoordinate(tet_bb_min);
        Voxel vmax = result.getGridCoordinate(tet_bb_max);

        // if(result.isInGrid(vmin) && result.isInGrid(vmax)){

        for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
            for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
                for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){

                    BasicPoint position = result.getWorldCoordinate(i,j,k);

                    GRID_TYPE value = result.value(i,j,k);
                    GRID_TYPE si = VoxelGrid::BACKGROUND_GRID_VALUE;

                    if( value < VoxelGrid::BACKGROUND_GRID_VALUE ){

                        float ld[4];
                        computeBarycentricCoordinates(cit, position, ld[0], ld[1], ld[2], ld[3]);

                        if(ld[0] >= 0 && ld[0] <=1. && ld[1] >= 0 && ld[1] <=1. && ld[2] >= 0 && ld[2] <=1. && ld[3] >= 0 && ld[3] <=1. ){

                            BasicPoint p0 = to_basic_point(initial_vertices_positions[cit->vertex(0)->info()]);
                            BasicPoint p1 = to_basic_point(initial_vertices_positions[cit->vertex(1)->info()]);
                            BasicPoint p2 = to_basic_point(initial_vertices_positions[cit->vertex(2)->info()]);
                            BasicPoint p3 = to_basic_point(initial_vertices_positions[cit->vertex(3)->info()]);

                            BasicPoint pos = ld[0]*p0 + ld[1]*p1 + ld[2]*p2 + ld[3]*p3;

                            // std::cout << pos << std::endl;

                            si = std::max(voxel_grid->value(pos), VoxelGrid::BACKGROUND_GRID_VALUE );
                            // si = 1;

                            result.setValue(i,j,k,si);
                        }
                    }
                }

            }
        }
    }
    //}
    computationTime += t.elapsed();
    std::cout << "Time elapsed for rasterization: " << t.elapsed() << " ms" << std::endl;
    // result.sort();

    //updatePositions();
    std::cout << "Deformation computed in " << computationTime << " ms " << std::endl;
}

void TetMeshCreator::computePseudoRasterDeformation(VoxelGrid & HRGrid, std::ifstream & inputImaFile, VoxelGrid & HRresult, std::ofstream & outputImaFile ){

    std::cout << "Compute deformation " << HRresult.xdim() << " - " << HRresult.ydim() << " - " << HRresult.zdim() << std::endl;
    QTime t;
    t.start();

    char current_icharacter;

    // Seek to the beginning of the files
    inputImaFile.seekg(0, ios::beg);

    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
        BasicPoint tet_bb_min, tet_bb_max;

        computeDeformedTetBB(cit, tet_bb_min, tet_bb_max);


        Voxel vmin = HRresult.getGridCoordinate(tet_bb_min);
        Voxel vmax = HRresult.getGridCoordinate(tet_bb_max);

        // if(result.isInGrid(vmin) && result.isInGrid(vmax)){

        for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
            for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
                for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){

                    BasicPoint position = HRresult.getWorldCoordinate(i,j,k);


                    float ld[4];
                    computeBarycentricCoordinates(cit, position, ld[0], ld[1], ld[2], ld[3]);

                    if(ld[0] >= 0 && ld[0] <=1. && ld[1] >= 0 && ld[1] <=1. && ld[2] >= 0 && ld[2] <=1. && ld[3] >= 0 && ld[3] <=1. ){

                        BasicPoint p0 = to_basic_point(initial_vertices_positions[cit->vertex(0)->info()]);
                        BasicPoint p1 = to_basic_point(initial_vertices_positions[cit->vertex(1)->info()]);
                        BasicPoint p2 = to_basic_point(initial_vertices_positions[cit->vertex(2)->info()]);
                        BasicPoint p3 = to_basic_point(initial_vertices_positions[cit->vertex(3)->info()]);

                        BasicPoint pos = ld[0]*p0 + ld[1]*p1 + ld[2]*p2 + ld[3]*p3;

                        // std::cout << pos << std::endl;

                        int input_index = HRGrid.getGridIndex( HRGrid.getGridCoordinate(pos) );

                        inputImaFile.seekg(input_index, ios::beg);
                        inputImaFile.read( &current_icharacter, 1 );

                        if( (int)current_icharacter > 0 ){

                            int output_index = HRresult.getGridIndex(i,j,k);

                            outputImaFile.seekp(output_index, ios::beg);
                            outputImaFile.write( &current_icharacter, 1 );
                        }
                        // si = 1;


                    }
                }

            }
        }
    }
    //}
    computationTime += t.elapsed();
    std::cout << "Time elapsed for rasterization: " << t.elapsed() << " ms" << std::endl;
    // result.sort();

    //updatePositions();
    std::cout << "Deformation computed in " << computationTime << " ms " << std::endl;
}




void TetMeshCreator::computeDeformation(VoxelGrid & result){
    computePseudoRasterDeformation(result);
    return ;

    std::cout << "Compute deformation " << result.xdim() << " - " << result.ydim() << " - " << result.zdim() << std::endl;
    std::cout << " Tree construction " << std::endl;
    std::list<Triangle_3> triangles;

    for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin(); fit != c3t3_with_info.facets_end() ; ++fit ){
        C3t3_with_info::Facet facet = *fit;
        if( !c3t3_with_info.is_in_complex(fit->first) )
            facet = c3t3_with_info.triangulation().mirror_facet(facet);

        const Point_3 & a = facet.first->vertex(indices[facet.second][0])->point();
        const Point_3 & b = facet.first->vertex(indices[facet.second][1])->point();
        const Point_3 & c = facet.first->vertex(indices[facet.second][2])->point();

        triangles.push_back(Triangle_3(SK::Point_3(a.x(), a.y(), a.z()), SK::Point_3(b.x(), b.y(), b.z()),SK::Point_3(c.x(), c.y(), c.z())));
    }
    std::cout << " Tree done " << std::endl;
    // constructs AABB tree
    Tree tree(triangles.begin(),triangles.end());

    Triangulation::Cell_handle start = defStart;
    std::vector<bool> visited (transfer_mesh->number_of_cells(), false);

    int startY = 0, incrY = 1;
    int startZ = 0, incrZ = 1;
    for( unsigned int i = 0 ; i < result.xdim() ; i ++ ){
        std::cout << "tranche " << i << std::endl;
        for( int j = startY ; j >= 0 && j < result.ydim() ; j += incrY ){
            for( int k = startZ ; k >= 0 && k < result.zdim() ; k += incrZ ){
                BasicPoint position = result.getWorldCoordinate(i,j,k);

                Triangulation::Cell_handle ch;
                GRID_TYPE si = VoxelGrid::BACKGROUND_GRID_VALUE;

                Triangulation::Locate_type lt;
                int li, lj;

                Point_3 point (position[0], position[1], position[2]);
                SK::Point_3 a (point.x(), point.y(), point.z());
                SK::Ray_3 ray_query( a, p_start);

                if(tree.do_intersect(ray_query) && locate(point, lt, li, lj, ch, start, visited)){
                    //if(locatePointInComplex(position, ch , start)){
                    float ld0, ld1, ld2, ld3;
                    computeBarycentricCoordinates(ch, position, ld0, ld1, ld2, ld3);
                    //
                    BasicPoint p0 = to_basic_point(initial_vertices_positions[ch->vertex(0)->info()]);
                    BasicPoint p1 = to_basic_point(initial_vertices_positions[ch->vertex(1)->info()]);
                    BasicPoint p2 = to_basic_point(initial_vertices_positions[ch->vertex(2)->info()]);
                    BasicPoint p3 = to_basic_point(initial_vertices_positions[ch->vertex(3)->info()]);

                    BasicPoint pos = ld0*p0 + ld1*p1 + ld2*p2 + ld3*p3;

                    // std::cout << pos << std::endl;

                    si = std::max(voxel_grid->value(pos), VoxelGrid::BACKGROUND_GRID_VALUE );
                    start = ch;
                    // si = 1;
                }
                result.setValue(i,j,k,si);

            }
            incrZ *= -1;
            if(startZ == 0) startZ = result.zdim() - 1;
            else startZ = 0;
        }
        incrY *= -1;
        if(startY == 0) startY = result.ydim() - 1;
        else startY = 0;
    }

    //result.sort();

    //updatePositions();
    std::cout << "Deformation computed" << std::endl;
}
//
//void TetMeshCreator::updatePositions(){
//    std::vector<BasicPoint> & positions = voxel_grid->getPositions();
//
//    if( positions.size() != initPointBaryCoords.size() ) {
//        std::cout << "TetMeshCreator::updatePositions::Wrong input image!!" << std::endl;
//        return;
//    }
//
//
//    for( unsigned int i = 0  ; i < positions.size() ; i ++){
//        Triangulation::Cell_handle & ch = initPointBaryCoords[i].first;
//        std::vector<float> & coords = initPointBaryCoords[i].second;
//
//        positions[i] = to_basic_point(ch->vertex(0)->point())*coords[0] +
//                       to_basic_point(ch->vertex(1)->point())*coords[1] +
//                       to_basic_point(ch->vertex(2)->point())*coords[2] +
//                       to_basic_point(ch->vertex(3)->point())*coords[3];
//    }
//
//}
//
//
//void TetMeshCreator::computeTetDistortion(){
//
//    std::vector< float > vertices_distortion (transfer_mesh->number_of_vertices(), 0.);
//    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
//
//        std::vector<Triangulation::Vertex_handle> neighbor_vertices;
//        transfer_mesh->incident_vertices(vit, std::back_inserter(neighbor_vertices));
//
//        for( unsigned int i = 0 ; i < neighbor_vertices.size() ; i ++  ){
//            const Triangulation::Vertex_handle & neighbor = neighbor_vertices[i];
//            Geom_traits::Vector_3 current_edge = (neighbor->point() - vit->point()) ;
//            current_edge = current_edge/ sqrt(current_edge*current_edge);
//            Geom_traits::Vector_3 initial_edge = initial_vertices_positions[neighbor->info()] - initial_vertices_positions[vit->info()];
//            initial_edge = initial_edge/ sqrt(initial_edge*initial_edge);
//
//            for( unsigned int j = i+1 ; j < neighbor_vertices.size() ; j ++  ){
//                const Triangulation::Vertex_handle & next_neighbor = neighbor_vertices[j];
//                Geom_traits::Vector_3 neighbor_edge = next_neighbor->point() - vit->point();
//                neighbor_edge = neighbor_edge/ sqrt(neighbor_edge*neighbor_edge);
//                Geom_traits::Vector_3 initial_neighbor_edge = initial_vertices_positions[next_neighbor->info()] - initial_vertices_positions[vit->info()];
//                initial_neighbor_edge = initial_neighbor_edge/ sqrt(initial_neighbor_edge*initial_neighbor_edge);
//
//                float current_angle = acos(current_edge*neighbor_edge)*180./M_PI;
//                float init_angle = acos(initial_edge*initial_neighbor_edge)*180./M_PI;
//
//                vertices_distortion[vit->info()] = std::max(float(fabs(init_angle - current_angle)), vertices_distortion[vit->info()]);
//            }
//        }
//
//    }
//
//    //    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
//    //        std::cout << vertices_distortion[vit->info()] << std::endl;
//    //    }
//
//}

void TetMeshCreator::computeBasisTransforms( ){
    std::cout << "TetMeshCreator::computeBasesTransform()::Start " << std::endl;

    //basis_transform.
    for(Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit){
        if(is_in_complex(cit)){
            std::vector<BasicPoint> basis_tr (3, BasicPoint(0.,0.,0.));
            computeBasisTransform( cit, basis_tr );
            cit->info().set_basis_def(basis_tr);
        }
    }

    std::cout << "TetMeshCreator::computeBasisTransforms()::End " << std::endl;
}

float TetMeshCreator::computeNorm(const std::vector<BasicPoint> &m ) {

    float sum = 0.;
    for( int i = 0 ; i < 3 ; i++ ){
        for( int j = 0 ; j < 3 ; j++ ){
            sum += m[i][j]*m[i][j];
        }
    }

    return sqrt(sum);
}

float TetMeshCreator::computeCellVolume(const Triangulation::Cell_handle & ch){
    Geom_traits::Vector_3 e10 = initial_vertices_positions[ch->vertex(1)->info()] - initial_vertices_positions[ch->vertex(0)->info()];
    Geom_traits::Vector_3 e20 = initial_vertices_positions[ch->vertex(2)->info()] - initial_vertices_positions[ch->vertex(0)->info()];

    return fabs(CGAL::cross_product(e10, e20)*(static_cast<Point_3>(ch->vertex(3)->point()) -
                                               static_cast<Point_3>(ch->vertex(0)->point())))/6.;



}
float TetMeshCreator::computeVolumeBasisLaplacian(const Triangulation::Cell_handle & ch){

    float basis_laplacian = 0.;
    if(is_in_complex(ch)){
        const std::vector<BasicPoint> & basis_transform = ch->info().basis_def();


        int count = 0;
        std::vector<BasicPoint> bt_dif(3, BasicPoint(0.,0.,0.));
        float sum_volume =0.;
        for( int i = 0 ; i < 4 ; i++ ){
            Triangulation::Cell_handle neighbor_ch = ch->neighbor(i);
            if(is_in_complex(neighbor_ch)){
                float volume = computeCellVolume(neighbor_ch);
                sum_volume += volume;
                const std::vector<BasicPoint> &  bt_neighbor = neighbor_ch->info().basis_def();
                for( int j = 0 ; j < 3 ; j++ )
                    bt_dif[j] += (basis_transform[j] - bt_neighbor[j])*volume;

                count++;
            }
        }

        if(count >0){
            basis_laplacian = computeNorm(bt_dif);
            basis_laplacian = basis_laplacian/(sum_volume);
        }
    }
    ch->info().set_distortion_laplacian(basis_laplacian);

    return basis_laplacian;
}

float TetMeshCreator::computeTopologicalBasisLaplacian(const Triangulation::Cell_handle & ch){

    float basis_laplacian = 0.;
    if(is_in_complex(ch)){
        const std::vector<BasicPoint> & basis_transform = ch->info().basis_def();


        int count = 0;
        std::vector<BasicPoint> bt_dif(3, BasicPoint(0.,0.,0.));
        for( int i = 0 ; i < 4 ; i++ ){
            Triangulation::Cell_handle neighbor_ch = ch->neighbor(i);
            if(is_in_complex(neighbor_ch)){


                const std::vector<BasicPoint> &  bt_neighbor = neighbor_ch->info().basis_def();
                for( int j = 0 ; j < 3 ; j++ )
                    bt_dif[j] += basis_transform[j] - bt_neighbor[j];

                count++;
            }
        }

        if(count >0){
            basis_laplacian = computeNorm(bt_dif);
            basis_laplacian = basis_laplacian/count;
        }
    }
    ch->info().set_distortion_laplacian(basis_laplacian);

    return basis_laplacian;
}

bool TetMeshCreator::invertMatrix( const std::vector<BasicPoint> & m, std::vector<BasicPoint> & invM ){

    float a = m[ 0 ][ 0 ]; float b = m[ 1 ][ 0 ]; float c = m[ 2 ][ 0 ];
    float d = m[ 0 ][ 1 ]; float e = m[ 1 ][ 1 ]; float f = m[ 2 ][ 1 ];
    float g = m[ 0 ][ 2 ]; float h = m[ 1 ][ 2 ]; float k = m[ 2 ][ 2 ];

    float det =  a*(e*k - h*f) + b*(g*f - d*k) + c*(d*h - g*e);

    if( det == 0 ) return false;

    invM[ 0 ][ 0 ] = (e*k - f*h)/det; invM[ 1 ][ 0 ] = (c*h - b*k)/det; invM[ 2 ][ 0 ] = (b*f - c*e)/det;
    invM[ 0 ][ 1 ] = (f*g - d*k)/det; invM[ 1 ][ 1 ] = (a*k - c*g)/det; invM[ 2 ][ 1 ] = (c*d - a*f)/det;
    invM[ 0 ][ 2 ] = (d*h - e*g)/det; invM[ 1 ][ 2 ] = (g*b - a*h)/det; invM[ 2 ][ 2 ] = (a*e - b*d)/det;

    return true;
}

void TetMeshCreator::computeBasisTransform( const Triangulation::Cell_handle &ch , std::vector<BasicPoint> &  D ){

    std::vector<BasicPoint> B (3, BasicPoint(0.,0.,0.));
    B [0] = to_basic_point(initial_vertices_positions[ch->vertex(3)->info()] - initial_vertices_positions[ch->vertex(0)->info()]);
    B [1] = to_basic_point(initial_vertices_positions[ch->vertex(1)->info()] - initial_vertices_positions[ch->vertex(0)->info()]);
    B [2] = to_basic_point(initial_vertices_positions[ch->vertex(2)->info()] - initial_vertices_positions[ch->vertex(0)->info()]) ;

    std::vector<BasicPoint> Bp (3, BasicPoint(0.,0.,0.));
    Bp [0] = to_basic_point(ch->vertex(3)->point()) - to_basic_point(ch->vertex(0)->point());
    Bp [1] = to_basic_point(ch->vertex(1)->point()) - to_basic_point(ch->vertex(0)->point());
    Bp [2] = to_basic_point(ch->vertex(2)->point()) - to_basic_point(ch->vertex(0)->point()) ;



    std::vector<BasicPoint> invB (3, BasicPoint(0.,0.,0.));

    if(invertMatrix(B, invB)){

        D[0] = BasicPoint (Bp[0][0]*invB[0][0] + Bp[1][0]*invB[0][1] + Bp[2][0]*invB[0][2],
                Bp[0][1]*invB[0][0] + Bp[1][1]*invB[0][1] + Bp[2][1]*invB[0][2],
                Bp[0][2]*invB[0][0] + Bp[1][2]*invB[0][1] + Bp[2][2]*invB[0][2] );

        D[1] = BasicPoint (Bp[0][0]*invB[1][0] + Bp[1][0]*invB[1][1] + Bp[2][0]*invB[1][2],
                Bp[0][1]*invB[1][0] + Bp[1][1]*invB[1][1] + Bp[2][1]*invB[1][2],
                Bp[0][2]*invB[1][0] + Bp[1][2]*invB[1][1] + Bp[2][2]*invB[1][2] );

        D[2] = BasicPoint (Bp[0][0]*invB[2][0] + Bp[1][0]*invB[2][1] + Bp[2][0]*invB[2][2],
                Bp[0][1]*invB[2][0] + Bp[1][1]*invB[2][1] + Bp[2][1]*invB[2][2],
                Bp[0][2]*invB[2][0] + Bp[1][2]*invB[2][1] + Bp[2][2]*invB[2][2] );

    }
}

void TetMeshCreator::computeVerticesMotion( std::vector< Geom_traits::Vector_3 > & vertices_motion ){
    std::cout << "TetMeshCreator::Vertices motion()::Start " << std::endl;
    const std::vector<BasicPoint> & V = cageInterface->get_mesh_modified_vertices();
    vertices_motion.clear();
    vertices_motion.resize(transfer_mesh->number_of_vertices());


    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
        if( vit->info() >= vertices_motion.size()) {
            std::cout << "motion count pb!!! " << vit->info() << std::endl;
            return;
        } else if(vit->info() >= initial_vertices_positions.size() ){
            std::cout << "transfer count pb!!! " << vit->info() << std::endl;
            return;
        } else if( vit->info() >= V.size()  ){
            std::cout << "cage int count pb!!! " << vit->info() << std::endl;
            return;
        }

        vertices_motion[vit->info()] = initial_vertices_positions[vit->info()] - to_point_3(V[vit->info()]);
    }
    std::cout << "TetMeshCreator::Vertices motion()::End " << std::endl;
}

void TetMeshCreator::computeBasisLaplacians(){
    std::cout << "TetMeshCreator::computeBasisLaplacians()::Start " << std::endl;

    computeBasisTransforms();

    max_gradient = -FLT_MAX;
    min_gradient = FLT_MAX;
    for(Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end() ; ++cit){
        if(is_in_complex(cit)){
            float basis_laplacian = computeVolumeBasisLaplacian(cit);
            max_gradient = std::max(basis_laplacian, max_gradient);
            min_gradient = std::min(basis_laplacian, min_gradient);
        }
    }

    std::cout << "max_value " << max_gradient << ",  " << min_gradient << std::endl;
    std::cout << "TetMeshCreator::computeBasisLaplacians()::End " << std::endl;
}

void TetMeshCreator::computeTetVertexGradient(){
    std::cout << "TetMeshCreator::computeTetVertexGradient()::Start " << std::endl;

    std::vector< Geom_traits::Vector_3 > vertices_motion (transfer_mesh->number_of_vertices());
    computeVerticesMotion(vertices_motion);


    vertices_gradient.clear();
    vertices_gradient.resize (transfer_mesh->number_of_vertices(), 0.);
    max_gradient = -FLT_MAX;
    min_gradient = FLT_MAX;
    for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){

        float motion_gradient = computeVertexGradient(vit, vertices_motion);

        max_gradient = std::max(motion_gradient, max_gradient);
        min_gradient = std::min(motion_gradient, min_gradient);
        vertices_gradient[vit->info()] = motion_gradient ;
    }

    std::cout << "TetMeshCreator::computeTetVertexGradient()::End " << std::endl;
}

float TetMeshCreator::computeVertexGradient( const Triangulation::Vertex_handle & vh, std::vector< Geom_traits::Vector_3 > & vertices_motion ){

    std::cout << "TetMeshCreator::computeVertexGradient()::Start " << std::endl;
    std::vector<Triangulation::Vertex_handle> neighbor_vertices;
    std::cout << "transfer_mesh->incident_vertices()::" << vh->info() << std::endl;
    transfer_mesh->finite_incident_vertices(vh, std::back_inserter(neighbor_vertices));

    std::cout << "transfer_mesh->incident_vertices()::Done" << std::endl;
    Geom_traits::Vector_3 average_motion = Geom_traits::Vector_3(0.,0.,0.);

    int count = 0;
    for( unsigned int i = 0 ; i < neighbor_vertices.size() ; i ++  ){
        if(hasInComplexCell(vh, neighbor_vertices[i])){
            average_motion = average_motion + vertices_motion[neighbor_vertices[i]->info()] ;
            count ++;

        }
    }

    float motion_gradient = 0.;

    if( count > 0 ){
        average_motion = average_motion/count;
        Geom_traits::Vector_3 motion = average_motion - vertices_motion[vh->info()];

        motion_gradient = CGAL::sqrt(motion*motion);
    }

    std::cout << "TetMeshCreator::computeVertexGradient()::End " << motion_gradient << std::endl;
    return motion_gradient ;
}

bool TetMeshCreator::computeSteinerPointPosition( const Triangulation::Vertex_handle & vh , K::Vector_3 & average_position ){

    std::cout << "TetMeshCreator::computeSteinerPointPosition()::Start " << vh->info() << std::endl;

    std::vector<Triangulation::Vertex_handle> neighbor_vertices;
    transfer_mesh->finite_incident_vertices(vh, std::back_inserter(neighbor_vertices));

    average_position = Geom_traits::Vector_3(0.,0.,0.);

    int count = 0;

    for( unsigned int i = 0 ; i < neighbor_vertices.size() ; i ++  ){
        if(hasInComplexCell(vh, neighbor_vertices[i])){
            Point_3 & point = initial_vertices_positions[neighbor_vertices[i]->info()];
            average_position = average_position + Geom_traits::Vector_3(point.x(), point.y(), point.z()) ;

            count ++;

        }
    }

    if( count == 0 ) return false;

    average_position = average_position/count;
    std::cout << "TetMeshCreator::computeSteinerPointPosition()::End " << average_position << std::endl;
    return true;
}
void TetMeshCreator::initializeErrorQueue(){

    if( distortionType == VERTEX_BASED){
        while(!vertex_error_queue.empty())
            vertex_error_queue.pop();

        suggestion.clear();
        suggestion.resize( transfer_mesh->number_of_vertices(), 0 );

        objectifGradient = 0.5*max_gradient;

        for( Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end(); ++vit ){
            if(vertices_gradient[vit->info()] > objectifGradient){
                vertex_error_queue.push(ErrorVHandleSug(vertices_gradient[vit->info()], VHandleSug(vit,0) ));
            }
        }
    } else{
        while(!cell_error_queue.empty())
            cell_error_queue.pop();

        for( Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin() ; cit != transfer_mesh->finite_cells_end(); ++cit ){
            if( is_in_complex(cit) && cit->info().distortion_laplacian() > objectifGradient){
                cell_error_queue.push(std::make_pair(cit->info().distortion_laplacian(), cit));
            }
        }
    }

}


void TetMeshCreator::computeDistortions(){

    if(distortionType == VERTEX_BASED) computeTetVertexGradient();
    else computeBasisLaplacians();

}

//bool TetMeshCreator::needsRefinement(const Triangulation::Cell_handle & ch ){
//
//}

void TetMeshCreator::adaptSampling( ){
    set_transfer_mesh_to_initial_state();
    added_points.clear();

    initializeErrorQueue();

    std::vector< Geom_traits::Vector_3 > vertices_motion ;
    computeVerticesMotion(vertices_motion);


    if( distortionType == VERTEX_BASED){

        while(!vertex_error_queue.empty()){
            ErrorVHandleSug errorVHandleSug = vertex_error_queue.top();
            vertex_error_queue.pop();

            Triangulation::Vertex_handle vh = errorVHandleSug.second.first;
            int sug = errorVHandleSug.second.second;

            if( sug == suggestion[vh->info()] ){
                if( samplingType == CELL_BARYCENTER ){
                    addVerticesAtCellsBarycenterAndUpdate( vh, vertices_motion );
                } else {
                    splitEdgesAndUpdate( vh, vertices_motion );
                }
            }
        }

    } else {

        while(!cell_error_queue.empty() && added_points.size() < 100){
            std::pair< float, Triangulation::Cell_handle > distCell = cell_error_queue.top();
            cell_error_queue.pop();

            Triangulation::Cell_handle ch = distCell.second;



            if( samplingType == CELL_BARYCENTER ){
                addVertexAtBarycenter( ch , vertices_motion );
            }
        }

    }

    updateVertices();
    transfer_mesh->is_valid(true);

    //    visu_mesh->clear();
    //   updateVertices();
    //   update_visu_mesh();
    //    visu_mesh->update();
    //
    // getMesh(mesh);
    //set_transfer_mesh_to_initial_state();
    std::cout << "TetMeshCreator::adaptSampling()::end" << std::endl;
}

void TetMeshCreator::getOppositeEdgeIndices(int i0, int i1, int & i2, int & i3 ){

    i2 = indices[i0][0];
    i3 = indices[i0][1];
    if(i2 == i1)
        i2 = indices[i0][2];
    else if(i3 == i1)
        i3 = indices[i0][2];

}

void TetMeshCreator::splitEdgesAndUpdate( const Triangulation::Vertex_handle & vh,  std::vector< K::Vector_3 > &vertices_motion){

    std::vector<Triangulation::Edge> iedges;

    transfer_mesh->finite_incident_edges(vh, std::back_inserter(iedges));

    std::vector<std::pair<Triangulation::Vertex_handle, Triangulation::Vertex_handle> > vpairs;

    for(unsigned int i = 0 ; i < iedges.size(); i ++){
        const Triangulation::Edge & edge = iedges[i];
        vpairs.push_back( std::make_pair(edge.first->vertex(edge.second), edge.first->vertex(edge.third) ) );

    }

    for(unsigned int i = 0 ; i < vpairs.size(); i ++){

        Triangulation::Cell_handle cell;
        int v0, v1;

        Triangulation::Vertex_handle vh0 = vpairs[i].first, vh1 = vpairs[i].second;
        if(transfer_mesh->is_edge(vh0, vh1, cell, v0, v1)){

            std::map<std::pair<int,int> , bool > oppositeVpairs;

            Triangulation::Cell_circulator cell_circulator = transfer_mesh->incident_cells( cell, v0, v1);
            Triangulation::Cell_circulator done(cell_circulator);

            do{
                if(!transfer_mesh->is_infinite(cell_circulator)){
                    int i0 = cell_circulator->index(vh0), i1= cell_circulator->index(vh1) , i2, i3;
                    getOppositeEdgeIndices(i0,i1,i2,i3);

                    oppositeVpairs[std::make_pair(cell->vertex(i2)->info(), cell->vertex(i3)->info())] = cell_circulator->info().in_complex();
                }
            }while (++cell_circulator != done);

            const Point_3 & p0 = vh0->point();
            const Point_3 & p1 = vh1->point();
            Point_3 new_point = p0 + (p1 -p0)/2.;

            BasicPoint deformed_point;
            BasicPoint point_to_add = BasicPoint(new_point.x(), new_point.y(), new_point.z());

            cageInterface->addModelVertexAndComputeCoordinates(point_to_add, deformed_point);

            Point_3 dPoint (deformed_point[0], deformed_point[1], deformed_point[2]);
            Point_3 iPoint (new_point.x(), new_point.y(), new_point.z());

            Triangulation::Vertex_handle vh = transfer_mesh->insert_in_edge(iPoint, cell, v0, v1);
            vh->info() = initial_vertices_positions.size();

            initial_vertices_positions.push_back(iPoint);
            points.push_back(point_to_add);

            added_points.push_back(deformed_point);

            vertices_motion.push_back(iPoint - dPoint);


            transfer_mesh->is_edge(vh0, vh, cell, v0, v1);

            cell_circulator = transfer_mesh->incident_cells(cell, v0, v1);
            done = cell_circulator;

            do{
                if(!transfer_mesh->is_infinite(cell_circulator)){
                    int i0 = cell_circulator->index(vh0), i1= cell_circulator->index(vh) , i2, i3;
                    getOppositeEdgeIndices(i0,i1,i2,i3);

                    if(oppositeVpairs.find(std::make_pair(cell_circulator->vertex(i2)->info(), cell_circulator->vertex(i3)->info())) == oppositeVpairs.end() )
                        std::cout << "Oups"<< std::endl;
                    cell_circulator->info().set_in_complex(oppositeVpairs[std::make_pair(cell_circulator->vertex(i2)->info(), cell_circulator->vertex(i3)->info())]);
                }
            }while (++cell_circulator != done);


            transfer_mesh->is_edge(vh, vh1, cell, v0, v1);

            cell_circulator = transfer_mesh->incident_cells(cell, v0, v1);
            done = cell_circulator;

            do{
                if(!transfer_mesh->is_infinite(cell_circulator)){
                    int i0 = cell_circulator->index(vh), i1= cell_circulator->index(vh1) , i2, i3;
                    getOppositeEdgeIndices(i0,i1,i2,i3);

                    if(oppositeVpairs.find(std::make_pair(cell_circulator->vertex(i2)->info(), cell_circulator->vertex(i3)->info())) == oppositeVpairs.end() )
                        std::cout << "Oups"<< std::endl;
                    cell_circulator->info().set_in_complex(oppositeVpairs[std::make_pair(cell_circulator->vertex(i2)->info(), cell_circulator->vertex(i3)->info())]);
                }
            }while (++cell_circulator != done);

            updateNewVertexAndNeighborsGradients(vh, vertices_motion);
        }


    }

    std::cout << "addVertexAndUpdate::end " << std::endl;
}

Point_3 TetMeshCreator::computeCellBarycenter(const Triangulation::Cell_handle & ch){

    K::Vector_3 barycenter (0.,0.,0.);
    for( unsigned int i = 0 ; i < 4; i ++ ){
        const Point_3 & point = ch->vertex(i)->point();
        K::Vector_3 p =  Vector_3(point.x(), point.y(), point.z());
        barycenter = p + barycenter;
    }

    barycenter = barycenter/4.;

    return Point_3(barycenter.x(), barycenter.y(), barycenter.z());
}

void TetMeshCreator::addVertexAtBarycenter(Triangulation::Cell_handle & ch, std::vector< K::Vector_3 > &vertices_motion){
    if(is_in_complex(ch)){

        Point_3 new_point = computeCellBarycenter(ch);

        BasicPoint deformed_point;
        BasicPoint point_to_add = BasicPoint(new_point.x(), new_point.y(), new_point.z());

        cageInterface->addModelVertexAndComputeCoordinates(point_to_add, deformed_point);

        Point_3 dPoint (deformed_point[0], deformed_point[1], deformed_point[2]);
        Point_3 iPoint (new_point.x(), new_point.y(), new_point.z());

        Triangulation::Vertex_handle vh = transfer_mesh->insert_in_cell(iPoint, ch);
        vh->info() = initial_vertices_positions.size();

        initial_vertices_positions.push_back(iPoint);
        points.push_back(point_to_add);

        added_points.push_back(deformed_point);

        vertices_motion.push_back(iPoint - dPoint);

        std::vector<Triangulation::Cell_handle> icells;
        transfer_mesh->finite_incident_cells( vh, std::back_inserter(icells) );

        for( unsigned int i = 0 ; i < icells.size() ; i ++ ){
            Triangulation::Cell_handle & ch = icells[i];
            ch->info().set_in_complex(true);
        }

        updateNewVertexAndNeighborsGradients(vh, vertices_motion);
    }
}

void TetMeshCreator::addVerticesAtCellsBarycenterAndUpdate(Triangulation::Vertex_handle & vh, std::vector< K::Vector_3 > &vertices_motion){


    std::vector<Triangulation::Cell_handle> iCells;

    transfer_mesh->finite_incident_cells(vh, std::back_inserter(iCells));


    for(unsigned int i = 0 ; i < iCells.size(); i ++){
        addVertexAtBarycenter(iCells[i], vertices_motion);
    }
    std::cout << "addVertexAndUpdate::end " << std::endl;

}

void TetMeshCreator::addVertexAndUpdate( const K::Vector_3 & v3,  std::vector< K::Vector_3 > &vertices_motion, Triangulation::Cell_handle ch ){

    std::cout << "addVertexAndUpdate::start " << std::endl;
    BasicPoint deformed_point;
    BasicPoint point_to_add = BasicPoint(v3.x(), v3.y(), v3.z());

    cageInterface->addModelVertexAndComputeCoordinates(point_to_add, deformed_point);

    Point_3 dPoint (deformed_point[0], deformed_point[1], deformed_point[2]);
    Point_3 iPoint (v3.x(), v3.y(), v3.z());

    Triangulation::Vertex_handle vh = transfer_mesh->insert(iPoint, ch);
    vh->info() = initial_vertices_positions.size();

    std::cout << "id " <<  vh->info() << std::endl;
    initial_vertices_positions.push_back(iPoint);
    points.push_back(point_to_add);

    added_points.push_back(deformed_point);

    vertices_motion.push_back(iPoint - dPoint);

    std::vector<Triangulation::Cell_handle> icells;
    transfer_mesh->finite_incident_cells( vh, std::back_inserter(icells) );

    for( unsigned int i = 0 ; i < icells.size() ; i ++ ){
        Triangulation::Cell_handle & ch = icells[i];
        ch->info().set_in_complex(is_tet_in_complex(ch));
    }


    updateNewVertexAndNeighborsGradients(vh, vertices_motion);


    std::cout << "addVertexAndUpdate::end " << std::endl;
}



void TetMeshCreator::updateNewVertexAndNeighborsGradients( const Triangulation::Vertex_handle & vh, std::vector< K::Vector_3 > &vertices_motion ){


    if( distortionType == VERTEX_BASED){
        float motion_gradient = computeVertexGradient(vh, vertices_motion);

        max_gradient = std::max(motion_gradient, max_gradient);
        min_gradient = std::min(motion_gradient, min_gradient);

        vertices_gradient.push_back( motion_gradient );
        suggestion.push_back(0);


        //    if(motion_gradient > objectifGradient){
        //        error_queue.push(ErrorVHandleSug(motion_gradient, VHandleSug(vh, 0)));
        //    }

        std::cout << "motion gradient after " << motion_gradient << std::endl;

        std::vector<Triangulation::Vertex_handle> neighbor_vertices;
        transfer_mesh->finite_incident_vertices(vh, std::back_inserter(neighbor_vertices));

        //    for( unsigned int i = 0 ; i < neighbor_vertices.size() ; i ++  ){
        //        Triangulation::Vertex_handle & nvh = neighbor_vertices[i];
        //        if(hasInComplexCell(vh, nvh, triangulation)){
        //            float motion_gradient = computeVertexGradient(nvh, triangulation, vertices_motion);
        //
        //            max_gradient = std::max(motion_gradient, max_gradient);
        //            vertices_gradient[nvh->info()] =  motion_gradient ;
        //
        //            suggestion[nvh->info()]++;
        //            if(motion_gradient > objectifGradient)
        //                error_queue.push(ErrorVHandleSug(motion_gradient, VHandleSug(nvh, suggestion[nvh->info()])));
        //        }
        //    }
    } else {

        std::vector<Triangulation::Cell_handle> iCells;
        transfer_mesh->finite_incident_cells(vh, std::back_inserter(iCells));

        for(unsigned int i = 0 ; i < iCells.size() ; i++){
            std::vector<BasicPoint> basis_tr (3, BasicPoint(0.,0.,0.));
            computeBasisTransform( iCells[i], basis_tr );
            iCells[i]->info().set_basis_def(basis_tr);
        }
        for(unsigned int i = 0 ; i < iCells.size() ; i++){
            float basis_tr_laplacian = computeVolumeBasisLaplacian(iCells[i]);
            max_gradient = std::max(max_gradient, basis_tr_laplacian);
            min_gradient = std::min(basis_tr_laplacian, min_gradient);
            //            if(basis_tr_laplacian > objectifGradient){
            //                cell_error_queue.push(std::make_pair(basis_tr_laplacian, iCells[i]));
            //            }
        }

    }
}

bool TetMeshCreator::is_tet_in_complex( const Triangulation::Cell_handle & ch ){

    BasicPoint tet_bb_min;
    BasicPoint tet_bb_max;

    computeInitialTetBB(ch, tet_bb_min, tet_bb_max);

    Voxel vmin = voxel_grid->getGridCoordinate(tet_bb_min);
    Voxel vmax = voxel_grid->getGridCoordinate(tet_bb_max);

    for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
        for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
            for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){

                BasicPoint position = voxel_grid->getWorldCoordinate(i,j,k);

                if( voxel_grid->is_in_offseted_domains(i,j,k) ){

                    float ld[4];
                    computeBarycentricCoordinates(ch, position, ld[0], ld[1], ld[2], ld[3]);

                    if(ld[0] >= 0 && ld[0] <=1. && ld[1] >= 0 && ld[1] <=1. && ld[2] >= 0 && ld[2] <=1. && ld[3] >= 0 && ld[3] <=1. ){
                        return true;
                    }
                }
            }

        }
    }

    return false;
}

bool TetMeshCreator::findInComplexCell( Triangulation::Cell_handle &ch, Triangulation::Locate_type locate_type, int li, int lj){

    if(locate_type != Triangulation::OUTSIDE_CONVEX_HULL && locate_type != Triangulation::OUTSIDE_AFFINE_HULL){

        if( is_in_complex(ch) ) {
            return true;
        }

        if( locate_type == Triangulation::CELL ){
            return false;
        }

        if( locate_type == Triangulation::VERTEX ){

            std::vector<Triangulation::Cell_handle> icells;
            transfer_mesh->finite_incident_cells( ch->vertex(li), std::back_inserter(icells) );

            for( unsigned int i = 0 ; i < icells.size() ; i ++ ){
                Triangulation::Cell_handle & ich = icells[i];
                if(is_in_complex(ich)){
                    ch = ich;
                    return true;
                }
            }

        } else if( locate_type == Triangulation::EDGE ) {

            Triangulation::Edge edge(ch, li, lj);

            Triangulation::Cell_circulator cell_circulator = transfer_mesh->incident_cells(edge);
            Triangulation::Cell_circulator done(cell_circulator);

            do{
                if(is_in_complex(cell_circulator)){
                    ch = cell_circulator;
                    return true;
                }
            }while (++cell_circulator != done);

        } else if( locate_type == Triangulation::FACET ) {
            Triangulation::Cell_handle n = ch->neighbor(li);
            if(is_in_complex(n)){
                ch = n;
                return true;
            }
        }
    }

    return false;
}

bool TetMeshCreator::locatePointInComplex(const BasicPoint & point, Triangulation::Cell_handle & ch, const Triangulation::Cell_handle & start ){

    Triangulation::Locate_type locate_type;
    int li, lj;
    ch = transfer_mesh->locate(to_point_3(point), locate_type, li, lj, start);

    //    if(locate_type != Triangulation::OUTSIDE_CONVEX_HULL && locate_type != Triangulation::OUTSIDE_AFFINE_HULL){
    //        if(is_in_complex(ch))
    //            return true;
    //    }
    //    return false;
    return findInComplexCell( ch, locate_type, li, lj);

}


void TetMeshCreator::checkIfPointsAreInsideTrMesh(){

    added_points.clear();
    std::vector<char> visited(voxel_grid->size(), 0);

    for( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin(); cit != c3t3_with_info.cells_end() ; ++cit ){
        BasicPoint tet_bb_min, tet_bb_max;

        computeDeformedTetBB(cit, tet_bb_min, tet_bb_max);
        //        Voxel vmin (std::max(0, int(bb_min[0]), std::max(0, int(bb_min[1]), std::max(0, int(bb_min[2]));
        //        Voxel vmax (std::min(result.xdim()-1, int(bb_max[0]), std::min(result.ydim()-1, int(bb_max[1]), std::max(result.zdim()-1, int(bb_max[2]));


        Voxel vmin = voxel_grid->getGridCoordinate(tet_bb_min);
        Voxel vmax = voxel_grid->getGridCoordinate(tet_bb_max);

        for( int i = vmin.i() ; i <=vmax.i() ; i ++ ){
            for( int j = vmin.j(); j <= vmax.j() ; j ++ ){
                for( int k = vmin.k() ; k <= vmax.k() ; k ++ ){

                    BasicPoint position = voxel_grid->getWorldCoordinate(i,j,k);
                    int g_id = voxel_grid->getGridIndex(i,j,k);
                    if(voxel_grid->is_in_offseted_domains(i,j,k) && visited[g_id] == 0){

                        float ld[4];
                        computeBarycentricCoordinates(cit, position, ld[0], ld[1], ld[2], ld[3]);
                        if(ld[0] >= 0 && ld[0] <=1. && ld[1] >= 0 && ld[1] <=1. && ld[2] >= 0 && ld[2] <=1. && ld[3] >= 0 && ld[3] <=1. ){


                            visited[g_id] = 1;
                        }
                    }
                }

            }
        }
    }

    visited.clear();
}

bool TetMeshCreator::deformPointsUsingTransferMesh(const std::vector<BasicPoint> & positions, std::vector<BasicPoint> & results ){

    set_transfer_mesh_to_initial_state();
    results.clear();
    results.resize(positions.size());

    const std::vector<BasicPoint> & V = cageInterface->get_mesh_modified_vertices();

    Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin();

    Triangulation::Cell_handle start = defStart;
    for(unsigned int i = 0; i < positions.size() ; i++ ){

        Triangulation::Cell_handle ch ;
        std::vector<float> coords (4, -1.);

        if(locatePointInComplex(positions[i], ch, start)){
            start = ch;
            computeBarycentricCoordinates(ch , positions[i], coords[0], coords[1], coords[2], coords[3]);

            const BasicPoint & p0 = V[ch->vertex(0)->info()];
            const BasicPoint & p1 = V[ch->vertex(1)->info()];
            const BasicPoint & p2 = V[ch->vertex(2)->info()];
            const BasicPoint & p3 = V[ch->vertex(3)->info()];

            results[i] = coords[0]*p0 + coords[1]*p1 + coords[2]*p2 + coords[3]*p3;

        } else {
            return false;
            std::cout << "error: point not inside Transfer Mesh..." << std::endl;

        }
    }
    updateVertices();
    return true;
}


//void TetMeshCreator::addVertexAtTetrahedronBarycenterAndUpdate( const Point_3 & point, Triangulation::Cell_handle & ch ){
//
//    Triangulation::Vertex_handle vh = transfer_mesh->insert(point, ch);
//
//
//}
//
//void TetMeshCreator::computeTetStretch(){
//
//    //    std::map< std::pair<Triangulation::Vertex_handle , Triangulation::Vertex_handle>, float > edgeElongation;
//    //    for( Triangulation::Finite_edges_iterator eit = transfer_mesh->finite_edges_begin() ; eit != transfer_mesh->finite_edges_end(); ++eit ){
//    //        const Triangulation::Cell_handle & ch = eit->first;
//    //        const Triangulation::Vertex_handle & vh0 = ch->vertex(eit->second);
//    //        const Triangulation::Vertex_handle & vh1 = ch->vertex(eit->third);
//    //
//    //        Geom_traits::Vector_3 initVector = initCh->vertex(eit->second)->point() - initCh->vertex(eit->third)->point();
//    //        float initLength = CGAL::sqrt(initVector*initVector);
//    //
//    //        Geom_traits::Vector_3 currentVector = vh0->point() - vh1->point();
//    //        float length = CGAL::sqrt(currentVector*currentVector);
//    //
//    //        edgeElongation[ std::make_pair(vh0, vh1) ] = length/initLength;
//    //    }
//    //
//    //    for( Triangulation::Finite_edges_iterator eit = transfer_mesh->finite_edges_begin() ; eit != transfer_mesh->finite_edges_end(); ++eit ){
//    //        std::cout << edgeElongation[ std::make_pair(eit->first->vertex(eit->second), eit->first->vertex(eit->third)) ] << std::endl;
//    //    }
//
//}
//
//void TetMeshCreator::recomputeInitialInfos( const std::vector<BasicPoint> & positions ){
//
//
//    defStart = Triangulation::Cell_handle();
//    initPointBaryCoords.clear();
//
//    initial_vertices_positions.clear();
//
//    for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin(); vit != transfer_mesh->finite_vertices_end() ; ++vit){
//        initial_vertices_positions.push_back(Point_3(vit->point()));
//    }
//
//    Triangulation::Finite_cells_iterator cit = transfer_mesh->finite_cells_begin();
//    Triangulation::Cell_handle start = cit;
//
//    for(unsigned int i = 0; i < positions.size() ; i++ ){
//        Triangulation::Locate_type locate_type;
//        int li, lj;
//        Triangulation::Cell_handle ch = transfer_mesh->locate(to_point_3(positions[i]), locate_type, li, lj, start);
//
//        std::vector<float> coords (4, -1.);
//
//        if(locate_type != Triangulation::OUTSIDE_CONVEX_HULL && locate_type != Triangulation::OUTSIDE_AFFINE_HULL){
//            start = ch;
//            computeBarycentricCoordinates(ch , positions[i], coords[0], coords[1], coords[2], coords[3]);
//        } else {
//            std::cout << "error..." << std::endl;
//        }
//        initPointBaryCoords.push_back( std::make_pair( ch, coords) );
//    }
//}
//

bool TetMeshCreator::hasInComplexCell( const Triangulation::Vertex_handle & vh0, const Triangulation::Vertex_handle & vh1 ){
    Triangulation::Cell_handle cell;
    int v0, v1;

    if(transfer_mesh->is_edge(vh0, vh1, cell, v0, v1)){
        Triangulation::Edge edge(cell, v0, v1);
        return hasInComplexCell(edge);
    }
    return false;
}

bool TetMeshCreator::hasInComplexCell( const Triangulation::Edge & edge ) {


    Triangulation::Cell_circulator cell_circulator = transfer_mesh->incident_cells(edge);
    Triangulation::Cell_circulator done(cell_circulator);

    do{
        if(is_in_complex(cell_circulator))
            return true;
    }while (++cell_circulator != done);
    return false;

}

void TetMeshCreator::glSmoothFacet(const Triangulation::Facet & facet){
    const Triangulation::Cell_handle & ch = facet.first;

    const Point_3 & pa = ch->vertex(indices[facet.second][0])->point();
    const Point_3 & pb = ch->vertex(indices[facet.second][1])->point();
    const Point_3 & pc = ch->vertex(indices[facet.second][2])->point();

    if(!visibility_check || isVisiblePoint(pa) && isVisiblePoint(pb) && isVisiblePoint(pc) && isVisiblePoint(ch->vertex(facet.second)->point())){
        const Geom_traits::Vector_3 & na = vertices_normals[ch->vertex(indices[facet.second][0])->info()];
        glNormal3d(na.x(),na.y(),na.z());
        glVertex3d(pa.x(),pa.y(),pa.z());

        const Geom_traits::Vector_3 & nb = vertices_normals[ch->vertex(indices[facet.second][1])->info()];
        glNormal3d(nb.x(),nb.y(),nb.z());
        glVertex3d(pb.x(),pb.y(),pb.z());

        const Geom_traits::Vector_3 & nc = vertices_normals[ch->vertex(indices[facet.second][2])->info()];
        glNormal3d(nc.x(),nc.y(),nc.z());
        glVertex3d(pc.x(),pc.y(),pc.z());
    }
}

void TetMeshCreator::glFacet(const Triangulation::Facet & facet){
    const Triangulation::Cell_handle & ch = facet.first;

    const Point_3 & pa = ch->vertex(indices[facet.second][0])->point();
    const Point_3 & pb = ch->vertex(indices[facet.second][1])->point();
    const Point_3 & pc = ch->vertex(indices[facet.second][2])->point();

    if(!visibility_check || isVisiblePoint(pa) && isVisiblePoint(pb) && isVisiblePoint(pc) ){
        Geom_traits::Vector_3 n = CGAL::cross_product(pb - pa, pc -pa);
        n = n / CGAL::sqrt(n*n);


        glNormal3d(n.x(),n.y(),n.z());

        glVertex3d(pa.x(),pa.y(),pa.z());
        glVertex3d(pb.x(),pb.y(),pb.z());
        glVertex3d(pc.x(),pc.y(),pc.z());
    }
}

void TetMeshCreator::drawCageTetMesh(){
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    glBegin (GL_TRIANGLES);

    for( Triangulation::Finite_facets_iterator fit = from_poly_c3t3.triangulation().finite_facets_begin() ; fit != from_poly_c3t3.triangulation().finite_facets_end(); ++fit ){

        if(from_poly_c3t3.is_in_complex(fit->first)){
            glFacet(*fit);
        } else {
            C3t3_with_info::Facet mirror_facet = from_poly_c3t3.triangulation().mirror_facet(*fit);
            if(from_poly_c3t3.is_in_complex(mirror_facet.first)){
                glFacet(mirror_facet);
            }
        }
    }

    glEnd();

    //glDisable(GL_DEPTH_TEST);
    //  glDisable(GL_DEPTH);
}

bool TetMeshCreator::isVisiblePoint( const Point_3 & point ){

    const BasicPoint pos = to_basic_point(point);
    float vis = dot( clippingNormal, pos - pointOnClipping );

    float cutVis[3];
    for( int i = 0 ; i < 3 ; i ++ )
        cutVis[i] = (pos[i] - cut[i])*cutDirection[i];

    if( vis < 0. || cutVis[0] < 0.|| cutVis[1] < 0.|| cutVis[2] < 0. )
        return false;
    return true;
}

bool TetMeshCreator::isVisibleTet(const Triangulation::Cell_handle & ch){

    int visCount = 0;
    for(unsigned int v = 0 ; v < 4 ; v++ )
        if( isVisiblePoint( ch->vertex(v)->point() ) )  visCount++;

    return ( visCount != 0 && visCount != 4 );
}

float TetMeshCreator::getDistortionColor(float value){

    float max = 250./360.;
    float v = ( value-min_gradient )/(max_gradient - min_gradient);

    return (1. - v)*max;
}

void TetMeshCreator::drawWithVertexGradient(const Triangulation::Cell_handle & ch, int i, int n_dir){


    const Point_3 & pa = ch->vertex(indices[i][0])->point();
    const Point_3 & pb = ch->vertex(indices[i][1])->point();
    const Point_3 & pc = ch->vertex(indices[i][2])->point();

    if(isVisiblePoint(pa) && isVisiblePoint(pb) && isVisiblePoint(pc)){
        Geom_traits::Vector_3 n = CGAL::cross_product(pb - pa, pc -pa);
        n = n_dir*n / CGAL::sqrt(n*n);
        QColor color;

        glNormal3d(n.x(),n.y(),n.z());

        color.setHsvF(getDistortionColor(vertices_gradient[ch->vertex(indices[i][0])->info()]), 1.,1.);
        glColor3f(color.redF(), color.greenF(), color.blueF());
        glVertex3d(pa.x(),pa.y(),pa.z());

        color.setHsvF(getDistortionColor(vertices_gradient[ch->vertex(indices[i][1])->info()]), 1.,1.);
        glColor3f(color.redF(), color.greenF(), color.blueF());
        glVertex3d(pb.x(),pb.y(),pb.z());

        color.setHsvF(getDistortionColor(vertices_gradient[ch->vertex(indices[i][2])->info()]), 1.,1.);
        glColor3f(color.redF(), color.greenF(), color.blueF());
        glVertex3d(pc.x(),pc.y(),pc.z());
    }
}

void TetMeshCreator::drawTransferMesh(bool displayDistortions, bool displayOutliers, QColor & defaultColor, float radius){

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);

    QColor selectionColor(180, 0, 0);

    if( outlier_vertices.size() > 0  && displayOutliers){
        drawVertices(radius);
    }

    //  updateVertices();

    glColor3f(defaultColor.redF(), defaultColor.greenF(), defaultColor.blueF());

    if(displayDistortions){
        if(distortionType == VERTEX_BASED){
            glBegin (GL_TRIANGLES);
            for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end(); ++fit ){
                Triangulation::Facet mirror_facet = c3t3_with_info.triangulation().mirror_facet(*fit);

                int n = 1;

                if(c3t3_with_info.is_in_complex(mirror_facet.first)) {
                    n = -1;

                }

                drawWithVertexGradient(fit->first, fit->second, n);

            }
            for ( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin() ; cit != c3t3_with_info.cells_end() ; ++cit  )
            {
                if(!visibility_check || isVisibleTet(cit)){
                    for( int i = 0; i < 4 ; i ++ ){
                        drawWithVertexGradient(cit, i);
                    }
                }
            }

            glEnd();

        } else {

            glBegin (GL_TRIANGLES);

            for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end(); ++fit ){
                Triangulation::Facet mirror_facet = c3t3_with_info.triangulation().mirror_facet(*fit);
                if(c3t3_with_info.is_in_complex(fit->first)){
                    QColor color;
                    color.setHsvF(getDistortionColor(fit->first->info().distortion_laplacian()), 1.,1.);
                    glColor3f(color.redF(), color.greenF(), color.blueF());
                    glSmoothFacet(*fit);
                } else  {
                    QColor color;
                    color.setHsvF(getDistortionColor(mirror_facet.first->info().distortion_laplacian()), 1.,1.);
                    glColor3f(color.redF(), color.greenF(), color.blueF());
                    glSmoothFacet(mirror_facet);
                }
            }

            for ( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin() ; cit != c3t3_with_info.cells_end() ; ++cit  )
            {
                if(!visibility_check || isVisibleTet(cit)){
                    QColor color;
                    color.setHsvF(getDistortionColor(cit->info().distortion_laplacian()), 1.,1.);
                    glColor3f(color.redF(), color.greenF(), color.blueF());

                    for( int i = 0; i < 4 ; i ++ ){

                        Triangulation::Facet facet (cit, i);
                        glFacet(facet);
                    }
                }
            }

            glEnd();
        }
    } else if (displayOutliers && outlier_vertices.size() > 0 ) {


        glBegin (GL_TRIANGLES);

        for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end(); ++fit ){
            Triangulation::Facet mirror_facet = c3t3_with_info.triangulation().mirror_facet(*fit);

            if( c3t3_with_info.is_in_complex(fit->first) ){
                QColor color = defaultColor;
                if( !fit->first->info().cage_inlier() )
                    color = selectionColor;
                glColor3f(color.redF(), color.greenF(), color.blueF());
                glSmoothFacet(*fit);
            } else  {
                QColor color = defaultColor;
                if( !mirror_facet.first->info().cage_inlier() )
                    color = selectionColor;
                glColor3f(color.redF(), color.greenF(), color.blueF());
                glSmoothFacet(mirror_facet);
            }
        }

        for ( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin() ; cit != c3t3_with_info.cells_end() ; ++cit  )
        {
            if(!visibility_check || isVisibleTet(cit)){
                QColor color = defaultColor;
                if( !cit->info().cage_inlier() )
                    color = selectionColor;
                glColor3f(color.redF(), color.greenF(), color.blueF());

                for( int i = 0; i < 4 ; i ++ ){

                    Triangulation::Facet facet (cit, i);
                    glFacet(facet);
                }
            }
        }

        glEnd();
    } else {

        glBegin (GL_TRIANGLES);

        for( C3t3_with_info::Facet_iterator fit = c3t3_with_info.facets_begin() ; fit != c3t3_with_info.facets_end(); ++fit ){

            Triangulation::Facet mirror_facet = c3t3_with_info.triangulation().mirror_facet(*fit);
            if(c3t3_with_info.is_in_complex(fit->first)){

                glSmoothFacet(*fit);
            } else  {

                glSmoothFacet(mirror_facet);
            }
        }

        for ( C3t3_with_info::Cell_iterator cit = c3t3_with_info.cells_begin() ; cit != c3t3_with_info.cells_end() ; ++cit  )
        {
            if( (!visibility_check || isVisibleTet(cit) )&& is_in_complex(cit) ){
                for( int i = 0; i < 4 ; i ++ ){
                    Triangulation::Facet facet (cit, i);
                    glFacet(facet);
                }
            }
        }

        glEnd();
    }

    //    set_transfer_mesh_to_initial_state();
    //  glDisable(GL_DEPTH_TEST);
    //   glDisable(GL_DEPTH);
    // drawVertices();

}

void TetMeshCreator::drawVertices(float radius){

    for(Triangulation::Finite_vertices_iterator vit = transfer_mesh->finite_vertices_begin() ; vit != transfer_mesh->finite_vertices_end() ; ++vit)
    {
        BasicPoint const & p = to_basic_point(vit->point());
        if(outlier_vertices[vit->info()] && isVisiblePoint(vit->point()))
            BasicGL::drawSphere( p[0], p[1], p[2], radius, 15, 15 );
    }

}

void TetMeshCreator::drawPointsToAdd(){

    if(added_points.size() > 0){

        glColor3f(0.5,0.5,0.);

        glBegin(GL_POINTS);
        for( unsigned int i = 0 ; i < added_points.size()  ; i++ ){
            glVertex(added_points[i]);
        }
        glEnd();

    }

}

void TetMeshCreator::drawPointDistribution(){

    glColor3f(0.f,0.f,1.f);
    glPointSize(4.);

    glBegin(GL_POINTS);

    for(unsigned int i = 0 ; i < points.size() ; i++){
        glVertex(points[i]);
    }

    glEnd();
}

void TetMeshCreator::drawEdges(){

    glBegin(GL_LINES);

    for( Triangulation::Finite_edges_iterator eit = transfer_mesh->finite_edges_begin(); eit != transfer_mesh->finite_edges_end() ; eit++ ){
        glColor3f(1.f,0.f,0.f);
        if(hasInComplexCell(*eit)){
            glColor3f(0.f,0.f,1.f);
        }

        glVertex(to_basic_point(eit->first->vertex(eit->second)->point()));
        glVertex(to_basic_point(eit->first->vertex(eit->third)->point()));
    }

    glEnd();
}

//
//void TetMeshCreator::drawBoundingBox(){
//    if(dataLoaded){
//        glPushMatrix();
//
//        glDisable(GL_LIGHTING);
//        glPolygonMode(GL_FRONT_AND_BACK , GL_LINE );
//        glEnable( GL_DEPTH_TEST );
//        glEnable(GL_DEPTH);
//        glColor3f(1.f,0.f,0.f);
//        BasicGL::drawCube(BBMin, BBMax);
//        glPolygonMode(GL_FRONT_AND_BACK , GL_FILL );
//
//        glPopMatrix();
//    }
//}
//

