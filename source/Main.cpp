#include "OpenGLHeader.h"
#include <QApplication>
#include "Window.h"
#include "OpenGLHeader.h"

errorCatcher* glCatcher;

int main(int argc, char *argv[])
{
    glCatcher = nullptr;
    QApplication a(argc, argv);
    Window w;
    w.show();
    return a.exec();
}
