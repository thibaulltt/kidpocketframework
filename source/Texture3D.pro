# -------------------------------------------------
# Project created by QtCreator 2010-01-27T15:21:45
# -------------------------------------------------
QT += xml
QT += opengl openglextensions
TARGET = texture3D
TEMPLATE = app
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
QMAKE_CXXFLAGS += -Wno-deprecated-copy -Wno-deprecated-declarations
DEPENDPATH += ./GLSL ./CageDeform  /usr/include/nifti ../external/
LIB_DIR += /usr/local/lib
INCLUDEPATH += ./GLSL ./CageDeform  /usr/include/nifti ../external/ /home/thibault/git/libQGLViewer-2.7.2
SOURCES += Main.cpp \
    Window.cpp \
    TextureViewer.cpp \
    GLSL/ProgramObject.cpp \
    GLSL/ShaderObject.cpp \
    GLSL/ShaderPool.cpp \
    GLSL/VertexShader.cpp \
    GLSL/GeometryShader.cpp \
    GLSL/FragmentShader.cpp \
    GLSL/BufferObject.cpp \
    GLSL/HelperFunctions.cpp \
    Texture.cpp \
    TextureDockWidget.cpp \
    Utils.cpp \
    Tetrahedron.cpp \
    Vertex.cpp \
    VBOHandler.cpp \
    VisuMesh.cpp
HEADERS += Window.h \
    TextureViewer.h \
    OpenGLHeader.h \
    GLSL/ProgramObject.h \
    GLSL/ShaderInclude.h \
    GLSL/ShaderObject.h \
    GLSL/ShaderPool.h \
    GLSL/VertexShader.h \
    GLSL/GeometryShader.h \
    GLSL/FragmentShader.h \
    GLSL/BufferObject.h \
    GLSL/HelperFunctions.h \
    Texture.h \
    TextureDockWidget.h \
    Utils.h \
    Vec3D.h \
    Tetrahedron.h \
    Vertex.h \
    Edge.h \
    VBOHandler.h \
    Face.h \
    Face.h \
    VisuMesh.h
LIBS = -L/usr/lib/x86_64-linux-gnu/ \
    -L/home/thibault/git/libQGLViewer-2.7.2/QGLViewer \
    -L/usr/local/lib \
    -lQGLViewer-qt5 \
    -lglut \
    -lGLU \
    -lGLEW \
    -lniftiio \
    -lznz \
    -lz
