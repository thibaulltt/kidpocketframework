#include "nifti1_io.h"
#include "TextureViewer.h"
#include <cfloat>
#include <QFileDialog>
#include <chrono>
#include <QGLViewer/manipulatedCameraFrame.h>

using namespace std;
using namespace qglviewer;

color4 hsv2rgb_normalized(float h, float s, float v) {
    QColor hsvConverted = QColor::fromHsv(h*400.f, s*255.f, v*255.f);
    double r, g, b, a;
    hsvConverted.getRgbF(&r, &g, &b, &a);
    return color4{static_cast<float>(r), static_cast<float>(g), static_cast<float>(b), static_cast<float>(a)};
}

TextureViewer::TextureViewer(QWidget *parent)
    : QGLViewer(parent), visu_mesh(nullptr) {
    this->colorMap = static_cast<color4*>(calloc(256, sizeof(color4)));
    this->visible_domains = static_cast<unsigned int*>(calloc(256, sizeof(unsigned int)));
}

void TextureViewer::resizeGL(int width, int height) {
    QGLViewer::resizeGL(width, height);
    std::cerr << "Framebuffer : " << width << 'x' << height << '\n';
}

void TextureViewer::draw(){
    // initLights();

    glDisable(GL_CULL_FACE);

    drawClippingPlane();
    glEnable(GL_LIGHTING);

    qglviewer::Vec cam = camera()->worldCoordinatesOf( qglviewer::Vec(0.,0.,0.) );

    glEnable(GL_DEPTH);
    glEnable(GL_DEPTH_TEST);

    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    glDisable(GL_BLEND);

    if (visu_mesh) {
	visu_mesh->setOrthongonalCut( cut, cutDirection );
	visu_mesh->draw( cam );

	if(displayTetrahedra)
	    visu_mesh->drawTetrahedra();
    }


    //BasicGL::drawSphere( lightPositions[activeLight][0], lightPositions[activeLight][1], lightPositions[activeLight][2], 10, 15, 15 );

    if( isRecording ){
	QString imageName = QString(recordingFolderName);
	imageName.append("/");
	imageName.append(QString::number(frameNb));
	frameNb++;
	imageName.append(".png");

	// QImage image = this->grabFramebuffer();
	//	image.save(imageName, "PNG");
    }

    glDisable(GL_LIGHTING);
}

void TextureViewer::drawClippingPlane(){


    glEnable(GL_LIGHTING);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

    glPushMatrix();
    glMultMatrixd(manipulatedFrame()->matrix());
    // Since the Clipping Plane equation is multiplied by the current modelView, we can define a
    // constant equation (plane normal along Z and passing by the origin) since we are here in the
    // manipulatedFrame coordinates system (we glMultMatrixd() with the manipulatedFrame matrix()).
    Vec to(0.,1.,0.);

    static const GLdouble constantEquation[] = { to.x, to.y, to.z, 0.0 };
    glClipPlane(GL_CLIP_PLANE0, constantEquation);

    // Draw a plane representation: Its normal...
    glColor3f(1.f, 0.f, 0.f);
    drawArrow(Vec(0.,0.,0.), to*camera()->sceneRadius()/9.);

    glPopMatrix();
    glDisable(GL_LIGHTING);

    GLdouble equation[4];
    glGetClipPlane(GL_CLIP_PLANE0, equation);

    qreal p[] = {0.,-equation[3]/equation[1], 0.};
    qreal projP[3];
    camera()->getWorldCoordinatesOf(p, projP);


    qreal norm[] = {equation[0] + p[0], equation[1]+ p[1], equation[2]+ p[2]};
    qreal normResult[3];
    camera()->getWorldCoordinatesOf(norm, normResult);

    Vec3Df normal(normResult[0]-projP[0], normResult[1]-projP[1], normResult[2]-projP[2]);
    Vec3Df point(projP[0], projP[1],projP[2]);
    if (visu_mesh) {
	visu_mesh->setClipeEquation( normal, point);
    }

}

void TextureViewer::record( bool is_recording ){
    isRecording = false;
    if( is_recording ){

	recordingFolderName = QFileDialog::getExistingDirectory(this , tr("Folder for the animation"), "./");

	// In case of Cancel
	if ( recordingFolderName.isEmpty() ) {
	    return;
	}
	isRecording = true;
	frameNb = 0;
    }
}

void TextureViewer::init()
{
	if (!glCatcher) {
		glCatcher = new errorCatcher();
	}
    this->initializeOpenGLFunctions();
    //initLights();
    setAnimationPeriod(0);

    animationTimer.setAnimationLengthInMilliSeconds(5000);
    animationTimer.playAnimation = false;


    // The ManipulatedFrame will be used as the clipping plane
    setManipulatedFrame(new ManipulatedFrame());

    // Enable plane clipping
    glEnable(GL_CLIP_PLANE0);

    //Set background color
    setBackgroundColor(QColor(255,255,255));
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

    //Set blend parameters
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#ifndef USE_QT_OPENGLFUNCTIONS
    //init Glew
    glewInit();

    //Check if the GLSL code can be executed
    if (GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader)
	cout << "Ready for GLSL\n";
    else {
	cout << "Not totally ready \n";
	exit(1);
    }

    // Check support for rectangle textures:
    if(glewGetExtension( "GL_TEXTURE_RECTANGLE_ARB")||glewGetExtension( "GL_ARB_texture_rectangle")||glewGetExtension( "GL_EXT_gpu_shader4"))
	cout <<"Rectangular texture supported" << endl;
    else {
	cout <<"Rectangular texture not supported" << endl;
	exit(1);
    }
#endif

    GLTools::initLights();
    GLTools::setSunriseLight();
    GLTools::setDefaultMaterial();

    imageLoaded = false;

    displayTetrahedra = false;

    cut = Vec3Df(0.,0.,0.),
	    cutDirection = Vec3Df(1.,1.,1.);

    activeLight = 0;
    lightPositions0.resize(7);
    lightPositions.resize(7);
    lightPositions0[0] = Vec3Df(0, 0, 50);
    lightPositions0[1] = Vec3Df(0, 0, 50);
    lightPositions0[2] = Vec3Df(52, 16, 50);
    lightPositions0[3] = Vec3Df(26, 48, 50);
    lightPositions0[4] = Vec3Df(-16, 52, 50);
    lightPositions0[5] = Vec3Df(42, 374, 161);
    lightPositions0[6] = Vec3Df(473, -351, -259);

    frameNb =0;
    isRecording = false;

}

void TextureViewer::clear(){

    texture.clear();

}


void TextureViewer::updateCamera(const qglviewer::Vec & center, float radius){
    camera()->setSceneCenter(center);
    camera()->setSceneRadius(radius);

    camera()->showEntireScene();
}

void TextureViewer::openColors(const QString & fileName){

    std::ifstream colorStream (fileName.toUtf8());
    if (!colorStream.is_open())
	return;

    fileColorMap.clear();
    int si;
    float r,g,b;
    std::string dummy;
    while ( colorStream.good() )
    {
	colorStream >> si;
	if( colorStream.good() ){
	    colorStream >> r; colorStream >> g; colorStream >> b;
	    colorStream >> dummy;
	    std::cout <<  si << " "<< r  << " "<< g  << " "<< b  << std::endl;
	    fileColorMap[si] = QColor(r*255., g*255.,b*255.);
	    this->colorMap[si] = color4{r*255.f, g*255.f, b*255.f, 255.f};
	}
    }
    colorStream.close();


    //update color if image loaded
    if(imageLoaded){
	texture.updateColorTexData(colorMap); //Doesn't work ?
    }
}

void TextureViewer::open3DImage(const QString & fileName){
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double, std::milli>> start_point, end_point, start_loading_image, stop_loading_image;
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double, std::milli>> text_build_start, text_build_stop;
    std::cerr << __PRETTY_FUNCTION__ << " : Starting to laod 3D Image" << std::endl;
    start_point = std::chrono::steady_clock::now();
    //Texture objet
    texture.clear();
    subdomain_indices.clear();
    iColorMap.clear();
    iDisplayMap.clear();

    std::vector<unsigned char> data;
    std::size_t nx, ny, nz;
    float dx, dy, dz;

    start_loading_image = std::chrono::steady_clock::now();
    //Load the data from the 3D image
    if(fileName.endsWith(".nii"))
	openNIFTI(fileName,data,subdomain_indices, nx,ny,nz,dx,dy,dz);
    else if (fileName.endsWith(".dim"))
	openIMA(fileName,data,subdomain_indices, nx,ny,nz,dx,dy,dz);
    else
	return;

    stop_loading_image = std::chrono::steady_clock::now();
    std::cerr << __PRETTY_FUNCTION__ << " : Took " << (stop_loading_image - start_loading_image).count() << " to load image" << '\n';

    for( unsigned int i = 0 ; i < subdomain_indices.size() ; i++ ){
	unsigned char currentLabel = subdomain_indices[i];

	std::map<unsigned char, QColor>::iterator it = fileColorMap.find( currentLabel );
	if( it == fileColorMap.end() ){
	    if( currentLabel ==0 ){

		colorMap[currentLabel] = color4{.0f, .0f, .0f, .0f};
		iColorMap[currentLabel] = QColor(0,0,0);
	    }
	    else {
		iColorMap[currentLabel].setHsvF(0.98*double(i)/subdomain_indices.size(), 0.8,0.8);
		colorMap[currentLabel] = hsv2rgb_normalized(float(i)/static_cast<float>(subdomain_indices.size()), 0.8f,0.8f);
	    }
	} else {
	    colorMap[currentLabel] = color4{static_cast<float>(it->second.redF()),
		    static_cast<float>(it->second.greenF()),
		    static_cast<float>(it->second.blueF())};
	    iColorMap[currentLabel] = it->second;
	}

	iDisplayMap[currentLabel] = true;
	this->visible_domains[currentLabel] = 1;
	std::cout << "Found label " << currentLabel << std::endl;
    }

    text_build_start = std::chrono::steady_clock::now();
    texture.build(data,subdomain_indices,nx,ny,nz,dx,dy,dz,this->colorMap,this->visible_domains);
    text_build_stop = std::chrono::steady_clock::now();
    std::cerr << __PRETTY_FUNCTION__ << " : Took " << (text_build_stop - text_build_start).count() << " to build texture" << '\n';

    imageLoaded = true;

    qglviewer::Vec maxTexture (texture.getXMax(), texture.getYMax() , texture.getZMax());

    updateCamera(maxTexture/2. , maxTexture.norm() );

    //Once the 3D image is loaded, grid size parameters are sent to the interface
    emit setMaxCutPlanes(texture.getWidth(), texture.getHeight(), texture.getDepth());


    lightCylindricPositions.clear();
    lightCylindricPositions.resize(7);
    lightPositions.clear();
    lightPositions.resize(7);

    Vec3Df dim ( texture.getXMax(), texture.getYMax() , texture.getZMax() );
    center = dim/2.;
    min_light_z = -center[2];
    max_light_z = dim[2] + center[2];

    makeDefaultMesh();

    float radius = std::max(center[0], center[1]);

    lightCylindricPositions[0][0] = 2*radius;
    lightCylindricPositions[0][1] = 0.;
    lightCylindricPositions[0][2] = -1.*radius/2.;

    lightCylindricPositions[1][0] = 2*radius;
    lightCylindricPositions[1][1] = 360./3.;
    lightCylindricPositions[1][2] = -1.*radius/2.;

    lightCylindricPositions[2][0] = 2*radius;
    lightCylindricPositions[2][1] = 2.*360./3.;
    lightCylindricPositions[2][2] = -1.*radius/2.;

    lightCylindricPositions[3][0] = 2*radius;
    lightCylindricPositions[3][1] = 0.;
    lightCylindricPositions[3][2] = dim[2] + radius/2.;

    lightCylindricPositions[4][0] = 2*radius;
    lightCylindricPositions[4][1] = 360./4.;
    lightCylindricPositions[4][2] = dim[2] + radius/2.;

    lightCylindricPositions[5][0] = 2*radius;
    lightCylindricPositions[5][1] = 2.*360./4.;
    lightCylindricPositions[5][2] = dim[2] + radius/2.;

    lightCylindricPositions[6][0] = 2*radius;
    lightCylindricPositions[6][1] = 3.*360./4.;
    lightCylindricPositions[6][2] = dim[2] + radius/2.;

    initLights();

    emit setMaxLightPos(15.* center[0], 360, int(fabs(max_light_z - min_light_z)) );

    end_point = std::chrono::steady_clock::now();

    std::cerr << __PRETTY_FUNCTION__ << " : Took " << (end_point - start_point).count() << " to complete function" << '\n';

}

void TextureViewer::initLights ( ) {

    //    float max_p = pos2[0];
    //    max_p = std::max(max_p, pos2[1]);
    //    max_p = std::max(max_p, pos2[2]);
#if 1
    for( unsigned int i = 0; i < lightPositions.size(); i++ ){
	lightPositions[i][0] = lightCylindricPositions[i][0]*cos(lightCylindricPositions[i][1]*3.14159265/180) + center[0];
	lightPositions[i][1] = lightCylindricPositions[i][0]*sin(lightCylindricPositions[i][1]*3.14159265/180) + center[1];
	lightPositions[i][2] = lightCylindricPositions[i][2];
    }

    GLfloat light_position0[4] = {lightPositions[0][0], lightPositions[0][1], lightPositions[0][2], 1};
    GLfloat light_position1[4] = {lightPositions[1][0], lightPositions[1][1], lightPositions[1][2], 1};
    GLfloat light_position2[4] = {lightPositions[2][0], lightPositions[2][1], lightPositions[2][2], 1};
    GLfloat light_position3[4] = {lightPositions[3][0], lightPositions[3][1], lightPositions[3][2], 1};
    GLfloat light_position4[4] = {lightPositions[4][0], lightPositions[4][1], lightPositions[4][2], 1};
    GLfloat light_position5[4] = {lightPositions[5][0], lightPositions[5][1], lightPositions[5][2], 1};
    GLfloat light_position6[4] = {lightPositions[6][0], lightPositions[6][1], lightPositions[6][2], 1};


    GLfloat direction1[3] = { center[0] - lightPositions[1][0],center[1] - lightPositions[1][1],center[2] - lightPositions[1][2]};
    GLfloat direction2[3] = { center[0] - lightPositions[2][0],center[1] - lightPositions[2][1],center[2] - lightPositions[2][2]};
    GLfloat direction3[3] = { center[0] - lightPositions[3][0],center[1] - lightPositions[3][1],center[2] - lightPositions[3][2]};
    GLfloat direction4[3] = { center[0] - lightPositions[4][0],center[1] - lightPositions[4][1],center[2] - lightPositions[4][2]};
    GLfloat direction5[3] = { center[0] - lightPositions[5][0],center[1] - lightPositions[5][1],center[2] - lightPositions[5][2]};
    GLfloat direction6[3] = { center[0] - lightPositions[6][0],center[1] - lightPositions[6][1],center[2] - lightPositions[6][2]};

    GLfloat color1[4] = {1.f, 0.8f, 0.8f, 1.f};
    GLfloat color2[4] = {0.8f, 1.f, 0.8f, 1.f};
    GLfloat color3[4] = {0.8, 0.8, 1., 1};
    GLfloat color4[4] = {1, 1, 1, 1};
    GLfloat color5[4] = {1, 1, 0.5, 1};
    GLfloat color6[4] = {0.5, 0.5, 0.5, 1};

    GLfloat specularColor4[4] = {0.8, 0.8, 0.8, 1};
    GLfloat specularColor5[4] = {0.8, 0.8, 0.8, 1};
    GLfloat specularColor6[4] = {0.8, 0.8, 0.8, 1};

    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT0, GL_POSITION, light_position0);

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);

    glLightfv (GL_LIGHT2, GL_POSITION, light_position2);
    glLightfv (GL_LIGHT2, GL_SPOT_DIRECTION, direction2);
    glLightfv (GL_LIGHT2, GL_DIFFUSE, color2);
    glLightfv (GL_LIGHT2, GL_SPECULAR, color2);

    glLightfv (GL_LIGHT3, GL_POSITION, light_position3);
    glLightfv (GL_LIGHT3, GL_SPOT_DIRECTION, direction3);
    glLightfv (GL_LIGHT3, GL_DIFFUSE, color3);
    glLightfv (GL_LIGHT3, GL_SPECULAR, color3);

    glLightfv (GL_LIGHT4, GL_POSITION, light_position4);
    glLightfv (GL_LIGHT4, GL_SPOT_DIRECTION, direction4);
    glLightfv (GL_LIGHT4, GL_DIFFUSE, color4);
    glLightfv (GL_LIGHT4, GL_SPECULAR, specularColor4);

    glLightfv (GL_LIGHT5, GL_POSITION, light_position5);
    glLightfv (GL_LIGHT5, GL_SPOT_DIRECTION, direction5);
    glLightfv (GL_LIGHT5, GL_DIFFUSE, color5);
    glLightfv (GL_LIGHT5, GL_SPECULAR, specularColor5);

    glLightfv (GL_LIGHT6, GL_POSITION, light_position6);
    glLightfv (GL_LIGHT6, GL_SPOT_DIRECTION, direction6);
    glLightfv (GL_LIGHT6, GL_DIFFUSE, color6);
    glLightfv (GL_LIGHT6, GL_SPECULAR, specularColor6);

    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);

    glEnable (GL_LIGHTING);

#else

    GLfloat light_position0[4] = {-50, 50, -10, 0};
    GLfloat light_position1[4] = {42, 374, 161, 0};
    GLfloat light_position2[4] = {473, -351, -259, 0};
    GLfloat light_position3[4] = {-438, 167, -48, 0};

    GLfloat direction1[3] = {-42, -374, -161,};
    GLfloat direction2[3] = {-473, 351, 259};
    GLfloat direction3[3] = {438, -167, 48};

    GLfloat color1[4] = {1.0, 1.0, 1.0, 1};
    GLfloat color2[4] = {0.28, 0.39, 1.0, 1};
    GLfloat color3[4] = {1.0, 0.69, 0.23, 1};

    GLfloat specularColor1[4] = {0.8, 0.8, 0.8, 1};
    GLfloat specularColor2[4] = {0.8, 0.8, 0.8, 1};
    GLfloat specularColor3[4] = {0.8, 0.8, 0.8, 1};

    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT0, GL_POSITION, light_position0);

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, specularColor1);

    glLightfv (GL_LIGHT2, GL_POSITION, light_position2);
    glLightfv (GL_LIGHT2, GL_SPOT_DIRECTION, direction2);
    glLightfv (GL_LIGHT2, GL_DIFFUSE, color2);
    glLightfv (GL_LIGHT2, GL_SPECULAR, specularColor2);

    glLightfv (GL_LIGHT3, GL_POSITION, light_position3);
    glLightfv (GL_LIGHT3, GL_SPOT_DIRECTION, direction3);
    glLightfv (GL_LIGHT3, GL_DIFFUSE, color3);
    glLightfv (GL_LIGHT3, GL_SPECULAR, specularColor3);

    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);

    glEnable (GL_LIGHTING);

#endif
}


void TextureViewer::saveCamera(const QString &filename){
    ofstream out (filename.toUtf8());
    if (!out)
	exit (EXIT_FAILURE);

    out << camera()->position() << " " <<
	   camera()->viewDirection() << " " <<
	   camera()->upVector() << " " <<
	   camera()->fieldOfView();
    out << endl;
    out.close ();
}

std::istream & operator>>(std::istream & stream, qglviewer::Vec & v)
{
    stream >>
	    v.x >>
	    v.y >>
	    v.z;

    return stream;
}
void TextureViewer::openCamera(const QString &filename){

    std::ifstream file;
    file.open(filename.toStdString().c_str());

    qglviewer::Vec pos;
    qglviewer::Vec view;
    qglviewer::Vec up;
    float fov;

    file >> pos >>
	    view >>
	    up >>
	    fov;

    camera()->setPosition(pos);
    camera()->setViewDirection(view);
    camera()->setUpVector(up);
    camera()->setFieldOfView(fov);

    camera()->computeModelViewMatrix();
    camera()->computeProjectionMatrix();

    update();
}

void TextureViewer::selectIAll(){
    for (uint i = 0; i < this->subdomain_indices.size(); ++i) {
	this->visible_domains[subdomain_indices[i]] = 1;
    }
    for(std::map<unsigned char, bool>::iterator it = iDisplayMap.begin() ; it != iDisplayMap.end(); ++it ){
	iDisplayMap[it->first] = true;
	visu_mesh->buildVisibilityTexture(iDisplayMap);
    }
    update();
}

void TextureViewer::discardIAll(){
    for (uint i = 0; i < this->subdomain_indices.size(); ++i) {
	this->visible_domains[subdomain_indices[i]] = 0;
    }
    for(std::map<unsigned char, bool>::iterator it = iDisplayMap.begin() ; it != iDisplayMap.end(); ++it ){
	iDisplayMap[it->first] = false;
	visu_mesh->buildVisibilityTexture(iDisplayMap);
    }
    update();
}

void TextureViewer::setIVisibility(unsigned int i, bool visibility){
    this->visible_domains[i] = (visibility) ? 1 : 0;
    if(iDisplayMap.find(i) != iDisplayMap.end()){
	iDisplayMap[i] = visibility;
	std::cout << "set visi" << std::endl;
	visu_mesh->buildVisibilityTexture(iDisplayMap);
    }
    update();
}


void TextureViewer::openIMA(const QString & fileName, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
			    std::size_t & nx , std::size_t & ny , std::size_t & nz, float & dx , float & dy , float & dz ){
    QString imaName = QString(fileName);

    imaName.replace(".dim", ".ima" );
    std::ifstream imaFile (imaName.toUtf8());
    if (!imaFile.is_open())
	return;

    std::ifstream dimFile (fileName.toUtf8());
    if (!dimFile.is_open())
	return;

    dimFile >> nx; dimFile >> ny; dimFile >> nz;

    string dummy, type;

    dimFile >> dummy;
    while (dummy.find("-type")==string::npos)
	dimFile >> dummy;

    dimFile >> type;

    while (dummy.find("-dx")==string::npos)
	dimFile >> dummy;

    dimFile >> dx;

    dimFile >> dummy;
    while (dummy.find("-dy")==string::npos)
	dimFile >> dummy;

    dimFile >> dy;

    dimFile >> dummy;
    while (dummy.find("-dz")==string::npos)
	dimFile >> dummy;

    dimFile >> dz;


    cout << "(nx,dx) = ( " << nx << " ; " << dx << " ) "<< endl;
    cout << "(ny,dy) = ( " << ny << " ; " << dy << " ) "<< endl;
    cout << "(nz,dz) = ( " << nz << " ; " << dz << " ) "<< endl;

    std::size_t size = nx*ny*nz;
    std::size_t sizeIn = size;

    if( type.find("S16")!=string::npos )
	sizeIn = size*2;
    if( type.find("FLOAT")!=string::npos )
	sizeIn = size*4;

    unsigned char * tempData = new unsigned char[sizeIn];

    imaFile.read(reinterpret_cast<char*>(tempData), sizeIn);

    data.clear();
    data.resize(size);

    if( type.find("S16")!=string::npos ){
	for(std::size_t i = 0, j=0 ; i < size ; i ++, j+=2){
	    unsigned char value = (unsigned char)tempData[j];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
    } else if( type.find("FLOAT")!=string::npos ){
	float * floatArray = (float*) tempData;

	for(std::size_t i = 0 ; i < size ; i ++){
	    unsigned char value = (unsigned char)floatArray[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
	delete [] floatArray;
    } else {
	for(std::size_t i = 0 ; i < size ; i ++){
	    unsigned char value = (unsigned char)tempData[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
    }

    delete [] tempData;
}

void TextureViewer::makeDefaultMesh(){

    if(visu_mesh) {
	delete visu_mesh;
	visu_mesh = nullptr;
	std::cerr << "resetting visu_mesh" << '\n';
    }
    visu_mesh = new VisuMesh(&texture);

    std::vector<Vertex> & vertices = visu_mesh->getVertices();
    std::vector<Tetrahedron> & tetrahedra = visu_mesh->getTetrahedra();
    std::vector<Vec3Df> & textureCoords = visu_mesh->getTextureCoords();

    const Vec3Di & Vmin = texture.getVmin();
    const Vec3Di & Vmax = texture.getVmax();

    Vec3Di diff = Vmax - Vmin;

    for (int i = 0; i < 2; i++){
	for (int j = 0; j < 2; j++){
	    for (int k = 0; k < 2; k++){
		Vec3Df position = Vec3Df(
			texture.dx()*(i*diff[0]+Vmin[0]+1) + (i-1.5)*texture.dx(),
			texture.dy()*(j*diff[1]+Vmin[1]+1) + (j-1.5)*texture.dy(),
			texture.dz()*(k*diff[2]+Vmin[2]+1) + (k-1.5)*texture.dz() );
		vertices.push_back (Vertex( position ));
		textureCoords.push_back( Vec3Df(std::max(std::min(1.f,position[0]/texture.getXMax()),0.f),
					 std::max(std::min(1.f,position[1]/texture.getYMax()),0.f),
			std::max(std::min(1.f,position[2]/texture.getZMax()),0.f)) );
	    }
	}
    }
    int i = 0 , j = 0, k = 0 ;
    tetrahedra.push_back(Tetrahedron(getIndice(i+1, j  , k  ), getIndice(i+1, j+1, k  ),
				     getIndice(i  , j+1, k  ), getIndice(i+1, j+1, k+1)));

    tetrahedra.push_back(Tetrahedron(getIndice(i  , j  , k+1), getIndice(i  , j  , k  ),
				     getIndice(i  , j+1, k+1), getIndice(i+1  , j  , k+1)));

    tetrahedra.push_back(Tetrahedron(getIndice(i  , j+1, k+1), getIndice(i+1, j  , k  ),
				     getIndice(i+1, j+1, k+1), getIndice(i+1  , j  , k+1)));

    tetrahedra.push_back(Tetrahedron(getIndice(i  , j  , k  ), getIndice(i+1, j  , k  ),
				     getIndice(i  , j+1, k+1), getIndice(i+1  , j  , k+1)));

    tetrahedra.push_back(Tetrahedron(getIndice(i  , j  , k  ), getIndice(i+1, j  , k  ),
				     getIndice(i  , j+1, k  ), getIndice(i  , j+1, k+1)));

    tetrahedra.push_back(Tetrahedron(getIndice(i  , j+1, k  ), getIndice(i+1, j  , k  ),
				     getIndice(i+1, j+1, k+1), getIndice(i  , j+1, k+1)));

    /*
    QString fileName( "./Test_save.mesh" );
    std::ofstream out (fileName.toUtf8());
    if (!out)
    exit (EXIT_FAILURE);

    out << "MeshVersionFormatted " << 1 << std::endl;
    out << "Dimension " << 3 << std::endl;
    out << "Vertices\n";
    out << vertices.size() << std::endl;

    // std::vector<Vertex_handle> vertices ( triangulation.number_of_vertices() );


    for(unsigned int i = 0 ; i < vertices.size(); i++){
    const Vec3Df & p = vertices[i].getPos();
    out << p[0] << " " << p[1] << " " << p[2] << " " << 2 << std::endl;
    }

    out << "Triangles\n";
    out << 0 << std::endl;

    out << "Tetrahedra\n";
    out << tetrahedra.size() << std::endl;
    for( unsigned int i = 0 ; i < tetrahedra.size() ; i ++ ){
    for( int v = 0; v < 4 ; v++ )
	out <<  tetrahedra[i].getVertex(v)+1 << " ";
    out << 1 << std::endl;
    }
    out << std::endl;
    out.close ();
*/
    visu_mesh->update();
    visu_mesh->buildVisibilityTexture(iDisplayMap);
}


int TextureViewer::getIndice(int i, int j , int k){
    return i*2*2 + j*2 +k;
}
void TextureViewer::openNIFTI(const QString & filename, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
			      std::size_t & nx , std::size_t & ny , std::size_t & nz, float & dx , float & dy , float & dz ){

    //Load nifti segmented image
    nifti_image * nim = nifti_image_read(filename.toUtf8(), 1);

    int gridSize = nim->nx*nim->ny*nim->nz;
    QString dataType(nifti_datatype_string(nim->datatype));

    cout << "nim->datatype : " <<qPrintable(dataType) << endl;

    data.clear();
    data.resize( gridSize );

    if(dataType == QString("UINT8")){
	unsigned char * tempData = (unsigned char*)nim->data;
	for(int i = 0 ; i<gridSize ; i++ ){
	    unsigned char value = (unsigned char)tempData[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
	delete [] tempData;
    }else if( dataType == QString("UINT16")){
	unsigned short * tempData = (unsigned short*)nim->data;
	for(int i = 0 ; i<gridSize ; i++ ){
	    unsigned char value = (unsigned char)tempData[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
	delete [] tempData;
    } else if(dataType == QString("UINT32") ) {
	unsigned int * tempData = (unsigned int*)nim->data;
	for(int i = 0 ; i<gridSize ; i++ ){
	    unsigned char value = (unsigned char)tempData[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
	delete [] tempData;;
    } else if( dataType == QString("UINT64") ){
	unsigned long * tempData = (unsigned long*)nim->data;
	for(int i = 0 ; i<gridSize ; i++ ){
	    unsigned char value = (unsigned char)tempData[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
	delete [] tempData;
    } else if( dataType == QString("INT16")){
	short * tempData = (short*)nim->data;
	for(int i = 0 ; i<gridSize ; i++ ){
	    unsigned char value = (unsigned char)tempData[i];
	    data[i] = value;
	    if ( std::find(labels.begin(), labels.end(), value) == labels.end() )
		labels.push_back(value);
	}
	delete [] tempData;

    }

    nx = nim->nx; ny = nim->ny; nz = nim->nz;
    dx = nim->dx; dy = nim->dy; dz = nim->dz;

    delete [] nim;
}

void TextureViewer::openFinalMesh(const QString &fileName){

    ifstream myfile (fileName.toLatin1());

    if (!myfile.is_open())
	exit (EXIT_FAILURE);


    string meshString;
    unsigned int sizeV, dimension;

    std::vector<Vertex> & vertices = visu_mesh->getVertices();


    myfile >> meshString;
    while (meshString.find("Dimension")==string::npos)
	myfile >> meshString;

    myfile>> dimension;
    cout << meshString << " " << dimension << endl;
    while (meshString.find("Vertices")==string::npos)
	myfile >> meshString;

    myfile >> sizeV;
    cout << meshString << " " << sizeV << endl;

    if( vertices.size() != sizeV ) return ;

    int s;
    Vec3Df BBmini (FLT_MAX, FLT_MAX, FLT_MAX);
    for (unsigned int i = 0; i < sizeV; i++) {
	double p[3];
	for (unsigned int j = 0; j < 3; j++){
	    BBmini[j] = std::min((float)p[j], BBmini[j]);
	    myfile >> p[j];
	}

	vertices[i] = Vertex( Vec3Df(p[0], p[1], p[2]) );
	myfile >> s;
    }

    vector<Vec3Df> V;
    for (unsigned int i = 0; i < vertices.size(); i++){

	vertices[i].setPos( vertices[i].getPos() - BBmini );//Maybe remove
	V.push_back(vertices[i].getPos() - BBmini);
    }


    myfile.close ();

    interpolationStruct.keys.push_back( V );
    visu_mesh->computeTexturesData();

    update();

}

void TextureViewer::openInitialMesh(const QString &fileName){
    openMesh(fileName);
}

void TextureViewer::openMesh(const QString &fileName){

    ifstream myfile (fileName.toLatin1());

    if (!myfile.is_open())
	exit (EXIT_FAILURE);


    string meshString;
    unsigned int sizeV, sizeT, sizeTet, dimension;

    if (visu_mesh) {
	delete visu_mesh;
	visu_mesh = nullptr;
    }
    visu_mesh = new VisuMesh(&texture);

    this->setGridIsDrawn();

    std::vector<Vertex> & vertices = visu_mesh->getVertices();
    std::vector<Tetrahedron> & tetrahedra = visu_mesh->getTetrahedra();
    std::vector<Vec3Df> & textureCoords = visu_mesh->getTextureCoords();

    myfile >> meshString;
    while (meshString.find("Dimension")==string::npos)
	myfile >> meshString;

    myfile>> dimension;
    cout << meshString << " " << dimension << endl;
    while (meshString.find("Vertices")==string::npos)
	myfile >> meshString;

    myfile >> sizeV;
    cout << meshString << " " << sizeV << endl;

    int s;
    std::vector<Vec3Df> V;
    for (unsigned int i = 0; i < sizeV; i++) {
	float p[3];
	for (unsigned int j = 0; j < 3; j++)
	    myfile >> p[j];

	Vec3Df position = Vec3Df(p[0], p[1], p[2]);
	vertices.push_back (Vertex( position ));
	V.push_back(position);
	textureCoords.push_back( Vec3Df(std::min(1.f,position[0]/texture.getXMax()), std::min(1.f,position[1]/texture.getYMax()), std::min(1.f,position[2]/texture.getZMax())) );
	myfile >> s;
    }
    interpolationStruct.keys.push_back( V );

    while (meshString.find("Triangles")==string::npos)
	myfile >> meshString;

    myfile >> sizeT;
    cout << meshString << " " << sizeT << endl;
    for (unsigned int i = 0; i < sizeT; i++) {
	unsigned int v[3];
	for (unsigned int j = 0; j < 3; j++)
	    myfile >> v[j];

	myfile >> s;
    }

    if( dimension == 3 ){
	while (meshString.find("Tetrahedra")==string::npos)
	    myfile >> meshString;

	myfile >> sizeTet;
	cout << meshString << " " << sizeTet << endl;
	for (unsigned int i = 0; i < sizeTet; i++) {
	    unsigned int v[4];
	    for (unsigned int j = 0; j < 4; j++)
		myfile >> v[j];
	    myfile >> s;
	    if( s > 0 )
		tetrahedra.push_back (Tetrahedron (v[0]-1, v[1]-1, v[2]-1, v[3]-1));
	}
    }
    myfile.close ();

    visu_mesh->update();
    visu_mesh->buildVisibilityTexture(iDisplayMap);

    update();

}

void TextureViewer::setRadiusLight(int _r){
    //int step = _x - center[0];

    lightCylindricPositions[activeLight][0] = _r;
    initLights();
    update();
}

void TextureViewer::setThetaLight(int _y){
    //   int step = _y - center[1];

    lightCylindricPositions[activeLight][1] = _y;
    initLights();
    update();
}

void TextureViewer::setZLight(int _z){

    lightCylindricPositions[activeLight][2] = min_light_z + _z;
    initLights();
    update();
}

void TextureViewer::setXCut(int _x){
	_x = int(2)*_x;
    texture.setXCut(_x);
    cut[0] =_x*texture.dx();
    update();
}

void TextureViewer::setYCut(int _y){
_y = int(2)*_y;
    texture.setYCut(_y);
    cut[1] =_y*texture.dy();
    update();
}

void TextureViewer::setZCut(int _z){
_z = int(2)*_z;
    texture.setZCut(_z);
    cut[2] =_z*texture.dz();
    update();
}

void TextureViewer::invertXCut(){
    texture.invertXCut();
    cutDirection[0] *= -1;
    update();
}

void TextureViewer::invertYCut(){
    texture.invertYCut();
    cutDirection[1] *= -1;
    update();
}

void TextureViewer::invertZCut(){
    texture.invertZCut();
    cutDirection[2] *= -1;
    update();
}

void TextureViewer::setXCutDisplay(bool _xCutDisplay){
    texture.setXCutDisplay(_xCutDisplay);
    update();
}

void TextureViewer::setYCutDisplay(bool _yCutDisplay){
    texture.setYCutDisplay(_yCutDisplay);
    update();
}

void TextureViewer::setZCutDisplay(bool _zCutDisplay){
    texture.setZCutDisplay(_zCutDisplay);
    update();
}

void TextureViewer::updateFromInterpolationStruct(double t) {
    std::vector< Vec3Df > V( visu_mesh->getVertices().size() );
    bool needsUpdate = interpolationStruct.interpolate(t , V);

    if(needsUpdate) {
	std::vector< Vertex > &vertices = visu_mesh->getVertices();
	for (unsigned int i = 0; i < vertices.size(); i++)
	    vertices[i].setPos( V[i] );

	visu_mesh->computeTexturesData();

	//        Vec3Df bb, BB;
	//        visu_mesh->getBBox(bb, BB);
	//        updateCuttingPlanes(bb,BB);

	update();
    }
}


void TextureViewer::keyPressEvent(QKeyEvent *e)
{
    switch (e->key())
    {
    case Qt::Key_R : visu_mesh->reloadShaders();update(); break;
    case Qt::Key_Plus : visu_mesh->increment();update(); break;
    case Qt::Key_Minus : visu_mesh->decrement();update(); break;
    case Qt::Key_Space :
    {
	animationTimer.togglePause();
	animationTimer.restart();
	startAnimation();
	update(); break;
    }
    case Qt::Key_S :
	animationTimer.recomputeU();
	if( animationTimer.isPlaying() ) {
	    double uAnim = animationTimer.getU();
	    if(uAnim > 1.0) {
		animationTimer.playAnimation = false;
		stopAnimation();
	    }
	    else{
		updateFromInterpolationStruct(uAnim);
	    }

	    // do something with uAnim
	    std::cout << uAnim << std::endl;
	}
	update(); break;
    default : QGLViewer::keyPressEvent(e);
    }
}

QString TextureViewer::helpString() const
{
    QString text("<h2>S i m p l e V i e w e r</h2>");
    text += "Use the mouse to move the camera around the object. ";
    text += "You can respectively revolve around, zoom and translate with the three mouse buttons. ";
    text += "Left and middle buttons pressed together rotate around the camera view direction axis<br><br>";
    text += "Pressing <b>Alt</b> and one of the function keys (<b>F1</b>..<b>F12</b>) defines a camera keyFrame. ";
    text += "Simply press the function key again to restore it. Several keyFrames define a ";
    text += "camera path. Paths are saved when you quit the application and restored at next start.<br><br>";
    text += "Press <b>F</b> to display the frame rate, <b>A</b> for the world axis, ";
    text += "<b>Alt+Return</b> for full screen mode and <b>Control+S</b> to save a snapshot. ";
    text += "See the <b>Keyboard</b> tab in this window for a complete shortcut list.<br><br>";
    text += "Double clicks automates single click actions: A left button double click aligns the closer axis with the camera (if close enough). ";
    text += "A middle button double click fits the zoom of the camera and the right button re-centers the scene.<br><br>";
    text += "A left button double click while holding right button pressed defines the camera <i>Revolve Around Point</i>. ";
    text += "See the <b>Mouse</b> tab and the documentation web pages for details.<br><br>";
    text += "Press <b>Escape</b> to exit the TextureViewer.";
    return text;
}
