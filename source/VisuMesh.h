// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend
//
// Copyright(C) 2007-2009
// Tamy Boubekeur
//
// All rights reserved.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
// for more details.
//
// --------------------------------------------------------------------------

#pragma once

#include <vector>

#include "Vertex.h"
#include "Tetrahedron.h"
#include "Edge.h"
#include "VBOHandler.h"
#include "ShaderPool.h"
#include "ShaderInclude.h"
#include "Texture.h"

class VisuMesh : public QT_OPENGL_VERSION_USED_COMPAT {
public:
    inline VisuMesh () { init(); }
    inline VisuMesh(Texture * texture): gridTexture(texture) { init();}
    inline VisuMesh (const std::vector<Vertex> & v) : vertices (v) {}
    inline VisuMesh (const std::vector<Vertex> & v,
		 const std::vector<Tetrahedron> & t) : vertices (v), tetrahedra (t)  {}
    inline VisuMesh (const VisuMesh & mesh) : vertices (mesh.vertices), tetrahedra (mesh.tetrahedra) {}
    inline ~VisuMesh () { clearTextures(); }
    std::vector<Vertex> & getVertices () { return vertices; }
    const std::vector<Vertex> & getVertices () const { return vertices; }
    std::vector<Tetrahedron> & getTetrahedra () { return tetrahedra; }
    const std::vector<Tetrahedron> & getTetrahedra () const { return tetrahedra; }
    std::vector<Vec3Df> & getTextureCoords () { return textureCoords; }
    const std::vector<Vec3Df> & getTextureCoords () const { return textureCoords; }

    void clear ();
    void clearGeometry ();
    void clearTopology ();

    void reloadShaders(){ shaderPool->ReloadShaders(); }
    void update();
    void computeTexturesData();
    void draw( const qglviewer::Vec & cam );
    void drawTetrahedra();

    void setOrthongonalCut(const Vec3Df & _cut, const Vec3Df & _cutDirection){ cut = _cut; cutDirection = _cutDirection; }
    void setClipeEquation(const Vec3Df & clipN, const Vec3Df & pointN){ clippingNormal = clipN; clippingNormal.normalize(); pointOnClipping = pointN; }

    void increment(){ c_incr++; if(c_incr >= tetrahedra.size() ) c_incr = 0 ;  }
    void decrement(){ c_incr--; if(c_incr < 0 ) c_incr = tetrahedra.size() ;  }


    void buildVisibilityTexture( std::map<unsigned char, bool> & visibility );

private:
    std::vector<Vertex> vertices;
    std::vector<Tetrahedron> tetrahedra;
    std::vector< std::vector<int> > neighbors;
    std::vector<Vec3Df> textureCoords;

    bool built;
    bool textureCreated;

    GLuint verticesVBOId;
    GLuint normalsVBOId;
    GLuint indicesVBOId;
    GLuint textureCoordsVBOId;
    GLuint texture3DCoordId;

    GLuint verticesCoordTextureId;
    GLuint verticesNormalsTextureId;
    GLuint tetNeighborsTextureId;
    GLuint tetNeighborsNbTextureId;
    GLuint visibilityTextureId;

    unsigned int textureWidth;
    unsigned int textureHeight;
    unsigned int neighborTWidth;
    unsigned int normalTWidth;
    unsigned int visibilityTWitdth;

    VBOHandler vboHandler;

    unsigned int indices[4][3];

    void init();

    void initTextures();
    void clearTextures();

    void collectNeighbors();

    ShaderPool * shaderPool;

    Texture *gridTexture;

    Vec3Df cut;
    Vec3Df cutDirection;

    Vec3Df clippingNormal;
    Vec3Df pointOnClipping;

    int c_incr;
};

// Some Emacs-Hints -- please don't remove:
//
//  Local Variables:
//  mode:C++
//  tab-width:4
//  End:
