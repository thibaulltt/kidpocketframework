#include "TextureDockWidget.h"
#include <QFileDialog>
#include <QComboBox>

using namespace std;
TextureDockWidget::TextureDockWidget(TextureViewer * _viewer, QWidget * parent ):QDockWidget(parent)
{
	viewer = _viewer;

	activeLight = 0;
	sliderValues.clear();
	sliderValues.resize(7, Vec3Di(10,10,10));

	QWidget * contents = new QWidget();

	QVBoxLayout * contentLayout = new QVBoxLayout(contents);

	QCheckBox * recordCheckB = new QCheckBox("Record");
	connect(recordCheckB, SIGNAL(clicked(bool)), viewer, SLOT(record(bool)));
	contentLayout->addWidget(recordCheckB);

	QGroupBox * groupBox = new QGroupBox("Cutting plane", parent);
	groupBox->setMaximumSize(QSize(16777215, 200));

	contentLayout->addWidget ( groupBox) ;

	QGridLayout * cuttingPlaneGridLayout = new QGridLayout(groupBox);
	xHSlider = new QSlider(groupBox);
	xHSlider->setOrientation(Qt::Horizontal);

	cuttingPlaneGridLayout->addWidget(xHSlider, 1, 0, 1, 1);

	yHSlider = new QSlider(groupBox);
	yHSlider->setOrientation(Qt::Horizontal);

	cuttingPlaneGridLayout->addWidget(yHSlider, 3, 0, 1, 1);

	zHSlider = new QSlider(groupBox);
	zHSlider->setMaximum(1);
	zHSlider->setOrientation(Qt::Horizontal);

	cuttingPlaneGridLayout->addWidget(zHSlider, 5, 0, 1, 1);

	QPushButton * invertXPushButton = new QPushButton("invert", groupBox);
	cuttingPlaneGridLayout->addWidget(invertXPushButton, 1, 1, 1, 1);

	QPushButton * invertYPushButton = new QPushButton("invert", groupBox);
	cuttingPlaneGridLayout->addWidget(invertYPushButton, 3, 1, 1, 1);

	QPushButton * invertZPushButton = new QPushButton("invert", groupBox);
	cuttingPlaneGridLayout->addWidget(invertZPushButton, 5, 1, 1, 1);

	QLabel * labelCutX = new QLabel("x cut position", groupBox);
	cuttingPlaneGridLayout->addWidget(labelCutX, 0, 0, 1, 1);

	QLabel * labelCutY = new QLabel("y cut position", groupBox);
	cuttingPlaneGridLayout->addWidget(labelCutY, 2, 0, 1, 1);

	QLabel * labelCutZ = new QLabel("z cut position", groupBox);
	cuttingPlaneGridLayout->addWidget(labelCutZ, 4, 0, 1, 1);

	QCheckBox * displayXCut = new QCheckBox("display", groupBox);
	cuttingPlaneGridLayout->addWidget(displayXCut, 0, 1, 1, 1);

	QCheckBox * displayYCut = new QCheckBox("display", groupBox);
	cuttingPlaneGridLayout->addWidget(displayYCut, 2, 1, 1, 1);

	QCheckBox * displayZCut = new QCheckBox("display", groupBox);
	cuttingPlaneGridLayout->addWidget(displayZCut, 4, 1, 1, 1);

	connect(xHSlider, SIGNAL(valueChanged(int)), viewer, SLOT(setXCut(int)));
	connect(yHSlider, SIGNAL(valueChanged(int)), viewer, SLOT(setYCut(int)));
	connect(zHSlider, SIGNAL(valueChanged(int)), viewer, SLOT(setZCut(int)));

	connect(invertXPushButton, SIGNAL(clicked()), viewer, SLOT(invertXCut()));
	connect(invertYPushButton, SIGNAL(clicked()), viewer, SLOT(invertYCut()));
	connect(invertZPushButton, SIGNAL(clicked()), viewer, SLOT(invertZCut()));

	connect(displayXCut, SIGNAL(clicked(bool)), viewer, SLOT(setXCutDisplay(bool)));
	connect(displayYCut, SIGNAL(clicked(bool)), viewer, SLOT(setYCutDisplay(bool)));
	connect(displayZCut, SIGNAL(clicked(bool)), viewer, SLOT(setZCutDisplay(bool)));

	connect(viewer, SIGNAL(setMaxCutPlanes(int,int,int)), this, SLOT(setMaxCutPlanes(int,int,int)));

	QPushButton * saveCameraPushButton = new QPushButton("Save camera");
	contentLayout->addWidget(saveCameraPushButton);
	connect(saveCameraPushButton, SIGNAL(clicked()), this, SLOT(saveCamera()));

	QPushButton * loadCameraPushButton = new QPushButton("Load camera");
	contentLayout->addWidget(loadCameraPushButton);
	connect(loadCameraPushButton, SIGNAL(clicked()), this, SLOT(openCamera()));

	displayImageGroupBox = new QGroupBox("Label display", this);

	QVBoxLayout * segIVLayout = new QVBoxLayout(displayImageGroupBox);

	QFrame * segIFrame = new QFrame();

	segIGridLayout = new QGridLayout(segIFrame);

	QScrollArea * displayIScrollArea = new QScrollArea(displayImageGroupBox);
	displayIScrollArea->setWidget(segIFrame);

	displayIScrollArea->setWidgetResizable(true);
	displayIScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	segIVLayout->addWidget(displayIScrollArea);
	signalIMapper = new QSignalMapper(displayImageGroupBox);

	connect(signalIMapper, SIGNAL(mapped(const int &)), this, SLOT(setIVisibility(const int &)));

	QGridLayout * segIGridKayout = new QGridLayout();

	QPushButton *selectIPushButton = new QPushButton("Select All", displayImageGroupBox);
	segIGridKayout->addWidget(selectIPushButton, 0, 0, 1, 1);

	QPushButton *discardIPushButton = new QPushButton("Discard All", displayImageGroupBox);
	segIGridKayout->addWidget(discardIPushButton, 0, 1, 1, 1);

	segIVLayout->addLayout(segIGridKayout);

	segIVLayout->addStretch(0);
	connect(discardIPushButton, SIGNAL(clicked()), this, SLOT(discardIAll()));
	connect(selectIPushButton, SIGNAL(clicked()), this, SLOT(selectIAll()));

	//connect(viewer, SIGNAL(setImageLabels()), this, SLOT(setImageLabels()));
	contentLayout->addWidget(displayImageGroupBox);

	QCheckBox * drawTetrahedraCheckBox = new QCheckBox ("Display Tetrahedra");
	contentLayout->addWidget( drawTetrahedraCheckBox );
	connect(drawTetrahedraCheckBox, SIGNAL(clicked(bool)), viewer, SLOT(setDisplayTetrahedra(bool)));

	QComboBox * modeComboBox = new QComboBox ();

	modeComboBox->addItem ("Light 0");
	modeComboBox->addItem ("Light 1");
	modeComboBox->addItem ("Light 2");
	modeComboBox->addItem ("Light 3");
	modeComboBox->addItem ("Light 4");
	modeComboBox->addItem ("Light 5");
	modeComboBox->addItem ("Light 6");
	modeComboBox->addItem ("Light 7");

	connect (modeComboBox, SIGNAL (activated (int)),
		 this, SLOT (setActiveLight (int)));
	contentLayout->addWidget (modeComboBox);

	QGroupBox * lightGroupBox = new QGroupBox("Lights", parent);
	lightGroupBox->setMaximumSize(QSize(16777215, 200));

	contentLayout->addWidget ( lightGroupBox) ;
	QGridLayout * lightsGridLayout = new QGridLayout(lightGroupBox);
	xLHSlider = new QSlider(lightGroupBox);
	xLHSlider->setOrientation(Qt::Horizontal);

	lightsGridLayout->addWidget(xLHSlider, 1, 0, 1, 1);

	yLHSlider = new QSlider(lightGroupBox);
	yLHSlider->setOrientation(Qt::Horizontal);

	lightsGridLayout->addWidget(yLHSlider, 3, 0, 1, 1);

	zLHSlider = new QSlider(lightGroupBox);
	zLHSlider->setMaximum(1);
	zLHSlider->setOrientation(Qt::Horizontal);

	lightsGridLayout->addWidget(zLHSlider, 5, 0, 1, 1);

	QLabel * labelLX = new QLabel("x light position", lightGroupBox);
	lightsGridLayout->addWidget(labelLX, 0, 0, 1, 1);

	QLabel * labelLY = new QLabel("y light position", lightGroupBox);
	lightsGridLayout->addWidget(labelLY, 2, 0, 1, 1);

	QLabel * labelLZ = new QLabel("z light position", lightGroupBox);
	lightsGridLayout->addWidget(labelLZ, 4, 0, 1, 1);

	connect(xLHSlider, SIGNAL(valueChanged(int)), this, SLOT(setXLight(int)));
	connect(yLHSlider, SIGNAL(valueChanged(int)), this, SLOT(setYLight(int)));
	connect(zLHSlider, SIGNAL(valueChanged(int)), this, SLOT(setZLight(int)));

	connect(viewer, SIGNAL(setMaxLightPos(int,int,int)), this, SLOT(setMaxLightPos(int,int,int)));

	contentLayout->addStretch(0);
	this->setWidget(contents);
}

void TextureDockWidget::setXLight(int i ){
	viewer->setRadiusLight(i);

	sliderValues[activeLight][0] = i ;
}

void TextureDockWidget::setYLight(int i ){
	viewer->setThetaLight(i);

	sliderValues[activeLight][1] = i ;
}

void TextureDockWidget::setZLight(int i ){
	viewer->setZLight(i);

	sliderValues[activeLight][2] = i ;
}

void TextureDockWidget::setActiveLight(int i ){
	activeLight = i;
	viewer->setActiveLight(i);

	xLHSlider->setValue(sliderValues[i][0]);
	yLHSlider->setValue(sliderValues[i][1]);
	zLHSlider->setValue(sliderValues[i][2]);
}

void TextureDockWidget::setImageLabels(){

	const std::map<unsigned char, QColor> & colorMap = viewer->getIColorMap();

	addImageLabels(colorMap);
	selectIAll();

}

void TextureDockWidget::addImageLabels(const std::map<unsigned char, QColor> & colors){

	indexIToLabel.clear();
	std::vector<unsigned char> subdomain_indices;
	viewer->getImageSubdomainIndices(subdomain_indices);
	int i = 0;
	for (std::map<unsigned char, QColor>::const_iterator it = colors.begin(); it != colors.end() ; ++it , i++){
		indexIToLabel.push_back(it->first);
		QColor color = it->second;

		if(labelICheckButtons.size() > i ){
			imageLabels[i]->setPalette(color);
			imageLabels[i]->setVisible(true);
			labelICheckButtons[i]->setVisible(true);
		} else {

			QLabel * labelText  = new QLabel(QString::number(subdomain_indices[it->first]));
			segIGridLayout->addWidget(labelText, i, 0, 1, 1);

			QCheckBox * labelICheckBox = new QCheckBox(displayImageGroupBox);
			segIGridLayout->addWidget(labelICheckBox, i, 2, 1, 1);

			QLabel * labelColor = new QLabel(displayImageGroupBox);
			labelColor->setPalette(color);
			labelColor->setAutoFillBackground(true);

			segIGridLayout->addWidget(labelColor, i, 1, 1, 1);

			imageLabels.push_back(labelColor);
			labelICheckButtons.push_back(labelICheckBox);

			signalIMapper->setMapping(labelICheckBox, i);
			connect(labelICheckBox, SIGNAL(clicked()), signalIMapper, SLOT (map()));
		}
	}

	if( (unsigned int)labelICheckButtons.size() > colors.size() ){
		for( int b = colors.size() ; b < labelICheckButtons.size() ; b++ ){
			labelICheckButtons[b]->setVisible(false);
			imageLabels[b]->setVisible(false);
		}
	}

	displayImageGroupBox->adjustSize();

}

void TextureDockWidget::setIVisibility(int i){
	if(i < (int)indexIToLabel.size())
		viewer->setIVisibility(indexIToLabel.at(i), labelICheckButtons[i]->isChecked());
}



void TextureDockWidget::selectIAll(){
	for (int i = 0;i<labelICheckButtons.size();i++){
		if(!labelICheckButtons[i]->isChecked()){
			labelICheckButtons[i]->setChecked(true);
		}
		setIVisibility(i);
	}
}

void TextureDockWidget::discardIAll(){
	for (int i = 0;i<labelICheckButtons.size();i++){
		if(labelICheckButtons[i]->isChecked()){
			labelICheckButtons[i]->setChecked(false);
		}
	}
	viewer->discardIAll();
}

void TextureDockWidget::setMaxCutPlanes( int x, int y , int z ){
	xHSlider->setRange(0,x);
	yHSlider->setRange(0,y);
	zHSlider->setRange(0,z);
}
void TextureDockWidget::setMaxLightPos( int x, int y , int z ){
	xLHSlider->setRange(0,x);
	xLHSlider->setValue((int)x/2);
	yLHSlider->setRange(0,y);
	yLHSlider->setValue((int)y/2);
	zLHSlider->setRange(0,z);
	zLHSlider->setValue((int)z/2);

	sliderValues.clear();
	sliderValues.resize(7, Vec3Di((int)x/2,(int)y/2, (int)z/2 ));

}

void TextureDockWidget::saveCamera(){
	QString fileName = QFileDialog::getSaveFileName(this, "Save camera file as ", "./data/", "Data (*.txt)");

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	if(!fileName.endsWith(".txt"))
		fileName.append(".txt");

	viewer->saveCamera(fileName);
}

void TextureDockWidget::openCamera(){

	QString selectedFilter, openFileNameLabel;

	QString fileFilter = "Known Filetypes (*.txt);; Data (*.txt)";

	QString fileName = QFileDialog::getOpenFileName(this,
							tr("Select an input camera"),
							openFileNameLabel,
							fileFilter,
							&selectedFilter);

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	viewer->openCamera(fileName);
}
