#ifndef TEXTUREVIEWER_H
#define TEXTUREVIEWER_H

// Includes the QOpenGLFunctions header
#include "OpenGLHeader.h"

#include <QKeyEvent>
#include <QGLViewer/qglviewer.h>

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <algorithm>
#include "Texture.h"
#include "VisuMesh.h"

color4 hsv2rgb_normalized(float h, float s, float v);

struct InterpolationStruct {
    std::vector< std::vector< Vec3Df > > keys;
    bool interpolate(double t , std::vector< Vec3Df > & pts ) {
        if( pts.size() < 2 ) return false;

        int key0 = (t* (keys.size()-1));
        int key1 = (key0 + 1) % keys.size();
        double alpha = (t* (double)(keys.size()-1)) - key0;
        key0 = (key0) % keys.size();

        int npts = keys[key0].size();
        for( int p = 0 ; p < npts ; ++p ) {
            pts[p] = (1.0 - alpha) * keys[key0][p] + alpha * keys[key1][p];
        }
        return true;
    }
};

#include <ctime>
#include <ratio>
#include <chrono>

struct AnimationTimer{
    bool playAnimation;
    double animationPeriodInMicroSeconds;
    double uAnimation;
    std::chrono::system_clock::time_point timerPrevious;
    bool loopAnimation;

    AnimationTimer() {
        playAnimation = false;
        animationPeriodInMicroSeconds = 100000000.0;
        uAnimation = 0.0;
    }

    void setLoopAnimation(bool l) {
        loopAnimation = l;
    }

    void setAnimationLengthInMilliSeconds( double totalTimeInMilliSeconds ) {
        animationPeriodInMicroSeconds = 1000 * totalTimeInMilliSeconds;
    }

    bool togglePause() {
        playAnimation = !playAnimation;
        if(playAnimation){
            timerPrevious = std::chrono::high_resolution_clock::now();
        }
        return playAnimation;
    }

    void restart( double u = 0.0 ) {
        timerPrevious = std::chrono::high_resolution_clock::now();
        uAnimation = u;
    }

    void recomputeU(  ) {
        if( playAnimation ) {
            auto timerCurrent = std::chrono::high_resolution_clock::now();
            auto elapsed = timerCurrent - timerPrevious;
            long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
            double uAnim = uAnimation + ((double)(microseconds)/animationPeriodInMicroSeconds);
            if(loopAnimation)
                uAnim = uAnim - floor(uAnim);
            timerPrevious = timerCurrent;
            uAnimation = uAnim;
        }
    }

    bool isPlaying() {
        return playAnimation;
    }

    void multiplySpeedBy( double ffactor ) {
        animationPeriodInMicroSeconds /= ffactor;
    }

    double getU() {
        return uAnimation;
    }
};


class TextureViewer : public QGLViewer, public QT_OPENGL_VERSION_USED_COMPAT {
    Q_OBJECT

public :
    TextureViewer(QWidget *parent);

    void open3DImage(const QString & fileName);
    void openInitialMesh(const QString & fileName);
    void openFinalMesh(const QString & fileName);
    void openColors(const QString & fileName);
    void saveCamera(const QString &filename);
    void openCamera(const QString &filename);

    void selectIAll();
    void discardIAll();
    void setIVisibility(unsigned int i, bool visibility);
    const std::map<unsigned char, QColor> & getIColorMap()const {return iColorMap;}
    void getImageSubdomainIndices(std::vector<unsigned char> & _subdomain_indices){ _subdomain_indices = subdomain_indices;}

    float min_light_z;
    float max_light_z;
protected :


    QString recordingFolderName;
    int frameNb;
    bool isRecording;

    Texture texture;

    VisuMesh* visu_mesh;

    bool imageLoaded;

    virtual void draw();
    virtual void resizeGL(int width, int height);
    virtual void init();
    virtual QString helpString() const;
    virtual void keyPressEvent(QKeyEvent *e);

    void drawClippingPlane();

    void clear();
    void updateCamera(const qglviewer::Vec & center, float radius);

    void openNIFTI(const QString & filename, std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
                   std::size_t & nx , std::size_t & ny , std::size_t & nz, float & dx , float & dy , float & dz );
    void openIMA(  const QString & filename,std::vector<unsigned char> & data, std::vector<unsigned char> & labels,
                   std::size_t & nx , std::size_t & ny , std::size_t & nz, float & dx , float & dy , float & dz );

    int getIndice(int i, int j , int k);
    void openMesh(const QString & fileName);

    void makeDefaultMesh();
    Vec3Df cut;
    Vec3Df cutDirection;
    bool displayTetrahedra;
    std::map<unsigned char, bool> iDisplayMap;
    std::map<unsigned char, QColor> iColorMap;
    std::map<unsigned char, QColor> fileColorMap;
    color4* colorMap;
    std::vector<unsigned char> subdomain_indices;
    unsigned int* visible_domains;

    void initLights ( );
    Vec3Df pos1;
    Vec3Df pos2;
    int activeLight;
    std::vector<Vec3Df> lightPositions;
    std::vector<Vec3Df> lightCylindricPositions;
    std::vector<Vec3Df> lightPositions0;
    Vec3Df center;


    InterpolationStruct interpolationStruct;
    AnimationTimer animationTimer;
public slots:

    void record( bool is_recording );
    void setXCut(int _x);
    void setYCut(int _y);
    void setZCut(int _z);

    void setRadiusLight(int _x);
    void setThetaLight(int _y);
    void setZLight(int _z);

    void invertXCut();
    void invertYCut();
    void invertZCut();

    void setXCutDisplay(bool _xCutDisplay);
    void setYCutDisplay(bool _yCutDisplay);
    void setZCutDisplay(bool _zCutDisplay);

    void setDisplayTetrahedra( bool _d ){ displayTetrahedra = _d; }
    void setActiveLight( int a_l ){ activeLight = a_l; update(); }

    void updateFromInterpolationStruct(double t);
signals:
    void setMaxCutPlanes(int _xMax, int _yMax, int _zMax);
    void setMaxLightPos(int _xMax, int _yMax, int _zMax);

};

#endif
