#include "Window.h"
#include <QFileDialog>
Window::Window()
{
	if (this->objectName().isEmpty())
		this->setObjectName("mainWindow");
	this->resize(929, 891);

	viewer = new TextureViewer(this);

	QWidget * madWidget = new QWidget(this);
	QGridLayout * gridLayout = new QGridLayout(madWidget);

	gridLayout->addWidget(viewer, 0, 1, 1, 1);

	// Create the menu
	QMenu * menuFile = new QMenu("File", this);
	QAction * actionLoad3Dimage = new QAction("Load 3D image", this);
	QAction * actionOpenInitialMesh = new QAction("Open Initial Mesh", this);
	QAction * actionOpenDeformedMesh = new QAction("Open Final Mesh", this);
	QAction * actionOpenColors = new QAction("Open Color File", this);
	menuFile->addAction(actionLoad3Dimage);
	menuFile->addAction(actionOpenInitialMesh);
	menuFile->addAction(actionOpenDeformedMesh);
	menuFile->addAction(actionOpenColors);
	// connect the actions to functions
	connect(actionLoad3Dimage, SIGNAL(triggered()), this, SLOT(open3DImage()));
	connect(actionOpenInitialMesh, SIGNAL(triggered()), this, SLOT(openInitialMesh()));
	connect(actionOpenDeformedMesh, SIGNAL(triggered()), this, SLOT(openDeformedMesh()));
	connect(actionOpenColors, SIGNAL(triggered()), this, SLOT(openColors()));

	QGroupBox * madViewerGroupBox = new QGroupBox ("Texture viewer", this);
	madViewerGroupBox->setAlignment(Qt::AlignCenter);
	QHBoxLayout * madViewerLayout = new QHBoxLayout (madViewerGroupBox);
	madViewerLayout->addWidget (madWidget);

	madDockWidget = new TextureDockWidget(viewer,this);

	this->addDockWidget(Qt::RightDockWidgetArea, madDockWidget);

	this->setCentralWidget(madViewerGroupBox);

	QMenuBar * menubar = new QMenuBar(this);

	menubar->addAction(menuFile->menuAction());

	this->setMenuBar(menubar);

	statusbar = new QStatusBar(this);

	this->setStatusBar(statusbar);

	this->setWindowTitle("Texture Viewer");
}

void Window::open3DImage(){

	QString selectedFilter, openFileNameLabel;
	QString fileFilter = "Known Filetypes (*.dim *.nii);;IMA (*.dim);;NIFTI (*.nii)";

	QString fileName = QFileDialog::getOpenFileName(this,
							tr("Select an input 3D image"),
							openFileNameLabel,
							fileFilter,
							&selectedFilter);

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	statusBar()->showMessage("Opening 3D image...");
	if(fileName.endsWith(".dim") || fileName.endsWith(".nii") ){
		viewer->open3DImage(fileName);
		statusBar()->showMessage("3D image opened");

		madDockWidget->setImageLabels();
	}
}

void Window::openColors(){

	QString selectedFilter, openFileNameLabel;
	QString fileFilter = "Known Filetypes (*.txt );;TXT (*.txt)";

	QString fileName = QFileDialog::getOpenFileName(this,
							tr("Select an color file"),
							openFileNameLabel,
							fileFilter,
							&selectedFilter);

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	statusBar()->showMessage("Opening color...");
	if(fileName.endsWith(".txt")){
		viewer->openColors(fileName);
		statusBar()->showMessage("Color file opened");
	}
}

void Window::openInitialMesh(){

	QString selectedFilter, openFileNameLabel;
	QString fileFilter = "Known Filetypes (*.mesh);;MESH (*.mesh)";

	QString fileName = QFileDialog::getOpenFileName(this,
							tr("Select an input mesh"),
							openFileNameLabel,
							fileFilter,
							&selectedFilter);

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	statusBar()->showMessage("Opening mesh...");
	if(fileName.endsWith(".mesh") ){
		viewer->openInitialMesh(fileName);
		statusBar()->showMessage("Mesh opened");
	}
}

void Window::openDeformedMesh(){

	QString selectedFilter, openFileNameLabel;
	QString fileFilter = "Known Filetypes (*.mesh);;MESH (*.mesh)";

	QString fileName = QFileDialog::getOpenFileName(this,
							tr("Select an input mesh"),
							openFileNameLabel,
							fileFilter,
							&selectedFilter);

	// In case of Cancel
	if ( fileName.isEmpty() ) {
		return;
	}

	statusBar()->showMessage("Opening mesh...");
	if(fileName.endsWith(".mesh") ){
		viewer->openFinalMesh(fileName);
		statusBar()->showMessage("Mesh opened");
	}

}
