#include "VBOHandler.h"

#include <math.h>

VBOHandler::VBOHandler()
{
    this->initializeOpenGLFunctions();
}

void VBOHandler::findGoodTexSize( unsigned int uiNumValuesNeeded, unsigned int& iTexWidth, unsigned int& iTexHeight, bool bUseAlphaChannel )
{
    unsigned int uiFactor = ( bUseAlphaChannel ? 4 : 3 );
    int iRoot = ( int )sqrtf( ( float ) ( uiNumValuesNeeded / uiFactor ) );

    iTexWidth = iRoot + 1;
    iTexHeight = iRoot;
    if ( ( unsigned int ) ( iTexWidth * iTexHeight ) * uiFactor < uiNumValuesNeeded ) {
        ++iTexHeight;
    }
    if ( ( unsigned int ) ( iTexWidth * iTexHeight ) * uiFactor < uiNumValuesNeeded ) {
        ++iTexWidth;
    }

}

GLuint VBOHandler::createVBO(const void* data, int dataSize, GLenum target, GLenum usage)
{
    GLuint id = 0;  // 0 is reserved, glGenBuffersARB() will return non-zero id if success

    glGenBuffers(1, &id);                        // create a vbo
    glBindBuffer(target, id);                    // activate vbo id to use
    glBufferData(target, dataSize, data, usage); // upload data to video card

    // check data size in VBO is same as input array, if not return 0 and delete VBO
    int bufferSize = 0;
    glGetBufferParameteriv(target, GL_BUFFER_SIZE_ARB, &bufferSize);
    if(dataSize != bufferSize)
    {
	glDeleteBuffers(1, &id);
        id = 0;
        std::cout << "[createVBO()] Data size is mismatch with input array\n";
    }

    return id;      // return VBO id
}

///////////////////////////////////////////////////////////////////////////////
// destroy a VBO
// If VBO id is not valid or zero, then OpenGL ignores it silently.
///////////////////////////////////////////////////////////////////////////////
void VBOHandler::deleteVBO(const GLuint _vboId)
{
    glDeleteBuffers(1, &_vboId);
}
