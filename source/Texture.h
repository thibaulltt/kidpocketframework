#ifndef TEXTURE_H
#define TEXTURE_H

// Includes the QOpenGLFunctions header
#include "OpenGLHeader.h"

#include "Vec3D.h"
#include "ShaderPool.h"
#include "ShaderInclude.h"

#include <QString>
#include <QColor>
#include <QVector>
#include <QGLViewer/vec.h>
//#include "nifti1_io.h"                  /*** NIFTI-1 header specification ***/

#include "Utils.h"

//! @brief 4color component, tightly packed in memory
struct alignas(8) color4 {
	float r=0.f,g=0.f,b=0.f,a=0.f;
	operator GLfloat*() {return &r;}
};

class Texture
{
private :

    GLuint textureId;
    GLuint colorTextureId;

    std::size_t n[3];
    float d[3];
    std::size_t gridSize;

    float minD;

    double xCut;
    double yCut;
    double zCut;

    double xCutPosition;
    double yCutPosition;
    double zCutPosition;

    int xCutDirection;
    int yCutDirection;
    int zCutDirection;

    double xMax;
    double yMax;
    double zMax;

    bool xCutDisplay;
    bool yCutDisplay;
    bool zCutDisplay;

    ShaderPool * shaderPool;
    ShaderPackage * shaderPackage;
    ProgramObject * programObject;

    bool textureCreated;

    std::vector<unsigned char> _labels;
    std::map<unsigned char, std::size_t> _labelsIDMap;


    void updateColorTexData();
public:

    Vec3Di Vmin;
    Vec3Di Vmax;

    Texture();
    ~Texture();

    void init();

    void clear();

    void initTexture();

    void deleteTexture();

    void draw();
    void drawCube();
    void fillCut();
    void drawBoundingBox();
    void drawCutPlanes();

    int getWidth(){return n[0];}
    int getHeight(){return n[1];}
    int getDepth(){return n[2];};
    int getGridSize(){return n[0]*n[2]*n[1];}

    const Vec3Di & getVmin() const { return Vmin; }
    Vec3Di & getVmin() { return Vmin; }
    const Vec3Di & getVmax() const { return Vmax; }
    Vec3Di & getVmax() { return Vmax; }

    float dx(){return d[0];}
    float dy(){return d[1];}
    float dz(){return d[2];}

    void setXCut(int _xCut);
    void setYCut(int _yCut);
    void setZCut(int _zCut);

    void invertXCut(){xCutDirection *= -1;}
    void invertYCut(){yCutDirection *= -1;}
    void invertZCut(){zCutDirection *= -1;}

    void setXCutDisplay(bool _xCutDisplay){xCutDisplay = _xCutDisplay;}
    void setYCutDisplay(bool _yCutDisplay){yCutDisplay = _yCutDisplay;}
    void setZCutDisplay(bool _zCutDisplay){zCutDisplay = _zCutDisplay;}

    float getXMax(){return xMax;}
    float getYMax(){return yMax;}
    float getZMax(){return zMax;}

    void updateColorTexData(color4* color_map);

    GLuint getTextureId(){return textureId;}
    GLuint getColorTextureId() { return colorTextureId; }

    float getGridStep(){return minD;}

    void build(const std::vector<unsigned char> & data, const std::vector<unsigned char> & labesl,
	       std::size_t & nx , std::size_t & ny , std::size_t & nz,
	       float & dx , float & dy , float & dz, color4* color_map,
	       unsigned int* visibility_check);

    int getLabelOfIndex(unsigned int i);

    unsigned int* visible_domains;
    color4* colorMap;
    float* colorTexData;
};

#endif // TEXTURE_H
