#ifndef OPENGLHEADER_H
#define OPENGLHEADER_H

// Include Qt's OpenGL implementation for rendering
#ifndef USE_QT_OPENGLFUNCTIONS
#	define USE_QT_OPENGLFUNCTIONS
#	include <QOpenGLFunctions_4_0_Core>
#	include <QOpenGLFunctions_4_0_Compatibility>
#	define QT_OPENGL_VERSION_USED_COMPAT QOpenGLFunctions_4_0_Compatibility
#	include <QOpenGLExtraFunctions>
#	include <QtOpenGLExtensions/QOpenGLExtensions>
#else
#	include <GL/glew.h>	// includes gl, glu
#endif

#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>

#define GL_BUFFER_OFFSET( i ) ( ( char* ) NULL + ( i ) )
#define GL_TYPED_BUFFER_OFFSET( i, type ) ( ( type* ) NULL + ( i ) )

inline int __GetOpenGLError ( char* szFile, int iLine )
{
        int iRetCode = 0;
        GLenum glErr = glGetError();
        while ( glErr != GL_NO_ERROR ) {
                std::cout << "GLError in file << " << szFile << " @ line " << iLine << ":" << gluErrorString( glErr ) << std::endl;
                iRetCode = 1;
                 glErr = glGetError();
        }
        return iRetCode;
}

struct errorCatcher : public QT_OPENGL_VERSION_USED_COMPAT {
	errorCatcher() {
		QOpenGLContext* context = QOpenGLContext::currentContext();
		if (context == nullptr) {
			throw std::runtime_error("errorCatcher::errorCatcher() : Could not find a current QOpenGLContext !");
		} else {
			this->initializeOpenGLFunctions();
		}
	}
	int operator()( char* szFile, int iLine ) {
		int iRetCode = 0;
		GLenum glErr = glGetError();
		while ( glErr != GL_NO_ERROR ) {
			std::cout << "GLError in file << " << szFile << " @ line " << iLine << ":" << gluErrorString( glErr ) << std::endl;
			iRetCode = 1;
			 glErr = glGetError();
		}
		return iRetCode;
	}
};
#ifndef EXTERN_GLCATCHER_VARIABLE_
#define EXTERN_GLCATCHER_VARIABLE_
extern errorCatcher* glCatcher;
#endif // EXTERN_GLCATCHER_VARIABLE_

#ifndef NDEBUG
#	ifndef USE_QT_OPENGLFUNCTIONS
#		define GetOpenGLError() __GetOpenGLError( ( char* )__FILE__, ( int )__LINE__ )
#	else
#		define GetOpenGLError() (*glCatcher)( ( char* )__FILE__, ( int )__LINE__ )
#	endif
#else
	#define GetOpenGLError()
#endif

#endif // OPENGLHEADER_H
