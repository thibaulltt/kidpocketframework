#include "ShaderPool.h"
#include "HelperFunctions.h"
#include <assert.h>
#include <algorithm>

// ----------------------------------------------------------------------
// Track memory leaks
// ----------------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif

#if defined(_MSC_VER)
	#pragma warning(disable : 4996)
#endif

// ----------------------------------------------------------------------
// Singleton call
// ----------------------------------------------------------------------
ShaderPool* ShaderPool::GetInstance()
{
	static ShaderPool SP;
	if ( !SP.m_bIsInitialized ) {
		if ( !SP.Initialize( true ) ) {
                    std::cout << "Failed to initialize ShaderPool!" << std::endl;
                    assert( 0 );
                    return 0;
		}
	}
	return &SP;
}

// ----------------------------------------------------------------------
// Cstr & Dstr
// ----------------------------------------------------------------------
ShaderPool::ShaderPool() :

        myFirstShader( 0 ),
        myVolumeShader( 0 ),

	m_bIsInitialized( false )
{
}

ShaderPool::~ShaderPool()
{
	Release();
}

// ----------------------------------------------------------------------
// Initializing the shaderpool
// ----------------------------------------------------------------------
bool ShaderPool::Initialize( bool bBreakOnError )
{
	// Here everything is Hardcoded, since the shader can not be interchanged
	bool bGood = false;

	GLint iMaxAttrib = 0;
	glGetIntegerv( GL_MAX_VERTEX_ATTRIBS, &iMaxAttrib );

	/////////////////////////////////////////////////
	// Shader Variables:
	VertexShader*		pVertexShader = 0;
	GeometryShader*		pGeometryShader = 0;
	FragmentShader*		pFragmentShader = 0;
	ProgramObject*		pProgram = 0;

	ShaderInitializer	AllShaders[] = {
		/////////////////////////////////////////////////
		// General lighting (for the unprocessed model)
		/////////////////////////////////////////////////
                //ShaderInitializer( "./GLSL/shaders/volume.vert", 0, "./GLSL/shaders/volume.frag", &myVolumeShader ),
                ShaderInitializer( "./GLSL/shaders/transfer_mesh.vert", 0, "./GLSL/shaders/transfer_mesh.frag", &myTrMeshShader ),
                ShaderInitializer( "./GLSL/shaders/tetrahedra.vert", 0, "./GLSL/shaders/tetrahedra.frag", &myTetrahedraShader ),
	
	};


	int iNumShaders = sizeof( AllShaders ) / sizeof( AllShaders[0] );
	for ( int i=0; i< iNumShaders; ++ i ) {
		ShaderInitializer& refSI = AllShaders[ i ];


		// reset values:
		pVertexShader = 0;
		pGeometryShader = 0;
		pFragmentShader = 0;
		pProgram = 0;


		bool bHasGeoSh = refSI.m_pszGeometryShaderFile != 0;

		// FileExistingCheck
		if ( !FileExists( refSI.m_pszVertexShaderFile ) ) {
                    std::cout << "Could not find VertexShader: " << refSI.m_pszVertexShaderFile << "!" << std::endl;
			assert( 0 );
			return false;
		}
		if ( !FileExists( refSI.m_pszFragmentShaderFile ) ) {
                    std::cout << "Could not find FragmentShader: " << refSI.m_pszFragmentShaderFile << "!" << std::endl;
			assert( 0 );
			return false;
		}
		// GeometryShader
		if ( bHasGeoSh ) {
			if ( !FileExists( refSI.m_pszGeometryShaderFile ) ) {
                                std::cout << "Could not find GeometryShader: " << refSI.m_pszGeometryShaderFile << "!" << std::endl;
				assert( 0 );
				return false;
			}
			
		}

		bGood = true;

		// Get memory
		pProgram = new ProgramObject();
		bGood &= pProgram != 0;

		pVertexShader = new VertexShader();
		bGood &= pVertexShader != 0;

		pFragmentShader = new FragmentShader();
		bGood &= pFragmentShader != 0;

		pProgram = new ProgramObject();
		bGood &= pProgram != 0;
		
		if ( bHasGeoSh ) {
			pGeometryShader = new GeometryShader();
			bGood &= pGeometryShader != 0;
		}

		if ( !bGood ) return false;

                if ( !pVertexShader->LoadShader( bBreakOnError, refSI.m_pszVertexShaderFile ) ) {
                    //return false;
                    // continue with the next shader...
                    continue;
                }

                if ( !pFragmentShader->LoadShader( bBreakOnError, refSI.m_pszFragmentShaderFile ) ) {
                    //return false;
                    continue;
                }
		
		if ( bHasGeoSh ) {
                    if ( !pGeometryShader->LoadShader( bBreakOnError, refSI.m_pszGeometryShaderFile ) ) {
                        // return false;
                        continue;
                    }
                    // I currently use only one geometry-shader... they're too damn slow
                    pProgram->SetGeometryShaderInputType( GL_TRIANGLES );
                    pProgram->SetGeometryShaderNumVerticesOut( 153 );				// means level 4, but actual 0 should do fine, too because none are really created
                    pProgram->SetGeometryShaderOutputType( GL_TRIANGLE_STRIP );

                    pProgram->AddShader( pGeometryShader );
		}

		pProgram->AddShader( pVertexShader );
		pProgram->AddShader( pFragmentShader );

		bGood &= pProgram->Link( bBreakOnError );					// for compiling
		if ( !bGood ) {
			delete pProgram;
			pProgram = 0;
		}

                ShaderPackage* pNewPackage = new ShaderPackage( pVertexShader, pGeometryShader, pFragmentShader, pProgram );

                if ( !pNewPackage ) {
                    std::cout << "Failed to allocate memory for new ShaderPackage!" << std::endl;
                    //return false;
                    continue;
                }

                // release the old shader:
                if (*refSI.m_pPointerToPrivateMember) {
                    delete (*refSI.m_pPointerToPrivateMember);

                    // remove the old one from the list:
                    m_ListPackages.erase( std::remove( m_ListPackages.begin(), m_ListPackages.end(), refSI.m_pPointerToPrivateMember ), m_ListPackages.end() );

                    *refSI.m_pPointerToPrivateMember = 0;
                }


                (*refSI.m_pPointerToPrivateMember) = pNewPackage;

                m_ListPackages.push_back( refSI.m_pPointerToPrivateMember );
	}

	m_bIsInitialized = true;
	return true;
}

// ----------------------------------------------------------------------
// To reload the shaders at run-time
// ----------------------------------------------------------------------
void ShaderPool::ReloadShaders()
{
	m_bIsInitialized = false;
        std::cout << "Shaders unloaded. Recompiling...";

        Initialize( false );


	if ( m_bIsInitialized ) {
                std::cout << "Done!" << std::endl;
	}        
	else {
            std::cout << "Error!" << std::endl;
	}
}

// ----------------------------------------------------------------------
// this will release all shaders
// ----------------------------------------------------------------------
void ShaderPool::Release()
{
    std::list< ShaderPackage** >::iterator itor;
    ShaderPackage** ppSP = 0;
    ShaderPackage* pSP = 0;

    for ( itor = m_ListPackages.begin(); itor != m_ListPackages.end(); ++itor )
    {
        // delete the shader
        pSP = *(*itor);
        delete pSP;

        // let the pointer point to 0
        ppSP = (ShaderPackage**)(**itor);
        *ppSP = 0;
    }
    m_ListPackages.clear();
}
