#include "FragmentShader.h"

// ------------------------------------------------------------------------------------
// Track memory leaks
// ------------------------------------------------------------------------------------
#if (defined(_DEBUG) || defined (DEBUG)) && defined(_MSC_VER)
	#define _CRTDBG_MAP_ALLOC
	#include <crtdbg.h>

	#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
	#define new DEBUG_NEW
#endif

// ------------------------------------------------------------------------------------
// Cstr & Dstr
// ------------------------------------------------------------------------------------
FragmentShader::FragmentShader():
	ShaderObject()
{
	m_TypeShader = TypeShader_FragmentShader;
	m_ShaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
}

FragmentShader::~FragmentShader()
{
}
