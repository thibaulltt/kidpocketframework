#ifndef _FRAGMENT_SHADER__H_
#define _FRAGMENT_SHADER__H_

/*
	A Simple FragmentShader. inherits from the general shader.

	Matthias Holl�nder 2008/2009
*/

#include "ShaderObject.h"

class FragmentShader : public ShaderObject
{
public:
	// ------------------------------------------------------------
	// Cstr & Dstr
	// ------------------------------------------------------------
	FragmentShader();
	virtual ~FragmentShader();
};

#endif
