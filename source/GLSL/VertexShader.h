#ifndef _VERTEX_SHADER__H_
#define _VERTEX_SHADER__H_

/*
	A Simple VertexShader-class. Inherits from the general shader.
	Matthias Holl�nder 2008/2009
*/

#include "ShaderObject.h"

class VertexShader : public ShaderObject
{
public:
	// ----------------------------------------------------------------
	// Cstr & Dstr
	// ----------------------------------------------------------------
	VertexShader();
	virtual ~VertexShader();
};

#endif

