#ifndef _PROGRAM_OBJECT__H_
#define _PROGRAM_OBJECT__H_

/*
	You can attach different Shaders ( Vertex, Geometry, Fragment ) to the ProgramObject and compile it.
	Matthias Holländer 2008/2009
*/

#include "OpenGLHeader.h"
#include "ShaderObject.h"

class BufferObject;

class ProgramObject : public QT_OPENGL_VERSION_USED_COMPAT
{
public:
	// ----------------------------------------------------------------
	// Cstr & Dstr
	// ----------------------------------------------------------------
	ProgramObject();
	virtual ~ProgramObject();

	// ----------------------------------------------------------------
	// public methods:
	// ----------------------------------------------------------------
	void			AddShader(ShaderObject* shader);
	void			RemoveShader(ShaderObject* shader);

	bool			Link( bool bBreakOnError );

	void			Begin();
	void			End();

	bool			CheckError();

	// For Geometry-Shader you must use these:
	void			SetGeometryShaderInputType(GLenum type);
	void			SetGeometryShaderOutputType(GLenum type);
	void			SetGeometryShaderNumVerticesOut(int iNumVerticesOut = 0);			// 0 will lead to maxvalue

	// For Buffers
	void			AddBuffer(BufferObject* pBuffer, const char* szBufferName);


	// Shader Variables:
	void			SetUniform1f(const char* szUniformName, GLfloat fValue0);
	void			SetUniform2f(const char* szUniformName, GLfloat fValue0, GLfloat fValue1);
	void			SetUniform3f(const char* szUniformName, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2);
	void			SetUniform4f(const char* szUniformName, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2, GLfloat fValue3);

	void			SetUniform1ui(const char* szUniformName, GLuint uiValue0);

	void			SetUniform1i(const char* szUniformName, GLint iValue0);
	void			SetUniform2i(const char* szUniformName, GLint iValue0, GLint iValue1);
	void			SetUniform3i(const char* szUniformName, GLint iValue0, GLint iValue1, GLint iValue2);
	void			SetUniform4i(const char* szUniformName, GLint iValue0, GLint iValue1, GLint iValue2, GLint iValue3);

	void			SetUniform1fv(const char* szUniformName, GLsizei iCount, GLfloat* pfPtr);
	void			SetUniform2fv(const char* szUniformName, GLsizei iCount, GLfloat* pfPtr);
	void			SetUniform3fv(const char* szUniformName, GLsizei iCount, GLfloat* pfPtr);
	void			SetUniform4fv(const char* szUniformName, GLsizei iCount, GLfloat* pfPtr);
	void			SetUniform1iv(const char* szUniformName, GLsizei iCount, GLint* piPtr);
	void			SetUniform2iv(const char* szUniformName, GLsizei iCount, GLint* piPtr);
	void			SetUniform3iv(const char* szUniformName, GLsizei iCount, GLint* piPtr);
	void			SetUniform4iv(const char* szUniformName, GLsizei iCount, GLint* piPtr);
	void			SetUniform1uiv(const char* szUniformName, GLsizei iCount, GLuint* piPtr);

	void			SetVertexAttrib1f(GLuint uiIndex, GLfloat fValue0);
	void			SetVertexAttrib2f(GLuint uiIndex, GLfloat fValue0, GLfloat fValue1);
	void			SetVertexAttrib3f(GLuint uiIndex, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2);
	void			SetVertexAttrib4f(GLuint uiIndex, GLfloat fValue0, GLfloat fValue1, GLfloat fValue2, GLfloat fValue3);

	void			SetUniformMatrix2fv(const char* szUniformName, GLsizei iCount, GLboolean bTranspose, GLfloat* pfPtr);
	void			SetUniformMatrix3fv(const char* szUniformName, GLsizei iCount, GLboolean bTranspose, GLfloat* pfPtr);
	void			SetUniformMatrix4fv(const char* szUniformName, GLsizei iCount, GLboolean bTranspose, GLfloat* pfPtr);

	GLint			GetUniformBufferSize(const char* szBufferName);

	GLint			GetAttribLocation(const char* szAttribLocationName);
	void			BindAttribLocation(GLuint uiIndex, const char* szAttribName);
private:
	// ----------------------------------------------------------------
	// private attributes
	// ----------------------------------------------------------------
	GLhandleARB		m_Program;

	GLint			GetUniformLocation(const char* szUniformName);
};

#endif
