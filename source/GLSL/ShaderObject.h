#ifndef _SHADER_OBJECT__H_
#define _SHADER_OBJECT__H_

/*
	ShaderObject-Class. Each Shader (Vertex, Geometry, Fragment) inherits from this class
	Matthias Holl�nder 2008/2009
*/

#include "OpenGLHeader.h"
#include <stdarg.h>

class ShaderObject : public QT_OPENGL_VERSION_USED_COMPAT
{
	// ----------------------------------------------------------------
	// Cstr & Dstr
	// ----------------------------------------------------------------
protected:
							ShaderObject();
public:
	virtual					~ShaderObject();

	// ----------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------
	bool					LoadShader( bool bBreakOnError, const char* szFilename, ... );
	bool					LoadShaderChar( bool bBreakOnError, const char* szShaderText );
	GLhandleARB&			GetShader();
	bool					CheckError();
protected:
	// ----------------------------------------------------------------
	// protected enum
	// ----------------------------------------------------------------
	enum ShaderType {
		TypeShader_VertexShader = 0,
		TypeShader_GeometryShader,
		TypeShader_FragmentShader,


		TypeShader_Unknown
	};

	// ----------------------------------------------------------------
	// protected methods:
	// ----------------------------------------------------------------
	char*					GetFileContents(const char* szFilename);

	// ----------------------------------------------------------------
	// protected attributes
	// ----------------------------------------------------------------
	GLhandleARB				m_ShaderHandle;
	ShaderType				m_TypeShader;
	char* filename;


};

#endif
