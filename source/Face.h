#ifndef FACE_H
#define FACE_H

#include <map>
#include <vector>

// -------------------------------------------------
// Intermediate Face structure for hashed adjacency
// -------------------------------------------------

struct Face {
public:
    inline Face ( unsigned int v0, unsigned int v1, unsigned int v2) {
        if (v1 < v0) std::swap(v0,v1);
        if (v2 < v1) std::swap(v1,v2);
        if (v1 < v0) std::swap(v0,v1);
        v[0] = v0; v[1] = v1; v[2] = v2;
    }
    //inline void std::swap(unsigned int &a, unsigned int &b){unsigned int tmp = a; a=b; b=tmp;}
    inline Face (const Face & f) { v[0] = f.v[0]; v[1] = f.v[1]; v[2] = f.v[2]; }
    inline virtual ~Face () {}
    inline Face & operator= (const Face & f) { v[0] = f.v[0]; v[1] = f.v[1]; v[2] = f.v[2]; return (*this); }
    inline bool operator== (const Face & f) { return (v[0] == f.v[0] && v[1] == f.v[1] && v[2] == f.v[2]); }
    inline bool operator< (const Face & f) const { return (v[0] < f.v[0] || (v[0] == f.v[0] && v[1] < f.v[1]) || (v[0] == f.v[0] && v[1] == f.v[1] && v[2] < f.v[2])); }
    inline bool contains (unsigned int i) const { return (v[0] == i || v[1] == i || v[2] == i); }
    inline unsigned int getVertex (unsigned int i) const { return v[i]; }
    unsigned int v[3];
};

struct compareFace {
    inline bool operator()(const Face f1, const Face f2) const {
        if( f1.v[0]<f2.v[0] ||
           (f1.v[0]==f2.v[0] && f1.v[1]<f2.v[1]) ||
           (f1.v[0]==f2.v[0] && f1.v[1]==f2.v[1] && f1.v[2]<f2.v[2]))
            return true;
        return false;
    }
};

typedef std::map<Face, unsigned int, compareFace> FaceMapIndex;



#endif // FACE_H
