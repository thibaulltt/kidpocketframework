#include "Tetrahedron.h"

using namespace std;

ostream & operator<< (ostream & output, const Tetrahedron & t) {
  output << t.getVertex (0) << " " << t.getVertex (1) << " " << t.getVertex (2) << " " << t.getVertex(3);
  return output;
}
