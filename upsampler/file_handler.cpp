#ifndef UPSAMPLER_FILE_HELPER_CPP_
#define UPSAMPLER_FILE_HELPER_CPP_

#include "file_handler.hpp"

#ifndef NDEBUG
#	include <iostream>
#	include <iomanip>
#	include "macros.hpp"
//#	define VERBOSE_FILE_EXTENSIONS	// Debug for FileBaseName()
//#	define VERBOSE_FILE_PATHS	// Debug for AppendExtension[__SELF]()
#endif

#include <fstream>
#include <cstring>
#include <cstdlib>

bool FileExists(const char* filename) {
	// We could do a more time-optimized version of it
	// by returning fileTester == nullptr, but I still
	// prefer to properly close the inode associated
	// with it, to sanitize the system.

	// Try to open the file :
	FILE* fileTester = fopen(filename, "r");
	// Is it open ? If not, return false :
	if (fileTester == nullptr) return false;
	// Otherwise, close the file and return true :
	fclose(fileTester);
	return true;
}

char* FileBaseName(const char* filename) {
	// Note : this function does not check if the file exists.
	// It only extracts the basename from the string provided.

	// Locate last occurence of the character dot '.'
	const char* separator = strrchr(filename, '.');
	const char* path_slash = strrchr(filename, '/');
	// If none was found, return nulltpr :
	if (separator == nullptr) { return nullptr; }
	// Otherwise, allocate a new string and copy contents :
	std::size_t length = static_cast<std::size_t>(separator - &filename[0]);
	std::size_t length_to_path = static_cast<std::size_t>(separator - path_slash);
	// If the dot is a for relative folder access, discard the file (since it has no extension)
	if (length_to_path < 0) { return nullptr; }
#if not defined NDEBUG && defined VERBOSE_FILE_EXTENSIONS
	// allocate string for spaces :
	char* seps = static_cast<char*>(calloc(length, sizeof(char)));
	// set to spaces :
	memset(seps, ' ', length*sizeof(char));
	// output result :
	std::cerr << "Position of the extension in the filename provided :" << '\n';
	std::cerr << filename << '\n';
	std::cerr << seps << '^' << '\n';
#endif
	// Allocate memory for file's basename
	char* basename = static_cast<char*>(calloc(length + 1, sizeof(char)));
	memcpy(basename, filename, length * sizeof(char));
	// null-terminate the string
	basename[length] = '\0';
	// return it !
	return basename;
}

char* AppendExtension(char* basename, const char* extension) {
	// Early exits
	if (basename == nullptr) return nullptr;
	if (extension == nullptr) return nullptr;

	std::size_t baselength = strlen(basename); //strlen ignores terminating \0 !
	std::size_t extlength = strlen(extension); //strlen ignores terminating \0 !

	// If basename is empty string, return nothing
	if (baselength == 0) return nullptr;
	// If extension is empty string, return basename
	if (extlength == 0) return basename;

	// Realloc basename (add one for the dot, and one for the terminating char) :
	char* new_basename = static_cast<char*>(calloc(baselength+extlength+2, sizeof(char)));
	if (basename == nullptr) {
#ifndef NDEBUG
		DebugError << "calloc() was not able to allocate " << baselength + extlength + 2 << " bytes" << '\n';
#endif
		return nullptr;
	}
	memcpy(new_basename, basename, baselength);
	new_basename[baselength] = '.';
	memcpy(new_basename+baselength+1, extension, extlength);
	new_basename[baselength+extlength+1] = '\0';
#if not defined NDEBUG && defined VERBOSE_FILE_PATHS
	DebugLog << "Started from :" << '\n';
	DebugLog << '\t' << basename << " and " << extension << " (" << baselength + extlength << " characters long)" << '\n';
	DebugLog << "Ended with :" << '\n';
	DebugLog << '\t' << new_basename << '\n';
#endif
	return new_basename;
}

char* AppendExtension__SELF(char* basename, const char* extension) {
	// Early exits
	if (basename == nullptr) return nullptr;
	if (extension == nullptr) return nullptr;

	std::size_t baselength = strlen(basename); //strlen ignores terminating \0 !
	std::size_t extlength = strlen(extension); //strlen ignores terminating \0 !

#ifndef NDEBUG
	char* basename_copy = nullptr;
	if (memcpy(basename_copy, basename, baselength) == nullptr) {
		DebugError << "Could not copy string to nullptr" << '\n';
	}
#endif

	// If basename is empty string, return nothing
	if (baselength == 0) return nullptr;
	// If extension is empty string, return basename
	if (extlength == 0) return basename;

	// Realloc basename (add one for the dot, and one for the terminating char) :
	basename = static_cast<char*>(realloc(basename, baselength+extlength+2));
	if (basename == nullptr) {
#ifndef NDEBUG
		DebugError << "realloc() was not able to modify from " << baselength << " bytes to " << baselength + extlength + 2 << '\n';
#endif
		return nullptr;
	}
	basename[baselength] = '.';
	memcpy(basename+baselength+1, extension, extlength);
	basename[baselength+extlength+1] = '\0';
#if not defined NDEBUG && defined VERBOSE_FILE_PATHS
	DebugLog << "Started from :" << '\n';
	DebugLog << '\t' << basename_copy << " and " << extension << '\n';
	DebugLog << "Ended with :" << '\n';
	DebugLog << '\t' << basename << '\n';
#endif
	return basename;
}

#endif // UPSAMPLER_FILE_HELPER_CPP_
