#ifndef UPSAMPLER_MACROS_HPP_
#define UPSAMPLER_MACROS_HPP_

#include <ostream>
#include <iostream>

std::ostream& _my_debug_message_(const char* func_name, const char* severity);

#ifndef NDEBUG

	// Simple defines to call them within a function, and have them print
	// the called function name and Log/Warning/Error message :
#	define DebugLog			_my_debug_message_(__PRETTY_FUNCTION__, "Log")
#	define DebugError		_my_debug_message_(__PRETTY_FUNCTION__, "ERROR")
#	define DebugWarning		_my_debug_message_(__PRETTY_FUNCTION__, "Warning")

#	define DebugLogShort		_my_debug_message_(__FUNCTION__, "Log")
#	define DebugErrorShort		_my_debug_message_(__FUNCTION__, "ERROR")
#	define DebugWarningShort	_my_debug_message_(__FUNCTION__, "Warning")

#else

	// TODO : Find a way to redirect those to /dev/null
#	define DebugLog			_my_debug_message_(nullptr, nullptr)
#	define DebugError		_my_debug_message_(nullptr, nullptr)
#	define DebugWarning		_my_debug_message_(nullptr, nullptr)

#	define DebugLogShort		_my_debug_message_(nullptr, nullptr)
#	define DebugErrorShort		_my_debug_message_(nullptr, nullptr)
#	define DebugWarningShort	_my_debug_message_(nullptr, nullptr)

#endif

#endif // UPSAMPLER_MACROS_HPP_
