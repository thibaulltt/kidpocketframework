#ifndef UPSAMPLER_MACROS_CPP_
#define UPSAMPLER_MACROS_CPP_

#include "macros.hpp"

std::ostream& _my_debug_message_(const char* func_name, const char* severity) {
	if (func_name == nullptr) func_name = "";
	if (severity == nullptr) severity = "";
	return std::cerr << "[" << func_name << "] " << severity << " : ";
}

#endif // UPSAMPLER_MACROS_CPP_

