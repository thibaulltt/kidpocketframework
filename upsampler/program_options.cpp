#ifndef UPSAMPLER_PROGRAM_OPTIONS_CPP_
#define UPSAMPLER_PROGRAM_OPTIONS_CPP_

#include "program_options.hpp"
#include "macros.hpp"

#ifndef NDEBUG
#include <iostream>
#endif

upsamplingOptions::upsamplingOptions() : options_desc("Allowed options"),
	positional_options_desc(), variables_map() {
#ifndef NDEBUG
	DebugLog << "Constructor of command line arguments called !" << '\n';
#endif

	// Add options to the option description :
	this->setParameters();
}

upsamplingOptions* upsamplingOptions::GetInstance(int argc, char* argv[]) {
	static upsamplingOptions singletonUpsamplingOptions;
	if (not singletonUpsamplingOptions.b_has_been_initialized) {
		singletonUpsamplingOptions.parseArguments(argc, argv);
	}
	return &singletonUpsamplingOptions;
}

bool upsamplingOptions::isHelp() const {
	if (this->b_has_been_initialized == false) {
		std::cout << "ERROR : Trying to access command line options before it has been parsed !\n";
		return false;
	}
	if (this->variables_map.count("help") or this->variables_map.count("H")) {
		return this->variables_map.count("help") or this->variables_map.count("H");
	}
	return false;
}

void upsamplingOptions::setParameters() {
	this->positional_options_desc.add("input-file", 1);
	this->positional_options_desc.add("output-file", 1);
	this->options_desc.add_options()
		("help,H", "Produce a help message")
		("upsampling-type,U", boost::program_options::value<int>()->default_value(2), "Upsampling type. Can be 0, 1, 2 or 3.")
		("input-file,i", boost::program_options::value<std::string>(), "Input file.")
		("output-file,o", boost::program_options::value<std::string>(), "Output file.")
	;
}

void upsamplingOptions::parseArguments(int argc, char* argv[]) {
	boost::program_options::store(
		boost::program_options::command_line_parser(argc, argv).options(this->options_desc)
			.positional(this->positional_options_desc).run(),
		this->variables_map
	);
	boost::program_options::notify(this->variables_map);
	
	// Set the singleton state to be initialized now :
	this->b_has_been_initialized = true;
}

#endif // UPSAMPLER_PROGRAM_OPTIONS_HPP_

// vim: set tabstop=8 colorcolumn=120 :
