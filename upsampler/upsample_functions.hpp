#ifndef UPSAMPLER_UPSAMPLE_FUNCTIONS_CPP_
#define UPSAMPLER_UPSAMPLE_FUNCTIONS_CPP_

#ifndef NDEBUG
#	include <iostream>
#endif

#include <cstdlib>

template <typename T> T* upsample_line(const T* original_line, std::size_t nbelem) {
	// Early exit :
	if (original_line == nullptr) { return nullptr; }

	// Allocate the new line :
	T* new_array = static_cast<T*>(calloc(nbelem*2, sizeof(T)));

	// Copy the elements, one by one :
	for (std::size_t i = 0; i < nbelem; ++i) {
		// Nearest neighbor used : copy the values here directly :
		new_array[i*2+0] = original_line[i];
		new_array[i*2+1] = original_line[i];
	}

	return new_array;
}

template <typename T> T* upsample_line_direct(T* original_line, T* destination, std::size_t nbelem) {
	// Early exit :
	if (original_line == nullptr) { return nullptr; }
	if (destination == nullptr) { return nullptr; }

	// Copy the elements, one by one :
	for (std::size_t i = 0; i < nbelem; ++i) {
		// Nearest neighbor used : copy the values here directly :
		destination[i*2+0] = original_line[i];
		destination[i*2+1] = original_line[i];
	}

	return destination;
}

#endif // UPSAMPLER_UPSAMPLE_FUNCTIONS_CPP_
