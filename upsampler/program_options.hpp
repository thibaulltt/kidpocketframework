#ifndef UPSAMPLER_PROGRAM_OPTIONS_HPP_
#define UPSAMPLER_PROGRAM_OPTIONS_HPP_

#include <boost/program_options.hpp>

/**
 * Here are the arguments that should be passed to the function :
 * 	0 - The executable name (done by bash)					[Required, handled by bash]
 * 	1 - The kind of upsampling to perform :					[Optional, default = 2]
 * 		- 0 : no upsampling, exit the program
 * 		- 1 : upsampling along the X axis. Only doubles the number of
 * 		      pixels on the lines of the images.
 * 		- 2 : upsampling along the X and Y axes : each image will be
 * 		      upscaled in its entirety, but no additional stacks will
 * 		      be created.
 * 		- 3 : upsampling along all axes. It will upsample each image,
 * 		      and add additional stacks to the image. (warning, very
 * 		      space consuming !)
 * 	2 - The base name of the file to upsample				[Required, one only]
 * 	3 - The name of the filename to write out				[Required, one only]
 */

class upsamplingOptions {
	// ------- //
	// Members //
	// ------- //
	private:
		/// \brief Boost options descriptor, describes possible options for the program
		boost::program_options::options_description options_desc;

		/// \brief Boost positional options, same as options_desc, but for options
		/// without leading parameter names
		boost::program_options::positional_options_description positional_options_desc;

		/// \brief Boost variable map, keeps the CLI options once parsed
		boost::program_options::variables_map variables_map;

		/// \brief Keeps track if the singleton has been created, or if we
		/// need to create a new one.
		bool b_has_been_initialized;

		/// \brief Default constructor. Constructs the parser, and parses the
		/// arguments provided
		upsamplingOptions();

	// --------- //
	// Functions //
	// --------- //
	public:
		/// \brief Get a pointer to a static instance of the parser.
		/// \details Allows only one static instance of upsamplingOptions to exist within the
		/// program's lifetime.
		///
		/// \param argc The number of arguments of the program (given by the user)
		/// \param argv The arguments of the program, as an array of arrays of char (given by the user)
		/// \return A pointer to a static instance of upsamplingOptions.
		static upsamplingOptions* GetInstance(int argc, char* argv[]);

		/// \brief Returns true if the user has '--help' or '-h' anywhere in
		/// the command line arguments
		bool isHelp() const;

		/// \brief Looks for the argument named "key" in the CLI arguments passed to the program.
		/// \details If the "key" argument was passed as a CLI argument to the program, store the
		/// value of it in the variable named "store". If not, "store" will be left untouched and
		/// the function will return false.
		///
		/// \note After a call to upsamplingOptions::GetInstance, one should call this operator as :
		/// 
		///
		/// \param key The name of the key to look for
		/// \param store The variable in which to store the value of "key", if it exists
		///
		/// \return True if the key has a value, false if it has not (meaning it has not been passed
		/// as an argument when launching the program).
		template<typename T> bool operator()(const char* key, T& store) {
			if (this->variables_map.count(key)) {
				store = this->variables_map[key].as<T>();
				return true;
			} else {
				return false;
		}
}
	private:
		/// \brief Sets the possible options to pass to the program.
		void setParameters();

		/// \brief Parses the arguments provided to it.
		/// \param argc The number of arguments
		/// \param argv The arguments themselves
		void parseArguments(int argc, char* argv[]);
};

#endif // UPSAMPLER_PROGRAM_OPTIONS_HPP_

// vim: set tabstop=8 colorcolumn=120 :
