#include <iostream>
#include <fstream>
#include <cstdlib>

#include "file_handler.hpp"
#include "program_options.hpp"
#include "upsample_functions.hpp"
#include "macros.hpp"

/// \brief Show a help message if the user doesn't provide the good arguments.
void ShowHelp(void);

int main(int argc, char* argv[]) {
	upsamplingOptions* options = upsamplingOptions::GetInstance(argc, argv);
	// Check to see if the number of arguments was respected :
	if (options->isHelp()) {
		ShowHelp();
		return EXIT_FAILURE;
	}

	// Get input and output filenames
	std::string input_filename, output_filename;
	if (not options->operator()("input-file", input_filename)) {
		std::cerr << "Error : No input file provided." << '\n';
		ShowHelp();
		return EXIT_FAILURE;
	}
	if (not options->operator()("output-file", output_filename)) {
		std::cerr << "Error : No output file provided." << '\n';
		ShowHelp();
		return EXIT_FAILURE;
	}
	char* basename_in = nullptr;
	char* basename_out = nullptr;
	if ((basename_in = FileBaseName(input_filename.data())) == nullptr) {
		std::cerr << "Input file has no extension.\nExiting." << '\n';
		return EXIT_FAILURE;
	}
	if ((basename_out = FileBaseName(output_filename.data())) == nullptr) {
		std::cerr << "Output file has no extension.\nExiting." << '\n';
		return EXIT_FAILURE;
	}

	// create the filenames for IMA and DIM files
	char* dim_input_file_path = AppendExtension(basename_in, "dim");
	char* ima_input_file_path = AppendExtension(basename_in, "ima");

	// check the filenames are properly created
	if (dim_input_file_path == nullptr) { DebugWarning << "base and ext concat could not be performed for dim" << '\n'; return EXIT_FAILURE; }
	if (ima_input_file_path == nullptr) { DebugWarning << "base and ext concat could not be performed for ima" << '\n'; return EXIT_FAILURE; }

	// Open the files, and check they are open
	std::ifstream dim_input_file(dim_input_file_path);
	if (not dim_input_file.is_open()) {
		DebugError << dim_input_file_path << " cannot be opened." << '\n';
		return EXIT_FAILURE;
	}

	// Read info from DIM file :
	std::size_t nx, ny, nz;

	// read number of voxels :
	dim_input_file >> nx;
	dim_input_file >> ny;
	dim_input_file >> nz;

	// read info :
	std::string token, type;
	double input_dx, input_dy, input_dz;

	do {
		dim_input_file >> token;
		if (token.find("-type") != std::string::npos) { dim_input_file >> type; }
		else if (token.find("-dx") != std::string::npos) { dim_input_file >> input_dx; }
		else if (token.find("-dy") != std::string::npos) { dim_input_file >> input_dy; }
		else if (token.find("-dz") != std::string::npos) { dim_input_file >> input_dz; }
		else {
			DebugWarning << "token " << token << " did not represent anything" << '\n';
		}
	} while (not dim_input_file.eof());

	std::cout << "IMA file at " << ima_input_file_path << " should contain a voxel grid with the following properties :" << '\n';
	std::cout << '\t' << "Voxels : " << nx << "x" << ny << "x" << nz << '\n';
	std::cout << '\t' << "Voxel dimensions : " << input_dx << "x" << input_dy << "x" << input_dz << '\n';
	std::cout << '\t' << "Type : " << type << '\n';

	dim_input_file.close();
	// Open the IMA file :
	std::ifstream ima_input_file(ima_input_file_path);
	if (not ima_input_file.is_open()) {
		DebugError << ima_input_file_path << " cannot be opened." << '\n';
		return EXIT_FAILURE;
	}

	// Check for upsampling type :
	int upsampling_type = 0;
	if ((*options).operator()("upsampling-type", upsampling_type) == false) {
		DebugError << "No upsampling type defined." << '\n';
		ShowHelp();
		ima_input_file.close();
		return EXIT_FAILURE;
	}

	/* Reminder for possible upsampling values :
	 * 0 - Nothing
	 * 1 - Only on X axis
	 * 2 - On X and Y axes
	 * 3 - On all axes
	 * Anything else : garbage value, to ignore.
	 */

	if (upsampling_type == 0) {
		std::cout << "No upsampling was chosen. Nothing will be performed." << '\n';
		ima_input_file.close();
		return EXIT_SUCCESS;
	}
	if (upsampling_type < 0 or upsampling_type > 3) {
		std::cout << "An unauthorized upsampling value was given. Nothing will be performed." << '\n';
		ima_input_file.close();
		return EXIT_SUCCESS;
	}

	std::size_t elementsize = sizeof(char);
	// Get the size of elements to allocate :
	if( type.find("S16") != std::string::npos ) {
		elementsize *= 2;
		DebugWarningShort << "The file contained signed integer 16-bit values." << '\n';
		DebugWarningShort << "Might segfault from here. Or write garbage to the file." << '\n';
		DebugWarningShort << "Probably the latter." << '\n';
	} else if( type.find("FLOAT") != std::string::npos ) {
		elementsize *= 4;
		DebugWarningShort << "The file contained floating-point 32-bit values." << '\n';
		DebugWarningShort << "Might segfault from here. Or write garbage to the file." << '\n';
		DebugWarningShort << "Probably the latter." << '\n';
	}
	// Get the number of elements to allocate :
	std::size_t new_nx(nx), new_ny(ny), new_nz(nz);
	double out_dx(input_dx), out_dy(input_dy), out_dz(input_dz);
	if (upsampling_type > 0) { new_nx *= 2; out_dx /= 2.0; }
	if (upsampling_type > 1) { new_ny *= 2; out_dy /= 2.0; }
	if (upsampling_type > 2) { new_nz *= 2; out_dz /= 2.0; }
	switch (upsampling_type) {
		case 1:
			std::cout << "Upsampling on X" << '\n';
			break;
		case 2:
			std::cout << "Upsampling on X and Y" << '\n';
			break;
		case 3:
			std::cout << "Upsampling on X, Y, and Z" << '\n';
			break;
		default:
			std::cout << "Upsampling on WHAT THE FUCK" << '\n';
			break;
	}

	std::cout << "Allocating a voxel grid with the following properties :" << '\n';
	std::cout << '\t' << "Voxels : " << new_nx << "x" << new_ny << "x" << new_nz << '\n';
	std::cout << '\t' << "Voxel dimensions : " << out_dx << "x" << out_dy << "x" << out_dz << '\n';

	// Open DIM target file :
	char* dim_output_file_path = AppendExtension(basename_out, "dim");
	std::ofstream dim_output_file(dim_output_file_path);
	if (not dim_output_file.is_open()) {
		std::cout << "Could not open dim file for output ! (" << dim_output_file_path << ")" << '\n';
		ima_input_file.close();
		return EXIT_FAILURE;
	}

	dim_output_file << new_nx << ' ' << new_ny << ' ' << new_nz << '\n';
	dim_output_file << "-type " << type << '\n';
	dim_output_file << "-dx " << out_dx << '\n';
	dim_output_file << "-dy " << out_dy << '\n';
	dim_output_file << "-dz " << out_dz << '\n';
	dim_output_file.flush(); // force output of characters (and sync with program)
	dim_output_file.close(); // close

	// Open IMA target file :
	char* ima_output_file_path = AppendExtension(basename_out, "ima");
	std::ofstream ima_output_file(ima_output_file_path, std::ios::trunc);
	if (not ima_output_file.is_open()) {
		std::cout << "Could not open the output file ! (" << ima_output_file_path << ")" << '\n';
		ima_input_file.close();
		return EXIT_FAILURE;
	}

	char** current_img = nullptr;
	for (std::size_t i = 0; i < nz; ++i) {
		if (upsampling_type > 2) {
			current_img = static_cast<char**>(calloc(new_ny, sizeof(char*)));
		}
		for (std::size_t j = 0; j < ny; ++j) {
			// read a line :
			char* img_line = static_cast<char*>(calloc(nx, sizeof(char)));
			ima_input_file.read(img_line, nx);
			// upsample the line :
			char* upsampled_line = nullptr;
			upsampled_line = upsample_line(img_line, nx);
			if (upsampled_line == nullptr) {
				DebugError << "Nullptr for copied line ! " << '\n';
			}
			ima_output_file.write(upsampled_line, new_nx);
			// if upsampling also on y, copy the current line to the next one :
			if (upsampling_type > 1) {
				ima_output_file.write(upsampled_line, new_nx);
				if(upsampling_type > 2) {
					// copy two lines for current image
					current_img[j*2+0] = static_cast<char*>(calloc(new_nx, sizeof(char)));
					current_img[j*2+1] = static_cast<char*>(calloc(new_nx, sizeof(char)));
					memcpy(current_img[j*2+0], upsampled_line, new_nx);
					memcpy(current_img[j*2+1], upsampled_line, new_nx);
				}
			}
			free(img_line);
			free(upsampled_line);
		}
		if (upsampling_type > 2) {
			for (std::size_t j = 0; j < new_ny; ++j) {
				ima_output_file.write(current_img[j], new_nx);
			}
			free(current_img);
		}
	}
	std::cerr << "Flushing the stream" << '\n';
	ima_output_file.flush();
	std::cerr << "Flushed" << '\n';
	ima_input_file.close();
	ima_output_file.close();

	free(basename_in);
	free(basename_out);

	return EXIT_SUCCESS;
}

/* Show help for unitiated users {{{ */
void ShowHelp() {
	std::cout << R"message(

This program is to be used this way :

	./upsampler OPTIONS [{-i |--input-file=}]input_file [{-o |--output-file=}]output_file

Where OPTIONS can be one of the following :

	--help			Show this help message.
	--upsampling-type	The mode of upsampling to use.
				Can be either 1, 2, 3.

)message" << '\n';
}
// }}}

// vim: set tabstop=8 colorcolumn=120 :
